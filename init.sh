#!/bin/sh

set -e;
export PATH="${DEPOT_TOOLS_DIR}:${PATH}";

if [ "${CHNET_USE_ASAN}" = "1" ]; then
	IS_ASAN=true;
else
	IS_ASAN=false;
fi;

# Download all submodules.
git submodule update --init --depth 1 --jobs 8;
mkdir -pv "${CHROMIUM_BUILD_DIR}";

###############################################
# Prepare chromium patches and build directory.
cd "${CHROMIUM_SRC_DIR}";
git reset --hard;
git clean -f;
git apply "${CHROMIUM_PATCHES_DIR}"/ubuntu.diff;
git apply "${CHROMIUM_PATCHES_DIR}"/aia_chasing.diff;
git apply "${CHROMIUM_PATCHES_DIR}"/connect_hook.diff;
git apply "${CHROMIUM_PATCHES_DIR}"/no_decompress.diff;
git apply "${CHROMIUM_PATCHES_DIR}"/kill_referer_removal.diff;
git apply "${CHROMIUM_PATCHES_DIR}"/custom_dns_map.diff;
git apply "${CHROMIUM_PATCHES_DIR}"/socks5_chnetd.diff;
date > "${CHROMIUM_PATCHES_DIR}"/cr.swo;
sudo build/install-build-deps.sh --no-arm --no-chromeos-fonts --no-prompt --no-nacl --no-backwards-compatible --unsupported;
gclient sync --nohooks --no-history -j$(nproc);
gclient runhooks -j$(nproc);
ln -svf "${CHNET_DIR}";
rm -f chnet_tests;
ln -svf "${CHNET_DIR}/../tests/cpp" chnet_tests;
# Check if fie not exists or empty.
if [ ! -s buildtools/linux64/gn.orig ]; then
        mv -v buildtools/linux64/gn buildtools/linux64/gn.orig;
        printf '#!/bin/bash\ntaskset -c 0 $0.orig "$@"\n' > buildtools/linux64/gn;
        chmod +x buildtools/linux64/gn;
fi;
mkdir -pv "${CHROMIUM_BUILD_DIR}";

# Generate build arguments
cd "${CHROMIUM_BUILD_DIR}";
echo "root_extra_deps = [ \"//chnet\", \"//chnet_tests:basic_test\" ]
is_component_build = false
is_debug = false
enable_nacl = false
blink_symbol_level = 0
v8_symbol_level = 0
symbol_level = 1
dcheck_always_on = true
is_asan = ${IS_ASAN}" > args.gn;

cd "${CHROMIUM_SRC_DIR}";
taskset -c 0 gn gen "${CHROMIUM_BUILD_DIR}";
