# Chnet

Chromium networking stack as a NodeJS addon. This addon only works on
Linux.

# Directory Info
- `chromium` contains chromium specific files.
- `net` contains C++ wrappers to use Chromium Networking Stack.
- `node-addon-api` is a submodule of the latest NodeJS addon API. This needs to
  be updated regularly.
- `scripts` contains JavaScript files (mostly examples).
- `web` contains PHP file for me testing integrating Chromium Networking Stack.

# Installation Steps
### 1. Install Node JS v18.

See: https://nodejs.org/en/download/package-manager

### 2. Configure the git.
```
git config --global user.name "Your Name"
git config --global user.email "your_email@somewhere.com"
git config --global credential.helper store
```

### 3. Clone the repository and its submodules.
```
git clone https://github.com/Hoody-Network/NodeChromiumNetworkStack.git chnet;

# Change dir.
cd chnet

# Clone chromium, depot_tools and node-addon-api (as submodules).
git submodule init
git submodule update --depth 1
```

## 4. Building Chromium
[Visual Studio 2022](https://learn.microsoft.com/en-us/visualstudio/releases/2022/release-notes)
is preferred. You must install the “Desktop development with C++” component and
the “MFC/ATL support” sub-components and the version "10.0.20348.0 Windows 10
SDK". Additionally `chnet` requires the "C++ Clang compiler for windows" This
can be done from the command line by passing these arguments to the Visual
Studio installer:
```
$ PATH_TO_INSTALLER.EXE ^
--add Microsoft.VisualStudio.Workload.NativeDesktop ^
--add Microsoft.VisualStudio.Component.VC.ATLMFC ^
--add Microsoft.VisualStudio.Component.VC.Llvm.ClangToolset ^
--add Microsoft.VisualStudio.Component.VC.Llvm.Clang ^
--add Microsoft.VisualStudio.Component.Windows10SDK.20348 ^
--includeRecommended
```

The SDK Debugging Tools must also be installed. If the Windows 10 SDK was
installed via the Visual Studio installer, then they can be installed by going
to: Control Panel -> Programs -> Programs and Features -> Select the "Windows
Software Development Kit" -> Change -> Change -> Check “Debugging Tools For
Windows” -> Change. Or, you can download the standalone SDK installer and use it
to install the Debugging Tools.

Add chromium\depot_tools at the front pf PATH environment variable.
```
set PATH=chromium\depot_tools;%PATH%;
```
Also, add a `DEPOT_TOOLS_WIN_TOOLCHAIN` environment variable in the same way,
and set it to 0. This tells depot_tools to use your locally installed version of
Visual Studio (by default, depot_tools will try to use a google-internal
version).

You may also have to set variable vs2017_install or vs2019_install or
vs2022_install to your installation path of Visual Studio 2017 or 19 or 22, like
`set vs2019_install=C:\Program Files (x86)\Microsoft Visual`
Studio\2019\Professional for Visual Studio 2019, or set
`vs2022_install=C:\Program Files\Microsoft Visual Studio\2022\Professional` for
Visual Studio 2022.

From a cmd.exe shell, run:
```
gclient
```

# Apply chromium patch.
```
cd chromium/src
git apply ../patches/no_decompress.diff
git apply ../patches/aia_chasing.diff
git apply ../patches/connect_hook_win.diff
```

# Sync dependencies (gclient is already in $PATH).
```
gclient sync -j8
```

# Create links.
```
mklink /J chnet ..\..\chnet
mklink /J chnet_tests ..\..\tests\cpp
```

# Setup ninja generator args.gn
```
mkdir out\Net
cd . > args.gn
notepad out\Net\args.gn
```

# Add build config to out/Net/args.gn
Copy paste these into args.gn files and save it.
```
root_extra_deps = [ "//chnet", "//chnet_tests:basic_test" ]
is_component_build = true
is_debug = false
enable_nacl = false
blink_symbol_level = 0
v8_symbol_level = 0
dcheck_always_on = true
symbol_level = 1
```
_For debug builds, remove `symbol_level = 1` and set `is_debug` to true_.

# Generate build toolchain files
```
gn gen out\Net
```

_For better debugging experience, alternatively run this command to generate
visual studio projects:_
```
gn gen --ide=vs --filters=//chnet;//chnet_tests:basic_test;//base;//net; --no-deps -v out\Net
```

# Build the chnet target
```
ninja -C out\Net -j10 chnet
ninja -C out\Net -j10 basic_test

```

or when `--ide` flag was specified to generate solution and project files then
you can also utilize MSBuild as follow to build chnet target: _Make sure to use
Developers Command Prompt VS2022_
```
cd out\Net
msbuild all.sln /target:chnet /property:Configure=Debug
```

# Build chnet node addon.
_Make sure you are in the chnet root directory_
_Get the path to python3 via `where python3`_
Run this command to run chnet addon:
```
npm install --python=<path-to-python3>
```
or alternatively (Make sure cmake is in the env or you are inside Developers
Command Prompt VS2022):
```
npm install --omit=dev
npm run cmake-build
```

# Maintainers

  - Ammar Faizi &lt;ammarfaizi2@gnuweeb.org&gt;
