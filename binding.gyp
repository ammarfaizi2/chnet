{
  "targets": [
    {
      "target_name": "action_before_build",
      "type": "none",
      "actions": [
        {
          "action_name": "copy_addon_deps",
          "inputs": [""],
          "outputs": [""],
          "conditions": [
            [
              "OS=='win'",
              {
                "action": [
                  "<(module_root_dir)/win_action_before_addon_build.bat",
                  "<(PRODUCT_DIR)",
                  "<(module_root_dir)/chromium/src/out/Net"
                ]
              }
            ],
            [
              "OS=='linux'",
              {
                "action": [
                  "cp",
                  "-vf",
                  "<(module_root_dir)/chromium/src/out/Net/libchnet.min.so",
                  "<(PRODUCT_DIR)/libchnet.so"
                ]
              }
            ]
          ]
        }
      ]
    },
    {
      "target_name": "chnet",
      "cflags!": [
        "-fno-exceptions"
      ],
      "sources": [
        "chnet/chnet_node.cc",
      ],
      "include_dirs": [
        "<!@(node -p \"require('node-addon-api').include\")"
      ],
      "conditions": [
        [
          "OS=='win'",
          {
            "libraries": [
              "<(PRODUCT_DIR)/chnet.lib"
            ]
          }
        ],
        [
          "OS=='linux'",
          {
            "libraries": [
              "-lchnet",
              "-L<(PRODUCT_DIR)/",
              "-Wl,-rpath,'$$ORIGIN'"
            ]
          }
        ]
      ],
      "defines": [
        "NAPI_DISABLE_CPP_EXCEPTIONS"
      ],
      "dependencies": [
        "action_before_build"
      ]
    }
  ]
}
