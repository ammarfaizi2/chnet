
const { CRHttpRing } = require('../../chnet/chnet.js');

let ring = new CRHttpRing;

let dreq = ring.CreateDNS("fb.com");
dreq.On("error", function (dreq, err) {
        console.log(`Error: ${err}`);
});
dreq.On("data", function (dreq, data) {
        console.log(data);
});
dreq.On("close", function (dreq) {
        console.log("===== DNS request closed ======");
});
dreq.Start();
