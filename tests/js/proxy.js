
const { CRHttpRing } = require('../../chnet/chnet.js');
const { URL } = require("node:url");
const http = require("node:http");
const net = require("node:net");

const PROXY_ADDR = "127.0.0.1";
const PROXY_PORT = 1337;
const PROXY_READ_CHUNK_SIZE = 4096;

const ring = new CRHttpRing;

function get_client_src(req)
{
	return `${req.socket.remoteAddress}:${req.socket.remotePort}`;
}

function zlog(req, ...args)
{
	if (args.length) {
		let x = args.shift();
		console.log(`[${get_client_src(req)}] ${x}`, ...args);
	}
}

function handle_proxy_req_error(preq, err)
{
	let udata = preq.user_data;
	let req = udata.req;
	let src = get_client_src(req);

	zlog(req, "Error when processing the request", err);
}

function handle_proxy_req_information(preq, info)
{
	let udata = preq.user_data;
	let res = udata.res;
	let hdr = {};
	let key;

	for (key in info.headers) {
		let tmp = key.toLowerCase();

		/*
		 * Skip several problematic response headers.
		 *
		 * Not sure how to handle them correctly. But
		 * these header removals work for me.
		 *
		 * Maybe some of them don't have any effect,
		 * though.
		 */
		if (tmp === "alt-svc")
			continue;
		if (tmp === "transfer-encoding")
			continue;
		if (tmp === "x-content-type-options")
			continue;
		if (tmp === "strict-transport-security")
			continue;
		if (tmp === "content-encoding")
			continue;

		hdr[key] = info.headers[key];
	}

	res.writeHead(200, hdr);
}

function handle_proxy_req_data(preq, read_bytes, data)
{
	let udata = preq.user_data;
	let res = udata.res;

	res.write(data);
	preq.Read(PROXY_READ_CHUNK_SIZE);
	udata.written_bytes += read_bytes;
}

function handle_proxy_req_close(preq)
{
	let udata = preq.user_data;
	let req = udata.req;
	let res = udata.res;

	res.end();
	zlog(req, `Closed the connection (${udata.written_bytes} bytes written)`);
}

async function handle_client(req, res)
{
	let path = req.url;

	zlog(req, `Received a new connection (path: ${path})`);

	/*
	 * Create an HTTP request from the proxy server.
	 */
	let preq = ring.Create(`https://mbasic.facebook.com${path}`, "GET");
	preq.SetRequestHeaders(req.headers);
	preq.SetUserData({
		req: req,
		res: res,
		written_bytes: 0
	});
	preq.On("data", handle_proxy_req_data);
	preq.On("error", handle_proxy_req_error);
	preq.On("close", handle_proxy_req_close);
	preq.On("information", handle_proxy_req_information);
	preq.Read(PROXY_READ_CHUNK_SIZE);
	zlog(req, "Proxy request started!");
}

const proxy = http.createServer();

proxy.listen(PROXY_PORT, PROXY_ADDR, function () {
	console.log(`Proxy server is running on ${PROXY_ADDR}:${PROXY_PORT}...`);
});

proxy.on("request", handle_client);
