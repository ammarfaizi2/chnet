
const { CRHttpRing } = require('../../chnet/chnet.js');

let ring = new CRHttpRing;

// Just an endpoint to show the request body.
let req = ring.Create("https://www.teainside.org/post.php", "POST");

req.SetUserData({buf: ""});
req.SetRequestHeaders({
	"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36"
})

req.On("error", function (req, err) {
	console.log("Error!", err);
});

req.On("information", function (req, info) {

});

req.On("data", function (req, read_bytes, data) {
	if (!data)
		return;

	req.user_data.buf += data.toString();
	req.Read(1024);
});

req.On("close", function (req) {
	if (req.user_data.buf != "AAABBBCCCDDD") {
		console.log("WRONG response body: "+req.user_data.buf);
		process.exit(1);
	}
	console.log("Response: "+req.user_data.buf);
	console.log("Connection closed!");
});

/*
 * StartChunkedPayload() has to be called before the first Read().
 */
req.StartChunkedPayload();

/*
 * Write() can be called anytime after StartChunkedPayload() and
 * before StopChunkedPayload().
 */
req.Write("AAA");
req.Write("BBB");

/*
 * This read will be deferred until the response
 * body is all sent. We have started sending data
 * when this Read() is called. But we won't be
 * receiving data until the StopChunkedPayload()
 * is called.
 */
req.Read(1024);

req.Write("CCC");
req.Write("DDD");
req.StopChunkedPayload();
