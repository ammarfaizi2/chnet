
const { CHNet, CNRing, CNRingSQE } = require('../../chnet/chnet.js');

function assert(cond)
{
	if (!cond)
		throw new Error("Assertion failed!");
}

function TEST_RES_HDR(ch)
{
	let x;

	x = ch.GetResponseHeaders();
	assert(x["X-Hdr-Test1"] === "aaaaaaaaa");
	assert(x["X-Hdr-Test2"] === "bbbbbbbbb");
	assert(x["X-Hdr-Test3"] === "ccccccccc");
	assert(x["X-Hdr-Test4"] === "ddddddddd");
}

function test_nop()
{
	const nr_loop = 1024 * 8;
	const entry = 4096;
	let ring = new CNRing(entry);
	let sqe;
	let cqe;
	let total;
	let ret;
	let i;

	for (i = 0; i < nr_loop; i++) {
		sqe = ring.GetSQE();
		if (!sqe) {
			assert(ring.Submit() == entry);
			sqe = ring.GetSQE();
			assert(sqe);
		}
		sqe.PrepNop();
		sqe.SetUserData(1);
	}
	assert(ring.Submit() == entry);
	assert(ring.WaitCQE(1));

	total = 0;
	while (total < nr_loop) {
		ret = ring.WaitCQE(1);
		i = ring.ForEachCQE(function (cqe) {
			assert(!cqe.res);
			assert(cqe.user_data == 1);
		});
		ring.CQAdvance(i);
		assert(ret >= 1);
		assert(i >= ret);
		total += ret;
	}
	assert(total == nr_loop);
	ring.Close();
}

function test_chnet_ring_read(do_sq_start = false)
{
	let ring = new CNRing(1);
	let sqe;
	let cqe;
	let ret;
	let ch;

	ch = new CHNet;
	ch.SetURL("http://127.0.0.1:8000/index.php?action=hello");
	ch.SetMethod("GET");

	if (do_sq_start) {
		sqe = ring.GetSQE();
		sqe.PrepStart(ch);
		assert(ring.Submit() == 1);

		ret = ring.WaitCQE(1);
		cqe = ring.GetCQEHead();
		assert(ret == 1);
		assert(cqe.res == 0);
		ring.CQAdvance(1);
	}

	sqe = ring.GetSQE();
	sqe.PrepRead(ch, 1024);
	assert(ring.Submit() == 1);

	ret = ring.WaitCQE(1);
	cqe = ring.GetCQEHead();

	assert(ret == 1);
	assert(cqe.res == 13);
	assert(ch.read_ret() == cqe.res);
	assert("Hello World!\n" == ch.read_buf());
	TEST_RES_HDR(ch);
	ring.CQAdvance(1);
	ch.Close();
	ring.Close();
}

function test_chnet_ring_partial_read(do_sq_start = false)
{
	let ring = new CNRing(1);
	let sqe;
	let cqe;
	let ret;
	let ch;

	ch = new CHNet;
	ch.SetURL("http://127.0.0.1:8000/index.php?action=hello");
	ch.SetMethod("GET");

	if (do_sq_start) {
		sqe = ring.GetSQE();
		sqe.PrepStart(ch);
		assert(ring.Submit() == 1);

		ret = ring.WaitCQE(1);
		cqe = ring.GetCQEHead();
		assert(ret == 1);
		assert(cqe.res == 0);
		ring.CQAdvance(1);
	}

	/*
	 * The first read.
	 */
	sqe = ring.GetSQE();
	sqe.PrepRead(ch, 4);
	assert(ring.Submit() == 1);

	ret = ring.WaitCQE(1);
	TEST_RES_HDR(ch);
	cqe = ring.GetCQEHead();

	assert(ret == 1);
	assert(cqe.res == 4);
	assert(ch.read_ret() == cqe.res);
	assert("Hell" == ch.read_buf());
	ring.CQAdvance(1);


	/*
	 * The second read.
	 */
	sqe = ring.GetSQE();
	sqe.PrepRead(ch, 4);
	assert(ring.Submit() == 1);

	ret = ring.WaitCQE(1);
	cqe = ring.GetCQEHead();

	assert(ret == 1);
	assert(cqe.res == 4);
	assert(ch.read_ret() == cqe.res);
	assert("o Wo" == ch.read_buf());
	ring.CQAdvance(1);


	/*
	 * The third read.
	 */
	sqe = ring.GetSQE();
	sqe.PrepRead(ch, 1024);
	assert(ring.Submit() == 1);

	ret = ring.WaitCQE(1);
	cqe = ring.GetCQEHead();

	assert(ret == 1);
	assert(cqe.res == 5);
	assert(ch.read_ret() == cqe.res);
	assert("rld!\n" == ch.read_buf());
	ring.CQAdvance(1);


	/*
	 * The 4-th read.
	 */
	sqe = ring.GetSQE();
	sqe.PrepRead(ch, 4);
	assert(ring.Submit() == 1);

	ret = ring.WaitCQE(1);
	cqe = ring.GetCQEHead();

	assert(ret == 1);
	assert(cqe.res == 0);
	assert(ch.read_ret() == cqe.res);
	assert(!ch.read_buf());
	ring.CQAdvance(1);
	ch.Close();
	ring.Close();
}

const NR_REQ = 4096;
function test_chnet_ring_read_parallel(do_sq_start = false)
{
	let ring = new CNRing(NR_REQ);
	let sqe;
	let cqe;
	let i;
	let ch_arr;

	ch_arr = [];

	for (i = 0; i < NR_REQ; i++) {
		ch_arr[i] = new CHNet;
		ch_arr[i].SetURL("http://127.0.0.1:8000/index.php?action=hello");
		ch_arr[i].SetMethod("GET");
	}

	if (do_sq_start) {
		for (i = 0; i < NR_REQ; i++) {
			sqe = ring.GetSQE();
			assert(sqe);
			sqe.PrepStart(ch_arr[i]);
			sqe.SetUserData(ch_arr[i]);
		}
		assert(ring.Submit() == NR_REQ);
		assert(ring.WaitCQE(NR_REQ) == NR_REQ);

		ring.ForEachCQE(function (cqe) {
			let ch = cqe.user_data;

			assert(ch);
			assert(cqe.res == 0);
		});
		ring.CQAdvance(NR_REQ);
	}


	for (i = 0; i < NR_REQ; i++) {
		sqe = ring.GetSQE();
		assert(sqe);
		sqe.PrepRead(ch_arr[i], 1024);
		sqe.SetUserData(ch_arr[i]);
	}
	assert(ring.Submit() == NR_REQ);
	assert(ring.WaitCQE(NR_REQ) == NR_REQ);

	ring.ForEachCQE(function (cqe) {
		let ch = cqe.user_data;

		assert(ch);
		TEST_RES_HDR(ch);
		assert(cqe.res == 13);
		assert(ch.read_ret() == cqe.res);
		assert("Hello World!\n" == ch.read_buf());
	});
	ring.CQAdvance(NR_REQ);

	for (i = 0; i < NR_REQ; i++)
		ch_arr[i].Close();

	ring.Close();
}

function test_chnet_ring_partial_read_parallel(do_sq_start = false)
{
	let ring = new CNRing(NR_REQ);
	let sqe;
	let cqe;
	let i;
	let ch_arr;

	ch_arr = [];

	for (i = 0; i < NR_REQ; i++) {
		ch_arr[i] = new CHNet;
		ch_arr[i].SetURL("http://127.0.0.1:8000/index.php?action=hello");
		ch_arr[i].SetMethod("GET");
	}

	if (do_sq_start) {
		for (i = 0; i < NR_REQ; i++) {
			sqe = ring.GetSQE();
			assert(sqe);
			sqe.PrepStart(ch_arr[i]);
			sqe.SetUserData(ch_arr[i]);
		}
		assert(ring.Submit() == NR_REQ);
		assert(ring.WaitCQE(NR_REQ) == NR_REQ);

		ring.ForEachCQE(function (cqe) {
			let ch = cqe.user_data;

			assert(ch);
			assert(cqe.res == 0);
		});
		ring.CQAdvance(NR_REQ);
	}


	/*
	 * The first read.
	 */
	for (i = 0; i < NR_REQ; i++) {
		sqe = ring.GetSQE();
		assert(sqe);
		sqe.PrepRead(ch_arr[i], 4);
		sqe.SetUserData(ch_arr[i]);
	}
	assert(ring.Submit() == NR_REQ);
	assert(ring.WaitCQE(NR_REQ) == NR_REQ);

	ring.ForEachCQE(function (cqe) {
		let ch = cqe.user_data;

		assert(ch);
		TEST_RES_HDR(ch);
		assert(cqe.res == 4);
		assert(ch.read_ret() == cqe.res);
		assert("Hell" == ch.read_buf());
	});
	ring.CQAdvance(NR_REQ);


	/*
	 * The second read.
	 */
	for (i = 0; i < NR_REQ; i++) {
		sqe = ring.GetSQE();
		assert(sqe);
		sqe.PrepRead(ch_arr[i], 4);
		sqe.SetUserData(ch_arr[i]);
	}
	assert(ring.Submit() == NR_REQ);
	assert(ring.WaitCQE(NR_REQ) == NR_REQ);

	ring.ForEachCQE(function (cqe) {
		let ch = cqe.user_data;

		assert(ch);
		assert(cqe.res == 4);
		assert(ch.read_ret() == cqe.res);
		assert("o Wo" == ch.read_buf());
	});
	ring.CQAdvance(NR_REQ);


	/*
	 * The third read.
	 */
	for (i = 0; i < NR_REQ; i++) {
		sqe = ring.GetSQE();
		assert(sqe);
		sqe.PrepRead(ch_arr[i], 1024);
		sqe.SetUserData(ch_arr[i]);
	}
	assert(ring.Submit() == NR_REQ);
	assert(ring.WaitCQE(NR_REQ) == NR_REQ);

	ring.ForEachCQE(function (cqe) {
		let ch = cqe.user_data;

		assert(ch);
		assert(cqe.res == 5);
		assert(ch.read_ret() == cqe.res);
		assert("rld!\n" == ch.read_buf());
	});
	ring.CQAdvance(NR_REQ);


	/*
	 * The 4-th read.
	 */
	for (i = 0; i < NR_REQ; i++) {
		sqe = ring.GetSQE();
		assert(sqe);
		sqe.PrepRead(ch_arr[i], 1024);
		sqe.SetUserData(ch_arr[i]);
	}
	assert(ring.Submit() == NR_REQ);
	assert(ring.WaitCQE(NR_REQ) == NR_REQ);

	ring.ForEachCQE(function (cqe) {
		let ch = cqe.user_data;

		assert(ch);
		assert(cqe.res == 0);
		assert(ch.read_ret() == cqe.res);
	});
	ring.CQAdvance(NR_REQ);

	for (i = 0; i < NR_REQ; i++)
		ch_arr[i].Close();

	ring.Close();
}

function test_chnet_ring_simple_post(do_sq_start = false)
{
	let ring = new CNRing(1);
	let sqe;
	let cqe;
	let ret;
	let ch;

	ch = new CHNet;
	ch.SetURL("http://127.0.0.1:8000/index.php?action=print_post&key=data");
	ch.SetMethod("POST");
	ch.SetPayload("data=AAAAAAAAA");
	ch.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded", true);

	if (do_sq_start) {
		sqe = ring.GetSQE();
		assert(sqe);
		sqe.PrepStart(ch);
		assert(ring.Submit() == 1);

		ret = ring.WaitCQE(1);
		cqe = ring.GetCQEHead();
		assert(ret == 1);
		assert(cqe.res == 0);
		ring.CQAdvance(1);
	}

	sqe = ring.GetSQE();
	sqe.PrepRead(ch, 1024);
	ring.Submit();

	ret = ring.WaitCQE(1);
	cqe = ring.GetCQEHead();

	assert(ret == 1);
	assert(cqe.res == 9);
	assert(ch.read_ret() == cqe.res);
	assert(ch.read_buf() === "AAAAAAAAA");
	ring.CQAdvance(1);

	ch.Close();
	ring.Close();
}

function test_chnet_ring_simple_post_parallel(do_sq_start = false)
{
	let ring = new CNRing(NR_REQ);
	let sqe;
	let cqe;
	let head;
	let tail;
	let i;
	let ch = [];

	for (i = 0; i < NR_REQ; i++) {
		ch[i] = new CHNet;
		ch[i].SetURL("http://127.0.0.1:8000/index.php?action=print_post&key=data");
		ch[i].SetMethod("POST");
		ch[i].SetPayload("data=AAAAAAAAA");
		ch[i].SetRequestHeader("Content-Type", "application/x-www-form-urlencoded", true);
	}

	if (do_sq_start) {
		for (i = 0; i < NR_REQ; i++) {
			sqe = ring.GetSQE();
			assert(sqe);
			sqe.PrepStart(ch[i]);
			sqe.SetUserData(ch[i]);
		}
		assert(ring.Submit() == NR_REQ);
		assert(ring.WaitCQE(NR_REQ) == NR_REQ);

		ring.ForEachCQE(function (cqe) {
			let ch = cqe.user_data;

			assert(cqe.res == 0);
			assert(!ch.GetErrorStr());
		});
		ring.CQAdvance(NR_REQ);
	}

	for (i = 0; i < NR_REQ; i++) {
		sqe = ring.GetSQE();
		assert(sqe);
		sqe.PrepRead(ch[i], 1024);
		sqe.SetUserData(ch[i]);
	}
	assert(ring.Submit() == NR_REQ);
	assert(ring.WaitCQE(NR_REQ) == NR_REQ);

	ring.ForEachCQE(function (cqe) {
		let ch = cqe.user_data;

		if (cqe.res != 9) {
			console.log(ch.read_buf());
			assert(0);
		}
		assert(cqe.res == 9);
		assert(ch.read_buf() === "AAAAAAAAA");
	});
	ring.CQAdvance(NR_REQ);

	for (i = 0; i < NR_REQ; i++)
		ch[i].Close();

	ring.Close();
}

function test_chnet_ring_chunked_post(do_sq_start = false)
{
	const nr_body_A = 1000 * 30;
	let ring = new CNRing(NR_REQ);
	let sqe;
	let cqe;
	let ch;
	let i;

	ch = new CHNet;
	ch.SetURL("http://127.0.0.1:8000/index.php?action=print_post&key=data");
	ch.SetMethod("POST");
	ch.StartChunkedPayload();
	ch.Write("data=");
	ch.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded", true);

	if (do_sq_start) {
		sqe = ring.GetSQE();
		assert(sqe);
		sqe.PrepStart(ch);
		assert(ring.Submit() == 1);

		ret = ring.WaitCQE(1);
		cqe = ring.GetCQEHead();
		assert(ret == 1);
		assert(cqe.res == 0);
		ring.CQAdvance(1);
	}

	ch.Write("12345");

	sqe = ring.GetSQE();
	assert(sqe);
	sqe.PrepRead(ch, 10);
	assert(ring.Submit() == 1);

	ch.Write("67890");
	let x = "";
	for (i = 0; i < nr_body_A; i++)
		x += 'A';
	ch.Write(x);
	ch.Write(x);
	ch.Write(x);
	ch.Write(x);
	ch.StopChunkedPayload();

	assert(ring.WaitCQE(1) == 1);
	cqe = ring.GetCQEHead();
	assert(cqe.res == 10);
	assert(ch.read_ret() == cqe.res);
	assert(ch.read_buf() == "1234567890");
	ring.CQAdvance(1);
	TEST_RES_HDR(ch);

	total = 0;
	while (total < nr_body_A*4) {
		sqe = ring.GetSQE();
		assert(sqe);
		sqe.PrepRead(ch, 1024);
		assert(ring.Submit() == 1);

		cqe = ring.GetCQEHead();
		assert(cqe.res >= 0);
		assert(cqe.res <= 1024);
		assert(ch.read_ret() == cqe.res);
		buf = ch.read_buf();

		for (i = 0; i < cqe.res; i++)
			assert(buf[i] == 'A');
		ring.CQAdvance(1);
		total += cqe.res;
	}

	ch.Close();
	ring.Close();
}

function main()
{
	let i;

	test_nop();
	for (i = 0; i < 2; i++) {
		test_chnet_ring_read(!!i);
		console.log("r1");
		test_chnet_ring_partial_read(!!i);
		console.log("r2");
		test_chnet_ring_read_parallel(!!i);
		console.log("r3");
		test_chnet_ring_partial_read_parallel(!!i);
		console.log("r4");
		test_chnet_ring_simple_post(!!i);
		console.log("r5");
		test_chnet_ring_simple_post_parallel(!!i);
		console.log("r6");
		test_chnet_ring_chunked_post(!!i);
		console.log("r7");
	}
}

main();
