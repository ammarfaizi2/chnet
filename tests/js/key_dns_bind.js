const { chnet, CRHttpRing } = require("../../chnet/chnet.js");

const ring = new CRHttpRing;

chnet.NetDNSMapInsert("www.teainside.org", "104.21.82.150", {
        bind_ip4: "10.5.5.3",
        bind_iface4: "teavpn2-cl-01"
});
chnet.NetDNSMapInsert("www.teainside.org", "172.67.203.12", {
        bind_ip4: "10.20.0.2",
        bind_iface4: "enx00e04c680cac"
});

// console.log(chnet.NetDNSMapGetAddrList("www.teainside.org", {
//         "bind_ip4": "10.5.5.3",
//         "bind_iface4": "teavpn2-cl-01"
// }));

// console.log(chnet.NetDNSMapGetAddrList("www.teainside.org", {
//         "bind_ip4": "10.20.0.2",
//         "bind_iface4": "enx00e04c680cac"
// }));

function exec_http(options)
{
        let req = ring.Create("https://www.teainside.org/ip.php", "GET", options);
        req.SetUserData({ip_target: null});
        req.SetRequestHeaders({"Accept-Encoding": ""});
        req.On("error", function (req, err) {
                console.log("Error!");
                console.log(err);
        });
        req.On("information", function (req, info) {
                req.user_data.ip_target = info.remote_endpoint;
        });
        req.On("data", function (req, read_bytes, data) {
                console.log(`Destination: ${req.user_data.ip_target}; Source: ${data.toString().trim()}`);
                req.Read(1024);
        });
        req.On("close", function (req) { console.log("Connection closed!"); });
        req.Read(1024);
}

console.log("=====================================");
exec_http({
        bind_ip4: "10.5.5.3",
        bind_iface4: "teavpn2-cl-01",
        bind_ip6: "#",
        bind_iface6: "#",
});
exec_http({
        bind_ip4: "10.20.0.2",
        bind_iface4: "enx00e04c680cac",
        bind_ip6: "#",
        bind_iface6: "#",
});