
const { CRHttpRing } = require('../../chnet/chnet.js');

let ring = new CRHttpRing;
let req = ring.Create("https://www.google.com", "GET");

req.SetUserData({
	req_id: 1234,
	headers: null,
	buffer: "",
});

req.SetRequestHeaders({
	"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36"
})

req.On("error", function (req, err) {
	console.log("Error!");
	console.log(err);
});

req.On("information", function (req, info) {
	console.log(info);
	req.user_data.headers = info.headers;
});

req.On("redirect", function (req, red_info) {
	console.log(red_info);
});

req.On("data", function (req, read_bytes, data) {
	console.log(data);

	/*
	 * Do the read again.
	 */
	req.Read(1024);
});

req.On("close", function (req) {
	console.log("Connection closed!");
});

/*
 * First read. It also starts the HTTP request.
 */
req.Read(1024);
