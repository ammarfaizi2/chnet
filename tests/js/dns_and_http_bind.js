
const { chnet, CRDnsResolver, CRHttpRing } = require('../../chnet/chnet.js');

const DOMAIN = "ip.me";
let ring = new CRHttpRing;

async function main() {
        function run(options)
        {
                /*
                * Just an endpoint to show the client IP address.
                */
                let req = ring.Create(`https://${DOMAIN}`, "GET", options);

                req.SetUserData({ip_target: null});

                req.SetRequestHeaders({
                        "Accept-Encoding": "",
                        "User-Agent": "curl"
                });

                req.On("error", function (req, err) {
                        console.log("Error!");
                        console.log(err);
                });

                req.On("information", function (req, info) {
                        console.log(info);
                        req.user_data.ip_target = info.remote_endpoint;
                });

                req.On("data", function (req, read_bytes, data) {
                        if (!data)
                        return;

                        console.log(data.toString())

                        /*
                        * Do the read again.
                        */
                        req.Read(1024);
                });

                req.On("close", function (req) {
                        console.log("Connection closed!");
                });

                /*
                * First read. It also starts the HTTP request.
                */
                req.Read(1024);
        }

        const TorChromiumResolver = new CRDnsResolver(["127.0.0.1"]);
        const ProxyResolvedIP = await new Promise(resolve => {
                let dreq = TorChromiumResolver.Resolve(DOMAIN);
                dreq.On("error", (_, err) => { console.log('DREQ DNS ERROR:', err); resolve() });
                dreq.On("data", (_, data) => { resolve(data[0]) });
                dreq.On("close", () => { resolve() });
                dreq.Start();
        })
        if(ProxyResolvedIP) {

                chnet.NetDNSMapInsert(DOMAIN, ProxyResolvedIP, { bind_iface4: "wg-biznet" });
                console.log(chnet.NetDNSMapGetAddrList(DOMAIN, { bind_iface4: "wg-biznet", }));
                run({
                        bind_iface4: "wg-biznet",
                        bind_iface6: "#",
                        bind_ip6: "#"
                });
        }
}
main();
