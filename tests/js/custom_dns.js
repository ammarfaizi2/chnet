
const { chnet, CRHttpRing } = require('../../chnet/chnet.js');

const ring = new CRHttpRing;

function http_on_error(req, err)
{
        console.log("HTTP error:", err);
}

function http_on_information(req, info)
{
        console.log(info);
        req.user_data.headers = info.headers;
}

function http_on_redirect(req, red_info)
{
        console.log(red_info);
}

function http_on_data(req, read_bytes, data)
{
        console.log(data);

        /*
         * Do the read again.
         */
        req.Read(4096);
}

function http_on_close(req)
{
        console.log("Connection closed!");
}

function do_http()
{
        let req = ring.Create("https://www.facebook.com", "GET");
        req.SetUserData({});
        req.SetRequestHeaders({
                "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36"
        });
        req.On("error", http_on_error);
        req.On("information", http_on_information);
        req.On("redirect", http_on_redirect);
        req.On("data", http_on_data);
        req.On("close", http_on_close);
        req.Read(4096);
}

function dns_on_error(dreq, err)
{
        console.log(`Error: ${err}`);
}

function dns_on_data(dreq, data)
{
        /*
         * @data is an array of strings (IPv4 and IPv6).
         *
         * If the "data" event is emitted, the DNS request
         * was successful and @data.length is guaranteed to
         * be greater than 0.
         */
        console.log("Resolved IP addresses:", data);

        /*
         * Force the chnet to use a specific IP addresses
         * for the domain name.
         *
         * NetDNSMapInsert() can be called multiple times
         * for the same domain name. The IP addresses will
         * be used in a stack manner. The last inserted IP
         * address will be used first.
         *
         * To remove all the IP addresses for a domain name:
         *    chnet.NetDNSMapPopAll("www.facebook.com");
         *
         * To remove the last inserted IP address for a domain name:
         *    chnet.NetDNSMapPop("www.facebook.com");
         *
         * To remove a specific IP address for a domain name:
         *    chnet.NetDNSMapRemoveAddr("www.facebook.com", "127.0.0.1");
         *
         * To get all inserted IP addresses for a domain name:
         *    console.log(chnet.NetDNSMapGetAddrList("www.facebook.com"));
         *
         */
        chnet.NetDNSMapInsert("www.facebook.com", data[0]);
        do_http();
}

function dns_on_close(dreq)
{
        console.log("===== DNS request closed ======");
}

function main()
{
        // // Bind DNS requests to a specific IP address and/or interface.
        // chnet.NetDNSBindIPv4("10.5.5.3");
        // chnet.NetDNSBindInterface("teavpn2-cl-01");

        /*
         * DNS request.
         */
        let dreq = ring.CreateDNS("www.facebook.com");
        dreq.On("error", dns_on_error);
        dreq.On("data", dns_on_data);
        dreq.On("close", dns_on_close);
        dreq.Start();
}

main();
