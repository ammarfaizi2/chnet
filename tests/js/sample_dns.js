
const { CRDnsResolver, CRHttpRing } = require('../../chnet/chnet.js');

const dns_res = new CRDnsResolver(["8.8.8.8", "1.1.1.1"], {
        "bind_ip4": "10.90.0.3",
        "bind_iface4": "tun0"
});
const ring = new CRHttpRing;

let url_str = "https://www.facebook.com";
let url = new URL(url_str);
let dreq = dns_res.Resolve(url.hostname);
dreq.On("error", function (dreq, err) {
        console.log(`Error: ${err}`);
});
dreq.On("data", function (dreq, data) {
        console.log(data);
});
dreq.On("close", function (dreq) {
        console.log("===== DNS request closed ======");
});
dreq.Start();
