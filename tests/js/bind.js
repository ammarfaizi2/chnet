const { CRHttpRing } = require("../../chnet/chnet.js");

let ring = new CRHttpRing;

function run(options)
{
	/*
	 * Just an endpoint to show the client IP address.
	 */
	let req = ring.Create("https://www.teainside.org/ip.php", "GET", options);

	req.SetUserData({ip_target: null});

	req.SetRequestHeaders({
		"Accept-Encoding": "",
		"User-Agent": "curl"
	});

	req.On("error", function (req, err) {
		console.log("Error!");
		console.log(err);
	});

	req.On("information", function (req, info) {
		req.user_data.ip_target = info.remote_endpoint;
	});

	req.On("data", function (req, read_bytes, data) {
		if (!data)
			return;

		console.log(`Destination: ${req.user_data.ip_target}; Source: ${data.toString().trim()}`);

		/*
		 * Do the read again.
		 */
		req.Read(1024);
	});

	req.On("close", function (req) {
		// console.log("Connection closed!");
	});

	/*
	 * First read. It also starts the HTTP request.
	 */
	req.Read(1024);
}

/*
 * Notes:
 * - '#' means do not connect.
 * - null means "Let the Chromium choose it if it's available".
 */
for (let i = 0; i < 2; i++) {
	// Use whatever network.
	run({});

	// It will *never* use IPv6.
	run({
		bind_ip4: "192.168.88.87",
		bind_iface4: "wlo1",
		bind_ip6: "#",
		bind_iface6: "#"
	});

	// It will *never* use IPv6.
	run({
		bind_ip4: "10.7.7.5",
		bind_iface4: "enx00e04c680cac",
		bind_ip6: "#",
		bind_iface6: "#"
	});

	// It *may* use IPv6 if available.
	run({
		bind_ip4: "192.168.88.87",
		bind_iface4: "wlo1",
		bind_ip6: null,
		bind_iface6: null
	});

	// It will *never* use IPv4.
	run({
		bind_ip4: "#",
		bind_iface4: "#",
		bind_ip6: "2404:8000:1021:2a9:6734:1263:61be:eb86",
		bind_iface6: "enx00e04c680cac"
	});

	// It will *never* use IPv4.
	run({
		bind_ip4: "#",
		bind_iface4: "#",
		bind_ip6: "2404:8000:1021:2a9:2e1c:f798:262f:a5c7",
		bind_iface6: "enx00e04c680cac"
	});

	// Provide both IPv4 and IPv6.
	run({
		bind_ip4: "192.168.88.87",
		bind_iface4: "wlo1",
		bind_ip6: "2404:8000:1021:2a9:2e1c:f798:262f:a5c7",
		bind_iface6: "enx00e04c680cac"
	});

	// In case we don't care with IP addresses, but network card.
	run({
		bind_ip4: null,
		bind_iface4: "wlo1",
		bind_ip6: null,
		bind_iface6: "enx00e04c680cac"
	});

	// In case we don't care with IP addresses, but network card.
	run({
		bind_ip4: null,
		bind_iface4: "enx00e04c680cac",
		bind_ip6: null,
		bind_iface6: "enx00e04c680cac"
	});

	// // This one is invalid, if you specify an IP address,
	// // you must also specify a network card.
	// //
	// // This may still perform well, but the behavior is
	// // undefined.
	// run({
	// 	bind_ip4: "192.168.88.87",
	// 	bind_iface4: null,
	// 	bind_ip6: "2404:8000:1021:2a9:2e1c:f798:262f:a5c1",
	// 	bind_iface6: null,
	// });
}
