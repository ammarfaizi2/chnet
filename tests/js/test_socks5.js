const { CRHttpRing } = require('../../chnet/chnet.js');

let ring = new CRHttpRing;
let req = ring.Create("https://www.teainside.org/ip.php", "GET", {"proxy": "socks5://127.0.0.1:8081"});

req.SetUserData({});
req.SetRequestHeaders({
        "User-Agent": "curl/7.81.0",
        "Accept-Encoding": "identity"
})
req.On("error", function (req, err) {
        console.log("Error!");
        console.log(err);
});
req.On("information", function (req, info) {
        console.log(info);
        req.user_data.headers = info.headers;
});
req.On("data", function (req, read_bytes, data) {
        console.log(data.toString());
        req.Read(4096);
});

// First read.
req.Read(4096);
