<?php

/*
 * This file is used by the unit test.
 *
 * We have to run a PHP server before
 * executing the tests.
 */

if (!isset($_GET["action"]) || !is_string($_GET["action"]))
	exit(0);

switch ($_GET["action"]) {
case "hello":
	echo "Hello World!\n";
	break;
case "body":
	echo file_get_contents("php://input");
	break;
case "user_agent":
	echo $_SERVER["HTTP_USER_AGENT"] ?? "";
	break;
case "print_post":
	if (!isset($_GET["key"]) || !is_string($_GET["key"])) {
		echo "Missing key!";
		break;
	}

	echo $_POST[$_GET["key"]] ?? "";
	break;
}
