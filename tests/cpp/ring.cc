
#include <assert.h>
#include <string.h>
#include <unistd.h>
#include <chnet_ring.h>

#define ASSERT_RES_HDR(res_hdr, KEY, STR) 			\
	assert(							\
		(res_hdr).find(KEY) != (res_hdr).end() &&	\
		!strcmp((res_hdr)[KEY].c_str(), STR)		\
	);

#define TEST_RES_HDR(ch)					\
do {								\
	auto res_hdr = ch->GetResponseHeader();			\
								\
	ASSERT_RES_HDR(res_hdr, "X-Hdr-Test1", "aaaaaaaaa");	\
	ASSERT_RES_HDR(res_hdr, "X-Hdr-Test2", "bbbbbbbbb");	\
	ASSERT_RES_HDR(res_hdr, "X-Hdr-Test3", "ccccccccc");	\
	ASSERT_RES_HDR(res_hdr, "X-Hdr-Test4", "ddddddddd");	\
} while (0)

static void test_nop(void)
{
	constexpr static uint32_t nr_loop = 1024 * 8;
	constexpr static uint32_t entry = 4096;
	CNRing ring(entry);
	CNRingSQE *sqe;
	CNRingCQE *cqe;
	uint32_t total;
	uint32_t head;
	uint32_t tail;
	uint32_t ret;
	uint32_t i;

	for (i = 0; i < nr_loop; i++) {
		sqe = ring.GetSQE();
		if (!sqe) {
			assert(ring.Submit() == entry);
			sqe = ring.GetSQE();
			assert(sqe);
		}
		sqe->PrepNop();
		sqe->SetUserData(1);
	}
	assert(ring.Submit() == entry);

	total = 0;
	while (total < nr_loop) {
		ret = ring.WaitCQE(1);
		i = 0;
		cnring_for_each_cqe(&ring, head, tail, cqe) {
			assert(cqe->op_ == CNRING_OP_NOP);
			assert(cqe->res_ == 0);
			assert(cqe->user_data_ == 1);
			i++;
		}
		ring.CQAdvance(i);
		assert(ret >= 1);
		assert(i >= ret);
		total += ret;
	}
	assert(total == nr_loop);
}

static void test_chnet_ring_read(bool do_sq_start = false)
{
	CNRing ring(1);
	CNRingSQE *sqe;
	CNRingCQE *cqe;
	uint32_t ret;
	CHNet *ch;

	ch = new CHNet;
	ch->SetURL("http://127.0.0.1:8000/index.php?action=hello");
	ch->SetMethod("GET");

	if (do_sq_start) {
		sqe = ring.GetSQE();
		assert(sqe);
		sqe->PrepStart(ch);
		assert(ring.Submit() == 1);

		ret = ring.WaitCQE(1);
		cqe = ring.GetCQEHead();
		assert(ret == 1);
		assert(cqe->op_ == CNRING_OP_START);
		assert(cqe->res_ == 0);
		ring.CQAdvance(1);
	}

	sqe = ring.GetSQE();
	sqe->PrepRead(ch, 1024);
	ring.Submit();

	ret = ring.WaitCQE(1);
	TEST_RES_HDR(ch);
	cqe = ring.GetCQEHead();

	assert(ret == 1);
	assert(cqe->op_ == CNRING_OP_READ);
	assert(cqe->res_ == 13);
	assert(ch->read_ret() == cqe->res_);
	assert(!strncmp("Hello World!\n", ch->read_buf(), cqe->res_));
	ring.CQAdvance(1);

	delete ch;
}

static void test_chnet_ring_partial_read(bool do_sq_start = false)
{
	CNRing ring(1);
	CNRingSQE *sqe;
	CNRingCQE *cqe;
	uint32_t ret;
	CHNet *ch;

	ch = new CHNet;
	ch->SetURL("http://127.0.0.1:8000/index.php?action=hello");
	ch->SetMethod("GET");

	if (do_sq_start) {
		sqe = ring.GetSQE();
		sqe->PrepStart(ch);
		assert(ring.Submit() == 1);

		ret = ring.WaitCQE(1);
		cqe = ring.GetCQEHead();
		assert(ret == 1);
		assert(cqe->op_ == CNRING_OP_START);
		assert(cqe->res_ == 0);
		ring.CQAdvance(1);
	}

	/*
	 * The first read.
	 */
	sqe = ring.GetSQE();
	sqe->PrepRead(ch, 4);
	assert(ring.Submit() == 1);

	ret = ring.WaitCQE(1);
	TEST_RES_HDR(ch);
	cqe = ring.GetCQEHead();

	assert(ret == 1);
	assert(cqe->op_ == CNRING_OP_READ);
	assert(cqe->res_ == 4);
	assert(ch->read_ret() == cqe->res_);
	assert(!strncmp("Hell", ch->read_buf(), cqe->res_));
	ring.CQAdvance(1);


	/*
	 * The second read.
	 */
	sqe = ring.GetSQE();
	sqe->PrepRead(ch, 4);
	assert(ring.Submit() == 1);

	ret = ring.WaitCQE(1);
	cqe = ring.GetCQEHead();

	assert(ret == 1);
	assert(cqe->op_ == CNRING_OP_READ);
	assert(cqe->res_ == 4);
	assert(ch->read_ret() == cqe->res_);
	assert(!strncmp("o Wo", ch->read_buf(), cqe->res_));
	ring.CQAdvance(1);


	/*
	 * The third read.
	 */
	sqe = ring.GetSQE();
	sqe->PrepRead(ch, 1024);
	assert(ring.Submit() == 1);

	ret = ring.WaitCQE(1);
	cqe = ring.GetCQEHead();

	assert(ret == 1);
	assert(cqe->op_ == CNRING_OP_READ);
	assert(cqe->res_ == 5);
	assert(ch->read_ret() == cqe->res_);
	assert(!strncmp("rld!\n", ch->read_buf(), cqe->res_));
	ring.CQAdvance(1);


	/*
	 * The 4-th read.
	 */
	sqe = ring.GetSQE();
	sqe->PrepRead(ch, 1024);
	assert(ring.Submit() == 1);

	ret = ring.WaitCQE(1);
	cqe = ring.GetCQEHead();

	assert(ret == 1);
	assert(cqe->op_ == CNRING_OP_READ);
	assert(cqe->res_ == 0);
	assert(ch->read_ret() == cqe->res_);
	ring.CQAdvance(1);

	delete ch;
}

#define NR_REQ 4096
static void test_chnet_ring_read_parallel(bool do_sq_start = false)
{
	CNRing ring(NR_REQ);
	CNRingSQE *sqe;
	CNRingCQE *cqe;
	uint32_t head;
	uint32_t tail;
	uint32_t i;
	CHNet **ch;

	ch = new CHNet *[NR_REQ];

	for (i = 0; i < NR_REQ; i++) {
		ch[i] = new CHNet;
		ch[i]->SetURL("http://127.0.0.1:8000/index.php?action=hello");
	}

	if (do_sq_start) {
		for (i = 0; i < NR_REQ; i++) {
			sqe = ring.GetSQE();
			assert(sqe);
			sqe->PrepStart(ch[i]);
			sqe->SetUserDataPtr(ch[i]);
		}
		assert(ring.Submit() == NR_REQ);
		assert(ring.WaitCQE(NR_REQ) == NR_REQ);

		cnring_for_each_cqe(&ring, head, tail, cqe) {
			CHNet *ch = (CHNet *) cqe->GetUserDataPtr();

			assert(ch);
			assert(cqe->res_ == 0);
			assert(cqe->op_ == CNRING_OP_START);
		}
		ring.CQAdvance(NR_REQ);
	}


	for (i = 0; i < NR_REQ; i++) {
		sqe = ring.GetSQE();
		assert(sqe);
		sqe->PrepRead(ch[i], 1024);
		sqe->SetUserDataPtr(ch[i]);
	}
	assert(ring.Submit() == NR_REQ);
	assert(ring.WaitCQE(NR_REQ) == NR_REQ);

	cnring_for_each_cqe(&ring, head, tail, cqe) {
		CHNet *ch = (CHNet *) cqe->GetUserDataPtr();

		assert(ch);
		TEST_RES_HDR(ch);
		assert(cqe->res_ == 13);
		assert(cqe->op_ == CNRING_OP_READ);
		assert(ch->read_ret() == cqe->res_);
		assert(!strncmp("Hello World!\n", ch->read_buf(), cqe->res_));
	}
	ring.CQAdvance(NR_REQ);

	for (i = 0; i < NR_REQ; i++)
		delete ch[i];

	delete[] ch;
}

static void test_chnet_ring_partial_read_parallel(bool do_sq_start = false)
{
	CNRing ring(NR_REQ);
	CNRingSQE *sqe;
	CNRingCQE *cqe;
	uint32_t head;
	uint32_t tail;
	uint32_t i;
	CHNet **ch;

	ch = new CHNet *[NR_REQ];

	for (i = 0; i < NR_REQ; i++) {
		ch[i] = new CHNet;
		ch[i]->SetURL("http://127.0.0.1:8000/index.php?action=hello");
		ch[i]->SetMethod("GET");
	}

	if (do_sq_start) {
		for (i = 0; i < NR_REQ; i++) {
			sqe = ring.GetSQE();
			assert(sqe);
			sqe->PrepStart(ch[i]);
			sqe->SetUserDataPtr(ch[i]);
		}
		assert(ring.Submit() == NR_REQ);
		assert(ring.WaitCQE(NR_REQ) == NR_REQ);

		cnring_for_each_cqe(&ring, head, tail, cqe) {
			CHNet *ch = (CHNet *) cqe->GetUserDataPtr();

			assert(ch);
			assert(cqe->res_ == 0);
			assert(cqe->op_ == CNRING_OP_START);
		}
		ring.CQAdvance(NR_REQ);
	}


	/*
	 * The first read.
	 */
	for (i = 0; i < NR_REQ; i++) {
		sqe = ring.GetSQE();
		assert(sqe);
		sqe->PrepRead(ch[i], 4);
		sqe->SetUserDataPtr(ch[i]);
	}
	assert(ring.Submit() == NR_REQ);
	assert(ring.WaitCQE(NR_REQ) == NR_REQ);

	cnring_for_each_cqe(&ring, head, tail, cqe) {
		CHNet *ch = (CHNet *) cqe->GetUserDataPtr();

		assert(ch);
		TEST_RES_HDR(ch);
		assert(cqe->res_ == 4);
		assert(cqe->op_ == CNRING_OP_READ);
		assert(ch->read_ret() == cqe->res_);
		assert(!strncmp("Hell", ch->read_buf(), cqe->res_));
	}
	ring.CQAdvance(NR_REQ);


	/*
	 * The second read.
	 */
	for (i = 0; i < NR_REQ; i++) {
		sqe = ring.GetSQE();
		assert(sqe);
		sqe->PrepRead(ch[i], 4);
		sqe->SetUserDataPtr(ch[i]);
	}
	assert(ring.Submit() == NR_REQ);
	assert(ring.WaitCQE(NR_REQ) == NR_REQ);

	cnring_for_each_cqe(&ring, head, tail, cqe) {
		CHNet *ch = (CHNet *) cqe->GetUserDataPtr();

		assert(ch);
		assert(cqe->res_ == 4);
		assert(cqe->op_ == CNRING_OP_READ);
		assert(ch->read_ret() == cqe->res_);
		assert(!strncmp("o Wo", ch->read_buf(), cqe->res_));
	}
	ring.CQAdvance(NR_REQ);


	/*
	 * The third read.
	 */
	for (i = 0; i < NR_REQ; i++) {
		sqe = ring.GetSQE();
		assert(sqe);
		sqe->PrepRead(ch[i], 100);
		sqe->SetUserDataPtr(ch[i]);
	}
	assert(ring.Submit() == NR_REQ);
	assert(ring.WaitCQE(NR_REQ) == NR_REQ);

	cnring_for_each_cqe(&ring, head, tail, cqe) {
		CHNet *ch = (CHNet *) cqe->GetUserDataPtr();

		assert(ch);
		assert(cqe->res_ == 5);
		assert(cqe->op_ == CNRING_OP_READ);
		assert(ch->read_ret() == cqe->res_);
		assert(!strncmp("rld!\n", ch->read_buf(), cqe->res_));
	}
	ring.CQAdvance(NR_REQ);


	/*
	 * The 4-th read.
	 */
	for (i = 0; i < NR_REQ; i++) {
		sqe = ring.GetSQE();
		assert(sqe);
		sqe->PrepRead(ch[i], 100);
		sqe->SetUserDataPtr(ch[i]);
	}
	assert(ring.Submit() == NR_REQ);
	assert(ring.WaitCQE(NR_REQ) == NR_REQ);

	cnring_for_each_cqe(&ring, head, tail, cqe) {
		CHNet *ch = (CHNet *) cqe->GetUserDataPtr();

		assert(ch);
		assert(cqe->res_ == 0);
		assert(cqe->op_ == CNRING_OP_READ);
		assert(ch->read_ret() == 0);
	}
	ring.CQAdvance(NR_REQ);

	for (i = 0; i < NR_REQ; i++)
		delete ch[i];

	delete[] ch;
}

static void test_chnet_ring_simple_post(bool do_sq_start = false)
{
	CNRing ring(1);
	CNRingSQE *sqe;
	CNRingCQE *cqe;
	uint32_t ret;
	CHNet *ch;

	ch = new CHNet;
	ch->SetURL("http://127.0.0.1:8000/index.php?action=print_post&key=data");
	ch->SetMethod("POST");
	ch->SetPayload("data=AAAAAAAAA", 14);
	ch->SetRequestHeader("Content-Type", "application/x-www-form-urlencoded", true);

	if (do_sq_start) {
		sqe = ring.GetSQE();
		assert(sqe);
		sqe->PrepStart(ch);
		assert(ring.Submit() == 1);

		ret = ring.WaitCQE(1);
		cqe = ring.GetCQEHead();
		assert(ret == 1);
		assert(cqe->op_ == CNRING_OP_START);
		assert(cqe->res_ == 0);
		ring.CQAdvance(1);
	}

	sqe = ring.GetSQE();
	sqe->PrepRead(ch, 1024);
	ring.Submit();

	ret = ring.WaitCQE(1);
	TEST_RES_HDR(ch);
	cqe = ring.GetCQEHead();

	assert(ret == 1);
	assert(cqe->op_ == CNRING_OP_READ);
	assert(cqe->res_ == 9);
	assert(ch->read_ret() == cqe->res_);
	assert(!strncmp("AAAAAAAAA", ch->read_buf(), cqe->res_));
	ring.CQAdvance(1);

	delete ch;
}

static void test_chnet_ring_simple_post_parallel(bool do_sq_start = false)
{
	CNRing ring(NR_REQ);
	CNRingSQE *sqe;
	CNRingCQE *cqe;
	uint32_t head;
	uint32_t tail;
	uint32_t i;
	CHNet **ch;

	ch = new CHNet *[NR_REQ];

	for (i = 0; i < NR_REQ; i++) {
		ch[i] = new CHNet;
		ch[i]->SetURL("http://127.0.0.1:8000/index.php?action=print_post&key=data");
		ch[i]->SetMethod("POST");
		ch[i]->SetPayload("data=AAAAAAAAA", 14);
		ch[i]->SetRequestHeader("Content-Type", "application/x-www-form-urlencoded", true);
	}

	if (do_sq_start) {
		for (i = 0; i < NR_REQ; i++) {
			sqe = ring.GetSQE();
			assert(sqe);
			sqe->PrepStart(ch[i]);
			sqe->SetUserDataPtr(ch[i]);
		}
		assert(ring.Submit() == NR_REQ);
		assert(ring.WaitCQE(NR_REQ) == NR_REQ);

		cnring_for_each_cqe(&ring, head, tail, cqe) {
			CHNet *ch = (CHNet *) cqe->GetUserDataPtr();

			assert(cqe->op_ == CNRING_OP_START);
			assert(cqe->res_ == 0);
			assert(!ch->GetErrorStr());
		}
		ring.CQAdvance(NR_REQ);
	}

	for (i = 0; i < NR_REQ; i++) {
		sqe = ring.GetSQE();
		assert(sqe);
		sqe->PrepRead(ch[i], 1024);
		sqe->SetUserDataPtr(ch[i]);
	}
	assert(ring.Submit() == NR_REQ);
	assert(ring.WaitCQE(NR_REQ) == NR_REQ);

	cnring_for_each_cqe(&ring, head, tail, cqe) {
		CHNet *ch = (CHNet *) cqe->GetUserDataPtr();

		assert(ch);
		TEST_RES_HDR(ch);
		assert(cqe->op_ == CNRING_OP_READ);
		assert(cqe->res_ == 9);
		assert(!strncmp(ch->read_buf(), "AAAAAAAAA", cqe->res_));
	}
	ring.CQAdvance(NR_REQ);

	for (i = 0; i < NR_REQ; i++)
		delete ch[i];

	delete[] ch;
}

static void test_chnet_ring_chunked_post(bool do_sq_start = false)
{
	constexpr static const uint32_t nr_body_A = 1000 * 30;
	const char *buf;
	CNRing ring(1);
	CNRingSQE *sqe;
	CNRingCQE *cqe;
	uint32_t total;
	uint32_t ret;
	uint32_t i;
	CHNet *ch;

	setvbuf(stdout, NULL, _IOLBF, 4096);

	ch = new CHNet;
	ch->SetURL("http://127.0.0.1:8000/index.php?action=print_post&key=data");
	ch->SetMethod("POST");
	ch->StartChunkedBody();
	ch->WriteBody("data=", 5);
	ch->SetRequestHeader("Content-Type", "application/x-www-form-urlencoded", true);

	if (do_sq_start) {
		sqe = ring.GetSQE();
		assert(sqe);
		sqe->PrepStart(ch);
		assert(ring.Submit() == 1);

		ret = ring.WaitCQE(1);
		cqe = ring.GetCQEHead();
		assert(ret == 1);
		assert(cqe->op_ == CNRING_OP_START);
		assert(cqe->res_ == 0);
		ring.CQAdvance(1);
	}

	ch->WriteBody("12345", 5);

	sqe = ring.GetSQE();
	assert(sqe);
	sqe->PrepRead(ch, 10);
	assert(ring.Submit() == 1);

	ch->WriteBody("67890", 5);
	char x[nr_body_A];
	memset(x, 'A', nr_body_A);
	ch->WriteBody(x, nr_body_A);
	ch->WriteBody(x, nr_body_A);
	ch->WriteBody(x, nr_body_A);
	ch->WriteBody(x, nr_body_A);
	ch->StopChunkedBody();

	assert(ring.WaitCQE(1) == 1);
	cqe = ring.GetCQEHead();
	assert(cqe->op_ == CNRING_OP_READ);
	assert(cqe->res_ == 10);
	TEST_RES_HDR(ch);
	assert(ch->read_ret() == cqe->res_);
	buf = ch->read_buf();
	assert(!strncmp("1234567890", buf, cqe->res_));
	ring.CQAdvance(1);

	total = 0;
	while (total < nr_body_A*4) {
		sqe = ring.GetSQE();
		assert(sqe);
		sqe->PrepRead(ch, 1024);
		assert(ring.Submit() == 1);

		cqe = ring.GetCQEHead();
		assert(cqe->op_ == CNRING_OP_READ);
		assert(cqe->res_ >= 0);
		assert(cqe->res_ <= 1024);
		assert(ch->read_ret() == cqe->res_);
		buf = ch->read_buf();

		for (i = 0; i < (uint32_t)cqe->res_; i++)
			assert(buf[i] == 'A');
		ring.CQAdvance(1);
		total += (uint32_t)cqe->res_;
	}

	delete ch;
}

/**
 * This test needs manual environment variables setup because
 * it is a machine dependent test.
 */
static void test_bind_ip_and_iface(void)
{
	#define MAX_NR_CHNET 10

	CHNet *ch_arr[MAX_NR_CHNET];
	CNRing ring(1024);
	CNRingSQE *sqe;
	char buf[128];
	uint32_t n;
	uint32_t i;

	memset(ch_arr, 0, sizeof(ch_arr));

	n = 0;
	for (i = 0; i < 10; i++) {
		const char *bind_iface;
		const char *bind_ip;
		CHNet *ch;

		snprintf(buf, sizeof(buf), "CH%u_BIND_IP", i);
		bind_ip = getenv(buf);

		snprintf(buf, sizeof(buf), "CH%u_BIND_IFACE", i);
		bind_iface = getenv(buf);

		if (!bind_ip && !bind_iface)
			continue;

		ch = new CHNet(bind_ip, bind_iface);
		ch->SetURL("https://ipecho.net/plain");
		sqe = ring.GetSQE();
		sqe->PrepRead(ch, sizeof(buf) - 1);
		ch_arr[i] = ch;
		n++;
	}

	if (!n)
		return;

	ring.Submit(n);
	ring.WaitCQE(n);

	for (i = 0; i < 10; i++) {
		CHNet *ch = ch_arr[i];
		int ret;

		if (!ch)
			continue;

		ret = ch->read_ret();
		printf("ch %u read_ret() = %d\n", i, ret);

		if (ret < 0) {
			printf("ch %u error: %s\n", i, ch->GetErrorStr());
			goto do_delete;
		}

		memcpy(buf, ch->read_buf(), ret);
		buf[ret] = '\0';
		printf("ch %u outbound IP = %s\n", i, buf);
	do_delete:
		delete ch;
	}
	printf("test_bind_ip_and_iface() finished!\n");
	#undef MAX_NR_CHNET
}

int main(void)
{
	int i;

	test_nop();
	chnet_global_init();
	test_bind_ip_and_iface();
	for (i = 0; i < 2; i++) {
		test_chnet_ring_read(!!i);
		test_chnet_ring_partial_read(!!i);
		test_chnet_ring_read_parallel(!!i);
		test_chnet_ring_partial_read_parallel(!!i);
		test_chnet_ring_simple_post(!!i);
		test_chnet_ring_simple_post_parallel(!!i);
		test_chnet_ring_chunked_post(!!i);
	}
	chnet_global_stop();
	return 0;
}
