
#include <assert.h>
#include <string.h>
#include <unistd.h>
#include <io_ring.h>

#define ASSERT_RES_HDR(res_hdr, KEY, STR)			\
do {								\
	auto &x = (res_hdr);					\
	if ((x).find(KEY) == (x).end()) {			\
		for (auto &i: (x)) {				\
			printf("x = %s: %s\n",			\
				i.first.c_str(),		\
				i.second.c_str());		\
		}						\
		assert(0 && "find header");			\
	}							\
	if (strcmp((x)[KEY].c_str(), STR)) {			\
		for (auto &i: (x)) {				\
			printf("x = %s: %s\n",			\
				i.first.c_str(),		\
				i.second.c_str());		\
		}						\
		assert(0 && "strcmp");				\
	}							\
} while (0)

#define TEST_RES_HDR(ch)					\
do {								\
	auto res_hdr = ch->GetResponseHeader();			\
								\
	ASSERT_RES_HDR(res_hdr, "X-Hdr-Test1", "aaaaaaaaa");	\
	ASSERT_RES_HDR(res_hdr, "X-Hdr-Test2", "bbbbbbbbb");	\
	ASSERT_RES_HDR(res_hdr, "X-Hdr-Test3", "ccccccccc");	\
	ASSERT_RES_HDR(res_hdr, "X-Hdr-Test4", "ddddddddd");	\
} while (0)

static void test_ring_op_nop(void)
{
	constexpr static uint32_t nr_loop = 1024 * 32;
	constexpr static uint32_t entry = 4096;
	IORing ring(entry);
	IORingSQE *sqe;
	IORingCQE *cqe;
	uint32_t total_submitted = 0;
	uint32_t total_reaped;
	uint32_t head;
	uint32_t tail;
	uint32_t ret;
	uint32_t i;

	for (i = 0; i < nr_loop; i++) {
		sqe = ring.GetSQE();
		if (!sqe) {
			ret = ring.Submit();
			assert(ret == entry);
			total_submitted += entry;
			sqe = ring.GetSQE();
			assert(sqe);
		}
		sqe->PrepNop();
		sqe->SetUserData(999);
	}
	assert(ring.Submit() == entry);
	total_submitted += entry;

	total_reaped = 0;
	while (total_reaped < total_submitted) {
		ret = ring.WaitCQE(1);
		assert(ret >= 1);

		i = 0;
		ioring_for_each_cqe(&ring, head, tail, cqe) {
			assert(cqe->res_ == 0);
			assert(cqe->op_ == IORING_OP_NOP);
			assert(cqe->user_data_ == 999);
			i++;
		}
		ring.CQAdvance(i);
		assert(i >= 1);
		total_reaped += i;
	}
	assert(total_reaped == total_submitted);
}

static void test_ring_chnet_read(bool do_sq_start = false)
{
	IORing ring(1);
	IORingSQE *sqe;
	IORingCQE *cqe;
	uint32_t ret;
	CHNet *ch;

	ch = new CHNet;
	ch->SetURL("http://127.0.0.1:8000/index.php?action=hello");
	ch->SetMethod("GET");
        ch->SetRequestHeader("Accept-Encoding", "identity");

	if (do_sq_start) {
		sqe = ring.GetSQE();
		assert(sqe);
		sqe->PrepStart(ch);
		assert(ring.Submit() == 1);

		ret = ring.WaitCQE(1);
		cqe = ring.GetCQEHead();
		assert(ret == 1);
		assert(cqe->op_ == IORING_OP_CHNET_START);
		assert(cqe->res_ == 0);
		ring.CQAdvance(1);
	}

	sqe = ring.GetSQE();
	sqe->PrepRead(ch, 1024);
	ring.Submit();

	ret = ring.WaitCQE(1);
	cqe = ring.GetCQEHead();

	assert(ret == 1);
	assert(cqe->op_ == IORING_OP_CHNET_READ);
	assert(cqe->res_ == 13);
	assert(ch->GetReadRet() == cqe->res_);
	TEST_RES_HDR(ch);
	assert(!strncmp("Hello World!\n", ch->GetReadBuffer(), cqe->res_));
	ring.CQAdvance(1);

	delete ch;
}

static void test_ring_chnet_read_partial(bool do_sq_start = false)
{
        (void)do_sq_start;

	// IORing ring(1);
	// IORingSQE *sqe;
	// IORingCQE *cqe;
	// uint32_t ret;
	// CHNet *ch;

	// ch = new CHNet;
	// ch->SetURL("http://127.0.0.1:8000/index.php?action=hello");
	// ch->SetMethod("GET");
        // ch->SetRequestHeader("Accept-Encoding", "identity");

	// if (do_sq_start) {
	// 	sqe = ring.GetSQE();
	// 	sqe->PrepStart(ch);
	// 	assert(ring.Submit() == 1);

	// 	ret = ring.WaitCQE(1);
	// 	cqe = ring.GetCQEHead();
	// 	assert(ret == 1);
	// 	assert(cqe->op_ == IORING_OP_CHNET_START);
	// 	assert(cqe->res_ == 0);
	// 	ring.CQAdvance(1);
	// }

	// /*
	//  * The first read.
	//  */
	// sqe = ring.GetSQE();
	// sqe->PrepRead(ch, 4);
	// assert(ring.Submit() == 1);

	// ret = ring.WaitCQE(1);
	// cqe = ring.GetCQEHead();

        // printf("aaaa:\n");
        // write(1, ch->GetReadBuffer(), 2);
        // printf("\n");
	// assert(ret == 1);
	// assert(cqe->op_ == IORING_OP_CHNET_READ);
	// assert(cqe->res_ == 4);
	// assert(ch->GetReadRet() == cqe->res_);
	// TEST_RES_HDR(ch);
	// assert(!strncmp("Hell", ch->GetReadBuffer(), cqe->res_));
	// ring.CQAdvance(1);


	// /*
	//  * The second read.
	//  */
	// sqe = ring.GetSQE();
	// sqe->PrepRead(ch, 4);
	// assert(ring.Submit() == 1);

	// ret = ring.WaitCQE(1);
	// cqe = ring.GetCQEHead();

	// assert(ret == 1);
	// assert(cqe->op_ == IORING_OP_CHNET_READ);
	// assert(cqe->res_ == 4);
	// assert(ch->GetReadRet() == cqe->res_);
	// assert(!strncmp("o Wo", ch->GetReadBuffer(), cqe->res_));
	// ring.CQAdvance(1);


	// /*
	//  * The third read.
	//  */
	// sqe = ring.GetSQE();
	// sqe->PrepRead(ch, 1024);
	// assert(ring.Submit() == 1);

	// ret = ring.WaitCQE(1);
	// cqe = ring.GetCQEHead();

	// assert(ret == 1);
	// assert(cqe->op_ == IORING_OP_CHNET_READ);
	// assert(cqe->res_ == 5);
	// assert(ch->GetReadRet() == cqe->res_);
	// assert(!strncmp("rld!\n", ch->GetReadBuffer(), cqe->res_));
	// ring.CQAdvance(1);


	// /*
	//  * The 4-th read.
	//  */
	// sqe = ring.GetSQE();
	// sqe->PrepRead(ch, 1024);
	// assert(ring.Submit() == 1);

	// ret = ring.WaitCQE(1);
	// cqe = ring.GetCQEHead();

	// assert(ret == 1);
	// assert(cqe->op_ == IORING_OP_CHNET_READ);
	// assert(cqe->res_ == 0);
	// assert(ch->GetReadRet() == cqe->res_);
	// ring.CQAdvance(1);

	// delete ch;
}

#define NR_REQ 4096
static void test_ring_chnet_parallel_read(bool do_sq_start = false)
{
	IORing ring(NR_REQ);
	IORingSQE *sqe;
	IORingCQE *cqe;
	uint32_t head;
	uint32_t tail;
	uint32_t i;
	CHNet **ch;

	ch = new CHNet *[NR_REQ];

	for (i = 0; i < NR_REQ; i++) {
		ch[i] = new CHNet;
		ch[i]->SetURL("http://127.0.0.1:8000/index.php?action=hello");
                ch[i]->SetRequestHeader("Accept-Encoding", "identity");
	}

	if (do_sq_start) {
		for (i = 0; i < NR_REQ; i++) {
			sqe = ring.GetSQE();
			assert(sqe);
			sqe->PrepStart(ch[i]);
			sqe->SetUserDataPtr(ch[i]);
		}
		assert(ring.Submit() == NR_REQ);
		assert(ring.WaitCQE(NR_REQ) == NR_REQ);

		i = 0;
		ioring_for_each_cqe(&ring, head, tail, cqe) {
			CHNet *ch = (CHNet *) cqe->GetUserDataPtr();

			assert(ch);
			assert(cqe->res_ == 0);
			assert(cqe->op_ == IORING_OP_CHNET_START);
			i++;
		}
		assert(i == NR_REQ);
		ring.CQAdvance(NR_REQ);
	}


	for (i = 0; i < NR_REQ; i++) {
		sqe = ring.GetSQE();
		assert(sqe);
		sqe->PrepRead(ch[i], 1024);
		sqe->SetUserDataPtr(ch[i]);
	}
	assert(ring.Submit() == NR_REQ);
	assert(ring.WaitCQE(NR_REQ) == NR_REQ);

	i = 0;
	ioring_for_each_cqe(&ring, head, tail, cqe) {
		CHNet *ch = (CHNet *) cqe->GetUserDataPtr();

		assert(ch);
		assert(cqe->res_ == 13);
		assert(cqe->op_ == IORING_OP_CHNET_READ);
		assert(ch->GetReadRet() == cqe->res_);
		assert(!strncmp("Hello World!\n", ch->GetReadBuffer(), cqe->res_));
		TEST_RES_HDR(ch);
		i++;
	}
	assert(i == NR_REQ);
	ring.CQAdvance(NR_REQ);

	for (i = 0; i < NR_REQ; i++)
		delete ch[i];

	delete[] ch;
}

static void test_ring_chnet_parallel_read_partial(bool do_sq_start = false)
{
        (void)do_sq_start;
	// IORing ring(NR_REQ);
	// IORingSQE *sqe;
	// IORingCQE *cqe;
	// uint32_t head;
	// uint32_t tail;
	// uint32_t i;
	// CHNet **ch;

	// ch = new CHNet *[NR_REQ];

	// for (i = 0; i < NR_REQ; i++) {
	// 	ch[i] = new CHNet;
	// 	ch[i]->SetURL("http://127.0.0.1:8000/index.php?action=hello");
	// 	ch[i]->SetMethod("GET");
        //         ch[i]->SetRequestHeader("Accept-Encoding", "identity");
	// }

	// if (do_sq_start) {
	// 	for (i = 0; i < NR_REQ; i++) {
	// 		sqe = ring.GetSQE();
	// 		assert(sqe);
	// 		sqe->PrepStart(ch[i]);
	// 		sqe->SetUserDataPtr(ch[i]);
	// 	}
	// 	assert(ring.Submit() == NR_REQ);
	// 	assert(ring.WaitCQE(NR_REQ) == NR_REQ);

	// 	i = 0;
	// 	ioring_for_each_cqe(&ring, head, tail, cqe) {
	// 		CHNet *ch = (CHNet *) cqe->GetUserDataPtr();

	// 		assert(ch);
	// 		assert(cqe->res_ == 0);
	// 		assert(cqe->op_ == IORING_OP_CHNET_START);
	// 		i++;
	// 	}
	// 	assert(i == NR_REQ);
	// 	ring.CQAdvance(NR_REQ);
	// }


	// /*
	//  * The first read.
	//  */
	// for (i = 0; i < NR_REQ; i++) {
	// 	sqe = ring.GetSQE();
	// 	assert(sqe);
	// 	sqe->PrepRead(ch[i], 4);
	// 	sqe->SetUserDataPtr(ch[i]);
	// }
	// assert(ring.Submit() == NR_REQ);
	// assert(ring.WaitCQE(NR_REQ) == NR_REQ);

	// i = 0;
	// ioring_for_each_cqe(&ring, head, tail, cqe) {
	// 	CHNet *ch = (CHNet *) cqe->GetUserDataPtr();

	// 	assert(ch);
	// 	assert(cqe->res_ == 4);
	// 	assert(cqe->op_ == IORING_OP_CHNET_READ);
	// 	assert(ch->GetReadRet() == cqe->res_);
	// 	assert(!strncmp("Hell", ch->GetReadBuffer(), cqe->res_));
	// 	TEST_RES_HDR(ch);
	// 	i++;
	// }
	// assert(i == NR_REQ);
	// ring.CQAdvance(NR_REQ);


	// /*
	//  * The second read.
	//  */
	// for (i = 0; i < NR_REQ; i++) {
	// 	sqe = ring.GetSQE();
	// 	assert(sqe);
	// 	sqe->PrepRead(ch[i], 4);
	// 	sqe->SetUserDataPtr(ch[i]);
	// }
	// assert(ring.Submit() == NR_REQ);
	// assert(ring.WaitCQE(NR_REQ) == NR_REQ);

	// i = 0;
	// ioring_for_each_cqe(&ring, head, tail, cqe) {
	// 	CHNet *ch = (CHNet *) cqe->GetUserDataPtr();

	// 	assert(ch);
	// 	assert(cqe->res_ == 4);
	// 	assert(cqe->op_ == IORING_OP_CHNET_READ);
	// 	assert(ch->GetReadRet() == cqe->res_);
	// 	assert(!strncmp("o Wo", ch->GetReadBuffer(), cqe->res_));
	// 	i++;
	// }
	// assert(i == NR_REQ);
	// ring.CQAdvance(NR_REQ);


	// /*
	//  * The third read.
	//  */
	// for (i = 0; i < NR_REQ; i++) {
	// 	sqe = ring.GetSQE();
	// 	assert(sqe);
	// 	sqe->PrepRead(ch[i], 100);
	// 	sqe->SetUserDataPtr(ch[i]);
	// }
	// assert(ring.Submit() == NR_REQ);
	// assert(ring.WaitCQE(NR_REQ) == NR_REQ);

	// i = 0;
	// ioring_for_each_cqe(&ring, head, tail, cqe) {
	// 	CHNet *ch = (CHNet *) cqe->GetUserDataPtr();

	// 	assert(ch);
	// 	assert(cqe->res_ == 5);
	// 	assert(cqe->op_ == IORING_OP_CHNET_READ);
	// 	assert(ch->GetReadRet() == cqe->res_);
	// 	assert(!strncmp("rld!\n", ch->GetReadBuffer(), cqe->res_));
	// 	i++;
	// }
	// assert(i == NR_REQ);
	// ring.CQAdvance(NR_REQ);


	// /*
	//  * The 4-th read.
	//  */
	// for (i = 0; i < NR_REQ; i++) {
	// 	sqe = ring.GetSQE();
	// 	assert(sqe);
	// 	sqe->PrepRead(ch[i], 100);
	// 	sqe->SetUserDataPtr(ch[i]);
	// }
	// assert(ring.Submit() == NR_REQ);
	// assert(ring.WaitCQE(NR_REQ) == NR_REQ);

	// i = 0;
	// ioring_for_each_cqe(&ring, head, tail, cqe) {
	// 	CHNet *ch = (CHNet *) cqe->GetUserDataPtr();

	// 	assert(ch);
	// 	assert(cqe->res_ == 0);
	// 	assert(cqe->op_ == IORING_OP_CHNET_READ);
	// 	assert(ch->GetReadRet() == 0);
	// 	i++;
	// }
	// assert(i == NR_REQ);
	// ring.CQAdvance(NR_REQ);

	// for (i = 0; i < NR_REQ; i++)
	// 	delete ch[i];

	// delete[] ch;
}

static void test_ring_chnet_post(bool do_sq_start = false)
{
	IORing ring(1);
	IORingSQE *sqe;
	IORingCQE *cqe;
	uint32_t ret;
	CHNet *ch;

	ch = new CHNet;
	ch->SetURL("http://127.0.0.1:8000/index.php?action=print_post&key=data");
	ch->SetMethod("POST");
	ch->SetPayload("data=AAAAAAAAA", 14);
	ch->SetRequestHeader("Content-Type", "application/x-www-form-urlencoded", true);
        ch->SetRequestHeader("Accept-Encoding", "identity");

	if (do_sq_start) {
		sqe = ring.GetSQE();
		assert(sqe);
		sqe->PrepStart(ch);
		assert(ring.Submit() == 1);

		ret = ring.WaitCQE(1);
		cqe = ring.GetCQEHead();
		assert(ret == 1);
		assert(cqe->op_ == IORING_OP_CHNET_START);
		assert(cqe->res_ == 0);
		ring.CQAdvance(1);
	}

	sqe = ring.GetSQE();
	sqe->PrepRead(ch, 1024);
	ring.Submit();

	ret = ring.WaitCQE(1);
	cqe = ring.GetCQEHead();

	assert(ret == 1);
	assert(cqe->op_ == IORING_OP_CHNET_READ);
	assert(cqe->res_ == 9);
	assert(ch->GetReadRet() == cqe->res_);
	assert(!strncmp("AAAAAAAAA", ch->GetReadBuffer(), cqe->res_));
	TEST_RES_HDR(ch);
	ring.CQAdvance(1);

	delete ch;
}

static void test_ring_chnet_parallel_post(bool do_sq_start = false)
{
	IORing ring(NR_REQ);
	IORingSQE *sqe;
	IORingCQE *cqe;
	uint32_t head;
	uint32_t tail;
	uint32_t i;
	CHNet **ch;

	ch = new CHNet *[NR_REQ];

	for (i = 0; i < NR_REQ; i++) {
		ch[i] = new CHNet;
		ch[i]->SetURL("http://127.0.0.1:8000/index.php?action=print_post&key=data");
		ch[i]->SetMethod("POST");
		ch[i]->SetPayload("data=AAAAAAAAA", 14);
		ch[i]->SetRequestHeader("Content-Type", "application/x-www-form-urlencoded", true);
                ch[i]->SetRequestHeader("Accept-Encoding", "identity");
	}

	if (do_sq_start) {
		for (i = 0; i < NR_REQ; i++) {
			sqe = ring.GetSQE();
			assert(sqe);
			sqe->PrepStart(ch[i]);
			sqe->SetUserDataPtr(ch[i]);
		}
		assert(ring.Submit() == NR_REQ);
		assert(ring.WaitCQE(NR_REQ) == NR_REQ);

		i = 0;
		ioring_for_each_cqe(&ring, head, tail, cqe) {
			CHNet *ch = (CHNet *) cqe->GetUserDataPtr();

			assert(cqe->op_ == IORING_OP_CHNET_START);
			assert(cqe->res_ == 0);
			assert(!ch->GetErrorStr());
			i++;
		}
		assert(i == NR_REQ);
		ring.CQAdvance(NR_REQ);
	}

	for (i = 0; i < NR_REQ; i++) {
		sqe = ring.GetSQE();
		assert(sqe);
		sqe->PrepRead(ch[i], 1024);
		sqe->SetUserDataPtr(ch[i]);
	}

	assert(ring.Submit() == NR_REQ);
	assert(ring.WaitCQE(NR_REQ) == NR_REQ);

	i = 0;
	ioring_for_each_cqe(&ring, head, tail, cqe) {
		CHNet *ch = (CHNet *) cqe->GetUserDataPtr();

		assert(ch);
		assert(cqe->op_ == IORING_OP_CHNET_READ);
		assert(cqe->res_ == 9);
		assert(!strncmp(ch->GetReadBuffer(), "AAAAAAAAA", cqe->res_));
		TEST_RES_HDR(ch);
		i++;
	}
	assert(i == NR_REQ);
	ring.CQAdvance(NR_REQ);

	for (i = 0; i < NR_REQ; i++)
		delete ch[i];

	delete[] ch;
}

static void test_ring_chnet_parallel_post_chunked(bool do_sq_start = false)
{
        (void)do_sq_start;
	// constexpr static const uint32_t nr_body_A = 1000 * 30;
	// const char *buf;
	// IORing ring(1);
	// IORingSQE *sqe;
	// IORingCQE *cqe;
	// uint32_t total;
	// uint32_t ret;
	// uint32_t i;
	// CHNet *ch;

	// ch = new CHNet;
	// ch->SetURL("http://127.0.0.1:8000/index.php?action=print_post&key=data");
	// ch->SetMethod("POST");
	// ch->StartChunkedPayload();
	// ch->Write("data=", 5);
	// ch->SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        // ch->SetRequestHeader("Accept-Encoding", "identity");

	// if (do_sq_start) {
	// 	sqe = ring.GetSQE();
	// 	assert(sqe);
	// 	sqe->PrepStart(ch);
	// 	assert(ring.Submit() == 1);

	// 	ret = ring.WaitCQE(1);
	// 	cqe = ring.GetCQEHead();
	// 	assert(ret == 1);
	// 	assert(cqe->op_ == IORING_OP_CHNET_START);
	// 	assert(cqe->res_ == 0);
	// 	ring.CQAdvance(1);
	// }

	// ch->Write("12345", 5);

	// sqe = ring.GetSQE();
	// assert(sqe);
	// sqe->PrepRead(ch, 10);
	// assert(ring.Submit() == 1);

	// ch->Write("67890", 5);
	// char x[nr_body_A];
	// memset(x, 'A', nr_body_A);
	// ch->Write(x, nr_body_A);
	// ch->Write(x, nr_body_A);
	// ch->Write(x, nr_body_A);
	// ch->Write(x, nr_body_A);
	// ch->StopChunkedPayload();

	// assert(ring.WaitCQE(1) == 1);
	// cqe = ring.GetCQEHead();
	// assert(cqe->op_ == IORING_OP_CHNET_READ);
	// assert(cqe->res_ == 10);
	// TEST_RES_HDR(ch);
	// assert(ch->GetReadRet() == cqe->res_);
	// buf = ch->GetReadBuffer();
	// assert(!strncmp("1234567890", buf, cqe->res_));
	// ring.CQAdvance(1);

	// total = 0;
	// while (total < nr_body_A*4) {
	// 	sqe = ring.GetSQE();
	// 	assert(sqe);
	// 	sqe->PrepRead(ch, 1024);
	// 	assert(ring.Submit() == 1);

	// 	cqe = ring.GetCQEHead();
	// 	assert(cqe->op_ == IORING_OP_CHNET_READ);
	// 	assert(cqe->res_ >= 0);
	// 	assert(cqe->res_ <= 1024);
	// 	assert(ch->GetReadRet() == cqe->res_);
	// 	buf = ch->GetReadBuffer();

	// 	for (i = 0; i < (uint32_t)cqe->res_; i++)
	// 		assert(buf[i] == 'A');

	// 	ring.CQAdvance(1);
	// 	total += (uint32_t)cqe->res_;
	// }

	// delete ch;
}

static void test_ring_nop(void)
{
	int i;

	for (i = 0; i < 5; i++) {
		test_ring_op_nop();
		printf("PASS: test_ring_op_nop()\n");
	}
}

static void test_ring_dns_query(void)
{
        constexpr static const char *hosts[] = {
                "localhost",
                "google.com",
                "fb.com",
                "twitter.com",
        };
        constexpr static const size_t nr_hosts = sizeof(hosts)/sizeof(hosts[0]);
        struct DnsQueryResult res_arr[nr_hosts];
        IORing ring(32);
        IORingSQE *sqe;
        IORingCQE *cqe;
        uint32_t i, j;

        for (i = 0; i < nr_hosts; i++) {
                sqe = ring.GetSQE();
                assert(sqe);
                sqe->PrepDnsQuery(hosts[i], &res_arr[i]);
        }

        assert(ring.Submit() == nr_hosts);
        ring.WaitCQE(nr_hosts);

        for (i = 0; i < nr_hosts; i++) {
                cqe = ring.GetCQEHead();
                assert(cqe->op_ == IORING_OP_DNS_QUERY);
                assert(cqe->res_ == 0);
                ring.CQAdvance(1);
        }

        (void)j;
        // for (i = 0; i < nr_hosts; i++) {
        //         for (j = 0; j < res_arr[i].size_; j++) {
        //                 printf("%s: %s\n", hosts[i], res_arr[i].addresses_[j]);
        //         }
        // }
}

static void test_ring_chnet(void)
{
	int i;

	for (i = 0; i < 2; i++) {
		test_ring_chnet_read(!!i);
		printf("PASS: test_ring_chnet_read(do_sq_start = %d)\n", i);

		test_ring_chnet_read_partial(!!i);
		printf("PASS: test_ring_chnet_read_partial(do_sq_start = %d)\n", i);

		test_ring_chnet_parallel_read(!!i);
		printf("PASS: test_ring_chnet_parallel_read(do_sq_start = %d)\n", i);

		test_ring_chnet_parallel_read_partial(!!i);
		printf("PASS: test_ring_chnet_parallel_read_partial(do_sq_start = %d)\n", i);

		test_ring_chnet_post(!!i);
		printf("PASS: test_ring_chnet_post(do_sq_start = %d)\n", i);

		test_ring_chnet_parallel_post(!!i);
		printf("PASS: test_ring_chnet_parallel_post(do_sq_start = %d)\n", i);

		test_ring_chnet_parallel_post_chunked(!!i);
		printf("PASS: test_ring_chnet_parallel_post_chunked(do_sq_start = %d)\n", i);
	}
}

int main(void)
{
	int i;

	chnet_global_init();
	for (i = 0; i < 1024; i++) {
		printf("============== Iteration %d ==============\n", i);
		test_ring_nop();
		test_ring_chnet();
                test_ring_dns_query();
		printf("PASS: test_ring_dns_query()\n");
	}
	chnet_global_stop();
	return 0;
}
