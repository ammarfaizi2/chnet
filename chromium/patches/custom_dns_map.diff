diff --git a/net/dns/BUILD.gn b/net/dns/BUILD.gn
index 30db1d060afd330a..064b2a5eae8ea292 100644
--- a/net/dns/BUILD.gn
+++ b/net/dns/BUILD.gn
@@ -30,6 +30,8 @@ source_set("dns") {
     "address_info.cc",
     "address_info.h",
     "address_sorter.h",
+    "chnet_dns_map.cc",
+    "chnet_dns_map.h",
     "context_host_resolver.cc",
     "context_host_resolver.h",
     "dns_alias_utility.cc",
diff --git a/net/dns/chnet_dns_map.cc b/net/dns/chnet_dns_map.cc
new file mode 100644
index 0000000000000000..0027397aa28f75d5
--- /dev/null
+++ b/net/dns/chnet_dns_map.cc
@@ -0,0 +1,164 @@
+
+#include "net/dns/chnet_dns_map.h"
+#include <mutex>
+#include <string>
+#include <vector>
+#include <algorithm>
+#include <unordered_map>
+
+#define __must_hold(LOCK)
+#define __releases(LOCK)
+#define __acquires(LOCK)
+
+static std::unordered_map<std::string, std::vector<std::string>> *g_dns_map;
+static std::mutex g_dns_lock;
+
+extern "C" {
+
+void chnet_dns_map_lock(void)
+	__acquires(&g_dns_lock)
+{
+	g_dns_lock.lock();
+}
+
+void chnet_dns_map_unlock(void)
+	__releases(&g_dns_lock)
+{
+	g_dns_lock.unlock();
+}
+
+void __chnet_dns_map_insert(const char *host, const char *ip)
+	__must_hold(&g_dns_lock)
+{
+	if (!g_dns_map) {
+		g_dns_map = new std::unordered_map<std::string, std::vector<std::string>>;
+	}
+
+	auto it = g_dns_map->find(host);
+	if (it == g_dns_map->end()) {
+		std::vector<std::string> v;
+		v.push_back(ip);
+		g_dns_map->insert(std::make_pair(host, v));
+		return;
+	}
+
+	auto *v = &it->second;
+	if (std::find(v->begin(), v->end(), ip) == v->end())
+		v->push_back(ip);
+}
+
+bool __chnet_dns_map_lookup(struct chnet_dns_lookup *lk)
+	__must_hold(&g_dns_lock)
+{
+	size_t idx;
+
+	if (!g_dns_map)
+		return false;
+
+	if (!lk->vec) {
+		auto it = g_dns_map->find(lk->host);
+		if (it == g_dns_map->end())
+			return false;
+		lk->vec = &it->second;
+		lk->iter = 0;
+	}
+
+	auto *v = static_cast<std::vector<std::string> *>(lk->vec);
+	if (lk->iter >= v->size())
+		return false;
+
+	idx = v->size() - lk->iter - 1;
+	lk->ip = v->at(idx).c_str();
+	lk->iter++;
+	return true;
+}
+
+
+void __chnet_dns_map_remove(const char *host, const char *ip)
+	__must_hold(&g_dns_lock)
+{
+	if (!g_dns_map)
+		return;
+
+	auto it = g_dns_map->find(host);
+	if (it == g_dns_map->end())
+		return;
+
+	auto *v = &it->second;
+	auto it2 = std::find(v->begin(), v->end(), ip);
+	if (it2 == v->end())
+		return;
+
+	v->erase(it2);
+	if (v->empty())
+		g_dns_map->erase(it);
+}
+
+void __chnet_dns_map_remove_all(const char *host)
+	__must_hold(&g_dns_lock)
+{
+	if (!g_dns_map)
+		return;
+
+	auto it = g_dns_map->find(host);
+	if (it == g_dns_map->end())
+		return;
+
+	g_dns_map->erase(it);
+}
+
+void __chnet_dns_map_pop(const char *host)
+	__must_hold(&g_dns_lock)
+{
+	if (!g_dns_map)
+		return;
+
+	auto it = g_dns_map->find(host);
+	if (it == g_dns_map->end())
+		return;
+
+	auto *v = &it->second;
+	v->pop_back();
+	if (v->empty())
+		g_dns_map->erase(it);
+}
+
+void chnet_dns_map_insert(const char *host, const char *ip)
+{
+	chnet_dns_map_lock();
+	__chnet_dns_map_insert(host, ip);
+	chnet_dns_map_unlock();
+}
+
+bool chnet_dns_map_lookup(struct chnet_dns_lookup *lk)
+{
+	bool ret;
+
+	chnet_dns_map_lock();
+	ret = __chnet_dns_map_lookup(lk);
+	chnet_dns_map_unlock();
+	return ret;
+}
+
+void chnet_dns_map_remove(const char *host, const char *ip)
+{
+	chnet_dns_map_lock();
+	__chnet_dns_map_remove(host, ip);
+	chnet_dns_map_unlock();
+}
+
+void chnet_dns_map_remove_all(const char *host)
+{
+	chnet_dns_map_lock();
+	__chnet_dns_map_remove_all(host);
+	chnet_dns_map_unlock();
+}
+
+void chnet_dns_map_pop(const char *host)
+{
+	chnet_dns_map_lock();
+	__chnet_dns_map_pop(host);
+	chnet_dns_map_unlock();
+}
+
+} /* extern "C" */
diff --git a/net/dns/chnet_dns_map.h b/net/dns/chnet_dns_map.h
new file mode 100644
index 0000000000000000..d2a61d2efdab4297
--- /dev/null
+++ b/net/dns/chnet_dns_map.h
@@ -0,0 +1,43 @@
+
+#ifndef NET_DNS_CHNET_DNS_MAP_H_
+#define NET_DNS_CHNET_DNS_MAP_H_
+
+#include "net/base/net_export.h"
+#include <cstddef>
+
+#ifdef WIN32
+#include <winsock2.h>
+#else
+#include <arpa/inet.h>
+#endif
+
+extern "C" {
+
+struct chnet_dns_lookup {
+	void		*vec;
+	const char	*host;
+	const char	*ip;
+	size_t		iter;
+};
+
+NET_EXPORT void chnet_dns_map_lock(void);
+NET_EXPORT void chnet_dns_map_unlock(void);
+
+NET_EXPORT void chnet_dns_map_insert(const char *host, const char *ip);
+NET_EXPORT void __chnet_dns_map_insert(const char *host, const char *ip);
+
+NET_EXPORT void chnet_dns_map_remove(const char *host, const char *ip);
+NET_EXPORT void __chnet_dns_map_remove(const char *host, const char *ip);
+
+NET_EXPORT void chnet_dns_map_remove_all(const char *host);
+NET_EXPORT void __chnet_dns_map_remove_all(const char *host);
+
+NET_EXPORT void chnet_dns_map_pop(const char *host);
+NET_EXPORT void __chnet_dns_map_pop(const char *host);
+
+NET_EXPORT bool chnet_dns_map_lookup(struct chnet_dns_lookup *lk);
+NET_EXPORT bool __chnet_dns_map_lookup(struct chnet_dns_lookup *lk);
+
+} /* extern "C" */
+
+#endif // NET_DNS_CHNET_DNS_MAP_H_
diff --git a/net/dns/host_resolver_manager.cc b/net/dns/host_resolver_manager.cc
index 38894d02099da408..daa38b3aca7b5c1d 100644
--- a/net/dns/host_resolver_manager.cc
+++ b/net/dns/host_resolver_manager.cc
@@ -4,6 +4,12 @@
 
 #include "net/dns/host_resolver_manager.h"
 
+#ifdef WIN32
+#include <winsock2.h>
+#else
+#include <arpa/inet.h>
+#endif
+
 #include <cmath>
 #include <iterator>
 #include <limits>
@@ -110,6 +116,7 @@
 #include "third_party/abseil-cpp/absl/types/variant.h"
 #include "url/scheme_host_port.h"
 #include "url/url_constants.h"
+#include "net/dns/chnet_dns_map.h"
 
 #if BUILDFLAG(ENABLE_MDNS)
 #include "net/dns/mdns_client_impl.h"
@@ -131,6 +138,11 @@
 #endif  // BUILDFLAG(IS_ANDROID)
 #endif  // BUILDFLAG(IS_POSIX) || BUILDFLAG(IS_FUCHSIA)
 
+extern "C" {
+NET_EXPORT
+const char *(*connect_bind_get_iface_key)(void);
+} // extern "C"
+
 namespace net {
 
 namespace {
@@ -3169,6 +3181,53 @@ HostCache::Entry HostResolverManager::ResolveLocally(
   if (ip_address.IsValid())
     return ResolveAsIP(job_key.query_types, resolve_canonname, ip_address);
 
+  base::StringPiece hostname = GetHostname(job_key.host);
+  std::vector<IPEndPoint> chnet_resolved_addr;
+  std::string raw_hostname;
+  if (connect_bind_get_iface_key) {
+    raw_hostname = std::string(connect_bind_get_iface_key()) +
+                   std::string(hostname.data());
+  } else {
+    raw_hostname = "##" + std::string(hostname.data());
+  }
+  struct chnet_dns_lookup ch_iter;
+
+  ch_iter.host = raw_hostname.c_str();
+  ch_iter.vec = nullptr;
+  chnet_dns_map_lock();
+  while (1) {
+    bool ret = __chnet_dns_map_lookup(&ch_iter);
+    if (!ret) {
+      break;
+    }
+
+    struct sockaddr_storage ss;
+    struct sockaddr_in *sin = (struct sockaddr_in *)&ss;
+    struct sockaddr_in6 *sin6 = (struct sockaddr_in6 *)&ss;
+    IPEndPoint ip_endpoint;
+
+    memset(&ss, 0, sizeof(ss));
+    if (inet_pton(AF_INET, ch_iter.ip, &sin->sin_addr) == 1) {
+      sin->sin_family = AF_INET;
+    } else if (inet_pton(AF_INET6, ch_iter.ip, &sin6->sin6_addr) == 1) {
+      sin6->sin6_family = AF_INET6;
+    } else {
+      continue;
+    }
+
+    if (!ip_endpoint.FromSockAddr((struct sockaddr *)&ss, sizeof(ss))) {
+      continue;
+    }
+
+    chnet_resolved_addr.push_back(ip_endpoint);
+  }
+  chnet_dns_map_unlock();
+
+  if (chnet_resolved_addr.size() > 0) {
+    return HostCache::Entry(OK, std::move(chnet_resolved_addr), /*aliases=*/{},
+                            HostCache::Entry::SOURCE_UNKNOWN);
+  }
+
   // Special-case localhost names, as per the recommendations in
   // https://tools.ietf.org/html/draft-west-let-localhost-be-localhost.
   absl::optional<HostCache::Entry> resolved =
