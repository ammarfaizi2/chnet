@echo off

set param_1=%1
set param_2=%2

REM Replace any `/` with `\`
set dst_path=%param_1:/=\%
set src_path=%param_2:/=\%

REM Copy chnet.dll.lib
copy /v /y "%src_path%\chnet.dll.lib" "%dst_path%\chnet.lib"

REM Copy dlls required by chnet.dll
copy /v /y "%src_path%\*.dll" "%dst_path%"

