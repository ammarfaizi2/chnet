
BASE_DIR := $(dir $(realpath $(lastword $(MAKEFILE_LIST))))
BASE_DIR := $(strip $(patsubst %/, %, $(BASE_DIR)))

CC := clang
CXX := clang++
override CXXFLAGS := -Wall -Wextra -O3 -ggdb -I$(BASE_DIR)/chnet $(CXXFLAGS)

CHNET_DIR := $(BASE_DIR)/chnet
CHROMIUM_SRC_DIR := $(BASE_DIR)/chromium/src
CHROMIUM_BUILD_DIR := $(CHROMIUM_SRC_DIR)/out/Net
CHROMIUM_PATCHES_DIR := $(BASE_DIR)/chromium/patches
DEPOT_TOOLS_DIR := $(BASE_DIR)/chromium/depot_tools
NINJA_DIR := $(BASE_DIR)/chromium/src/third_party/ninja
LIBCHNET_SO := $(CHROMIUM_BUILD_DIR)/libchnet.so
LIBCHNET_MIN_SO := $(CHROMIUM_BUILD_DIR)/libchnet.min.so
TO_PACK_DIR := $(BASE_DIR)/build/Release
BORING_SSL_SO := $(CHROMIUM_BUILD_DIR)/libboringssl.so

CHROMIUM_PATCHES := \
	$(CHROMIUM_PATCHES_DIR)/aia_chasing.diff \
	$(CHROMIUM_PATCHES_DIR)/connect_hook.diff \
	$(CHROMIUM_PATCHES_DIR)/custom_dns_map.diff \
	$(CHROMIUM_PATCHES_DIR)/kill_referer_removal.diff \
	$(CHROMIUM_PATCHES_DIR)/no_decompress.diff \
	$(CHROMIUM_PATCHES_DIR)/ubuntu.diff

CHROMIUM_PATCH_SWO = $(CHROMIUM_PATCHES_DIR)/cr.swo

LIBCHNET_SO_DEPS := \
	$(CHNET_DIR)/arch/x86/boringssl_fixup.h \
	$(CHNET_DIR)/chnet.cc \
	$(CHNET_DIR)/chnet.h \
	$(CHNET_DIR)/chnet_node.cc \
	$(CHNET_DIR)/chnet_node.h \
	$(CHNET_DIR)/common.h \
	$(CHNET_DIR)/fixup.cc \
	$(CHNET_DIR)/io_ring.cc \
	$(CHNET_DIR)/io_ring.h \
	$(CHNET_DIR)/net/chnet_config.h \
	$(CHNET_DIR)/net/chnet_payload.cc \
	$(CHNET_DIR)/net/chnet_payload.h \
	$(CHNET_DIR)/net/chnet_thread.cc \
	$(CHNET_DIR)/net/chnet_thread.h \
	$(CHNET_DIR)/net/chnet_thread_pool.cc \
	$(CHNET_DIR)/net/chnet_thread_pool.h \
	$(CHNET_DIR)/net/chnetx.cc \
	$(CHNET_DIR)/net/chnetx.h \
	$(CHNET_DIR)/net/dns_map.h \
	$(CHNET_DIR)/net/dns_map.cc \
	$(CHNET_DIR)/net/dns_req.h \
	$(CHNET_DIR)/net/dns_req.cc \
	$(CHNET_DIR)/net/ureq_ctx_pool.cc \
	$(CHNET_DIR)/net/ureq_ctx_pool.h \
	$(CHNET_DIR)/WorkQueue.cc \
	$(CHNET_DIR)/WorkQueue.h

export BASE_DIR
export CC
export CXX
export CXXFLAGS
export LIBCHNET_SO

ifeq ($(CHNET_USE_ASAN),1)
	ifndef LIBASAN_SO
		LIBASAN_SO=$(shell $(CXX) --print-file-name libclang_rt.asan-x86_64.so)
	endif

	LD_ASAN_FLAGS= \
		-fsanitize=address \
		-shared-libasan \
		-Wl,-rpath,$(dir $(LIBASAN_SO))

	override __LD_PRELOAD := $(LIBASAN_SO):$(LD_PRELOAD)
	export LD_ASAN_FLAGS
	export __LD_PRELOAD
	export LIBASAN_SO
endif

all: $(LIBCHNET_SO) $(LIBCHNET_MIN_SO)

$(CHROMIUM_PATCH_SWO): $(CHROMIUM_PATCHES)
	cd $(CHROMIUM_SRC_DIR) && \
	git reset --hard && \
	git clean -f && \
	git apply "${CHROMIUM_PATCHES_DIR}"/ubuntu.diff && \
	git apply "${CHROMIUM_PATCHES_DIR}"/aia_chasing.diff && \
	git apply "${CHROMIUM_PATCHES_DIR}"/connect_hook.diff && \
	git apply "${CHROMIUM_PATCHES_DIR}"/no_decompress.diff && \
	git apply "${CHROMIUM_PATCHES_DIR}"/kill_referer_removal.diff && \
	git apply "${CHROMIUM_PATCHES_DIR}"/custom_dns_map.diff && \
	git apply "${CHROMIUM_PATCHES_DIR}"/socks5_chnetd.diff && \
	rm -f chnet && \
	ln -svf "${CHNET_DIR}" && \
	rm -f chnet_tests && \
	ln -svf "${CHNET_DIR}/../tests/cpp" chnet_tests && \
	date > $(CHROMIUM_PATCH_SWO);

$(LIBCHNET_SO): $(LIBCHNET_SO_DEPS) $(CHROMIUM_PATCH_SWO)
	+env PATH="${NINJA_DIR}:${DEPOT_TOOLS_DIR}:${PATH}" \
		autoninja chnet test_dns_resolver -j$$(nproc) \
		-C "${CHROMIUM_BUILD_DIR}";

test: $(LIBCHNET_SO)
	+$(MAKE) -C "${BASE_DIR}/tests/cpp" runtests;

clean:
	+@$(MAKE) -C "${BASE_DIR}/tests/cpp" clean;
	@rm -rf "${LIBCHNET_SO}" chnet-0.0.0.tgz build;

libchnet_path:
	@echo "${LIBCHNET_SO}";

boring_ssl_path:
	@echo "${BORING_SSL_SO}";

init:
	env \
		CHNET_DIR="$(CHNET_DIR)" \
		CHROMIUM_SRC_DIR="$(CHROMIUM_SRC_DIR)" \
		CHROMIUM_BUILD_DIR="$(CHROMIUM_BUILD_DIR)" \
		CHROMIUM_PATCHES_DIR="$(CHROMIUM_PATCHES_DIR)" \
		DEPOT_TOOLS_DIR="$(DEPOT_TOOLS_DIR)" \
		sh init.sh;

test_npm:
	+env LD_PRELOAD="${LIBCHNET_SO}" \
		bash -c "for i in {0..0}; do npm run \"test\$${i}\"; done;"

$(LIBCHNET_MIN_SO): $(LIBCHNET_SO)
	@cp -vf $< $@;
	@strip --strip-unneeded $@;

$(TO_PACK_DIR): $(LIBCHNET_MIN_SO)
	@mkdir -pv $(TO_PACK_DIR)

install: $(TO_PACK_DIR) $(LIBCHNET_MIN_SO)
	npm install --verbose;

pack: install
	npm pack;

.PHONY: all test clean libchnet_path init test_npm pack install
