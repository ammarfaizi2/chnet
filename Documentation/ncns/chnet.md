
# Introduction

chnet is a NodeJS addon that implements HTTP request functions built on
top of the Chromium networking stack. This addon supports parallelism
using a ring design (similar to io\_uring). We need a ring object and a
chnet object to perform an HTTP request. A ring object can handle many
chnet objects. Each blocking operation is done via an SQ (Submission
Queue), and when the operation finishes, The ring will return the result
in a CQ (Completion Queue).

# Addon Objects

## 1. Ring

A ring is used to handle blocking operations. It can handle multiple
operations in parallel and asynchronously. To create a ring object, use
CreateRing() method from the chnet object:

```js
const max_op = 16;
const chnet = require("bindings")("chnet");
let ring = chnet.CreateRing(max_op);
```

The `CreateRing()` method has an argument `max_op`. It is an integer to
determine how big the ring should be. It will become the number of
parallel operations that the ring can handle. The `max_op` is always
rounded up to the power of 2 integers internally for efficient circular
queue calculation purposes.

Available methods in the ring object:

   1) `GetSQE`
   2) `Submit`
   3) `WaitCQE`
   4) `ForEachCQE`
   5) `CQAdvance`
   6) `GetCQEHead`
   7) `Close`


### ring.GetSQE()

This method is used to get an SQE from the ring. It returns an SQE
object when the SQE is available. When the SQE is not available, it
returns null. After the caller gets an SQE object, it should call a prep
function from the returned SQE object before calling the `ring.Submit()`
method.

Note: When the SQ ring is full, this function returns null. Calling
`ring.Submit()` will flush the SQ ring and make the SQE available again.

This function doesn't sleep.


### ring.Submit()

This method is used to submit the prepared SQEs. It returns the number
of processed SQEs.

This function doesn't sleep.


### ring.WaitCQE(min\_wait)

This method is used to wait for CQE. After calling the `ring.Submit()`
method; we can wait for the submitted operations to complete by calling
this function. This function has an argument, `min\_wait`. The
`min\_wait` argument is a positive integer to specify the minimum CQEs
generated before the call returns.

This function will be sleeping until `min\_wait` operations are
completed.

## 2. SQE

## 3. CQE

## 4. Net

# Minimal Working Examples

## 1. Simple single GET request
```js

const chnet = require("bindings")("chnet");

function get_google()
{
	const bytes_to_read = 1024;
	let ring;
	let ch;
	let sqe;
	let cqe;

	ring = chnet.CreateRing(1);

	ch = chnet.CreateNet();
	ch.SetURL("https://www.google.com");
	ch.SetMethod("GET");

	sqe = ring.GetSQE();
	sqe.PrepRead(ch, bytes_to_read);
	sqe.SetUserData(ch);

	ring.Submit();
	ring.WaitCQE(1);
	cqe = ring.GetCQEHead();

	console.log(`Bytes read: ${cqe.res} bytes`);
	console.log(`Content: ${ch.read_buf()}`);
	ring.CQAdvance(1);

	ch.Close();
	ring.Close();
}

get_google();
```

## 2. Simple parallel GET requests
```js

const chnet = require("bindings")("chnet");

function get_google_parallel()
{
	const NR_REQ = 10; // number of requests
	const bytes_to_read = 1024;
	let ring;
	let ch_arr;
	let sqe;
	let cqe;
	let i;

	ring = chnet.CreateRing(NR_REQ);

	ch_arr = [];

	// Initialize chnet objects.
	for (i = 0; i < NR_REQ; i++) {
		ch_arr[i] = chnet.CreateNet();
		ch_arr[i].SetURL("https://www.google.com");
		ch_arr[i].SetMethod("GET");
	}

	// Prepare read operation on each chnet object.
	for (i = 0; i < NR_REQ; i++) {
		sqe = ring.GetSQE();
		sqe.PrepRead(ch_arr[i], bytes_to_read);
		sqe.SetUserData(ch_arr[i]);
	}

	// Perform all prepared SQE operations in parallel.
	ring.Submit();

	// Wait for NR_REQ CQs generated.
	// Note: Only this function may sleep.
	ring.WaitCQE(NR_REQ);

	// Get all results.
	i = 0;
	ring.ForEachCQE(function (cqe) {
		let ch = cqe.user_data;

		console.log(`Bytes read: ${cqe.res} bytes`);
		console.log(`Content: ${ch.read_buf()}`);
		console.log("===============================");
		i++;
	});
	ring.CQAdvance(i);

	for (i = 0; i < NR_REQ; i++)
		ch_arr[i].Close();

	ring.Close();
}

get_google_parallel();
```

## 3. Simple non-chunked POST request
```js

const chnet = require("bindings")("chnet");

function simple_post()
{
	const bytes_to_read = 1024;
	let ring;
	let ch;
	let sqe;
	let cqe;

	ring = chnet.CreateRing(1);

	ch = chnet.CreateNet();
	ch.SetURL("http://127.0.0.1:8000/index.php?action=print_post&key=data");
	ch.SetMethod("POST");
	ch.SetPayload("data=test");
	ch.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded", true);

	sqe = ring.GetSQE();
	sqe.PrepRead(ch, bytes_to_read);
	sqe.SetUserData(ch);

	ring.Submit();
	ring.WaitCQE(1);
	cqe = ring.GetCQEHead();

	console.log(`Bytes read: ${cqe.res} bytes`);
	console.log(`Content: ${ch.read_buf()}`);
	ring.CQAdvance(1);

	ch.Close();
	ring.Close();
}

simple_post();

```
