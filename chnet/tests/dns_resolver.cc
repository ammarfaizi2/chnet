
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include "../DnsResolver.h"
#include "../io_ring.h"

int main(void)
{
        DnsResolver *dns_resolver = new DnsResolver("10.5.5.1");
        IORing *ring = new IORing(1024);
        DnsQueryResult result;
        IORingSQE *sqe;

        sqe = ring->GetSQE();
        sqe->PrepDedicatedDnsQuery(dns_resolver, "www.google.com", &result);
        sqe->SetUserDataPtr(&result);
        ring->Submit();
        ring->WaitCQE(1);

        for (size_t i = 0; i < result.size_; i++)
                printf("%s\n", result.addresses_[i]);

        delete ring;
        delete dns_resolver;
}
