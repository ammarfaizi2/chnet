
#ifndef CHNET__PBCHNET_H
#define CHNET__PBCHNET_H

#ifdef __FOR_CHROMIUM_INTERNAL
#include "net/chnetx.h"
#include "net/chnet_thread.h"
#include "net/chnet_thread_pool.h"
#endif

#include <map>
#include "common.h"
#include "net/chnet_config.h"

struct NodeRedirectInfo {

        NodeRedirectInfo(void);
        ~NodeRedirectInfo(void);
        void SetRedirectInfo(const void *redirect_info);

        int     status_code_ = -1;
        bool    insecure_scheme_was_upgraded_ = false;
        bool    is_signed_exchange_fallback_redirect_ = false;
        char    *new_method_ = nullptr;
        char    *new_url_ = nullptr;
        char    *new_referrer_ = nullptr;
private:
        char    *heap__ = nullptr;
};

class CHNET_EXPORT CHNet {
public:
        CHNet(struct tbind_config *cfg = nullptr);

        ~CHNet(void);

        void SetURL(const char *url);

        void SetMethod(const char *method);

        void Start(void);

        int Read(int len);

        void Cancel(void);

        void StartChunkedPayload(void);

        int Write(const void *buf, size_t len);

        void StopChunkedPayload(void);

        const char *GetReadBuffer(void);

        void SetPayload(const void *p, size_t len);

        void SetRequestHeader(const char *key, const char *val,
                              bool overwrite = true);

        int GetReadRet(void);

        const char *GetErrorStr(void);

        bool GetRedirectInfo(struct NodeRedirectInfo *nri);

        bool __GetResponseHeader(size_t *iter, const char **val,
                                 const char **key);

        void LockResponseHeader(void);

        void UnlockResponseHeader(void);

        struct chnet_buf_ptr *GetBufferPtr(void);

        static void PutBufferPtr(struct chnet_buf_ptr *ptr);

        std::map<std::string, std::string> GetResponseHeader(void);

        int GetResponseCode(void);

        const char *GetAlpnNegotiatedProtocol(void);

        size_t GetRemoteEndpoint(char *buf, size_t buf_siz);

        void SetFollowRedirect(bool r);

        NodeRedirectInfo *GetNodeRedirectInfo(void);

#ifdef __FOR_CHROMIUM_INTERNAL
        net::CHNetx *ch(void);
#else
        void *ch(void);
#endif

private:
#ifdef __FOR_CHROMIUM_INTERNAL
        net::CHNetx	*ch_;
#else
        void		*ch_;
#endif
        struct NodeRedirectInfo	*redirect_info_ = nullptr;
};

#ifdef __FOR_CHROMIUM_INTERNAL
inline net::CHNetx* CHNet::ch(void)
{
        return ch_;
}
#else
inline void* CHNet::ch(void)
{
        return ch_;
}
#endif

inline std::map<std::string, std::string> CHNet::GetResponseHeader(void)
{
        std::map<std::string, std::string> ret;
        const char* key;
        const char* val;
        size_t iter = 0;

        LockResponseHeader();
        while (__GetResponseHeader(&iter, &key, &val))
                ret[key] = val;
        UnlockResponseHeader();
        return ret;
}

extern "C" {

CHNET_EXPORT void chnet_global_init(void);
CHNET_EXPORT void chnet_global_stop(void);

} /* extern "C" */

#endif /* #ifndef CHNET__PBCHNET_H */
