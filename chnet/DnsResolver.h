
#ifndef CHNET__NET__DNSRESOLVER_H
#define CHNET__NET__DNSRESOLVER_H

#include "common.h"

struct ares_thread;

class CHNET_EXPORT DnsResolver {
public:
        DnsResolver(const char *server = nullptr,
                    const char *bind_ip4 = nullptr,
                    const char *bind_iface4 = nullptr);
        ~DnsResolver(void);
        struct ares_thread *ares_thread_;
};

#endif /* #ifndef CHNET__NET__DNSRESOLVER_H */
