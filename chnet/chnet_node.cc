
#include "chnet_node.h"

#include <cstdlib>
#include <cassert>
#include <cstdint>
#include <atomic>
#include <thread>
#include <queue>

#include "net/dns_map.h"
#include "DnsResolver.h"

using Napi::AsyncProgressWorker;

#define VALIDATE_ARG 1

__cold
__no_inline static void throw_js_exception(Napi::Env env, const char *err_msg)
{
        Napi::TypeError::New(env, err_msg).ThrowAsJavaScriptException();
}

template<typename T>
static inline void obj_add_func(Napi::Env &env, Napi::Object &obj,
                                const T &func, const char *name)
{
        obj[name] = Napi::Function::New(env, func, name);
}

template<typename T>
static inline int64_t ptr_to_s64(T *ptr)
{
        return static_cast<int64_t>(reinterpret_cast<intptr_t>(ptr));
}

template<typename T>
static inline T *s64_to_ptr(int64_t n)
{
        return reinterpret_cast<T *>(static_cast<intptr_t>(n));
}

template<typename T>
static inline T *GetPtrFromS64(Napi::Value v)
{
        int64_t tmp;

        if (unlikely(VALIDATE_ARG && !v.IsNumber()))
                return nullptr;

        tmp = v.As<Napi::Number>().Int64Value();
        return s64_to_ptr<T>(tmp);
}

static int GetU32FromNapiValue(Napi::Value v, uint32_t *val)
{
        int64_t tmp;

        if (unlikely(VALIDATE_ARG && !v.IsNumber()))
                return -EINVAL;

        tmp = v.ToNumber().Int64Value();
        if (unlikely(tmp < 0 || tmp > 0xFFFFFFFF))
                return -EINVAL;

        *val = (uint32_t)tmp;
        return 0;
}

class NodeCHNet {
public:
        NodeCHNet(struct tbind_config *bind_cfg);
        ~NodeCHNet(void);
        void Close(void);

        CHNet		*ch_;
        std::string	payload_;
        void            *payload_buffer_ = nullptr;
        size_t          payload_buffer_size_ = 0;
};

inline NodeCHNet::NodeCHNet(struct tbind_config *bind_cfg):
        ch_(new CHNet(bind_cfg))
{
}

inline void NodeCHNet::Close(void)
{
        if (ch_) {
                delete ch_;
                ch_ = nullptr;
        }

        if (payload_buffer_) {
                free(payload_buffer_);
                payload_buffer_ = nullptr;
        }
}

inline NodeCHNet::~NodeCHNet(void)
{
        Close();
}

struct NodeDNSQuery {
        std::string     hostname_;
        DnsQueryResult  result_;
};

class NodeCHNetRing {
public:
        NodeCHNetRing(uint32_t entry);
        ~NodeCHNetRing(void);
        void Close(void);

        IORing			*ring_;
        std::atomic<uint32_t>	nr_in_flights_;
        std::mutex		mutex_;
        Napi::FunctionReference	wait_cqe_callback_;
        bool			has_wait_cqe_callback_ = false;
};

inline void NodeCHNetRing::Close(void)
{
        if (ring_) {
                delete ring_;
                ring_ = nullptr;
        }
}

inline NodeCHNetRing::NodeCHNetRing(uint32_t entry):
        ring_(new IORing(entry))
{
        nr_in_flights_.store(0u, std::memory_order_relaxed);
}

inline NodeCHNetRing::~NodeCHNetRing(void)
{
        Close();
}

class NodeDedicateDnsRes {
public:
        NodeDedicateDnsRes(const char *server = nullptr,
                           const char *bind_ip4 = nullptr,
                           const char *bind_iface4 = nullptr);
        ~NodeDedicateDnsRes(void);
        void Close(void);

        DnsResolver *dns_res_;
};

inline NodeDedicateDnsRes::NodeDedicateDnsRes(const char *server,
                                              const char *bind_ip4,
                                              const char *bind_iface4):
        dns_res_(new DnsResolver(server, bind_ip4, bind_iface4))
{
}

inline void NodeDedicateDnsRes::Close(void)
{
        if (dns_res_) {
                delete dns_res_;
                dns_res_ = nullptr;
        }
}

inline NodeDedicateDnsRes::~NodeDedicateDnsRes(void)
{
        Close();
}
class WaitCQEWorker: public AsyncProgressWorker<uint8_t> {
public:
        inline WaitCQEWorker(NodeCHNetRing *ring):
                AsyncProgressWorker(ring->wait_cqe_callback_.Value()),
                ring_(ring)
        {
        }

        inline ~WaitCQEWorker()
        {
        }

        void Execute(const ExecutionProgress &progress) override;

        void OnProgress(const uint8_t *unused, size_t count) override;

        void OnOK(void) override;

private:
        NodeCHNetRing		*ring_;
};

void WaitCQEWorker::Execute(const ExecutionProgress &progress)
{
        IORing *r = ring_->ring_;
        uint8_t unused = 0;
        uint32_t temp;
        uint32_t ret;

        while (true) {

                ret = r->WaitCQE(1);
                if (ret)
                        progress.Send(&unused, 1);

                ring_->mutex_.lock();
                temp = ring_->nr_in_flights_.load(std::memory_order_acquire);
                ring_->mutex_.unlock();
                if (!temp)
                        break;
        }
}

static uint32_t ReapCQEs(Napi::Env env, NodeCHNetRing *ring)
{
        IORingCQE *cqe = nullptr;
        uint32_t head = 0;
        uint32_t tail = 0;
        uint32_t i = 0;

        ioring_for_each_cqe(ring->ring_, head, tail, cqe) {
                Napi::HandleScope scope(env);
                Napi::Object obj = Napi::Object::New(env);

                obj["res"] = Napi::Number::New(env, (int64_t)cqe->res_);
                if (cqe->user_data_ != static_cast<uint64_t>(-1UL)) {
                        int64_t udata = static_cast<int64_t>(cqe->user_data_);
                        obj["user_data"] = Napi::Number::New(env, udata);
                }

                ring->wait_cqe_callback_.Call({obj});
                i++;
        }

        if (i) {
                ring->mutex_.lock();
                ring->ring_->CQAdvance(i);
                ring->nr_in_flights_.fetch_sub(i);
                ring->mutex_.unlock();
        }
        return i;
}

void WaitCQEWorker::OnProgress(const uint8_t *unused, size_t count)
{
        (void)unused;
        (void)count;
        ReapCQEs(Env(), ring_);
}

void WaitCQEWorker::OnOK(void)
{
        ReapCQEs(Env(), ring_);
}

static bool GetPtrFromNapiObject(const Napi::Value &obj, Napi::Object *tobj)
{
        Napi::Object tmp;
        Napi::Value val;

        if (unlikely(VALIDATE_ARG && !obj.IsObject()))
                return false;

        tmp = obj.As<Napi::Object>();
        if (unlikely(VALIDATE_ARG && !tmp.Has("p")))
                return false;

        if (unlikely(VALIDATE_ARG && !tmp.Get("p").IsNumber()))
                return false;

        *tobj = tmp;
        return true;
}

template<typename T>
static T *__GetPtrType(Napi::Env env, const Napi::Value &obj, const char *err)
{
        Napi::Object tobj;
        Napi::Value val;
        T *ret;

        if (unlikely(!GetPtrFromNapiObject(obj, &tobj)))
                return nullptr;

        val = tobj.Get("p");
        ret = s64_to_ptr<T>(val.As<Napi::Number>().Int64Value());
        if (likely(ret))
                return ret;

        throw_js_exception(env, err);
        return ret;
}

__hot
__no_inline static bool ClearPtr(Napi::Env env, const Napi::Value &obj)
{
        Napi::Object tobj;
        Napi::Value val;

        if (unlikely(!GetPtrFromNapiObject(obj, &tobj)))
                return false;

        val = tobj.Get("p");
        tobj.Set("p", Napi::Number::New(env, 0u));
        return true;
}

__hot
__no_inline static NodeCHNetRing *GetRingPtr(Napi::Env env,
                                             const Napi::Value &obj)
{
        constexpr static const char err[] = "Attempting to use a closed ring object!";
        return __GetPtrType<NodeCHNetRing>(env, obj, err);
}

__hot
__no_inline static NodeCHNet *GetNetPtr(Napi::Env env, const Napi::Value &obj)
{
        constexpr static const char err[] = "Attempting to use a closed net object!";
        return __GetPtrType<NodeCHNet>(env, obj, err);
}

__hot
__no_inline static NodeDNSQuery *GetDnsPtr(Napi::Env env, const Napi::Value &obj)
{
        constexpr static const char err[] = "Attempting to use a closed DNS object!";
        return __GetPtrType<NodeDNSQuery>(env, obj, err);
}

__hot
__no_inline static NodeDedicateDnsRes *GetDedicatedDnsPtr(Napi::Env env,
                                                     const Napi::Value &obj)
{
        constexpr static const char err[] = "Attempting to use a closed DNS resolver object!";
        return __GetPtrType<NodeDedicateDnsRes>(env, obj, err);
}

__cold
static void RingFinalizer(Napi::Env env, NodeCHNetRing *ring)
{
        delete ring;
        (void)env;
}

__cold
static Napi::Value CHN_RingCreate(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        int64_t entry = 1024;
        NodeCHNetRing *ring;
        Napi::Object obj;

        if (info.Length() > 0) {
                entry = info[0].ToNumber().Int64Value();
                if (unlikely(entry < 512))
                        entry = 1024;
                else if (unlikely(entry > 1073741824))
                        entry = 1073741824;
        }

        ring = new NodeCHNetRing(static_cast<uint32_t>(entry));
        obj = Napi::Object::New(env);
        obj["p"] = Napi::Number::New(env, ptr_to_s64<NodeCHNetRing>(ring));
        obj.AddFinalizer(RingFinalizer, ring);
        return obj;
}

__cold
static void CHN_RingClose(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        NodeCHNetRing *ring;

        if (unlikely(VALIDATE_ARG && info.Length() != 1))
                goto err_einval;

        ring = GetRingPtr(env, info[0]);
        if (unlikely(VALIDATE_ARG && !ring))
                goto err_einval;

        ClearPtr(env, info[0]);
        ring->Close();
        return;

err_einval:
        throw_js_exception(env, "CHN_RingClose(): Invalid argument");
}

__hot
static Napi::Value CHN_RingGetSQE(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        NodeCHNetRing *ring;
        IORingSQE *sqe;

        if (unlikely(VALIDATE_ARG && info.Length() != 1))
                goto err_einval;

        ring = GetRingPtr(env, info[0]);
        if (unlikely(VALIDATE_ARG && !ring))
                goto err_einval;

        sqe = ring->ring_->GetSQE();
        if (unlikely(!sqe))
                /*
                 * Aiee, we are running out of SQE...
                 * Don't throw any exception here!
                 *
                 * At the point, the caller must call ring.Submit() to
                 * make the SQE available again.
                 */
                return env.Null();

        sqe->SetUserData(static_cast<uint64_t>(-1UL));
        return Napi::Number::New(env, ptr_to_s64<IORingSQE>(sqe));

err_einval:
        throw_js_exception(env, "CHN_RingGetSQE(): Invalid argument");
        return env.Null();
}

__hot
static void CHN_RingSetWaitCQECallback(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        Napi::Function callback;
        NodeCHNetRing *ring;
        int nr_arg;

        nr_arg = info.Length();
        if (unlikely(VALIDATE_ARG && nr_arg != 2))
                goto err_einval;

        ring = GetRingPtr(env, info[0]);
        if (unlikely(VALIDATE_ARG && !ring))
                goto err_einval;

        if (unlikely(!info[1].IsFunction()))
                goto err_einval;

        callback = info[1].As<Napi::Function>();
        ring->wait_cqe_callback_.Reset(callback, 1);
        ring->has_wait_cqe_callback_ = true;
        return;

err_einval:
        throw_js_exception(env, "CHN_RingSubmitCallback(): Invalid argument");
}

__hot
static Napi::Value CHN_RingSubmitCallback(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        NodeCHNetRing *ring;
        uint32_t submitted;
        uint32_t tmp;
        int nr_arg;

        nr_arg = info.Length();
        if (unlikely(VALIDATE_ARG && nr_arg != 1))
                goto err_einval;

        ring = GetRingPtr(env, info[0]);
        if (unlikely(VALIDATE_ARG && !ring))
                goto err_einval;

        if (unlikely(!ring->has_wait_cqe_callback_))
                goto err_einval;

        submitted = ring->ring_->Submit(-1u);

        ring->mutex_.lock();
        tmp = ring->nr_in_flights_.load(std::memory_order_acquire);
        ring->nr_in_flights_.store(tmp + submitted, std::memory_order_release);
        ring->mutex_.unlock();

        if (tmp == 0) {
                WaitCQEWorker *w;

                w = new WaitCQEWorker(ring);
                w->Queue();
        }
        return Napi::Number::New(env, submitted);

err_einval:
        throw_js_exception(env, "CHN_RingSubmitCallback(): Invalid argument");
        return env.Null();
}

__hot
static Napi::Value CHN_RingSubmit(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        uint32_t to_submit = -1U;
        NodeCHNetRing *ring;
        int nr_arg;

        nr_arg = info.Length();
        if (unlikely(VALIDATE_ARG && (nr_arg < 1 || nr_arg > 2)))
                goto err_einval;

        ring = GetRingPtr(env, info[0]);
        if (unlikely(VALIDATE_ARG && !ring))
                goto err_einval;

        if (nr_arg == 2) {
                if (unlikely(GetU32FromNapiValue(info[1], &to_submit)))
                        goto err_einval;
        }

        return Napi::Number::New(env, ring->ring_->Submit(to_submit));

err_einval:
        throw_js_exception(env, "CHN_RingSubmit(): Invalid argument");
        return env.Null();
}

__hot
static Napi::Value CHN_RingWaitCQE(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        NodeCHNetRing *ring;
        uint32_t to_wait = 1;
        int nr_arg;

        nr_arg = info.Length();
        if (unlikely(VALIDATE_ARG && (nr_arg < 1 || nr_arg > 2)))
                goto err_einval;

        ring = GetRingPtr(env, info[0]);
        if (unlikely(VALIDATE_ARG && !ring))
                goto err_einval;

        if (nr_arg == 2) {
                if (unlikely(GetU32FromNapiValue(info[1], &to_wait)))
                        goto err_einval;
        }

        return Napi::Number::New(env, ring->ring_->WaitCQE(to_wait));

err_einval:
        throw_js_exception(env, "CHN_RingWaitCQE(): Invalid argument");
        return env.Null();
}

__hot
static uint32_t ioring_for_each_cqe_invoke(Napi::Env env, IORing *ring,
                                           Napi::Function &func)
{
        IORingCQE *cqe = nullptr;
        uint32_t head = 0;
        uint32_t tail = 0;
        uint32_t i = 0;

        ioring_for_each_cqe(ring, head, tail, cqe) {
                Napi::HandleScope scope(env);
                Napi::Object obj = Napi::Object::New(env);

                obj["res"] = Napi::Number::New(env, cqe->res_);
                if (cqe->user_data_ != static_cast<uint64_t>(-1UL)) {
                        int64_t udata = static_cast<int64_t>(cqe->user_data_);
                        obj["user_data"] = Napi::Number::New(env, udata);
                }

                func.Call({obj});
                i++;
        }

        return i;
}

static Napi::Value __CHN_RingForEachCQE(const Napi::CallbackInfo &info,
                                        bool advance);

__hot
static Napi::Value CHN_RingForEachCQE(const Napi::CallbackInfo &info)
{
        return __CHN_RingForEachCQE(info, false);
}

__hot
static Napi::Value CHN_RingForEachCQEAdvance(const Napi::CallbackInfo &info)
{
        return __CHN_RingForEachCQE(info, true);
}

__hot
__no_inline static Napi::Value __CHN_RingForEachCQE(
        const Napi::CallbackInfo &info,
        bool advance)
{
        Napi::Env env = info.Env();
        Napi::Function func;
        NodeCHNetRing *ring;
        uint32_t ret;

        if (unlikely(VALIDATE_ARG && info.Length() != 2))
                goto err_einval;

        ring = GetRingPtr(env, info[0]);
        if (unlikely(VALIDATE_ARG && !ring))
                goto err_einval;

        if (unlikely(VALIDATE_ARG && !info[1].IsFunction()))
                goto err_einval;

        func = info[1].As<Napi::Function>();
        ret  = ioring_for_each_cqe_invoke(env, ring->ring_, func);
        if (advance)
                ring->ring_->CQAdvance(ret);
        return Napi::Number::New(env, ret);

err_einval:
        throw_js_exception(env, "CHN_RingForEachCQE(): Invalid argument");
        return env.Null();
}

__hot
static void CHN_RingCQAdvance(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        NodeCHNetRing *ring;
        uint32_t n;

        if (unlikely(VALIDATE_ARG && info.Length() != 2))
                goto err_einval;

        ring = GetRingPtr(env, info[0]);
        if (unlikely(VALIDATE_ARG && !ring))
                goto err_einval;

        if (unlikely(GetU32FromNapiValue(info[1], &n)))
                goto err_einval;

        ring->ring_->CQAdvance(n);
        return;

err_einval:
        throw_js_exception(env, "CHN_RingCQAdvance(): Invalid argument");
}

__hot
static Napi::Value CHN_RingGetCQEHead(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        NodeCHNetRing *ring;
        Napi::Object obj;
        IORingCQE *cqe;

        if (unlikely(VALIDATE_ARG && info.Length() != 1))
                goto err_einval;

        ring = GetRingPtr(env, info[0]);
        if (unlikely(VALIDATE_ARG && !ring))
                goto err_einval;

        cqe = ring->ring_->GetCQEHead();
        obj = Napi::Object::New(env);

        obj["res"] = Napi::Number::New(env, cqe->res_);
        if (cqe->user_data_ != static_cast<uint64_t>(-1UL)) {
                int64_t udata = static_cast<int64_t>(cqe->user_data_);
                obj["user_data"] = Napi::Number::New(env, udata);
        }

        return obj;

err_einval:
        throw_js_exception(env, "CHN_RingGetCQEHead(): Invalid argument");
        return env.Null();
}

__hot
static void CHN_RingSQEPrepNop(const Napi::CallbackInfo &info)
{
        IORingSQE *sqe;

        if (unlikely(VALIDATE_ARG && info.Length() != 1))
                goto err_einval;

        sqe = GetPtrFromS64<IORingSQE>(info[0]);
        if (unlikely(VALIDATE_ARG && !sqe))
                goto err_einval;

        sqe->PrepNop();
        return;

err_einval:
        throw_js_exception(info.Env(), "CHN_RingSQEPrepNop(): Invalid argument");
}

__hot
static void CHN_RingSQEPrepRead(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        int32_t read_size;
        IORingSQE *sqe;
        NodeCHNet *ch;

        if (unlikely(VALIDATE_ARG && info.Length() != 3))
                goto err_einval;

        sqe = GetPtrFromS64<IORingSQE>(info[0]);
        if (unlikely(VALIDATE_ARG && !sqe))
                goto err_einval;

        ch = GetNetPtr(env, info[1]);
        if (unlikely(VALIDATE_ARG && !ch))
                goto err_einval;

        if (unlikely(VALIDATE_ARG && !info[2].IsNumber()))
                goto err_einval;

        read_size = info[2].As<Napi::Number>().Int32Value();
        sqe->PrepRead(ch->ch_, read_size);
        return;

err_einval:
        throw_js_exception(info.Env(), "CHN_RingSQEPrepRead(): Invalid argument");
}

__hot
static void CHN_RingSQEPrepStart(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        IORingSQE *sqe;
        NodeCHNet *ch;

        if (unlikely(VALIDATE_ARG && info.Length() != 2))
                goto err_einval;

        sqe = GetPtrFromS64<IORingSQE>(info[0]);
        if (unlikely(VALIDATE_ARG && !sqe))
                goto err_einval;

        ch = GetNetPtr(env, info[1]);
        if (unlikely(VALIDATE_ARG && !ch))
                goto err_einval;

        sqe->PrepStart(ch->ch_);
        return;

err_einval:
        throw_js_exception(info.Env(), "CHN_RingSQEPrepStart(): Invalid argument");
}

__hot
static void CHN_RingSQEPrepDnsQuery(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        NodeDNSQuery *dq;
        IORingSQE *sqe;

        if (unlikely(VALIDATE_ARG && info.Length() != 2))
                goto err_einval;

        sqe = GetPtrFromS64<IORingSQE>(info[0]);
        if (unlikely(VALIDATE_ARG && !sqe))
                goto err_einval;

        dq = GetDnsPtr(env, info[1]);
        if (unlikely(VALIDATE_ARG && !dq))
                goto err_einval;

        sqe->PrepDnsQuery(dq->hostname_.c_str(), &dq->result_);
        return;

err_einval:
        throw_js_exception(info.Env(), "CHN_RingSQEPrepDnsQuery(): Invalid argument");
}

__hot
static void CHN_RingSQEPrepDedicatedDnsQuery(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        NodeDedicateDnsRes *res;
        NodeDNSQuery *dq;
        IORingSQE *sqe;

        if (unlikely(VALIDATE_ARG && info.Length() != 3))
                goto err_einval;

        sqe = GetPtrFromS64<IORingSQE>(info[0]);
        if (unlikely(VALIDATE_ARG && !sqe)) {
                goto err_einval;
        }

        res = GetDedicatedDnsPtr(env, info[1]);
        if (unlikely(VALIDATE_ARG && !res)) {
                goto err_einval;
        }

        dq = GetDnsPtr(env, info[2]);
        if (unlikely(VALIDATE_ARG && !dq)) {
                goto err_einval;
        }

        sqe->PrepDedicatedDnsQuery(res->dns_res_, dq->hostname_.c_str(),
                                   &dq->result_);
        return;

err_einval:
        throw_js_exception(info.Env(), "CHN_RingSQEPrepDnsQuery(): Invalid argument");
}


__hot
static void CHN_RingSQESetUserData(const Napi::CallbackInfo &info)
{
        IORingSQE *sqe;

        if (unlikely(VALIDATE_ARG && info.Length() != 2))
                goto err_einval;

        sqe = GetPtrFromS64<IORingSQE>(info[0]);
        if (unlikely(VALIDATE_ARG && !sqe))
                goto err_einval;

        if (unlikely(VALIDATE_ARG && !info[1].IsNumber()))
                goto err_einval;

        sqe->SetUserData(info[1].ToNumber().Int64Value());
        return;

err_einval:
        throw_js_exception(info.Env(), "CHN_RingSQESetUserData(): Invalid argument");
}

__cold
static void NetFinalizer(Napi::Env env, NodeCHNet *ch)
{
        delete ch;
        (void)env;
}

static void free_bind_config(struct tbind_config *bc)
{
        if (bc->bind_ip4_)
                free(bc->bind_ip4_);
        if (bc->bind_iface4_)
                free(bc->bind_iface4_);
        if (bc->bind_ip6_)
                free(bc->bind_ip6_);
        if (bc->bind_iface6_)
                free(bc->bind_iface6_);
        if (bc->proxy_)
                free(bc->proxy_);
}

static int GetStrForBindCfg(Napi::Object *obj, const char *key, const char **out)
{
        Napi::Value v;

        if (!obj->Has(key))
                return 0;

        v = obj->Get(key);
        if (v.IsString()) {
                const std::string &str = v.ToString().Utf8Value();

                *out = strdup(str.c_str());
                if (unlikely(!*out))
                        return -ENOMEM;

                return 0;
        }

        if (v.IsNull()) {
                *out = nullptr;
                return 0;
        }

        return -EINVAL;
}

static bool ParseNetCreateArgument(Napi::Env env, Napi::Value v,
                                   struct tbind_config *bc)
{
        Napi::Object obj;
        Napi::Value tmp;
        int ret;

        if (unlikely(VALIDATE_ARG && !v.IsObject()))
                return false;

        memset(bc, 0, sizeof(*bc));
        obj = v.As<Napi::Object>();
        ret = GetStrForBindCfg(&obj, "bind_ip4", &bc->bind_ip4_);
        if (unlikely(ret))
                goto err;

        ret = GetStrForBindCfg(&obj, "bind_ip6", &bc->bind_ip6_);
        if (unlikely(ret))
                goto err;

        ret = GetStrForBindCfg(&obj, "bind_iface4", &bc->bind_iface4_);
        if (unlikely(ret))
                goto err;

        ret = GetStrForBindCfg(&obj, "bind_iface6", &bc->bind_iface6_);
        if (unlikely(ret))
                goto err;

        ret = GetStrForBindCfg(&obj, "proxy", &bc->proxy_);
        if (unlikely(ret))
                goto err;

        return true;

err:
        free_bind_config(bc);
        if (ret == -EINVAL)
                throw_js_exception(env, "ParseNetCreateArgument(): Invalid argument");
        else if (ret == -ENOMEM)
                throw_js_exception(env, "ParseNetCreateArgument(): Cannot allocate memory");

        return false;
}

__hot
static Napi::Value CHN_NetCreate(const Napi::CallbackInfo &info)
{
        struct tbind_config bind_cfg;
        Napi::Env env = info.Env();
        Napi::Object obj;
        NodeCHNet *ch;
        int len;

        /*
         * Only accepts 0 or 1 number of argument.
         */
        len = info.Length();
        if (unlikely(VALIDATE_ARG && len > 1))
                goto err_einval;

        if (len == 1) {
                bool tmp = ParseNetCreateArgument(env, info[0], &bind_cfg);
                if (unlikely(!tmp))
                        return env.Null();
        } else {
                memset(&bind_cfg, 0, sizeof(bind_cfg));
        }

        ch = new NodeCHNet(&bind_cfg);
        free_bind_config(&bind_cfg);
        obj = Napi::Object::New(env);
        obj["p"] = Napi::Number::New(info.Env(), ptr_to_s64<NodeCHNet>(ch));
        obj.AddFinalizer(NetFinalizer, ch);
        return obj;

err_einval:
        throw_js_exception(env, "CHN_NetCreate(): Invalid argument");
        return env.Null();
}

__cold
static void CHN_NetClose(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        NodeCHNet *ch;

        if (unlikely(VALIDATE_ARG && info.Length() != 1))
                goto err_einval;

        ch = GetNetPtr(env, info[0]);
        if (unlikely(VALIDATE_ARG && !ch))
                goto err_einval;

        ClearPtr(env, info[0]);
        ch->Close();
        return;

err_einval:
        throw_js_exception(env, "CHN_NetClose(): Invalid argument");
}

__hot
static void CHN_NetSetURL(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        NodeCHNet *ch;

        if (unlikely(VALIDATE_ARG && info.Length() != 2))
                goto err_einval;

        ch = GetNetPtr(env, info[0]);
        if (unlikely(VALIDATE_ARG && !ch))
                goto err_einval;

        if (unlikely(VALIDATE_ARG && !info[1].IsString())) {
                goto err_einval;
        } else {
                const std::string &url = info[1].ToString().Utf8Value();
                ch->ch_->SetURL(url.c_str());
                return;
        }

err_einval:
        throw_js_exception(info.Env(), "CHN_NetSetURL(): Invalid argument");
}

__hot
static void CHN_NetSetMethod(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        NodeCHNet *ch;

        if (unlikely(VALIDATE_ARG && info.Length() != 2))
                goto err_einval;

        ch = GetNetPtr(env, info[0]);
        if (unlikely(VALIDATE_ARG && !ch))
                goto err_einval;

        if (likely(VALIDATE_ARG && !info[1].IsString())) {
                goto err_einval;
        } else {
                const std::string &method = info[1].ToString().Utf8Value();
                ch->ch_->SetMethod(method.c_str());
                return;
        }

err_einval:
        throw_js_exception(info.Env(), "CHN_NetSetMethod(): Invalid argument");
}

__hot
static void __CHN_NetSetRequestHeader(const Napi::CallbackInfo &info,
                                      NodeCHNet *ch, bool overwrite)
{
        const std::string &key = info[1].ToString().Utf8Value();
        const std::string &val = info[2].ToString().Utf8Value();

        ch->ch_->SetRequestHeader(key.c_str(), val.c_str(), overwrite);
}

__hot
static void CHN_NetSetRequestHeader(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        bool overwrite = true;
        NodeCHNet *ch;
        int nr_arg;

        nr_arg = info.Length();
        if (unlikely(VALIDATE_ARG && nr_arg != 3 && nr_arg != 4))
                goto err_einval;

        ch = GetNetPtr(env, info[0]);
        if (unlikely(VALIDATE_ARG && !ch))
                goto err_einval;

        if (unlikely(VALIDATE_ARG && !info[1].IsString()))
                goto err_einval;

        if (unlikely(VALIDATE_ARG && !info[2].IsString()))
                goto err_einval;

        if (nr_arg == 4) {
                if (unlikely(VALIDATE_ARG && !info[3].IsBoolean()))
                        goto err_einval;

                overwrite = static_cast<bool>(info[3].As<Napi::Boolean>());
        }

        __CHN_NetSetRequestHeader(info, ch, overwrite);
        return;

err_einval:
        throw_js_exception(info.Env(), "CHN_NetSetRequestHeader(): Invalid argument");
}

__hot
static Napi::Value CHN_NetGetResponseHeaders(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        Napi::Array ret;
        const char *key;
        const char *val;
        size_t iter = 0;
        size_t idx = 0;
        NodeCHNet *ch;
        CHNet *ch_;

        if (unlikely(VALIDATE_ARG && info.Length() != 1))
                goto err_einval;

        ch = GetNetPtr(env, info[0]);
        if (unlikely(VALIDATE_ARG && !ch))
                goto err_einval;

        ch_ = ch->ch_;
        ret = Napi::Array::New(env);

        ch_->LockResponseHeader();
        while (ch_->__GetResponseHeader(&iter, &key, &val)) {
                Napi::Array tmp = Napi::Array::New(env);

                tmp[0u] = Napi::String::New(env, key);
                tmp[1u] = Napi::String::New(env, val);
                ret[idx++] = tmp;
        }
        ch_->UnlockResponseHeader();

        return ret;

err_einval:
        throw_js_exception(env, "CHN_NetGetResponseHeaders(): Invalid argument");
        return env.Null();
}

__hot
static Napi::Value CHN_NetReadRet(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        NodeCHNet *ch;

        if (unlikely(VALIDATE_ARG && info.Length() != 1))
                goto err_einval;

        ch = GetNetPtr(env, info[0]);
        if (unlikely(VALIDATE_ARG && !ch))
                goto err_einval;

        return Napi::Number::New(env, ch->ch_->GetReadRet());

err_einval:
        throw_js_exception(env, "CHN_NetReadRet(): Invalid argument");
        return env.Null();
}

__hot
static Napi::Value CHN_NetReadBuf(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        NodeCHNet *ch;
        int ret;

        if (unlikely(VALIDATE_ARG && info.Length() != 1))
                goto err_einval;

        ch = GetNetPtr(env, info[0]);
        if (unlikely(VALIDATE_ARG && !ch))
                goto err_einval;

        ret = ch->ch_->GetReadRet();

        if (ret > 0)
                return Napi::String::New(env, ch->ch_->GetReadBuffer(), (size_t)ret);
        else
                return env.Null();

err_einval:
        throw_js_exception(env, "CHN_NetReadBuf(): Invalid argument");
        return env.Null();
}

__hot
static Napi::Value CHN_NetGetErrorStr(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        const char *err;
        NodeCHNet *ch;

        if (unlikely(VALIDATE_ARG && info.Length() != 1))
                goto err_einval;

        ch = GetNetPtr(env, info[0]);
        if (unlikely(VALIDATE_ARG && !ch))
                goto err_einval;

        err = ch->ch_->GetErrorStr();
        if (err)
                return Napi::String::New(env, err);

        return env.Null();

err_einval:
        throw_js_exception(env, "CHN_NetGetErrorStr(): Invalid argument");
        return env.Null();
}

__hot
static void CHN_NetSetPayload(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        NodeCHNet *ch;

        if (unlikely(VALIDATE_ARG && info.Length() != 2))
                goto err_einval;

        ch = GetNetPtr(env, info[0]);
        if (unlikely(VALIDATE_ARG && !ch))
                goto err_einval;

        if (likely(info[1].IsBuffer())) {
                Napi::Buffer<uint8_t> p = info[1].As<Napi::Buffer<uint8_t>>();
                ch->payload_buffer_ = memdup(p.Data(), p.Length());
                ch->ch_->SetPayload(ch->payload_buffer_, p.Length());
        } else if (likely(info[1].IsString())) {
                ch->payload_ = info[1].ToString().Utf8Value();
                ch->ch_->SetPayload(ch->payload_.c_str(), ch->payload_.size());
        } else {
                goto err_einval;
        }

        return;

err_einval:
        throw_js_exception(env, "CHN_NetSetPayload(): Invalid argument");
}

__hot
static void CHN_NetStartChunkedPayload(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        NodeCHNet *ch;

        if (unlikely(VALIDATE_ARG && info.Length() != 1))
                goto err_einval;

        ch = GetNetPtr(env, info[0]);
        if (unlikely(VALIDATE_ARG && !ch))
                goto err_einval;

        ch->ch_->StartChunkedPayload();
        return;

err_einval:
        throw_js_exception(env, "CHN_NetStartChunkedPayload(): Invalid argument");
}

__hot
static void CHN_NetStopChunkedPayload(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        NodeCHNet *ch;

        if (unlikely(VALIDATE_ARG && info.Length() != 1))
                goto err_einval;

        ch = GetNetPtr(env, info[0]);
        if (unlikely(VALIDATE_ARG && !ch))
                goto err_einval;

        ch->ch_->StopChunkedPayload();
        return;

err_einval:
        throw_js_exception(env, "CHN_NetStopChunkedPayload(): Invalid argument");
}

__hot
static Napi::Value CHN_NetWrite(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        NodeCHNet *ch;
        int ret;

        if (unlikely(VALIDATE_ARG && info.Length() != 2))
                goto err_einval;

        ch = GetNetPtr(env, info[0]);
        if (unlikely(VALIDATE_ARG && !ch))
                goto err_einval;

        if (likely(info[1].IsBuffer())) {
                Napi::Buffer<uint8_t> p = info[1].As<Napi::Buffer<uint8_t>>();
                ret = ch->ch_->Write(p.Data(), p.Length());
        } else if (likely(info[1].IsString())) {
                const std::string &p = info[1].ToString().Utf8Value();
                ret = ch->ch_->Write(p.c_str(), p.size());
        } else {
                goto err_einval;
        }

        return Napi::Number::New(env, ret);

err_einval:
        throw_js_exception(env, "CHN_NetWrite(): Invalid argument");
        return env.Null();
}

__hot
static Napi::Value CHN_NetGetResponseCode(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        NodeCHNet *ch;

        if (unlikely(VALIDATE_ARG && info.Length() != 1))
                goto err_einval;

        ch = GetNetPtr(env, info[0]);
        if (unlikely(VALIDATE_ARG && !ch))
                goto err_einval;

        return Napi::Number::New(env, ch->ch_->GetResponseCode());

err_einval:
        throw_js_exception(env, "CHN_NetGetResponseCode(): Invalid argument");
        return env.Null();
}

__hot
static Napi::Value CHN_NetGetAlpnNegotiatedProtocol(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        NodeCHNet *ch;
        const char *p;

        if (unlikely(VALIDATE_ARG && info.Length() != 1))
                goto err_einval;

        ch = GetNetPtr(env, info[0]);
        if (unlikely(VALIDATE_ARG && !ch))
                goto err_einval;

        p = ch->ch_->GetAlpnNegotiatedProtocol();
        if (!p || !*p)
                return env.Null();

        return Napi::String::New(env, p);

err_einval:
        throw_js_exception(env, "CHN_NetGetAlpnNegotiatedProtocol(): Invalid argument");
        return env.Null();
}

__hot
static Napi::Value CHN_NetGetRemoteEndpoint(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        NodeCHNet *ch;
        char buf[128];

        if (unlikely(VALIDATE_ARG && info.Length() != 1))
                goto err_einval;

        ch = GetNetPtr(env, info[0]);
        if (unlikely(VALIDATE_ARG && !ch))
                goto err_einval;

        if (ch->ch_->GetRemoteEndpoint(buf, sizeof(buf)))
                return Napi::String::New(env, buf);
        else
                return env.Null();
err_einval:
        throw_js_exception(env, "CHN_NetGetRemoteEndpoint(): Invalid argument");
        return env.Null();
}

static void GetBufferFinalizer(Napi::Env env, void *ptr,
                               struct chnet_buf_ptr *buf)
{
        CHNet::PutBufferPtr(buf);
        (void)env;
        (void)ptr;
}

__hot
static Napi::Value CHN_NetGetBufferPtr(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        struct chnet_buf_ptr *buf;
        NodeCHNet *ch;

        if (unlikely(VALIDATE_ARG && info.Length() != 1))
                goto err_einval;

        ch = GetNetPtr(env, info[0]);
        if (unlikely(VALIDATE_ARG && !ch))
                goto err_einval;

        buf = ch->ch_->GetBufferPtr();
        if (unlikely(!buf))
                return env.Null();

        return Napi::Buffer<uint8_t>::New(env, static_cast<uint8_t *>(buf->ptr_),
                                          buf->len_, GetBufferFinalizer, buf);
err_einval:
        throw_js_exception(env, "CHN_NetGetBufferPtr(): Invalid argument");
        return env.Null();
}

__hot
static void CHN_NetSetFollowRedirect(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        NodeCHNet *ch;
        bool p;

        if (unlikely(VALIDATE_ARG && info.Length() != 2))
                goto err_einval;

        ch = GetNetPtr(env, info[0]);
        if (unlikely(VALIDATE_ARG && !ch))
                goto err_einval;

        if (likely(VALIDATE_ARG && !info[1].IsBoolean()))
                goto err_einval;

        p = static_cast<bool>(info[1].As<Napi::Boolean>());
        ch->ch_->SetFollowRedirect(p);
        return;
err_einval:
        throw_js_exception(info.Env(), "CHN_NetSetFollowRedirect(): Invalid argument");
}

__hot
static Napi::Value CHN_NetGetRedirectInfo(const Napi::CallbackInfo &info)
{
        const struct NodeRedirectInfo *red;
        Napi::Env env = info.Env();
        Napi::Object ret;
        NodeCHNet *ch;

        if (unlikely(VALIDATE_ARG && info.Length() != 1))
                goto err_einval;

        ch = GetNetPtr(env, info[0]);
        if (unlikely(VALIDATE_ARG && !ch))
                goto err_einval;

        red = ch->ch_->GetNodeRedirectInfo();
        if (unlikely(!red))
                return env.Null();

        ret = Napi::Object::New(env);
        ret["status_code"] = Napi::Number::New(env, red->status_code_);
        ret["insecure_scheme_was_upgraded"] = Napi::Boolean::New(env, red->insecure_scheme_was_upgraded_);
        ret["is_signed_exchange_fallback_redirect"] = Napi::Boolean::New(env, red->is_signed_exchange_fallback_redirect_);
        ret["new_method"] = Napi::String::New(env, red->new_method_);
        ret["new_url"] = Napi::String::New(env, red->new_url_);
        ret["new_referrer"] = Napi::String::New(env, red->new_referrer_);

        return ret;

err_einval:
        throw_js_exception(env, "CHN_NetGetRedirectInfo(): Invalid argument");
        return env.Null();
}

__hot
static void CHN_NetDNSMapInsert(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        std::string host;
        std::string ip;
        int len = info.Length();

        /*
         * Accept a 3rd argument that specificies map bind_ip4 and bind_iface4.
         * 
         * If the 3rd argument is specified, the inserted entry will be used
         * only for outgoing connection that's explicitly bind to the same
         * bind_ip4 and bind_iface4.
         */
        if (unlikely(VALIDATE_ARG && len != 2 && len != 3))
                goto err_einval;
        if (unlikely(VALIDATE_ARG && !info[0].IsString()))
                goto err_einval;
        if (unlikely(VALIDATE_ARG && !info[1].IsString()))
                goto err_einval;

        if (len == 3) {
                std::string bind_ip4 = "";
                std::string bind_iface4 = "";

                if (unlikely(VALIDATE_ARG && !info[2].IsObject())) {
                        goto err_einval;
                }

                Napi::Object obj = info[2].As<Napi::Object>();

                auto ip4 = obj.Get("bind_ip4");
                if (ip4.IsString()) {
                        bind_ip4 = ip4.As<Napi::String>();
                }

                auto iface4 = obj.Get("bind_iface4");
                if (iface4.IsString()) {
                        bind_iface4 = iface4.As<Napi::String>();
                }

                host += bind_ip4 + "#";
                host += bind_iface4 + "#";
        } else {
                host = "##";
        }

        host += info[0].As<Napi::String>();
        ip = info[1].As<Napi::String>();
        g_chnet_dns_map_insert(host.c_str(), ip.c_str());
        return;

err_einval:
        throw_js_exception(env, "CHN_NetDNSMapInsert(): Invalid argument");
}

__hot
static Napi::Value CHN_NetDNSMapGetAddrList(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        struct chnet_dns_lookup lk;
        std::string host;
        Napi::Array ret;
        uint32_t i;
        int len = info.Length();

        if (unlikely(VALIDATE_ARG && len != 1 && len != 2))
                goto err_einval;
        if (unlikely(VALIDATE_ARG && !info[0].IsString()))
                goto err_einval;

        if (len == 2) {
                std::string bind_ip4 = "";
                std::string bind_iface4 = "";

                if (unlikely(VALIDATE_ARG && !info[1].IsObject())) {
                        goto err_einval;
                }

                Napi::Object obj = info[1].As<Napi::Object>();

                auto ip4 = obj.Get("bind_ip4");
                if (ip4.IsString()) {
                        bind_ip4 = ip4.As<Napi::String>();
                }

                auto iface4 = obj.Get("bind_iface4");
                if (iface4.IsString()) {
                        bind_iface4 = iface4.As<Napi::String>();
                }

                host += bind_ip4 + "#";
                host += bind_iface4 + "#";
        } else {
                host = "##";
        }

        host += info[0].As<Napi::String>();

        lk.host = host.c_str();
        lk.vec = nullptr;
        lk.iter = 0;
        lk.ip = nullptr;

        g_chnet_dns_map_lock();
        ret = Napi::Array::New(env);
        i = 0;
        while (1) {
                if (!g___chnet_dns_map_lookup(&lk))
                        break;

                ret[i++] = Napi::String::New(env, lk.ip);
        }
        g_chnet_dns_map_unlock();
        return ret;

err_einval:
        throw_js_exception(env, "CHN_NetDNSMapGetAddrList(): Invalid argument");
        return env.Null();
}

__hot
static void CHN_NetDNSMapPop(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        std::string host;

        if (unlikely(VALIDATE_ARG && info.Length() != 1))
                goto err_einval;
        if (unlikely(VALIDATE_ARG && !info[0].IsString()))
                goto err_einval;

        host = info[0].As<Napi::String>();
        g_chnet_dns_map_pop(host.c_str());
        return;

err_einval:
        throw_js_exception(env, "CHN_NetDNSMapPop(): Invalid argument");
}

__hot
static void CHN_NetDNSMapPopAll(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        std::string host;

        if (unlikely(VALIDATE_ARG && info.Length() != 1))
                goto err_einval;
        if (unlikely(VALIDATE_ARG && !info[0].IsString()))
                goto err_einval;

        host = info[0].As<Napi::String>();
        g_chnet_dns_map_remove_all(host.c_str());
        return;

err_einval:
        throw_js_exception(env, "CHN_NetDNSMapPop(): Invalid argument");
}

__hot
static void CHN_NetDNSMapRemoveAddr(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        std::string host;
        std::string ip;

        if (unlikely(VALIDATE_ARG && info.Length() != 2))
                goto err_einval;
        if (unlikely(VALIDATE_ARG && !info[0].IsString()))
                goto err_einval;
        if (unlikely(VALIDATE_ARG && !info[1].IsString()))
                goto err_einval;

        host = info[0].As<Napi::String>();
        ip = info[1].As<Napi::String>();
        g_chnet_dns_map_remove(host.c_str(), ip.c_str());
        return;

err_einval:
        throw_js_exception(env, "CHN_NetDNSMapRemoveAddr(): Invalid argument");
}

static void NetDNSFinalizer(Napi::Env env, NodeDNSQuery *res)
{
        delete res;
        (void)env;
}

__hot
static Napi::Value CHN_NetDNSCreate(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        NodeDNSQuery *dq;
        std::string host;
        Napi::Object obj;

        if (unlikely(VALIDATE_ARG && info.Length() != 1))
                goto err_einval;
        if (unlikely(VALIDATE_ARG && !info[0].IsString()))
                goto err_einval;

        dq = new NodeDNSQuery();
        dq->hostname_ = info[0].As<Napi::String>();
        memset(&dq->result_, 0, sizeof(dq->result_));
        obj = Napi::Object::New(env);
        obj["p"] = Napi::Number::New(info.Env(), ptr_to_s64<NodeDNSQuery>(dq));
        obj["host"] = info[0].As<Napi::String>();
        obj.AddFinalizer(NetDNSFinalizer, dq);
        return obj;

err_einval:
        throw_js_exception(env, "CHN_NetDNSCreate(): Invalid argument");
        return env.Null();
}

__hot
static Napi::Value CHN_NetDNSGetResult(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        NodeDNSQuery *dq;
        Napi::Object obj;
        Napi::Array arr;
        uint32_t i;

        if (unlikely(VALIDATE_ARG && info.Length() != 1))
                goto err_einval;
        if (unlikely(VALIDATE_ARG && !info[0].IsObject()))
                goto err_einval;
        dq = GetDnsPtr(env, info[0]);
        if (unlikely(VALIDATE_ARG && !dq))
                goto err_einval;

        arr = Napi::Array::New(env);
        for (i = 0; i < dq->result_.size_; i++)
                arr[i] = Napi::String::New(env, dq->result_.addresses_[i]);

        return arr;

err_einval:
        throw_js_exception(env, "CHN_NetDNSGetResult(): Invalid argument");
        return env.Null();
}

#if defined(__linux__)

__hot
static Napi::Value CHN_NetDNSGetError(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        NodeDNSQuery *dq;
        Napi::Object obj;
        Napi::String ret;

        if (unlikely(VALIDATE_ARG && info.Length() != 1))
                goto err_einval;
        if (unlikely(VALIDATE_ARG && !info[0].IsObject()))
                goto err_einval;
        dq = GetDnsPtr(env, info[0]);
        if (unlikely(VALIDATE_ARG && !dq))
                goto err_einval;

        if (!dq->result_.error_)
                return env.Null();

        return Napi::String::New(env, dq->result_.error_);

err_einval:
        throw_js_exception(env, "CHN_NetDNSGetError(): Invalid argument");
        return env.Null();
}

__hot
static void CHN_NetDNSBindIPv4(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        std::string ip;

        if (unlikely(VALIDATE_ARG && info.Length() != 1))
                goto err_einval;

        if (unlikely(VALIDATE_ARG && !info[0].IsString()))
                goto err_einval;

        ip = info[0].As<Napi::String>();
        chnet_dns_bind_ipv4(ip.c_str());
        return;

err_einval:
        throw_js_exception(env, "CHN_NetDNSBindIPv4(): Invalid argument");
}

__hot
static void CHN_NetDNSBindIPv6(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        std::string ip;

        if (unlikely(VALIDATE_ARG && info.Length() != 1))
                goto err_einval;

        if (unlikely(VALIDATE_ARG && !info[0].IsString()))
                goto err_einval;

        ip = info[0].As<Napi::String>();
        chnet_dns_bind_ipv6(ip.c_str());
        return;

err_einval:
        throw_js_exception(env, "CHN_NetDNSBindIPv6(): Invalid argument");
}

__hot
static void CHN_NetDNSBindInterface(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        std::string iface;

        if (unlikely(VALIDATE_ARG && info.Length() != 1))
                goto err_einval;

        if (unlikely(VALIDATE_ARG && !info[0].IsString()))
                goto err_einval;

        iface = info[0].As<Napi::String>();
        chnet_dns_bind_iface(iface.c_str());
        return;

err_einval:
        throw_js_exception(env, "CHN_NetDNSBindIface(): Invalid argument");
}

#endif

static void NetDNSResolverFinalizer(Napi::Env env, void *data)
{
        (void)env;
        delete (NodeDedicateDnsRes *)data;
}

/**
 * - First argument list of DNS servers to use (comma separated).
 * - Second argument is source IP address to use for DNS queries.
 * - Third argument is interface to bind to.
 * 
 * All arguments are a string.
 */
__hot
static Napi::Value CHN_NetDNSCreateResolver(const Napi::CallbackInfo &info)
{
        std::string dns_servers = "";
        std::string bind_ip4 = "";
        std::string bind_iface4 = "";

        Napi::Env env = info.Env();
        NodeDedicateDnsRes *ddr;
        Napi::Object obj;
        int len;

        len = info.Length();
        if (unlikely(VALIDATE_ARG && len > 3))
                goto err_einval;

        if (len > 0) {
                if (unlikely(VALIDATE_ARG && !info[0].IsString()))
                        goto err_einval;

                dns_servers = info[0].As<Napi::String>().Utf8Value();
        }

        if (len > 1) {
                if (unlikely(VALIDATE_ARG && !info[1].IsString()))
                        goto err_einval;

                bind_ip4 = info[1].As<Napi::String>().Utf8Value();
        }

        if (len > 2) {
                if (unlikely(VALIDATE_ARG && !info[2].IsString()))
                        goto err_einval;

                bind_iface4 = info[2].As<Napi::String>().Utf8Value();
        }

        {
                const char *dns_servers_ptr = dns_servers.empty() ? nullptr : dns_servers.c_str();
                const char *bind_ip4_ptr = bind_ip4.empty() ? nullptr : bind_ip4.c_str();
                const char *bind_iface4_ptr = bind_iface4.empty() ? nullptr : bind_iface4.c_str();
                ddr = new NodeDedicateDnsRes(dns_servers_ptr, bind_ip4_ptr, bind_iface4_ptr);
        }

        obj = Napi::Object::New(env);
        obj["p"] = Napi::Number::New(info.Env(), ptr_to_s64<NodeDedicateDnsRes>(ddr));
        obj.AddFinalizer(NetDNSResolverFinalizer, ddr);
        return obj;

err_einval:
        throw_js_exception(env, "CHN_NetDNSCreateResolver(): Invalid argument");
        return env.Null();
}

static void CHN_NetCancel(const Napi::CallbackInfo &info)
{
        Napi::Env env = info.Env();
        NodeCHNet *ch;

        if (unlikely(VALIDATE_ARG && info.Length() != 1))
                goto err_einval;

        ch = GetNetPtr(env, info[0]);
        if (unlikely(VALIDATE_ARG && !ch))
                goto err_einval;

        ch->ch_->Cancel();
        return;

err_einval:
        throw_js_exception(env, "CHN_NetCancel(): Invalid argument");
}

__cold
static Napi::Object CHN_GlobalInit(Napi::Env env, Napi::Object obj)
{
        chnet_global_init();

        /*
         * Ring methods.
         */
        obj_add_func(env, obj, CHN_RingCreate, "RingCreate");
        obj_add_func(env, obj, CHN_RingClose, "RingClose");
        obj_add_func(env, obj, CHN_RingGetSQE, "RingGetSQE");
        obj_add_func(env, obj, CHN_RingSubmit, "RingSubmit");
        obj_add_func(env, obj, CHN_RingWaitCQE, "RingWaitCQE");
        obj_add_func(env, obj, CHN_RingForEachCQE, "RingForEachCQE");
        obj_add_func(env, obj, CHN_RingForEachCQEAdvance, "RingForEachCQEAdvance");
        obj_add_func(env, obj, CHN_RingCQAdvance, "RingCQAdvance");
        obj_add_func(env, obj, CHN_RingGetCQEHead, "RingGetCQEHead");
        obj_add_func(env, obj, CHN_RingSubmitCallback, "RingSubmitCallback");
        obj_add_func(env, obj, CHN_RingSetWaitCQECallback, "RingSetWaitCQECallback");

        /*
         * RingSQE methods.
         */
        obj_add_func(env, obj, CHN_RingSQEPrepNop, "RingSQEPrepNop");
        obj_add_func(env, obj, CHN_RingSQEPrepRead, "RingSQEPrepRead");
        obj_add_func(env, obj, CHN_RingSQEPrepStart, "RingSQEPrepStart");
        obj_add_func(env, obj, CHN_RingSQESetUserData, "RingSQESetUserData");
        obj_add_func(env, obj, CHN_RingSQEPrepDnsQuery, "RingSQEPrepDnsQuery");
        obj_add_func(env, obj, CHN_RingSQEPrepDedicatedDnsQuery, "RingSQEPrepDedicatedDnsQuery");

        /*
         * Net methods.
         */
        obj_add_func(env, obj, CHN_NetCreate, "NetCreate");
        obj_add_func(env, obj, CHN_NetClose, "NetClose");
        obj_add_func(env, obj, CHN_NetSetURL, "NetSetURL");
        obj_add_func(env, obj, CHN_NetSetMethod, "NetSetMethod");
        obj_add_func(env, obj, CHN_NetSetRequestHeader, "NetSetRequestHeader");
        obj_add_func(env, obj, CHN_NetGetResponseHeaders, "NetGetResponseHeaders");
        obj_add_func(env, obj, CHN_NetReadRet, "Netread_ret");
        obj_add_func(env, obj, CHN_NetReadBuf, "Netread_buf");
        obj_add_func(env, obj, CHN_NetGetErrorStr, "NetGetErrorStr");
        obj_add_func(env, obj, CHN_NetSetPayload, "NetSetPayload");
        obj_add_func(env, obj, CHN_NetStartChunkedPayload, "NetStartChunkedPayload");
        obj_add_func(env, obj, CHN_NetStopChunkedPayload, "NetStopChunkedPayload");
        obj_add_func(env, obj, CHN_NetWrite, "NetWrite");
        obj_add_func(env, obj, CHN_NetGetResponseCode, "NetGetResponseCode");
        obj_add_func(env, obj, CHN_NetGetAlpnNegotiatedProtocol, "NetGetAlpnNegotiatedProtocol");
        obj_add_func(env, obj, CHN_NetGetRemoteEndpoint, "NetGetRemoteEndpoint");
        obj_add_func(env, obj, CHN_NetGetBufferPtr, "NetGetBufferPtr");
        obj_add_func(env, obj, CHN_NetSetFollowRedirect, "NetSetFollowRedirect");
        obj_add_func(env, obj, CHN_NetGetRedirectInfo, "NetGetRedirectInfo");
        obj_add_func(env, obj, CHN_NetCancel, "NetCancel");

        /*
         * DNS method.
         */
        obj_add_func(env, obj, CHN_NetDNSMapInsert, "NetDNSMapInsert");
        obj_add_func(env, obj, CHN_NetDNSMapGetAddrList, "NetDNSMapGetAddrList");
        obj_add_func(env, obj, CHN_NetDNSMapPop, "NetDNSMapPop");
        obj_add_func(env, obj, CHN_NetDNSMapPopAll, "NetDNSMapPopAll");
        obj_add_func(env, obj, CHN_NetDNSMapRemoveAddr, "NetDNSMapRemoveAddr");
        obj_add_func(env, obj, CHN_NetDNSCreate, "NetDNSCreate");
        obj_add_func(env, obj, CHN_NetDNSGetResult, "NetDNSGetResult");
        obj_add_func(env, obj, CHN_NetDNSGetError, "NetDNSGetError");
#if defined(__linux__)
        obj_add_func(env, obj, CHN_NetDNSBindIPv4, "NetDNSBindIPv4");
        obj_add_func(env, obj, CHN_NetDNSBindIPv6, "NetDNSBindIPv6");
        obj_add_func(env, obj, CHN_NetDNSBindInterface, "NetDNSBindInterface");
#endif
        obj_add_func(env, obj, CHN_NetDNSBindIPv4, "NetDNSBindIPv4");
        obj_add_func(env, obj, CHN_NetDNSBindIPv6, "NetDNSBindIPv6");
        obj_add_func(env, obj, CHN_NetDNSBindInterface, "NetDNSBindInterface");
        obj_add_func(env, obj, CHN_NetDNSCreateResolver, "NetDNSCreateResolver");

        obj.Seal();
        obj.Freeze();
        return obj;
}
NODE_API_MODULE(chnet, CHN_GlobalInit);
