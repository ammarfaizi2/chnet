
#if !defined(WIN32)
        #define PATCH_BORING_SSL 1
#endif

#include "common.h"

#if PATCH_BORING_SSL
#include "arch/x86/boringssl_fixup.h"
#endif

__cold void chnet_fix_chromium_boringssl(void)
{
#if PATCH_BORING_SSL
        fix_chromium_boring_ssl();
#endif
}
