
const chnet = require("bindings")("chnet");

class CNRingSQE {
	constructor(ring, _sqe)
	{
		this.ring = ring;
		this.sqe_ = _sqe;
	}

	PrepNop()
	{
		chnet.RingSQEPrepNop(this.sqe_);
	}

	PrepRead(ch, size)
	{
		chnet.RingSQEPrepRead(this.sqe_, ch._net, size);
	}

	PrepStart(ch)
	{
		chnet.RingSQEPrepStart(this.sqe_, ch._net);
	}

        PrepDnsQuery(dns_obj)
        {
                chnet.RingSQEPrepDnsQuery(this.sqe_, dns_obj);
        }

        PrepDedicatedDnsQuery(resolver, dns_obj)
        {
                chnet.RingSQEPrepDedicatedDnsQuery(this.sqe_, resolver, dns_obj);
        }

	SetUserData(udata)
	{
		let seq_id = this.ring.udata_seq++;

		chnet.RingSQESetUserData(this.sqe_, seq_id);
		this.ring.udata[seq_id] = udata;
	}
};

class CNRing {
	constructor(entry)
	{
		this.ring_ = chnet.RingCreate(entry);
		this.udata_seq = 0;
		this.udata = {};
		this.entry = entry;
	}

	Close()
	{
		chnet.RingClose(this.ring_);
	}

	GetSQE()
	{
		let _sqe = chnet.RingGetSQE(this.ring_);
		if (!_sqe)
			return null;

		return new CNRingSQE(this, _sqe);
	}

	Submit(num = null)
	{
		if (num)
			return chnet.RingSubmit(this.ring_, num);
		else
			return chnet.RingSubmit(this.ring_);
	}

	SubmitAllAndReap(callback)
	{
		return chnet.RingSubmitAllAndReap(this.ring_, callback);
	}

	WaitCQE(n)
	{
		return chnet.RingWaitCQE(this.ring_, n);
	}

	__ForEachCQE(func, callback)
	{
		let that = this;
		let i = 0;

		func(this.ring_, function (cqe) {

			if ("user_data" in cqe)
				cqe.user_data = that.udata[cqe.user_data];

			callback(cqe);
			i++;
		});
		return i;
	}

	ForEachCQE(callback)
	{
		return this.__ForEachCQE(chnet.RingForEachCQE, callback);
	}

	ForEachCQEAdvance(callback)
	{
		return this.__ForEachCQE(chnet.RingForEachCQEAdvance, callback);
	}

	GetCQEHead()
	{
		let cqe = chnet.RingGetCQEHead(this.ring_);
		if (!cqe)
			return null;

		if ("user_data" in cqe)
			cqe.user_data = that.udata[cqe.user_data];

		return cqe;
	}

	CQAdvance(n)
	{
		chnet.RingCQAdvance(this.ring_, n);
	}

	SetWaitCQECallback(callback)
	{
		return chnet.RingSetWaitCQECallback(this.ring_, callback);
	}

	SubmitCallback()
	{
		return chnet.RingSubmitCallback(this.ring_);
	}
};

class CHNet {
	constructor(...args)
	{
		this._net = chnet.NetCreate(...args);
	}

	Close()
	{
		chnet.NetClose(this._net);
	}

	SetURL(url)
	{
		return chnet.NetSetURL(this._net, url);
	}

	SetMethod(method)
	{
		return chnet.NetSetMethod(this._net, method);
	}

	SetRequestHeader(key, val, overwrite = true)
	{
		return chnet.NetSetRequestHeader(this._net, key, val, overwrite);
	}

	GetResponseHeaders()
	{
		return chnet.NetGetResponseHeaders(this._net);
	}

	read_ret()
	{
		return chnet.Netread_ret(this._net);
	}

	read_buf()
	{
		return chnet.Netread_buf(this._net);
	}

	GetErrorStr()
	{
		return chnet.NetGetErrorStr(this._net);
	}

	SetPayload(payload)
	{
		return chnet.NetSetPayload(this._net, payload);
	}

	StartChunkedPayload()
	{
		return chnet.NetStartChunkedPayload(this._net);
	}

	StopChunkedPayload()
	{
		return chnet.NetStopChunkedPayload(this._net);
	}

	Write(payload)
	{
		return chnet.NetWrite(this._net, payload);
	}

	GetResponseCode()
	{
		return chnet.NetGetResponseCode(this._net);
	}

	GetAlpnNegotiatedProtocol()
	{
		return chnet.NetGetAlpnNegotiatedProtocol(this._net);
	}

	GetRemoteEndpoint()
	{
		return chnet.NetGetRemoteEndpoint(this._net);
	}

	GetBuffer()
	{
		return chnet.NetGetBufferPtr(this._net);
	}

	SetFollowRedirect(p)
	{
		chnet.NetSetFollowRedirect(this._net, !!p);
	}

	GetRedirectInfo()
	{
		return chnet.NetGetRedirectInfo(this._net);
	}
};

const NEXT_TICK = setImmediate;

const CH_OP_READ = 1;
const CH_OP_START = 2;
const CH_OP_DNS_QUERY = 3;

const CRH_STATE_INIT = 0;
const CRH_STATE_CONNECT = 1;
const CRH_STATE_INFORMATION = 2;
const CRH_STATE_DATA = 3;
const CRH_STATE_ERROR = 4;
const CRH_STATE_CLOSE = 5;
const CRH_STATE_REDIRECT = 6;

class CRHttp {
	constructor(cr_http_ring, url, method = "GET", options = {})
	{
		this.crhr = cr_http_ring;
		this.url = url;
		this.method = method;
		this.ch = new CHNet(options);
		this.ch.SetURL(url);
		this.ch.SetMethod(method);
		this.callback = {
			error: null,
			connect: null,
			information: null,
			data: null,
			close: null,
			redirect: null
		};
		this.user_data = null;
		this.state = CRH_STATE_INIT;
	}

	SetAlwaysFollowRedirect(p)
	{
		return this.ch.SetFollowRedirect(p);
	}

	SetUserData(data)
	{
		this.user_data = data;
	}

	Read(len)
	{
		let ring = this.crhr.ring;
		let sqe = ring.GetSQE();

		if (!sqe) {
			ring.SubmitCallback();
			sqe = ring.GetSQE();
		}

		sqe.PrepRead(this.ch, len);
		sqe.SetUserData({op: CH_OP_READ, chr: this});
		this.crhr.SubmitCallback();
		return this;
	}


        Cancel()
        {
                // console.log(this.ch._net);
                chnet.NetCancel(this.ch._net);
        }

	On(event, callback)
	{
		switch (event) {
		case "error":
		case "connect":
		case "information":
		case "data":
		case "close":
		case "redirect":
			this.callback[event] = callback;
			break;
		default:
			throw new Error(`Invalid event ${event}`);
		}
		return this;
	}

        SetPayload(payload)
        {
                return this.ch.SetPayload(payload);
        }

	StartChunkedPayload()
	{
		return this.ch.StartChunkedPayload();
	}

	StopChunkedPayload()
	{
		return this.ch.StopChunkedPayload();
	}

	Write(buf)
	{
		return this.ch.Write(buf);
	}

	SetRequestHeaders(hdr)
	{
		let i;

		for (i in hdr)
			this.ch.SetRequestHeader(i, hdr[i], true);
	}

	Close()
	{
		if (this.callback.close)
			this.callback.close(this);
	}
};

function CQE_HANDLE_INIT(chr, res, op)
{
	if (res < 0 && res !== -3)
		chr.state = CRH_STATE_ERROR;
	else
		chr.state = CRH_STATE_CONNECT;

	CQE_CALLBACK_LOOP(chr, res, op);
}

function CQE_HANDLE_CONNECT(chr, res, op)
{
	if (res < 0 && res !== -3) {
		chr.state = CRH_STATE_ERROR;
	} else {
		if (chr.callback.connect)
			chr.callback.connect(chr);

		chr.state = CRH_STATE_INFORMATION;
	}

	CQE_CALLBACK_LOOP(chr, res, op);
}

function handle_info(chr, res, op, ch, code)
{
	let info = {
		code: code,
		alpn_neg_protocol: ch.GetAlpnNegotiatedProtocol(),
		remote_endpoint: ch.GetRemoteEndpoint(),
		headers: ch.GetResponseHeaders()
	};

	if (chr.callback.information)
		chr.callback.information(chr, info);

	if (300 <= code && code <= 399) {
		chr.state = CRH_STATE_REDIRECT;
	} else {
		chr.state = CRH_STATE_DATA;
	}

	CQE_CALLBACK_LOOP(chr, res, op);
}

function CQE_HANDLE_INFORMATION(chr, res, op)
{
	let ch = chr.ch;
	let code = 0;

	if (res === -3)
		code = ch.GetResponseCode();

	if (res < 0 && code === 0) {
		chr.state = CRH_STATE_ERROR;
		CQE_CALLBACK_LOOP(chr, res, op);
	}  else {
		if (code === 0)
			code = ch.GetResponseCode();
		handle_info(chr, res, op, ch, code);
	}
}

function CQE_HANDLE_DATA(chr, res, op)
{
	if (res <= 0) {

		if (res == 0)
			chr.state = CRH_STATE_CLOSE;
		else 
			chr.state = CRH_STATE_ERROR;

		CQE_CALLBACK_LOOP(chr, res, op);
		return;
	}

	if (chr.callback.data)
		chr.callback.data(chr, res, chr.ch.GetBuffer());
}

function CQE_HANDLE_REDIRECT(chr, res, op)
{
	if (chr.callback.redirect)
		chr.callback.redirect(chr, chr.ch.GetRedirectInfo());

	chr.state = CRH_STATE_CLOSE;
	CQE_CALLBACK_LOOP(chr, res, op);
}

function CQE_HANDLE_ERROR(chr, res, op)
{
	if (chr.callback.error) {
		chr.callback.error(chr, {
			code: res,
			message: chr.ch.GetErrorStr()
		});
	}

	chr.state = CRH_STATE_CLOSE;
	CQE_CALLBACK_LOOP(chr, res, op);
}

function CQE_HANDLE_CLOSE(chr, res, op)
{
	chr.Close();
}

function CQE_CALLBACK_LOOP(chr, res, op)
{
	switch (chr.state) {
	case CRH_STATE_INIT:
		f = function () { CQE_HANDLE_INIT(chr, res, op); };
		break;
	case CRH_STATE_CONNECT:
		f = function () { CQE_HANDLE_CONNECT(chr, res, op); };
		break;
	case CRH_STATE_INFORMATION:
		f = function () { CQE_HANDLE_INFORMATION(chr, res, op); };
		break;
	case CRH_STATE_DATA:
		f = function () { CQE_HANDLE_DATA(chr, res, op); };
		break;
	case CRH_STATE_REDIRECT:
		f = function () { CQE_HANDLE_REDIRECT(chr, res, op); };
		break;
	case CRH_STATE_ERROR:
		f = function () { CQE_HANDLE_ERROR(chr, res, op); };
		break;
	case CRH_STATE_CLOSE:
		f = function () { CQE_HANDLE_CLOSE(chr, res, op); };
		break;
	default:
		console.log("You hit a condition that must not happen!");
		console.log("Please report this issue with a reproducer if possible!");
		console.log("Dump:");
		console.log(chr, res, op);
		process.exit(1);
		return;
	}

	NEXT_TICK(f);
}

function CQE_CALLBACK_DNS_QUERY(chr, res, op)
{
        let addrs;
        let err;

        if (!res) {
                addrs = chnet.NetDNSGetResult(chr.dns);
                if (addrs.length == 0) {
                        err = "addr_not_found";
                        res = 1;
                }
        } else {
                err = chnet.NetDNSGetError(chr.dns);
        }

        if (res == 0) {
                if ("data" in chr.callback)
                        chr.callback.data(chr, chnet.NetDNSGetResult(chr.dns));
        } else {
                if ("error" in chr.callback)
                        chr.callback.error(chr, err);
        }

        if ("close" in chr.callback)
                chr.callback.close(chr);
}

function CQE_CALLBACK(cqe)
{
	let udata = cqe.user_data;
	let chr = udata.chr;

	switch (udata.op) {
	case CH_OP_READ:
		CQE_CALLBACK_LOOP(chr, cqe.res, udata.op);
		break;
        case CH_OP_DNS_QUERY:
                CQE_CALLBACK_DNS_QUERY(chr, cqe.res, udata.op);
                break;
	}
}

class CRDns {
        constructor(ring, host, options)
        {
                this.ring = ring;
                this.dns = chnet.NetDNSCreate(host);
                this.host = host;
                this.options = options;
                this.callback = {};
                this.resolver = null;
        }

        On(event, callback)
        {
                switch (event) {
                case "error":
                case "data":
                case "close":
                        this.callback[event] = callback;
                        break;
                default:
                        throw new Error(`Invalid event ${event}`);
                }
                return this;
        }

        Start()
        {
                let ring = this.ring.ring;
                let sqe = ring.GetSQE();

                if (!sqe) {
                        ring.SubmitCallback();
                        sqe = ring.GetSQE();
                }

                if (this.resolver !== null) {
                        sqe.PrepDedicatedDnsQuery(this.resolver.dns, this.dns);
                } else {
                        sqe.PrepDnsQuery(this.dns);
                }

                sqe.SetUserData({op: CH_OP_DNS_QUERY, chr: this});
                this.ring.SubmitCallback();
                return this;
        }

        SetResolver(resolver)
        {
                this.resolver = resolver;
        }
};

class CRHttpRing {
	constructor()
	{
		this.ring = new CNRing(512);
		this.nr_in_flights = 0;

		let that = this.ring;
		let that2 = this;
		this.ring.SetWaitCQECallback(function (cqe) {
			let do_delete = false;
			let index = -1;

			if ("user_data" in cqe) {
				index = cqe.user_data;
				cqe.user_data = that.udata[index];
				do_delete = true;
			}

			CQE_CALLBACK(cqe);
			that2.nr_in_flights--;
			if (do_delete)
				delete that.udata[index];
		});
	}

	Create(url, method = "GET", options = {})
	{
		return new CRHttp(this, url, method, options);
	}

        CreateDNS(host, options = {})
        {
                return new CRDns(this, host, options);
        }

        CreateDedicatedDNS(resolver, host, options = {})
        {
                let ret = new CRDns(this, host, options);
                ret.SetResolver(resolver);
                return ret;
        }

	SubmitCallback()
	{
		this.nr_in_flights += this.ring.SubmitCallback();
	}
};

class DnsResolver {
        constructor(server, bind_ip4, bind_iface4)
        {
                server = server.join(",");
                this.dns = chnet.NetDNSCreateResolver(server, bind_ip4, bind_iface4);
        }
};

class CRDnsResolver {
        constructor(server, options = {})
        {
                let args = [server];

                if ("bind_ip4" in options) {
                        args.push(options.bind_ip4);
                } else {
                        args.push("");
                }

                if ("bind_iface4" in options) {
                        args.push(options.bind_iface4);
                } else {
                        args.push("");
                }

                this.ring = new CRHttpRing;
                this.resolver = new DnsResolver(...args);
        }

        Resolve(host)
        {
                return this.ring.CreateDedicatedDNS(this.resolver, host);
        }
};

module.exports = {
	chnet,
	CNRingSQE,
	CNRing,
	CHNet,
	CRHttpRing,
	CRHttp,
        CRDns,
        CRDnsResolver
};
