
#include "chnet_thread_pool.h"
#include "chnet_thread.h"
#include <unordered_map>
#include <cstdlib>
#include <thread>

namespace net {

static std::unordered_map<std::string, CHNetThreadPool *> *g_thpool;
static uint32_t g_max_nlwp_per_key;
static std::mutex g_thpool_lock;

// static
__always_inline
std::string CHNetThreadPool::GenerateKey(struct tbind_config *bind_cfg)
{
        std::string key = "x;";

        if (!bind_cfg)
                return key;

        key += "ip4=";
        if (bind_cfg->bind_ip4_)
                key += bind_cfg->bind_ip4_;

        key += ";ip6=";
        if (bind_cfg->bind_ip6_)
                key += bind_cfg->bind_ip6_;

        key += ";if4=";
        if (bind_cfg->bind_iface4_)
                key += bind_cfg->bind_iface4_;

        key += ";if6=";
        if (bind_cfg->bind_iface6_)
                key += bind_cfg->bind_iface6_;

        key += ";proxy=";
        if (bind_cfg->proxy_)
                key += bind_cfg->proxy_;

        return key;
}

// static
CHNetThreadPool *CHNetThreadPool::GetThreadPool(struct tbind_config *bind_cfg)
{
        std::string key = GenerateKey(bind_cfg);
        CHNetThreadPool *ret;

        g_thpool_lock.lock();
        auto it = g_thpool->find(key);
        if (it == g_thpool->end()) {
                ret = new CHNetThreadPool(bind_cfg);
                (*g_thpool)[key] = ret;
        } else {
                ret = it->second;
        }
        g_thpool_lock.unlock();
        return ret;
}

// static
CHNetThread *CHNetThreadPool::GetThread(struct tbind_config *bind_cfg)
{
        CHNetThreadPool *thpool = GetThreadPool(bind_cfg);

        return thpool->__GetThread();
}

// static
void CHNetThreadPool::GlobalInit(void)
{
        int32_t hc;

        hc = std::thread::hardware_concurrency();
        if (hc < 2)
                hc = 2;

        g_max_nlwp_per_key = hc;

        g_thpool_lock.lock();
        CHECK(g_thpool == nullptr);
        g_thpool = new __typeof__(*g_thpool);
        g_thpool_lock.unlock();
}

// static
void CHNetThreadPool::GlobalDestroy(void)
{
        g_thpool_lock.lock();
        if (g_thpool) {

                for (auto it: (*g_thpool))
                        delete it.second;

                delete g_thpool;
                g_thpool = nullptr;
        }
        g_thpool_lock.unlock();
}

__always_inline
void CHNetThreadPool::InitThreadBindConfig(struct tbind_config *cfg)
{
        memset(&bind_cfg_, 0, sizeof(bind_cfg_));

        if (!cfg)
                return;

        if (cfg->bind_ip4_) {
                bind_cfg_.bind_ip4_ = strdup(cfg->bind_ip4_);
                CHECK(bind_cfg_.bind_ip4_);
        }

        if (cfg->bind_ip6_) {
                bind_cfg_.bind_ip6_ = strdup(cfg->bind_ip6_);
                CHECK(bind_cfg_.bind_ip6_);
        }

        if (cfg->bind_iface4_) {
                bind_cfg_.bind_iface4_ = strdup(cfg->bind_iface4_);
                CHECK(bind_cfg_.bind_iface4_);
        }

        if (cfg->bind_iface6_) {
                bind_cfg_.bind_iface6_ = strdup(cfg->bind_iface6_);
                CHECK(bind_cfg_.bind_iface6_);
        }

        if (cfg->proxy_) {
                bind_cfg_.proxy_ = strdup(cfg->proxy_);
                CHECK(bind_cfg_.proxy_);
        }
}

__always_inline
void CHNetThreadPool::InitThreadPool(void)
{
        size_t nr;

        nr = g_max_nlwp_per_key;
        if (nr < 2)
                nr = 2;

        pool_ = static_cast<CHNetThread **>(calloc(nr, sizeof(*pool_)));
        CHECK(pool_);
        nr_thread_ = nr;
}

CHNetThreadPool::CHNetThreadPool(struct tbind_config *bind_cfg):
        ref_count_(0)
{
        InitThreadBindConfig(bind_cfg);
        InitThreadPool();
}

__always_inline void CHNetThreadPool::DestroyBindConfig(void)
{
        free(bind_cfg_.bind_ip4_);
        free(bind_cfg_.bind_ip6_);
        free(bind_cfg_.bind_iface4_);
        free(bind_cfg_.bind_iface6_);
}

CHNetThreadPool::~CHNetThreadPool(void)
{
        uint32_t i = nr_thread_;

        Lock();
        while (i--) {
                if (!pool_[i])
                        continue;

                delete pool_[i];
        }

        free(pool_);
        pool_ = nullptr;
        Unlock();
        DestroyBindConfig();
}

__always_inline CHNetThread *CHNetThreadPool::__GetThread(void)
{
        const uint32_t max_nlwp_per_key = nr_thread_;
        const uint32_t nr_ref_split = 2048;
        CHNetThread *ret = nullptr;
        CHNetThread *tmp = nullptr;
        uint32_t min_ref_count = 0;
        uint32_t min_ref_idx = 0;
        uint32_t i;

        Lock();
        /*
         *
         * TODO(ammarfaizi2):
         * Imrpove the thread choosing algorithm for better parallelism.
         *
         */
        for (i = 0; i < max_nlwp_per_key; i++) {
                uint32_t cur_ref_count;

                tmp = pool_[i];
                /*
                 * If this slot hasn't been created, create it.
                 */
                if (!tmp) {
                        ret = new CHNetThread(i, this);
                        pool_[i] = ret;
                        goto out;
                }

                /*
                 * If the current ref count is less than @nr_ref_split,
                 * use it.
                 */
                cur_ref_count = tmp->ref_count_;
                if (cur_ref_count < nr_ref_split) {
                        ret = tmp;
                        goto out;
                }

                /*
                 * Save the thread index that has the minimum ref count.
                 * We will use that index to pick a thread if this loop
                 * reaches its worst case.
                 *
                 * Currently, we assume the thread with the smallest
                 * ref count is the best thread to be given a task.
                 */
                if (cur_ref_count < min_ref_count) {
                        min_ref_count = cur_ref_count;
                        min_ref_idx = i;
                }
        }

        /*
         * Aiee... we hit the worst case. Pick the thread with
         * the smallest ref count here. At this point, @ret
         * must be a nullptr.
         */
        CHECK(ret == nullptr);
        ret = pool_[min_ref_idx];

out:
        ref_count_++;
        ret->ref_count_++;
        Unlock();
        return ret;
}

void CHNetThreadPool::PutThread(uint32_t idx)
{
        CHNetThread *to_delete = nullptr;
        CHNetThread *ret;

        lock_.lock();
        ret = pool_[idx];
        if (--ret->ref_count_ == 0) {
                to_delete = ret;
                pool_[idx] = nullptr;
        }
        ref_count_--;
        lock_.unlock();

        if (to_delete)
                delete to_delete;
}

} /* namespace net */
