
#ifndef CHNET__NET__CHNET_THREAD_POOL_H
#define CHNET__NET__CHNET_THREAD_POOL_H

#include "base/task/thread_pool/thread_pool_instance.h"
#include "base/threading/thread.h"
#include "base/threading/thread_task_runner_handle.h"
#include "../common.h"
#include "chnet_config.h"
#include <mutex>

namespace net {

class CHNetThread;

class CHNetThreadPool {
public:
        CHNetThreadPool(struct tbind_config *bind_cfg);
        ~CHNetThreadPool(void);

        void PutThread(uint32_t idx);

        static CHNetThreadPool *GetThreadPool(struct tbind_config *bind_cfg);

        static CHNetThread *GetThread(struct tbind_config *bind_cfg);

        static void GlobalInit(void);

        static void GlobalDestroy(void);

        inline void Lock(void)
        {
                lock_.lock();
        }

        inline void Unlock(void)
        {
                lock_.unlock();
        }

        inline struct tbind_config *GetBindCfg(void)
        {
                return &bind_cfg_;
        }

private:
        std::mutex		lock_;
        struct tbind_config	bind_cfg_;
        CHNetThread		**pool_;
        uint32_t		nr_thread_;
        uint32_t		ref_count_;

        static std::string GenerateKey(struct tbind_config *bind_cfg);

        void InitThreadBindConfig(struct tbind_config *bind_cfg);

        void InitThreadPool(void);

        void DestroyBindConfig(void);

        CHNetThread *__GetThread(void);
};

} /* namespace net */

#endif /* #ifndef CHNET__NET__CHNET_THREAD_POOL_H */
