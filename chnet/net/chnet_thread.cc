
#include "chnet_thread.h"
#include <cstdio>
#include <cstdlib>
#include <cerrno>
#include <cstring>
#if !defined(WIN32)
#include <sys/socket.h>
#include <arpa/inet.h>
#endif
#include "net/base/net_errors.h"

namespace net {

static __thread CHNetThread *g_thread;

#ifndef WIN32
static int do_bind_src_addr4(int fd, const char *addr)
{
        struct sockaddr_in saddr;
        int ret = 0;

        memset(&saddr, 0, sizeof(saddr));
        saddr.sin_family = AF_INET;
        saddr.sin_port = 0;
        ret = inet_pton(AF_INET, addr, &saddr.sin_addr.s_addr);
        if (likely(ret == 1)) {
                ret = bind(fd, (struct sockaddr *)&saddr, sizeof(saddr));
                if (unlikely(ret))
                        ret = -errno;
        } else if (unlikely(ret == 0)) {
                ret = -EINVAL;
        }

        return ret;
}

static int do_bind_src_addr6(int fd, const char *addr)
{
        struct sockaddr_in6 saddr;
        int ret = 0;

        memset(&saddr, 0, sizeof(saddr));
        saddr.sin6_family = AF_INET6;
        saddr.sin6_port = 0;
        ret = inet_pton(AF_INET6, addr, &saddr.sin6_addr.s6_addr);
        if (likely(ret == 1)) {
                ret = bind(fd, (struct sockaddr *)&saddr, sizeof(saddr));
                if (unlikely(ret))
                        ret = -errno;
        } else if (unlikely(ret == 0)) {
                ret = -EINVAL;
        }

        return ret;
}

static int do_bind_src_addr(int fd, int ip_ver, const char *addr)
{
        int ret;

        if (addr[0] == '#')
                return ERR_ADDRESS_UNREACHABLE;

        switch (ip_ver) {
        case 4:
                ret = do_bind_src_addr4(fd, addr);
                break;
        case 6:
                ret = do_bind_src_addr6(fd, addr);
                break;
        default:
                CHECK(0 && "Wrong ip_ver!");
                __builtin_unreachable();
        }

        if (unlikely(ret)) {
                fprintf(stderr, "[ERROR INTERNAL CHNET]: Failed to bind the source IPv%d to \"%s\" (err: %s)\n",
                        ip_ver, addr, strerror(-ret));
                return ERR_CONNECTION_FAILED;
        }

        return 0;
}

static int do_bind_iface(int fd, int ip_ver, const char *iface)
{
        size_t len;
        int ret;

        if (iface[0] == '#')
                return ERR_ADDRESS_UNREACHABLE;

        len = strlen(iface) + 1;
        ret = setsockopt(fd, SOL_SOCKET, SO_BINDTODEVICE, iface, len);
        if (unlikely(ret)) {
                fprintf(stderr, "[ERROR INTERNAL CHNET]: Failed to bind the IPv%d network card to \"%s\" (err: %s)\n",
                        ip_ver, iface, strerror(errno));
                return ERR_CONNECTION_FAILED;
        }

        return 0;
}

static int chnet_connect_hook_ipv4(int fd, const struct sockaddr *daddr,
                                   socklen_t len, struct tbind_config *bc)
{
        int ret;

        if (bc->bind_ip4_) {
                ret = do_bind_src_addr(fd, 4, bc->bind_ip4_);
                if (unlikely(ret))
                        return ret;
        }

        if (bc->bind_iface4_) {
                ret = do_bind_iface(fd, 4, bc->bind_iface4_);
                if (unlikely(ret))
                        return ret;
        }

        return 0;
}

static int chnet_connect_hook_ipv6(int fd, const struct sockaddr *daddr,
                                   socklen_t len, struct tbind_config *bc)
{
        int ret;

        if (bc->bind_ip6_) {
                ret = do_bind_src_addr(fd, 6, bc->bind_ip6_);
                if (unlikely(ret))
                        return ret;
        }

        if (bc->bind_iface6_) {
                ret = do_bind_iface(fd, 6, bc->bind_iface6_);
                if (unlikely(ret))
                        return ret;
        }

        return 0;
}

static int chnet_connect_hook(int fd, const struct sockaddr *daddr,
                              socklen_t len)
{
        CHNetThread *t = g_thread;
        struct tbind_config *bc;
        unsigned short af;

        /*
         * We don't have any thread data. Skip.
         */
        if (unlikely(!t))
                return 0;

        /*
         * Only hook on IPv4 and IPv6 requests.
         */
        af = daddr->sa_family;
        bc = t->GetBindCfg();

        if (likely(af == AF_INET))
                return chnet_connect_hook_ipv4(fd, daddr, len, bc);

        if (likely(af == AF_INET6))
                return chnet_connect_hook_ipv6(fd, daddr, len, bc);

        return 0;
}

int chnet_connect_hook_sock(int sock_type, int fd, const struct sockaddr *daddr,
                            socklen_t len)
{
        /*
         * Only hook on TCP and UDP requests.
         */
        if (sock_type != SOCK_STREAM && sock_type != SOCK_DGRAM) {
                return 0;
        }

        return chnet_connect_hook(fd, daddr, len);
}
#endif

__always_inline std::string CHNetThread::GenThreadName(uint32_t idx)
{
        return std::string("chromium-") + std::to_string(idx);
}

static void SetGThread(Waiter *sig, CHNetThread *ct)
{
        g_thread = ct;
        sig->Signal();
}

CHNetThread *GetGThread(void)
{
        return g_thread;
}

CHNetThread::CHNetThread(uint32_t idx, CHNetThreadPool *ctp):
        thread_(GenThreadName(idx)),
        ctp_(ctp),
        idx_(idx),
        ref_count_(0)
{
        CHECK(static_cast<void *>(&thread_) == static_cast<void *>(this));
        base::Thread::Options options(base::MessagePumpType::IO, 0);
        CHECK(thread_.StartWithOptions(std::move(options)));

        auto *r = thread_.task_runner().get();
        Waiter sig;
        CHECK(r->PostTask(FROM_HERE,
                          base::BindOnce(SetGThread, &sig,
                                         base::Unretained(this))));
        sig.Wait();
}

void CHNetThread::PutThread(void)
{
        ctp_->PutThread(idx_);
}

const char *connect_bind_get_iface_key(void)
{
        __thread static char ret[256];
        CHNetThread *t = g_thread;

        if (unlikely(!t)) {
                return "##";
        }

        if (ret[0] == '\0') {
                struct tbind_config *bc = t->GetBindCfg();
                const char *iface4 = bc->bind_iface4_;
                const char *ip4 = bc->bind_ip4_;
                snprintf(ret, sizeof(ret), "%s#%s#", ip4 ? ip4 : "",
                         iface4 ? iface4 : "");
        }

        return ret;
}

} /* namespace net */

