
#include "chnet_payload.h"
#include "chnetx.h"

namespace net {

CHNetPayload::~CHNetPayload(void)
{
	free(payload_);
}

int CHNetPayload::InitInternal(const net::NetLogWithSource& net_log)
{
	SetSize(len_);
	return OK;
}

int CHNetPayload::ReadInternal(net::IOBuffer *buf, int buf_len)
{
	int remaining_size;
	int pos;
	int ret;

	pos = static_cast<int>(position());
	remaining_size = size() - pos;
	if (remaining_size < buf_len)
		ret = remaining_size;
	else
		ret = buf_len;

	memcpy(buf->data(), &payload_[pos], ret);
	return ret;
}

void CHNetPayload::ResetInternal(void)
{
}

CHNetPayloadChunked::CHNetPayloadChunked(CHNetx *ch):
	UploadDataStream(true, 0),
	ch_(ch)
{
	(void)ch_;
}


inline void CHNetPayloadChunked::ClearPendingQueue(void)
	__must_hold(&buf_queue_lock_)
{
	while (!buf_queue_.empty()) {
		struct ChunkVec &c = buf_queue_.front();

		if (c.buf_)
			free(c.buf_);

		buf_queue_.pop();
	}
}

CHNetPayloadChunked::~CHNetPayloadChunked(void)
{
	buf_queue_lock_.lock();
	ClearPendingQueue();
	buf_queue_lock_.unlock();
}

int CHNetPayloadChunked::InitInternal(const net::NetLogWithSource &net_log)
{
	return OK;
}

__always_inline bool CHNetPayloadChunked::__FlushBufferQueue(int *ret_p)
	__must_hold(&buf_queue_lock_)
{
	struct ChunkVec &c = buf_queue_.front();
	size_t copy_size;
	bool should_pop;

	if (c.len_ == (size_t)~0ul) {
		buf_queue_.pop();
		SetIsFinalChunk();
		return false;
	}

	if (c.len_ <= dst_len_) {
		copy_size = c.len_;
		should_pop = true;
	} else {
		copy_size = dst_len_;
		should_pop = false;
	}

	memcpy(dst_ptr_, c.buf_, copy_size);
	dst_ptr_ += copy_size;
	dst_len_ -= copy_size;
	copied_size_ += copy_size;
	*ret_p += static_cast<int>(copy_size);

	if (should_pop) {
		free(c.buf_);
		buf_queue_.pop();
	} else {
		c.len_ -= copy_size;
		memmove(c.buf_, &c.buf_[copy_size], c.len_);
	}

	if (!dst_len_) {
		dst_ptr_ = nullptr;
		return false;
	}

	return true;
}

inline int CHNetPayloadChunked::FlushBufferQueue(void)
	__must_hold(&buf_queue_lock_)
{
	int ret = 0;

	DCHECK(dst_ptr_);

	while (!buf_queue_.empty()) {
		if (!__FlushBufferQueue(&ret))
			break;
	}

	return ret;
}

int CHNetPayloadChunked::ReadInternal(net::IOBuffer *buf, int buf_len)
{
	int ret;

	buf_queue_lock_.lock();
	dst_ptr_ = buf->data();
	dst_len_ = buf_len;
	ret = FlushBufferQueue();
	buf_queue_lock_.unlock();

	if (ret < buf_len && !IsEOF())
		return ERR_IO_PENDING;

	return ret;
}

inline int CHNetPayloadChunked::EnqueueBuffer(const void *buf, size_t len)
	__must_hold(&buf_queue_lock_)
{
	char *tmp;

	tmp = static_cast<char *>(memdup(buf, len));
	if (unlikely(!tmp))
		return -ENOMEM;

	buf_queue_.emplace(tmp, len);
	return len;
}

int CHNetPayloadChunked::Write(const void *buf_, size_t len)
{
	const char *buf = static_cast<const char *>(buf_);
	size_t copy_size;
	int ret = 0;

	if (unlikely(!len))
		return 0;

	std::unique_lock<std::mutex> lock(buf_queue_lock_);

	/*
	 * The consumer is not ready, queue it.
	 */
	if (!dst_ptr_)
		return EnqueueBuffer(buf, len);

	/*
	 * @dst_ptr_ is not a nullptr. We can flush the
	 * buffer to the consumer now...
	 */
	FlushBufferQueue();

	/*
	 * If FlushBufferQueue() sets @dst_ptr_ to a nullptr,
	 * that means, the consumer has already consumed the
	 * buffer queue at its capacity. Thus, the read is
	 * completed.
	 *
	 * Queue this write buffer for the next Read().
	 */
	if (!dst_ptr_) {
		InvokeOnReadCompleted(copied_size_);
		return EnqueueBuffer(buf, len);
	}

	/*
	 * The buffer queue is empty and the consumer is ready
	 * for consuming more buffer.
	 */
	if (len <= dst_len_) {
		copy_size = len;
	} else {
		copy_size = dst_len_;
		ret = EnqueueBuffer(&buf[copy_size], len - copy_size);
		if (unlikely(ret < 0))
			return ret;
	}

	memcpy(dst_ptr_, buf, copy_size);
	dst_ptr_ += copy_size;
	dst_len_ -= copy_size;
	copied_size_ += copy_size;

	if (!dst_len_) {
		dst_ptr_ = nullptr;
		InvokeOnReadCompleted(copied_size_);
	}

	return len;
}

void CHNetPayloadChunked::StopWriting(void)
{
	buf_queue_lock_.lock();
	buf_queue_.emplace(nullptr, (size_t)~0ul);
	if (dst_ptr_) {
		FlushBufferQueue();
		InvokeOnReadCompleted(copied_size_);
	}
	buf_queue_lock_.unlock();
}

inline void CHNetPayloadChunked::InvokeOnReadCompleted(int result)
{
	CHECK(ch_->PostTask(&CHNetPayloadChunked::OnReadCompleted,
			    base::Unretained(this), result));
}

void CHNetPayloadChunked::ResetInternal(void)
{
	buf_queue_lock_.lock();
	dst_ptr_ = nullptr;
	dst_len_ = 0;
	copied_size_ = 0;
	buf_queue_lock_.unlock();
}

} /* namespace net */
