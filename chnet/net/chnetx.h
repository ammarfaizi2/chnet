
#ifndef CHNET__NET__CHNET_H
#define CHNET__NET__CHNET_H

#include "../common.h"
#include "chnet_config.h"
#include "base/at_exit.h"
#include "base/command_line.h"
#include "base/message_loop/message_pump_type.h"
#include "net/base/io_buffer.h"
#include "net/base/net_errors.h"
#include "net/base/request_priority.h"
#include "net/dns/public/secure_dns_policy.h"
#include "net/url_request/url_request_context_builder.h"
#include "net/url_request/url_request_context.h"
#include "net/url_request/url_request.h"

#include "chnet_thread.h"
#include "chnet_thread_pool.h"
#include "ureq_ctx_pool.h"
#include "chnet_payload.h"

namespace net {

enum {
        CHNET_PAYLOAD_SINGLE	= 1,
        CHNET_PAYLOAD_CHUNKED	= 2,
};

template<typename T>
using fc_t = std::function<T>;

class CHNetx: public URLRequest::Delegate {

public:
        CHNetx(CHNetThread *thread);

        ~CHNetx(void) override;

        void SetURL(const char *url);

        void SetMethod(const char *method);

        void SetPayload(const void *p, size_t len);

        void SetRequestHeader(const std::string &key, const std::string &val,
                              bool overwrite = true);

        void Start(void);

        void _Read(int size, int *ret, Waiter *sig = nullptr);

        int Read(int len);

        void StartChunkedPayload(void);

        int Write(const void *buf, size_t len);

        void StopChunkedPayload(void);

        /*
         * Called each time a connection is obtained, before any data is sent.
         *
         * |ureq| is never nullptr. Caller retains ownership.
         *
         * |info| describes the newly-obtained connection.
         *
         * This may be called several times if the request creates multiple
         * HTTP transactions, e.g. if the request is redirected. It may also be
         * called several times per transaction, e.g. if the connection is
         * retried, after each HTTP auth challenge, or for split HTTP range
         * requests.
         *
         * If this returns an error, the transaction will stop. The transaction
         * will continue when the |callback| is run. If run with an error, the
         * transaction will fail.
         */
        int OnConnected(URLRequest *ureq, const TransportInfo& info,
                        CompletionOnceCallback callback) override;

        /*
         * Called upon receiving a redirect.  The delegate may call the
         * request's Cancel method to prevent the redirect from being followed.
         * Since there may be multiple chained redirects, there may also be more
         * than one redirect call.
         *
         * When this function is called, the request will still contain the
         * original URL, the destination of the redirect is provided in
         * |redirect_info.new_url|.  If the delegate does not cancel the request
         * and |*defer_redirect| is false, then the redirect will be followed,
         * and the request's URL will be changed to the new URL.  Otherwise if
         * the delegate does not cancel the request and |*defer_redirect| is
         * true, then the redirect will be followed once FollowDeferredRedirect
         * is called on the URLRequest.
         *
         * The caller must set |*defer_redirect| to false, so that delegates do
         * not need to set it if they are happy with the default behavior of not
         * deferring redirect.
         */
        void OnReceivedRedirect(URLRequest *ureq,
                                const RedirectInfo& redirect_info,
                                bool *defer_redirect) override;

        /*
         * After calling Start(), the delegate will receive an OnResponseStarted
         * callback when the request has completed. |net_error| will be set to
         * OK or an actual net error.  On success, all redirects have been
         * followed and the final response is beginning to arrive.  At this
         * point, meta data about the response is available, including for
         * example HTTP response headers if this is a request for a HTTP
         * resource.
         */
        void OnResponseStarted(URLRequest *ureq, int net_error) override;

        /*
         * Called when the a Read of the response body is completed after an
         * IO_PENDING status from a Read() call.
         * The data read is filled into the buffer which the caller passed
         * to Read() previously.
         *
         * If an error occurred, |bytes_read| will be set to the error.
         */
        void OnReadCompleted(URLRequest *ureq, int bytes_read) override;

        void OnSSLCertificateError(URLRequest* request, int net_error,
                                   const SSLInfo& ssl_info, bool fatal) override;

        inline void SetFollowRedirect(bool r)
        {
                follow_redirect_ = r;
        }

        inline base::Thread *thread(void)
        {
                return thread_->thread();
        }

        inline auto *task_runner(void)
        {
                return thread()->task_runner().get();
        }

        inline auto *r(void)
        {
                return task_runner();
        }

        template <typename T, typename... Types>
        inline bool PostTask(T t, Types... u)
        {
                return r()->PostTask(FROM_HERE, base::BindOnce(t, u...));
        }

        inline bool has_req_started(void)
        {
                return has_req_started_.load(std::memory_order_acquire);
        }

        inline const char *GetReadBuffer(void)
        {
                return read_buf_->data();
        }

        inline int GetReadRet(void)
        {
                return read_ret_.load(std::memory_order_relaxed);
        }

        inline void LockResponseHeader(void)
                __acquires(&res_hdr_lock_)
        {
                res_hdr_lock_.lock();
        }

        inline void UnlockResponseHeader(void)
                __releases(&res_hdr_lock_)
        {
                res_hdr_lock_.unlock();
        }

        inline URLRequestContext *ureq_ctx(void)
        {
                return ureq_ctx_->ureq_ctx();
        }

        bool GetResponseHeader(size_t *iter, const char **key,
                               const char **val);

        inline void SetChromiumError(int code)
        {
                SetError("chromium_net_err:" + ErrorToString(code));
        }

        inline void SetError(const std::string &err)
        {
                err_ = err;
        }

        inline bool IsPayloadChunked(void)
        {
                return !!payload_chunked_p_;
        }

        inline URLRequest *ureq(void)
        {
                if (is_cancelled_)
                        return nullptr;

                return ureq_.get();
        }

        inline const RedirectInfo *GetRedirectInfo(void)
        {
                return redirect_info_.get();
        }

        inline void _Cancel(void)
        {
                if (!ureq_ || !has_req_started_)
                        return;

                ureq_->Cancel();
        }

        inline void Cancel(void)
        {
                if (!ureq_ || !has_req_started_)
                        return;

                PostTask(&CHNetx::_Cancel, base::Unretained(this));
                is_cancelled_ = true;
        }

private:
        std::string				url_;
        std::string				method_;
        UReqCtx					*ureq_ctx_ = nullptr;
        std::unique_ptr<URLRequest>		ureq_;
        CHNetThread				*thread_;

public:
        scoped_refptr<IOBufferWithSize>		read_buf_ = nullptr;
        std::string				err_;

private:
        std::mutex				res_hdr_lock_;
        std::vector<std::vector<std::string>>	*res_hdr_ = nullptr;
        std::unique_ptr<CHNetPayload>		payload_single_;
        std::unique_ptr<CHNetPayloadChunked>	payload_chunked_;
        CHNetPayloadChunked			*payload_chunked_p_ = nullptr;

        std::unique_ptr<RedirectInfo>		redirect_info_;
        std::atomic<int>			read_ret_;
        uint8_t					payload_type_ = 0;
        bool					url_is_set_ = false;
        bool					is_read_allowed_ = false;
        bool					follow_redirect_ = false;
        bool					is_redirected_ = false;
        bool                                    is_cancelled_ = false;
        std::atomic<bool>			has_req_started_ = false;

        void DestroyCtx(Waiter *sig);

        void DestroyCallback(void);

        void _Start(void);

        void BuildURLRequest(bool with_post_task = true, Waiter *sig = nullptr);
        void CollectResponseHeaders(URLRequest *ureq);
        void HandleDeferredReadDueToChunkedPayload(int size);
        int __Read(int size);

public:
        fc_t<int(URLRequest *ureq, const TransportInfo& info,
                 CompletionOnceCallback &callback)> connected_ = nullptr;

        fc_t<void(URLRequest *ureq, const RedirectInfo& redirect_info,
                  bool *defer_redirect)> received_redirect_ = nullptr;

        fc_t<void(URLRequest *ureq, int net_error)>
                response_started_ = nullptr;

        fc_t<void(URLRequest *ureq, int bytes_read)>
                read_completed_ = nullptr;

};

} /* namespace net */

#endif /* #ifndef CHNET__NET__CHNET_H */
