
#ifndef CHNET__NET__DNS_REQ_H
#define CHNET__NET__DNS_REQ_H

#include <stack>
#include <mutex>
#include <vector>
#include "net/dns/host_resolver.h"
#include "net/dns/resolve_context.h"
#include "net/dns/context_host_resolver.h"
#include "net/dns/host_resolver_manager.h"
#include "net/base/completion_once_callback.h"

namespace net {

struct DnsReqQueue;
class DnsReq;

using DnsReqCallback = base::OnceCallback<void(struct DnsReqQueue *, void *data)>;

struct DnsReqQueue {
        std::unique_ptr<HostResolver::ResolveHostRequest> req_;
        DnsReqCallback callback_;
        DnsReq *dns_req_;
        void *data_;
        uint32_t idx_;
        int err_ = -1;

        DnsReqQueue(void);
        ~DnsReqQueue(void);
        void Complete(int err);
        CompletionOnceCallback GetCompletCallback(void);
};

class DnsReq {
public:
        DnsReq(void);
        ~DnsReq(void);
        int64_t Resolve(const char *hostname, DnsReqCallback cb, void *data);
        static DnsReq *Get(void);
        void Put(uint32_t idx);

private:
        void __Resolve(const char *hostname, uint32_t idx, DnsReqCallback cb,
                       void *data);

        std::unique_ptr<HostResolverManager>    manager_;
        std::unique_ptr<ContextHostResolver>    resolver_;

        std::mutex                      lock_;
        std::stack<uint32_t>            free_slot_;
        struct DnsReqQueue              *requests_;
        constexpr static uint32_t       MAX_SLOT = 1024;
};

} // namespace net

#endif // CHNET__NET__DNS_REQ_H
