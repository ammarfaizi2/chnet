
#define __FOR_CHROMIUM_INTERNAL
#include "dns_map.h"

void g_chnet_dns_map_lock(void)
{
        chnet_dns_map_lock();
}

void g_chnet_dns_map_unlock(void)
{
        chnet_dns_map_unlock();
}

void g_chnet_dns_map_insert(const char *host, const char *ip)
{
        chnet_dns_map_insert(host, ip);
}

void g___chnet_dns_map_insert(const char *host, const char *ip)
{
        __chnet_dns_map_insert(host, ip);
}

bool g_chnet_dns_map_lookup(struct chnet_dns_lookup *lk)
{
        return chnet_dns_map_lookup(lk);
}

bool g___chnet_dns_map_lookup(struct chnet_dns_lookup *lk)
{
        return __chnet_dns_map_lookup(lk);
}

void g_chnet_dns_map_remove(const char *host, const char *ip)
{
        chnet_dns_map_remove(host, ip);
}

void g___chnet_dns_map_remove(const char *host, const char *ip)
{
        __chnet_dns_map_remove(host, ip);
}

void g_chnet_dns_map_remove_all(const char *host)
{
        chnet_dns_map_remove_all(host);
}

void g___chnet_dns_map_remove_all(const char *host)
{
        __chnet_dns_map_remove_all(host);
}

void g_chnet_dns_map_pop(const char *host)
{
        chnet_dns_map_pop(host);
}

void g___chnet_dns_map_pop(const char *host)
{
        __chnet_dns_map_pop(host);
}
