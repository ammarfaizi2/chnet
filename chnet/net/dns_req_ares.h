
#ifndef CHNET__NET__DNS_REQ_ARES_H
#define CHNET__NET__DNS_REQ_ARES_H

#include <ares.h>
#include <ares_dns.h>
#include <threads.h>

#ifdef __cplusplus
extern "C" {
#endif

struct ares_thread {
        ares_channel	channel;
        fd_set		readers;
        fd_set		writers;
        uint32_t	queue;
        thrd_t		thread;
        mtx_t		lock;
        cnd_t		cond;
        volatile bool	should_stop;
        volatile bool	need_wakeup;
};

void t_ares_getaddrinfo(struct ares_thread *at, const char *node,
                        const char *service,
                        const struct ares_addrinfo_hints *hints,
                        ares_addrinfo_callback callback, void *arg);

struct ares_thread *create_ares_thread(void);
void destroy_ares_thread(struct ares_thread *at);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* #ifndef CHNET__NET__DNS_REQ_ARES_H */
