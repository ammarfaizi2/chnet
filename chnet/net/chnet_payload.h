
#ifndef CHNET__NET__CHNET_PAYLOAD_H
#define CHNET__NET__CHNET_PAYLOAD_H

#include "../common.h"
#include "net/base/io_buffer.h"
#include "net/base/net_errors.h"
#include "net/base/upload_data_stream.h"

#include <mutex>
#include <queue>

namespace net {

class CHNetx;

class CHNetPayload: public UploadDataStream {
public:
        inline CHNetPayload(const void *payload, size_t len):
                UploadDataStream(false, 0)
        {
                len_ = len;
                payload_ = static_cast<const char *>(memdup(payload, len));
                CHECK(likely(payload_));
        }

        ~CHNetPayload(void) override;

private:
        const char	*payload_;
        size_t		len_;

        int InitInternal(const net::NetLogWithSource &net_log) override;

        int ReadInternal(IOBuffer *buf, int buf_len) override;

        void ResetInternal(void) override;
};

struct ChunkVec {
        char	*buf_;
        size_t	len_;

        ChunkVec(void) = default;

        inline ChunkVec(char *buf, size_t len):
                buf_(buf),
                len_(len)
        {
        }
};

class CHNetPayloadChunked: public UploadDataStream {
public:
        CHNetPayloadChunked(CHNetx *ch);

        ~CHNetPayloadChunked(void) override;

        int Write(const void *buf, size_t len);

        void StopWriting(void);

private:
        char				*dst_ptr_ = nullptr;
        size_t				dst_len_ = 0;
        size_t				copied_size_ = 0;
        CHNetx				*ch_;
        std::queue<struct ChunkVec>	buf_queue_;
        std::mutex			buf_queue_lock_;

        int InitInternal(const net::NetLogWithSource &net_log) override;

        int ReadInternal(IOBuffer *buf, int buf_len) override;

        void ResetInternal(void) override;

        int FlushBufferQueue(void);

        bool __FlushBufferQueue(int *ret_p);

        int EnqueueBuffer(const void *buf, size_t len);

        void InvokeOnReadCompleted(int result);

        void ClearPendingQueue(void);
};

} /* namespace net */

#endif /* #ifndef CHNET__NET__CHNET_PAYLOAD_H */
