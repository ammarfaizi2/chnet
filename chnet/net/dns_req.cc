
#include "dns_req.h"
#include "net/log/net_log.h"
#include "net/dns/public/host_resolver_results.h"
#include "base/task/thread_pool/thread_pool_instance.h"
#include "base/threading/thread.h"
#include "base/threading/thread_task_runner_handle.h"
#include "../common.h"

#include <mutex>

namespace net {

static base::Thread *g_dns_thread = nullptr;
static DnsReq *g_dns_req = nullptr;
static std::mutex g_lock;

static void init_dns_thread(void)
{
        g_dns_thread = new base::Thread("dns_thread");
        base::Thread::Options options(base::MessagePumpType::IO, 0);
        CHECK(g_dns_thread->StartWithOptions(std::move(options)));
}

static void __init_g_dns_req(Waiter *sig)
{
        g_dns_req = new DnsReq();
        sig->Signal();
}

static void init_g_dns_req(void)
{
        Waiter sig;
        bool ret;

        init_dns_thread();
        ret = g_dns_thread
                ->task_runner()
                ->PostTask(FROM_HERE, base::BindOnce(__init_g_dns_req, &sig));

        CHECK(ret);
        sig.Wait();
}

// static
DnsReq *DnsReq::Get(void)
{
        std::unique_lock<std::mutex> l(g_lock);

        if (unlikely(!g_dns_req)) {
                init_g_dns_req();
        }

        return g_dns_req;
}

DnsReq::DnsReq(void):
        manager_(std::make_unique<HostResolverManager>(
                        HostResolver::ManagerOptions(), nullptr, nullptr)),
        resolver_(std::make_unique<ContextHostResolver>(
                manager_.get(), std::make_unique<ResolveContext>(nullptr,
                                        true /* enable_caching */)))
{
        uint32_t i = MAX_SLOT;

        requests_ = new DnsReqQueue[i];
        while (i--) {
                free_slot_.push(i);
        }
}

void DnsReq::Put(uint32_t idx)
{
        std::unique_lock<std::mutex> l(lock_);

        free_slot_.push(idx);
}

inline DnsReqQueue::DnsReqQueue(void)
{
}

inline DnsReqQueue::~DnsReqQueue(void)
{
}

inline void DnsReqQueue::Complete(int err)
{
        this->err_ = err;
        if (callback_) {
                std::move(callback_).Run(this, data_);
        }

        data_ = nullptr;
        this->req_.reset();
        dns_req_->Put(this->idx_);
}

inline CompletionOnceCallback DnsReqQueue::GetCompletCallback(void)
{
        return base::BindOnce(&DnsReqQueue::Complete, base::Unretained(this));
}

void DnsReq::__Resolve(const char *hostname, uint32_t idx, DnsReqCallback cb,
                       void *data)
{
        struct DnsReqQueue *rq = &requests_[idx];
        int ret;

        rq->callback_ = std::move(cb);
        rq->dns_req_ = this;
        rq->data_ = data;
        rq->idx_ = idx;
        rq->req_ = resolver_->CreateRequest(
                        url::SchemeHostPort(url::kHttpsScheme, hostname, 0),
                        NetworkAnonymizationKey(), NetLogWithSource(),
                        absl::nullopt);

        ret = rq->req_->Start(rq->GetCompletCallback());
        if (ret != ERR_IO_PENDING) {
                rq->Complete(ret);
        }
}

int64_t DnsReq::Resolve(const char *hostname, DnsReqCallback cb, void *data)
{
        std::unique_lock<std::mutex> l(lock_);
        uint32_t ret;
        bool tmp;

        if (unlikely(free_slot_.empty())) {
                return -EAGAIN;
        }

        ret = free_slot_.top();
        free_slot_.pop();
        tmp = g_dns_thread
                ->task_runner()
                ->PostTask(FROM_HERE, base::BindOnce(&DnsReq::__Resolve,
                                        base::Unretained(this), hostname, ret,
                                        std::move(cb), data));
        if (unlikely(!tmp)) {
                free_slot_.push(ret);
                return -EAGAIN;
        }

        return static_cast<int64_t>(ret);
}

DnsReq::~DnsReq(void)
{
        delete[] requests_;
}

} // namespace net
