
#ifndef CHNET__NET__CHNET_THREAD_H
#define CHNET__NET__CHNET_THREAD_H

#include "base/task/thread_pool/thread_pool_instance.h"
#include "base/threading/thread.h"
#include "base/threading/thread_task_runner_handle.h"
#include "chnet_thread_pool.h"
#include "chnet_config.h"
#include "../common.h"

#ifndef WIN32
extern int (*socket_posix_connect_hook)(int sock_type, int fd,
                                        const struct sockaddr *daddr,
                                        socklen_t addrlen);

extern const char *(*connect_bind_get_iface_key)(void);
#endif

namespace net {
#ifndef WIN32
int chnet_connect_hook_sock(int sock_type, int fd, const struct sockaddr *daddr,
                            socklen_t len);
const char *connect_bind_get_iface_key(void);
#endif
class CHNetThread {
public:
        CHNetThread(uint32_t idx, CHNetThreadPool *ctp);

        void PutThread(void);

        inline base::Thread *thread(void)
        {
                return &thread_;
        }

        inline struct tbind_config *GetBindCfg(void)
        {
                return ctp_->GetBindCfg();
        }

private:
        std::string GenThreadName(uint32_t idx);

        base::Thread		thread_;
        CHNetThreadPool		*ctp_;
        uint32_t		idx_;
public:
        uint32_t		ref_count_;
};

CHNetThread *GetGThread(void);

} /* namespace net */

#endif /* #ifndef CHNET__NET__CHNET_THREAD_H */
