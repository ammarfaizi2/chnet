
#include "chnetx.h"
#include "ureq_ctx_pool.h"

#include <cstdlib>
#include <cstring>

namespace net {

static constexpr net::NetworkTrafficAnnotationTag traffic_annotation =
net::DefineNetworkTrafficAnnotation("CHNetDelegate", R"(
        semantics {
                sender: "CHNetDelegate."
                description: "CHNetDelegate."
                trigger: "CHNetDelegate."
                data: "CHNetDelegate."
                destination: OTHER
        }
        policy {
                cookies_allowed: YES
                setting: "CHNetDelegate."
                policy_exception_justification: "Not implemented."
        })");


CHNetx::CHNetx(CHNetThread *thread):
        method_("GET"),
        thread_(thread),
        err_("")
{
        read_ret_.store(ERR_IO_PENDING, std::memory_order_relaxed);
}

inline void CHNetx::DestroyCtx(Waiter *sig)
{
        if (ureq_)
                ureq_->Cancel();

        ureq_.reset();
        PutUReqCtx(ureq_ctx_);
        sig->Signal();
}

CHNetx::~CHNetx(void)
{
        LockResponseHeader();
        if (res_hdr_) {
                delete res_hdr_;
                res_hdr_ = nullptr;
        }
        UnlockResponseHeader();

        if (ureq_ctx_) {
                Waiter sig;
                CHECK(PostTask(&CHNetx::DestroyCtx, base::Unretained(this),
                               &sig));
                sig.Wait();
        }

        thread_->PutThread();
}

void CHNetx::SetURL(const char *url)
{
        url_ = url;
        url_is_set_ = true;
}

void CHNetx::SetMethod(const char *method)
{
        method_ = method;
}

inline void CHNetx::BuildURLRequest(bool with_post_task, Waiter *sig)
{
        if (with_post_task) {
                Waiter qsig;
                CHECK(PostTask(&CHNetx::BuildURLRequest, base::Unretained(this),
                               false, &qsig));
                qsig.Wait();
                return;
        }

        ureq_ctx_ = GetUReqCtx();
        ureq_ = ureq_ctx()->CreateRequest(GURL(url_), DEFAULT_PRIORITY, this,
                                          traffic_annotation, false);
        ureq_->set_allow_credentials(false);
        if (sig)
                sig->Signal();
}

void CHNetx::SetRequestHeader(const std::string &key, const std::string &val,
                              bool overwrite)
{
        if (!ureq_)
                BuildURLRequest(true);

        ureq_->SetExtraRequestHeaderByName(key, val, overwrite);
}

inline void CHNetx::_Start(void)
{
        if (!ureq_)
                BuildURLRequest(false);

        ureq_->set_method(method_);
        switch (payload_type_) {
        case CHNET_PAYLOAD_SINGLE:
                ureq_->set_upload(std::move(payload_single_));
                break;
        case CHNET_PAYLOAD_CHUNKED:
                ureq_->set_upload(std::move(payload_chunked_));
                break;
        default:
                break;
        }

        ureq_->Start();
        has_req_started_.store(true);
}

void CHNetx::Start(void)
{
        CHECK(PostTask(&CHNetx::_Start, base::Unretained(this)));
}

int CHNetx::__Read(int size)
{
        int ret;

        read_buf_ = base::MakeRefCounted<net::IOBufferWithSize>(size);
        ret = ureq_->Read(read_buf_.get(), size);
        read_ret_.store(ret);
        return ret;
}

inline void CHNetx::HandleDeferredReadDueToChunkedPayload(int size)
{
        auto pc = std::move(response_started_);
        response_started_ = [=, pc = std::move(pc)](auto *ureq, int err){
                int ret;

                if (pc)
                        std::move(pc)(ureq, err);

                if (err != OK) {
                        ret = err;
                } else {
                        ret = __Read(size);
                        is_read_allowed_ = true;
                }

                if (ret != ERR_IO_PENDING)
                        OnReadCompleted(this->ureq_.get(), ret);
        };
}

void CHNetx::_Read(int size, int *ret_p, Waiter *sig)
{
        if (!is_read_allowed_) {
                /*
                 * When we perform an HTTP request with a chunked
                 * payload, we can't perform the read until the
                 * OnResponseStarted() callback is invoked.
                 */
                CHECK(IsPayloadChunked());
                HandleDeferredReadDueToChunkedPayload(size);
                *ret_p = ERR_IO_PENDING;
        } else {
                *ret_p = __Read(size);
        }

        /*
         * Tell the caller, we have finished.
         */
        if (sig)
                sig->Signal();
}

int CHNetx::Read(int size)
{
        Waiter sig;
        int ret;

        CHECK(PostTask(&CHNetx::_Read, base::Unretained(this), size, &ret,
                       &sig));

        sig.Wait();
        return ret;
}

int CHNetx::OnConnected(URLRequest *ureq, const TransportInfo &info,
                        CompletionOnceCallback callback)
{
        read_ret_.store(ERR_IO_PENDING, std::memory_order_release);
        if (connected_)
                return std::move(connected_)(ureq, info, callback);

        return OK;
}

inline void CHNetx::CollectResponseHeaders(URLRequest *ureq)
{
        HttpResponseHeaders *res_hdr;

        res_hdr = ureq->response_headers();
        if (res_hdr) {
                size_t iter;

                LockResponseHeader();
                res_hdr_ = new __typeof__(*res_hdr_);
                std::string key;
                std::string val;
                iter = 0;

                while (res_hdr->EnumerateHeaderLines(&iter, &key, &val))
                        res_hdr_->push_back({key, val});
                UnlockResponseHeader();
        }
}

void CHNetx::OnReceivedRedirect(URLRequest *ureq,
                                const RedirectInfo &redirect_info,
                                bool *defer_redirect)
{
        CollectResponseHeaders(ureq);

        redirect_info_ = std::make_unique<RedirectInfo>();
        *redirect_info_ = redirect_info;
        is_redirected_ = true;
        if (!follow_redirect_) {
                *defer_redirect = true;
                ureq->Cancel();
        }

        if (received_redirect_)
                std::move(received_redirect_)(ureq, redirect_info,
                                              defer_redirect);
}

void CHNetx::OnResponseStarted(URLRequest *ureq, int net_error)
{
        read_ret_.store(net_error, std::memory_order_release);
        if (net_error) {
                SetChromiumError(net_error);
        } else  {
                CollectResponseHeaders(ureq);
                is_read_allowed_ = true;
        }

        if (response_started_)
                std::move(response_started_)(ureq, net_error);
}

void CHNetx::OnReadCompleted(URLRequest *ureq, int bytes_read)
{
        if (bytes_read < 0)
                SetChromiumError(bytes_read);

        read_ret_.store(bytes_read);
        if (read_completed_)
                std::move(read_completed_)(ureq, bytes_read);
}

bool CHNetx::GetResponseHeader(size_t *iter, const char **key, const char **val)
        __must_hold(&res_hdr_lock_)
{
        if (unlikely(!res_hdr_))
                return false;
        if (unlikely(*iter >= res_hdr_->size()))
                return false;

        *key = (*res_hdr_)[*iter][0].c_str();
        *val = (*res_hdr_)[*iter][1].c_str();
        (*iter)++;
        return true;
}

void CHNetx::SetPayload(const void *p, size_t len)
{
        payload_type_ = CHNET_PAYLOAD_SINGLE;
        payload_single_ = std::make_unique<CHNetPayload>(p, len);
}

void CHNetx::StartChunkedPayload(void)
{
        payload_type_ = CHNET_PAYLOAD_CHUNKED;
        payload_chunked_ = std::make_unique<CHNetPayloadChunked>(this);
        payload_chunked_p_ = payload_chunked_.get();
}

int CHNetx::Write(const void *buf, size_t len)
{
        if (unlikely(!payload_chunked_p_))
                return -EINVAL;

        return payload_chunked_p_->Write(buf, len);
}

void CHNetx::StopChunkedPayload(void)
{
        if (unlikely(!payload_chunked_p_))
                return;

        payload_chunked_p_->StopWriting();
}

void CHNetx::OnSSLCertificateError(URLRequest* request, int net_error,
                                   const SSLInfo& ssl_info, bool fatal)
{
        request->ContinueDespiteLastError();
}

} /* namespace net */
