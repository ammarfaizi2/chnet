

#ifndef CHNET__NET__CHNET_CONFIG_H
#define CHNET__NET__CHNET_CONFIG_H

#include "../common.h"

#ifdef __FOR_CHROMIUM_INTERNAL
#include "net/base/io_buffer.h"
#endif

struct tbind_config {
        const char *bind_ip4_;
        const char *bind_iface4_;
        const char *bind_ip6_;
        const char *bind_iface6_;
        const char *proxy_;
};

struct chnet_buf_ptr {
public:
        chnet_buf_ptr(void);
        ~chnet_buf_ptr(void);

        void    *ptr_;
        size_t  len_;
#ifdef __FOR_CHROMIUM_INTERNAL
        scoped_refptr<net::IOBufferWithSize>	__read_buf;
        char    __pad[48 - sizeof(scoped_refptr<net::IOBufferWithSize>)];
#else
        char    pad_[48];
#endif
};

#endif /* #ifndef CHNET__NET__CHNET_CONFIG_H */
