
#include "net/proxy_resolution/proxy_config.h"
#include "net/proxy_resolution/proxy_config_service_fixed.h"
#include "chnet_thread.h"

#include "../common.h"
#include "ureq_ctx_pool.h"
#include <stack>
#include <cstdlib>

namespace net {

#define MAX_REQ_PER_CTX 8

class UReqCtxPool {
public:
        UReqCtxPool(void);
        ~UReqCtxPool(void);
        UReqCtx *Get(void);
        void Put(UReqCtx *ureq_ctx);

private:
        void ResizePoolUp(void);

        UReqCtx                 **pool_arr_ = nullptr;
        size_t                  nr_ = 0;
        std::stack<uint32_t>    free_idx_;

public:
        uint32_t                ref_ = 0;
};

static __thread std::mutex g_uctx_lock;
static __thread UReqCtxPool *g_uctx_pool = nullptr;

constexpr NetworkTrafficAnnotationTag kAnnot =
    DefineNetworkTrafficAnnotation("proxy_config_direct", R"(
    semantics {
      sender: "Proxy Config"
      description:
        "Direct connections are being used instead of a proxy. This is a place "
        "holder annotation that would include details about where the "
        "configuration, which can trigger fetching a PAC file, came from."
      trigger:
        "Connecting directly to destination sites instead of using a proxy is "
        "the default behavior."
      data:
        "None."
      destination: WEBSITE
    }
    policy {
      cookies_allowed: NO
      setting:
        "This isn't a real network request. A proxy can be selected in "
        "settings."
      policy_exception_justification:
        "Using 'ProxySettings' policy can set Chrome to use specific proxy "
        "settings and avoid directly connecting to the websites."
    })");

inline UReqCtx::UReqCtx(void)
        __must_hold(&g_uctx_lock)
{
        URLRequestContextBuilder uctx_b;
        CHNetThread *ct = GetGThread();
        struct tbind_config *bind = ct->GetBindCfg();
        ProxyConfig pc;
        if (bind && bind->proxy_) {
                pc.proxy_rules().ParseFromString(bind->proxy_);
        }
        ProxyConfigWithAnnotation pcwa(pc, kAnnot);

        /*
         * On Linux, use a fixed ProxyConfigService, since the default one
         * depends on glib.
         *
         * TODO(akalin): Remove this once http://crbug.com/146421 is fixed.
         */
        uctx_b.set_proxy_config_service(
                std::make_unique<ProxyConfigServiceFixed>(pcwa));

        ureq_ctx_ = uctx_b.Build();
}

inline UReqCtx::~UReqCtx(void)
{
}

inline UReqCtxPool::UReqCtxPool(void)
        __must_hold(&g_uctx_lock)
{
        uint32_t i;

        nr_ = 256;
        pool_arr_ = static_cast<UReqCtx **>(calloc(nr_, sizeof(*pool_arr_)));
        CHECK(pool_arr_);

        i = nr_;
        while (i--)
                free_idx_.push(i);
}

inline UReqCtxPool::~UReqCtxPool(void)
        __must_hold(&g_uctx_lock)
{
        uint32_t i;

        i = nr_;
        while (i--) {
                if (!pool_arr_[i])
                        continue;

                CHECK(pool_arr_[i]->ref_ == 0);
                delete pool_arr_[i];
                pool_arr_[i] = nullptr;
        }
        free(pool_arr_);
}

inline void UReqCtxPool::ResizePoolUp(void)
        __must_hold(&g_uctx_lock)
{
        uint32_t old_nr = nr_;
        uint32_t i;
        UReqCtx **tmp;

        nr_ *= 2;
        tmp = static_cast<UReqCtx **>(realloc(pool_arr_, nr_ * sizeof(*tmp)));
        CHECK(likely(tmp));

        memset(&tmp[old_nr], 0, sizeof(*tmp) * (nr_ - old_nr));

        for (i = nr_ - 1; i >= old_nr; i--)
                free_idx_.push(i);

        pool_arr_ = tmp;
}

inline UReqCtx *UReqCtxPool::Get(void)
        __must_hold(&g_uctx_lock)
{
        uint32_t idx;
        UReqCtx *ret;

        if (unlikely(free_idx_.empty()))
                ResizePoolUp();

        idx = free_idx_.top();
        ret = pool_arr_[idx];
        if (!ret) {
                ret = new UReqCtx;
                ret->idx_ = idx;
                pool_arr_[idx] = ret;
        }
        ret->ref_++;

        if (ret->ref_ == MAX_REQ_PER_CTX)
                free_idx_.pop();

        return ret;
}

inline void UReqCtxPool::Put(UReqCtx *ureq_ctx)
        __must_hold(&g_uctx_lock)
{
        uint32_t ref = ureq_ctx->ref_--;
        uint32_t idx = ureq_ctx->idx_;

        if (ref == MAX_REQ_PER_CTX)
                free_idx_.push(idx);
}

UReqCtx *GetUReqCtx(void)
{
        UReqCtx *ret;

        g_uctx_lock.lock();
        if (!g_uctx_pool)
                g_uctx_pool = new UReqCtxPool;

        ret = g_uctx_pool->Get();
        g_uctx_pool->ref_++;
        g_uctx_lock.unlock();
        return ret;
}

void PutUReqCtx(UReqCtx *ureq_ctx)
{
        UReqCtxPool *to_delete = nullptr;

        g_uctx_lock.lock();
        CHECK(g_uctx_pool);
        CHECK(g_uctx_pool->ref_ > 0);
        CHECK(ureq_ctx->ref_ > 0);
        g_uctx_pool->Put(ureq_ctx);
        if (--g_uctx_pool->ref_ == 0) {
                to_delete = g_uctx_pool;
                g_uctx_pool = nullptr;
        }
        g_uctx_lock.unlock();

        if (to_delete)
                delete to_delete;
}

} /* namespace net */
