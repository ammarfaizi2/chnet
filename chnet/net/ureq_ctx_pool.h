
#ifndef CHNET__NET__UREQ_CTX_POOL_H
#define CHNET__NET__UREQ_CTX_POOL_H

#include "net/url_request/url_request_context_builder.h"
#include "net/url_request/url_request_context.h"
#include "net/url_request/url_request.h"

namespace net {

class UReqCtx {
public:
        UReqCtx(void);
        ~UReqCtx(void);

        inline URLRequestContext *ureq_ctx(void)
        {
                return ureq_ctx_.get();
        }

private:
        std::unique_ptr<URLRequestContext>      ureq_ctx_;

public:
        uint32_t        ref_ = 0;
        uint32_t        idx_;
};

UReqCtx *GetUReqCtx(void);
void PutUReqCtx(UReqCtx *ureq_ctx);

} /* namespace net */

#endif /* #ifndef CHNET__NET__UREQ_CTX_POOL_H */
