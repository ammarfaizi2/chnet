
#ifndef CHNET__NET__DNS_MAP_H
#define CHNET__NET__DNS_MAP_H

#ifdef __FOR_CHROMIUM_INTERNAL
#include "net/dns/chnet_dns_map.h"
#endif

#include "../common.h"

extern "C" {

#ifndef __FOR_CHROMIUM_INTERNAL
struct chnet_dns_lookup {
        void		*vec;
        const char	*host;
        const char	*ip;
        size_t		iter;
};
#endif

CHNET_EXPORT void g_chnet_dns_map_lock(void);
CHNET_EXPORT void g_chnet_dns_map_unlock(void);

CHNET_EXPORT void g_chnet_dns_map_insert(const char *host, const char *ip);
CHNET_EXPORT void g___chnet_dns_map_insert(const char *host, const char *ip);

CHNET_EXPORT bool g_chnet_dns_map_lookup(struct chnet_dns_lookup *lk);
CHNET_EXPORT bool g___chnet_dns_map_lookup(struct chnet_dns_lookup *lk);

CHNET_EXPORT void g_chnet_dns_map_remove(const char *host, const char *ip);
CHNET_EXPORT void g___chnet_dns_map_remove(const char *host, const char *ip);

CHNET_EXPORT void g_chnet_dns_map_remove_all(const char *host);
CHNET_EXPORT void g___chnet_dns_map_remove_all(const char *host);

CHNET_EXPORT void g_chnet_dns_map_pop(const char *host);
CHNET_EXPORT void g___chnet_dns_map_pop(const char *host);

} /* extern "C" */

#endif // CHNET__NET__DNS_MAP_H
