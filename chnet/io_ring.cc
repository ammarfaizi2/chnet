
#define __FOR_CHROMIUM_INTERNAL
#include "io_ring.h"
#include "net/dns_req_ares.h"

using namespace net;

inline IORingState::IORingState(void):
        wq_(1024 * 32, 4096, 1)
{
        wait_cqe_to_wait_.store(0, std::memory_order_relaxed);
        nr_post_cqe_waiting_.store(0, std::memory_order_relaxed);
}

inline IORingState::~IORingState(void)
        __releases(&state_->cqe_lock_)
        __releases(&state_->sqe_lock_)
{
        cqe_lock_.unlock();
        sqe_lock_.unlock();
}

IORing::IORing(uint32_t entry)
{
        constexpr static uint32_t max_entry = 1024u * 1024u * 1024u;
        constexpr static uint32_t min_entry = 8u;
        uint32_t sq_max;
        uint32_t cq_max;

        if (entry < min_entry)
                entry = min_entry;
        if (entry > max_entry)
                entry = max_entry;

        sq_max = 1;
        while (sq_max < entry)
                sq_max *= 2;

        cq_max = sq_max * 2;

        sq_mask_ = sq_max - 1;
        cq_mask_ = cq_max - 1;
        sq_tail_.store(0, std::memory_order_relaxed);
        sq_head_.store(0, std::memory_order_relaxed);
        cq_tail_.store(0, std::memory_order_relaxed);
        cq_head_.store(0, std::memory_order_relaxed);
        cq_max_to_wait_.store(0, std::memory_order_relaxed);
        sqes_ = new IORingSQE[sq_max];
        cqes_ = new IORingCQE[cq_max];
        state_ = new IORingState;
        CHECK(state_->wq_.Init() == 0);
}

__always_inline void IORing::WaitStopPostCQE(void)
        __must_hold(&state_->cqe_lock_)
{
        std::atomic<uint32_t> *nr_waiting = &state_->nr_post_cqe_waiting_;

        while (1) {
                if (!nr_waiting->load(std::memory_order_acquire))
                        break;

                state_->post_cqe_cond_.notify_all();
                UnlockCQE();
                LockCQE();
        }
}

IORing::~IORing(void)
{
        state_->stop_ = true;

        LockSQE();
        LockCQE();
        WaitStopPostCQE();
        delete[] cqes_;
        delete[] sqes_;
        cqes_ = nullptr;
        sqes_ = nullptr;
        delete state_;
        state_ = nullptr;
}

void IORing::LockSQE(void)
        __acquires(&state_->sqe_lock_)
{
        state_->sqe_lock_.lock();
}

void IORing::UnlockSQE(void)
        __releases(&state_->sqe_lock_)
{
        state_->sqe_lock_.unlock();
}

void IORing::LockCQE(void)
        __acquires(&state_->cqe_lock_)
{
        state_->cqe_lock_.lock();
}

void IORing::UnlockCQE(void)
        __releases(&state_->cqe_lock_)
{
        state_->cqe_lock_.unlock();
}

inline bool IORing::IsSQESlotFull(void)
        __must_hold(&state_->sqe_lock_)
{
        return (SQESize() >= (sq_mask_ + 1));
}

inline bool IORing::IsCQESlotFull(void)
        __must_hold(&state_->cqe_lock_)
{
        return (CQESize() >= (cq_mask_ + 1));
}

__always_inline bool IORing::ShouldStop(void)
{
        return unlikely(state_->stop_);
}

IORingSQE *IORing::GetSQE(void)
{
        IORingSQE *sqe;

        LockSQE();
        if (likely(!IsSQESlotFull()))
                sqe = &sqes_[sq_tail_.fetch_add(1) & sq_mask_];
        else
                sqe = nullptr;
        UnlockSQE();
        return sqe;
}

uint32_t IORing::Submit(uint32_t to_submit)
{
        uint32_t head;
        uint32_t tail;
        uint32_t mask;
        uint32_t ret;

        LockSQE();
        head = sq_head_.load(std::memory_order_acquire);
        tail = sq_tail_.load(std::memory_order_acquire);
        mask = sq_mask_;
        ret = 0;

        while (to_submit--) {
                if (head == tail)
                        break;

                if (IssueSQE(&sqes_[head++ & mask]))
                        ret++;
        }

        sq_head_.store(head, std::memory_order_release);
        UnlockSQE();

        return ret;
}

inline bool IORing::IssueSQE(const struct IORingSQE *sqe)
        __must_hold(&state_->sqe_lock_)
{
        bool is_ok = false;

        switch (sqe->op_) {
        case IORING_OP_NOP:
                is_ok = IssueSQENop(sqe);
                break;
        case IORING_OP_CHNET_READ:
                is_ok = IssueSQECHNetRead(sqe);
                break;
        case IORING_OP_CHNET_START:
                is_ok = IssueSQECHNetStart(sqe);
                break;
        case IORING_OP_DNS_QUERY:
                is_ok = IssueSQEDNSQuery(sqe);
                break;
        case IORING_OP_DEDICATED_DNS_QUERY:
                is_ok = IssueSQEDedicatedDNSQuery(sqe);
                break;
        }

        if (likely(is_ok))
                cq_max_to_wait_.fetch_add(1, std::memory_order_release);

        return is_ok;
}

inline void IORing::NotifyWaitCQE(void)
        __must_hold(&state_->cqe_lock_)
{
        uint32_t to_wait;

        to_wait = state_->wait_cqe_to_wait_.load(std::memory_order_acquire);
        if (!to_wait)
                return;

        if (CQESize() >= to_wait)
                state_->wait_cqe_cond_.notify_one();
}

inline bool IORing::__PostCQE(int32_t res, const struct IORingSQE *sqe)
        __must_hold(&state_->cqe_lock_)
{
        if (likely(!IsCQESlotFull())) {
                struct IORingCQE *cqe;
                uint32_t tail;

                tail = cq_tail_.load();
                cqe = &cqes_[tail & cq_mask_];
                cqe->op_ = sqe->op_;
                cqe->res_ = res;
                cqe->user_data_ = sqe->user_data_;
                cq_tail_.store(tail + 1);
                NotifyWaitCQE();
                return true;
        }

        return false;
}

inline bool IORing::PostCQE(int32_t res, const struct IORingSQE *sqe)
{
        bool ret;

        LockCQE();
        ret = __PostCQE(res, sqe);
        UnlockCQE();
        return ret;
}

inline bool IORing::__PostCQEDeferred(int32_t res, const struct IORingSQE *sqe)
{
        std::unique_lock<std::mutex> lock(state_->cqe_lock_);
        std::atomic<uint32_t> *nr = &state_->nr_post_cqe_waiting_;
        bool ret;

        nr->fetch_add(1);
        while (1) {
                if (IsCQESlotFull())
                        state_->post_cqe_cond_.wait(lock);

                if (ShouldStop()) {
                        ret = false;
                        break;
                }

                if (__PostCQE(res, sqe)) {
                        ret = true;
                        break;
                }
        }
        nr->fetch_sub(1);
        return ret;
}

inline bool IORing::PostCQEDeferred(int32_t res, const struct IORingSQE *sqe)
{
        int ret;

        ret = state_->wq_.FTryScheduleWork([this, res, sqe = *sqe](void *data){
                __PostCQEDeferred(res, &sqe);
        });

        return (ret == 0);
}

inline bool IORing::PostCQEMaybeDeffered(int32_t res,
                                         const struct IORingSQE *sqe)
{
        if (unlikely(!PostCQE(res, sqe)))
                return PostCQEDeferred(res, sqe);

        return true;
}

__always_inline void IORing::NotifyPostCQEWaiting(uint32_t n)
        __must_hold(&state_->cqe_lock_)
{
        std::atomic<uint32_t> *nr = &state_->nr_post_cqe_waiting_;
        uint32_t nr_waiting = nr->load(std::memory_order_relaxed);

        if (!nr_waiting)
                return;

        if (nr_waiting > 1 && n > 1)
                state_->post_cqe_cond_.notify_all();
        else
                state_->post_cqe_cond_.notify_one();
}

void IORing::CQAdvance(uint32_t n)
{
        if (!n)
                return;

        LockCQE();
        cq_head_.fetch_add(n);
        cq_max_to_wait_.fetch_sub(n);
        NotifyPostCQEWaiting(n);
        UnlockCQE();
}

__always_inline uint32_t IORing::__WaitCQE(uint32_t to_wait,
                                           uint32_t timeout_ms,
                                           std::unique_lock<std::mutex> &lock)
        __must_hold(&state_->cqe_lock_)
{
        uint32_t ret;

        state_->wait_cqe_to_wait_.store(to_wait);
        while (1) {
                ret = CQESize();
                if (ret >= to_wait)
                        break;

                state_->wait_cqe_cond_.wait(lock);
        }
        state_->wait_cqe_to_wait_.store(0);
        return ret;
}

uint32_t IORing::WaitCQE(uint32_t to_wait)
{
        uint32_t max;
        uint32_t ret;

        if (unlikely(!to_wait))
                return 0;

        std::unique_lock<std::mutex> lock(state_->cqe_lock_);
        max = cq_max_to_wait_.load();

        if (to_wait > max)
                to_wait = max;

        ret = CQESize();
        if (ret >= to_wait)
                return ret;

        return __WaitCQE(to_wait, -1u, lock);
}

inline bool IORing::IssueSQENop(const struct IORingSQE *sqe)
{
        return PostCQEMaybeDeffered(0, sqe);
}

inline void IORing::PrepCHNetStartChunked(CHNetx *chx,
                                          const struct IORingSQE *sqe)
{
        chx->connected_ = [this, sqe = *sqe](auto *, const auto &, auto &){
                PostCQEMaybeDeffered(0, &sqe);
                return OK;
        };
}

inline void IORing::PrepCHNetStartSingle(CHNetx *chx,
                                         const struct IORingSQE *sqe)
{
        chx->response_started_ = [this, sqe = *sqe](auto *, int err) {
                PostCQEMaybeDeffered(err, &sqe);
        };
}

inline bool IORing::IssueSQECHNetStart(const struct IORingSQE *sqe)
{
        const struct IORingSQEOP_ChnetStart *sqe_ch = &sqe->chnet_start_;
        CHNet *ch = sqe_ch->ch_;
        CHNetx *chx = ch->ch();

        if (chx->IsPayloadChunked())
                PrepCHNetStartChunked(chx, sqe);
        else
                PrepCHNetStartSingle(chx, sqe);

        chx->Start();
        return true;
}

inline void IORing::PrepCHNetRead(CHNetx *chx, const struct IORingSQE *sqe)
{
        chx->read_completed_ = [this, sqe = *sqe](auto *ureq, int bytes_read){
                PostCQEMaybeDeffered(bytes_read, &sqe);
        };
}

inline bool IORing::__IssueSQECHNetRead(const struct IORingSQE *sqe,
                                        bool post_task)
{
        const struct IORingSQEOP_ChnetRead *sqe_ch = &sqe->chnet_read_;
        CHNetx *chx;
        CHNet *ch;
        int size;
        int ret;

        ch = sqe_ch->ch_;
        chx = ch->ch();
        size = sqe_ch->size_;

        if (post_task)
                ret = chx->Read(size);
        else
                chx->_Read(size, &ret);

        if (ret != net::ERR_IO_PENDING)
                PostCQEMaybeDeffered(ret, sqe);

        return true;
}

inline bool IORing::IssueSQECHNetReadNeedStart(CHNetx *chx,
                                               const struct IORingSQE *sqe)
{
        /*
         * SQE read on a URLRequest that hasn't been started
         * has to start the request first, then do the read
         * after OnResponseStarted().
         */
        chx->response_started_ = [this, sqe = *sqe](auto *, int err){
                if (err != OK) {
                        PostCQEMaybeDeffered(err, &sqe);
                        return;
                }
                __IssueSQECHNetRead(&sqe, false);
        };

        chx->Start();
        return true;
}

inline bool IORing::IssueSQECHNetRead(const struct IORingSQE *sqe)
{
        const struct IORingSQEOP_ChnetRead *sqe_ch = &sqe->chnet_read_;
        CHNet *ch = sqe_ch->ch_;
        CHNetx *chx = ch->ch();

        PrepCHNetRead(chx, sqe);
        if (!chx->has_req_started())
                return IssueSQECHNetReadNeedStart(chx, sqe);

        return __IssueSQECHNetRead(sqe, true);
}

struct DnsReqData {
        DnsResolver             *dns_ = nullptr;
        IORing                  *ring_;
        struct IORingSQE        sqe_;
        struct DnsQueryResult   *result_;
};

DnsQueryResult::DnsQueryResult(void):
        capacity_(0),
        size_(0),
        addresses_(nullptr),
        error_(nullptr)
{
}

DnsQueryResult::~DnsQueryResult(void)
{
        if (addresses_) {
                free(addresses_);
                addresses_ = nullptr;
        }

        if (error_) {
                free(error_);
                error_ = nullptr;
        }
}

inline void DnsQueryResult::Allocate(size_t target_size)
{
        void *ret;

        if (target_size < capacity_)
                return;

        capacity_ = target_size + 32;
        ret = realloc(addresses_, capacity_ * sizeof(*addresses_));
        CHECK(ret);
        addresses_ = static_cast<addr_str_t *>(ret);
}

#if defined(__linux__)

/*
 * Protects:
 *  - g_ares_thread
 *  - g_ares_bind_ipv4
 *  - g_ares_bind_ipv6
 *  - g_ares_bind_ifname
 */
static std::mutex g_ares_thread_lock;

static struct ares_thread *g_ares_thread;
static unsigned int g_ares_bind_ipv4;
static unsigned char g_ares_bind_ipv6[16];
static char g_ares_bind_ifname[64];

static void dns_q_callback(void *arg, int status, int timeouts,
                           struct ares_addrinfo *result)
{
        struct DnsReqData *data = static_cast<DnsReqData *>(arg);
        struct DnsQueryResult *res = data->result_;
        struct ares_addrinfo_node *n;
        size_t i;

        if (status != ARES_SUCCESS) {
                res->error_ = strdup(ares_strerror(status));
                CHECK(res->error_);
                goto out;
        }

        i = 0;
        for (n = result->nodes; n; n = n->ai_next) {
                struct sockaddr_in6 *addr6 = (struct sockaddr_in6 *)n->ai_addr;
                struct sockaddr_in *addr4 = (struct sockaddr_in *)n->ai_addr;
                void *paddr;
                char *addr;

                if (n->ai_family == AF_INET6)
                        paddr = &addr6->sin6_addr;
                else if (n->ai_family == AF_INET)
                        paddr = &addr4->sin_addr;
                else
                        continue;

                i++;
                res->Allocate(i);
                addr = res->addresses_[i - 1];
                ares_inet_ntop(n->ai_family, paddr, addr, sizeof(addr_str_t));
        }

        res->size_ = i;
        ares_freeaddrinfo(result);
        status = 0;
out:
        data->ring_->PostCQEMaybeDeffered(status, &data->sqe_);
        delete data;
}

static struct ares_thread *__get_ares_thread(bool force_reload = false)
{
        if (likely(g_ares_thread && !force_reload))
                return g_ares_thread;

        g_ares_thread = create_ares_thread();
        CHECK(g_ares_thread);

        if (g_ares_bind_ipv4) {
                ares_set_local_ip4(g_ares_thread->channel, g_ares_bind_ipv4);
        }

        if (g_ares_bind_ipv6[0]) {
                ares_set_local_ip6(g_ares_thread->channel, g_ares_bind_ipv6);
        }

        if (g_ares_bind_ifname[0]) {
                ares_set_local_dev(g_ares_thread->channel, g_ares_bind_ifname);
        }

        return g_ares_thread;
}

static struct ares_thread *get_ares_thread(bool force_reload = false)
{
        std::unique_lock<std::mutex> lock(g_ares_thread_lock);
        return __get_ares_thread(force_reload);
}

void chnet_dns_bind_ipv4(const char *addr)
{
        std::unique_lock<std::mutex> lock(g_ares_thread_lock);

        if (inet_pton(AF_INET, addr, &g_ares_bind_ipv4) != 1)
                g_ares_bind_ipv4 = 0;
        else
                g_ares_bind_ipv4 = ntohl(g_ares_bind_ipv4);

        if (g_ares_thread)
                __get_ares_thread(true);
}

void chnet_dns_bind_ipv6(const char *addr)
{
        std::unique_lock<std::mutex> lock(g_ares_thread_lock);

        if (inet_pton(AF_INET6, addr, g_ares_bind_ipv6) != 1)
                g_ares_bind_ipv6[0] = 0;

        if (g_ares_thread)
                __get_ares_thread(true);
}

void chnet_dns_bind_iface(const char *dev)
{
        std::unique_lock<std::mutex> lock(g_ares_thread_lock);

        if (dev[0]) {
                strncpy(g_ares_bind_ifname, dev, sizeof(g_ares_bind_ifname) - 1);
                g_ares_bind_ifname[sizeof(g_ares_bind_ifname) - 1] = 0;
        } else {
                g_ares_bind_ifname[0] = 0;
        }

        if (g_ares_thread)
                __get_ares_thread(true);
}

inline bool IORing::IssueSQEDNSQuery(const struct IORingSQE *sqe)
{
        const struct IORingSQEOP_DnsQuery *d = &sqe->dns_query_;
        struct ares_addrinfo_hints hints;
        struct DnsReqData *data;
        struct ares_thread *at;

        hints.ai_family = AF_INET;
        hints.ai_socktype = SOCK_STREAM;
        hints.ai_protocol = IPPROTO_TCP;
        hints.ai_flags = ARES_AI_CANONNAME;

        data = new DnsReqData();
        data->sqe_ = *sqe;
        data->dns_ = nullptr;
        data->result_ = d->result_;
        data->ring_ = this;

        at = get_ares_thread();
        t_ares_getaddrinfo(at, d->hostname_, NULL, &hints, dns_q_callback, data);
        return true;
}

inline bool IORing::IssueSQEDedicatedDNSQuery(const struct IORingSQE *sqe)
{
        const struct IORingSQEOP_DedicatedDnsQuery *d = &sqe->dedicated_dns_query_;
        struct ares_addrinfo_hints hints;
        struct DnsReqData *data;
        struct ares_thread *at;

        hints.ai_family = AF_INET;
        hints.ai_socktype = SOCK_STREAM;
        hints.ai_protocol = IPPROTO_TCP;
        hints.ai_flags = ARES_AI_CANONNAME;

        data = new DnsReqData();
        data->sqe_ = *sqe;
        data->dns_ = d->dns_;
        data->result_ = d->result_;
        data->ring_ = this;

        at = d->dns_->ares_thread_;
        t_ares_getaddrinfo(at, d->hostname_, NULL, &hints, dns_q_callback, data);
        return true;
}

#else /* defined(__linux__) */

inline void IORing::__PostCQEDNSQuery(const std::vector<net::IPEndPoint> *addr,
                                      DnsQueryResult *res)
{
        res->Allocate(res->size_ + addr->size());

        for (const auto &i: *addr) {
                std::string str_addr;
                const char *raw_addr;
                addr_str_t *dst;

                dst = &res->addresses_[res->size_++];
                str_addr = i.ToStringWithoutPort();
                raw_addr = str_addr.c_str();

                strncpy(*dst, raw_addr, sizeof(*dst));
                (*dst)[sizeof(*dst) - 1] = '\0';
        }
}

inline void IORing::PostCQEDNSQuery(DnsReqQueue *drq, void *data)
{
        DnsReqData *drd = static_cast<DnsReqData *>(data);
        const std::vector<HostResolverEndpointResult> *r;

        if (drq->err_ == 0) {
                r = drq->req_->GetEndpointResults();
                for (const auto &i: *r)
                        __PostCQEDNSQuery(&i.ip_endpoints, drd->result_);
        }

        PostCQEMaybeDeffered(0, &drd->sqe_);
        delete drd;
}

inline bool IORing::IssueSQEDNSQuery(const struct IORingSQE *sqe)
{
        const struct IORingSQEOP_DnsQuery *d = &sqe->dns_query_;
        struct DnsReqData *data;
        DnsReq *req;
        int64_t ret;

        data = new DnsReqData();
        data->sqe_ = *sqe;
        data->result_ = d->result_;

        req = DnsReq::Get();
        ret = req->Resolve(d->hostname_,
                base::BindOnce(&IORing::PostCQEDNSQuery,
                base::Unretained(this)),
                data);
        if (ret < 0) {
                delete data;
                return PostCQEMaybeDeffered(ret, sqe);
        }

        return true;
}
#endif /* defined(__linux__) */
