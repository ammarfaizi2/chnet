
#define __FOR_CHROMIUM_INTERNAL

#include "chnet.h"
#include "net/chnet_thread.h"
#include "net/chnet_thread_pool.h"
#include "net/ssl/ssl_key_logger.h"
#include "net/ssl/ssl_key_logger_impl.h"

#include <mutex>
#include <cstring>

using net::CHNetThreadPool;
using net::CHNetThread;
using net::URLRequest;
using net::IOBufferWithSize;

CHNet::CHNet(struct tbind_config *bind_cfg)
{
        CHNetThread *thread;

        thread = CHNetThreadPool::GetThread(bind_cfg);
        CHECK(thread);
        ch_ = new net::CHNetx(thread);
}

CHNet::~CHNet(void)
{
        if (redirect_info_)
                delete redirect_info_;

        delete ch_;
}

void CHNet::SetURL(const char *url)
{
        ch_->SetURL(url);
}

void CHNet::SetMethod(const char *method)
{
        ch_->SetMethod(method);
}

void CHNet::Start(void)
{
        ch_->Start();
}

int CHNet::Read(int len)
{
        return ch_->Read(len);
}

void CHNet::Cancel(void)
{
        ch_->Cancel();
}

void CHNet::StartChunkedPayload(void)
{
        ch_->StartChunkedPayload();
}

int CHNet::Write(const void *buf, size_t len)
{
        return ch_->Write(buf, len);
}

void CHNet::StopChunkedPayload(void)
{
        ch_->StopChunkedPayload();
}

const char *CHNet::GetReadBuffer(void)
{
        return ch_->GetReadBuffer();
}

void CHNet::SetPayload(const void *p, size_t len)
{
        return ch_->SetPayload(p, len);
}

void CHNet::SetRequestHeader(const char *key, const char *val,
                             bool overwrite)
{
        return ch_->SetRequestHeader(std::string(key), std::string(val),
                                     overwrite);
}

int CHNet::GetReadRet(void)
{
        return ch_->GetReadRet();
}

const char *CHNet::GetErrorStr(void)
{
        if (ch_->err_ == "")
                return nullptr;

        return ch_->err_.c_str();
}

bool CHNet::__GetResponseHeader(size_t *iter, const char **val, const char **key)
{
        return ch_->GetResponseHeader(iter, val, key);
}

void CHNet::LockResponseHeader(void)
{
        ch_->LockResponseHeader();
}

void CHNet::UnlockResponseHeader(void)
{
        ch_->UnlockResponseHeader();
}

int CHNet::GetResponseCode(void)
{
        URLRequest *ureq = ch_->ureq();
        int c;

        if (ureq)
                c = ureq->response_info().headers->response_code();
        else
                c = 0;

        return c;
}

const char *CHNet::GetAlpnNegotiatedProtocol(void)
{
        URLRequest *ureq = ch_->ureq();
        const char *ret;

        if (ureq)
                ret = ureq->response_info().alpn_negotiated_protocol.c_str();
        else
                ret = nullptr;

        return ret;
}

size_t CHNet::GetRemoteEndpoint(char *buf, size_t buf_siz)
{
        URLRequest *ureq = ch_->ureq();
        std::string ip;
        size_t cp_size;

        if (!ureq)
                return 0;

        ip = ureq->response_info().remote_endpoint.ToString();
        cp_size = ip.size();
        if (!cp_size)
                return 0;

        if (cp_size > (buf_siz - 1))
                cp_size = buf_siz - 1;

        memcpy(buf, ip.c_str(), cp_size);
        buf[cp_size] = '\0';
        return cp_size;
}

inline chnet_buf_ptr::chnet_buf_ptr(void):
        ptr_(nullptr),
        len_(0)
{
        memset(__pad, 0, sizeof(__pad));
        CHECK(sizeof(struct chnet_buf_ptr) == 64);
        CHECK(sizeof(scoped_refptr<IOBufferWithSize>) < 48);
}

inline chnet_buf_ptr::~chnet_buf_ptr(void)
{
}

struct chnet_buf_ptr *CHNet::GetBufferPtr(void)
{
        struct chnet_buf_ptr *ret;
        int len;

        if (!ch_->read_buf_)
                return nullptr;

        len = ch_->GetReadRet();
        if (len < 0)
                return nullptr;

        ret = new struct chnet_buf_ptr;
        ret->__read_buf = ch_->read_buf_;
        ret->ptr_ = ret->__read_buf->data();
        ret->len_ = static_cast<size_t>(len);
        return ret;
}

void CHNet::SetFollowRedirect(bool r)
{
        ch_->SetFollowRedirect(r);
}

NodeRedirectInfo *CHNet::GetNodeRedirectInfo(void)
{
        const net::RedirectInfo *red;
        NodeRedirectInfo *ret;

        if (redirect_info_)
                return redirect_info_;

        red = ch_->GetRedirectInfo();
        if (!red)
                return nullptr;

        ret = new struct NodeRedirectInfo;
        ret->SetRedirectInfo(red);
        redirect_info_ = ret;
        return ret;
}

NodeRedirectInfo::NodeRedirectInfo(void)
{
}

NodeRedirectInfo::~NodeRedirectInfo(void)
{
        free(heap__);
}

void NodeRedirectInfo::SetRedirectInfo(const void *red_)
{
        const net::RedirectInfo *red;

        red = static_cast<const net::RedirectInfo *>(red_);
        const std::string &red_new_url = red->new_url.spec();
        const std::string &red_new_method = red->new_method;
        const std::string &red_new_referrer = red->new_referrer;
        char *heap;
        size_t len;

        len =	red_new_url.size() +
                red_new_method.size() +
                red_new_referrer.size() + 3;

        heap = static_cast<char *>(malloc(len));
        CHECK(heap);
        heap__ = heap;

        memcpy(heap, red_new_url.c_str(), red_new_url.size() + 1);
        new_url_ = heap;
        heap += red_new_url.size() + 1;

        memcpy(heap, red_new_method.c_str(), red_new_method.size() + 1);
        new_method_ = heap;
        heap += red_new_method.size() + 1;

        memcpy(heap, red_new_referrer.c_str(), red_new_referrer.size() + 1);
        new_referrer_ = heap;

        status_code_ = red->status_code;

        insecure_scheme_was_upgraded_ =
                red->insecure_scheme_was_upgraded;

        is_signed_exchange_fallback_redirect_ =
                red->is_signed_exchange_fallback_redirect;
}

// static
void CHNet::PutBufferPtr(struct chnet_buf_ptr *ptr)
{
        delete ptr;
}

extern "C" {

static base::AtExitManager *g_at_exit_manager;
static base::ScopedClosureRunner *g_clean_up;
static std::mutex g_init_lock;
static bool is_initialized;

extern void chnet_fix_chromium_boringssl(void);

static void __chnet_global_init(void)
        __must_hold(&g_init_lock)
{
        auto clean_up_callback = []{
                base::ThreadPoolInstance::Get()->Shutdown();
        };

        g_at_exit_manager = new base::AtExitManager;
        g_clean_up = new base::ScopedClosureRunner(
                                base::BindOnce(std::move(clean_up_callback)));

        base::CommandLine::Init(0, NULL);
        base::ThreadPoolInstance::InitParams p(256);
        base::ThreadPoolInstance::Create("chnet");
        base::ThreadPoolInstance::Get()->Start(p);
}

#ifdef WIN32
static void set_ssl_key_logger(void)
{
        const wchar_t *log_file;

        log_file = _wgetenv(L"SSLKEYLOGFILE");
        if (!log_file)
                return;

        net::SSLClientSocket::SetSSLKeyLogger(
                std::make_unique<net::SSLKeyLoggerImpl>(
                        base::FilePath(log_file))
        );
}
#else
static void set_ssl_key_logger(void)
{
        const char *log_file;

        log_file = getenv("SSLKEYLOGFILE");
        if (!log_file)
                return;

        net::SSLClientSocket::SetSSLKeyLogger(
                std::make_unique<net::SSLKeyLoggerImpl>(
                        base::FilePath(log_file))
        );
}
#endif

__cold void chnet_global_init(void)
{
        g_init_lock.lock();
        if (is_initialized) {
                g_init_lock.unlock();
                return;
        }

        chnet_fix_chromium_boringssl();
        SSL_library_init();
        __chnet_global_init();
        CHNetThreadPool::GlobalInit();
        is_initialized = true;
#ifndef WIN32
        socket_posix_connect_hook = &net::chnet_connect_hook_sock;
        connect_bind_get_iface_key = &net::connect_bind_get_iface_key;
#endif
        set_ssl_key_logger();
        g_init_lock.unlock();
}

__cold void chnet_global_stop(void)
{
        g_init_lock.lock();
        CHNetThreadPool::GlobalDestroy();
        delete g_at_exit_manager;
        delete g_clean_up;
        g_at_exit_manager = nullptr;
        g_clean_up = nullptr;
        is_initialized = false;
        g_init_lock.unlock();
}

} /* extern "C" */
