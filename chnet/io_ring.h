
#ifndef CHNET__NET__RING_H
#define CHNET__NET__RING_H

#include "chnet.h"
#include "common.h"

#include <cstdint>
#include <atomic>

#ifdef WIN32
#include <winsock2.h>
#else
#include <arpa/inet.h>
#endif

#ifdef __FOR_CHROMIUM_INTERNAL
#include "net/chnetx.h"
#include "net/dns_req.h"
#endif

#include "DnsResolver.h"

enum IORingOP {
        IORING_OP_NOP         = 0,
        IORING_OP_CHNET_READ  = 1,
        IORING_OP_CHNET_START = 2,
        IORING_OP_DNS_QUERY   = 3,
        IORING_OP_DEDICATED_DNS_QUERY = 4
};

struct IORingSQEOP_ChnetRead {
        CHNet		*ch_;
        uint32_t	size_;
};

struct IORingSQEOP_ChnetStart {
        CHNet		*ch_;
};

using addr_str_t = char[INET6_ADDRSTRLEN + 1u];
class IORing;

#ifdef __linux__
CHNET_EXPORT void chnet_dns_bind_ipv4(const char *addr);
CHNET_EXPORT void chnet_dns_bind_ipv6(const char *addr);
CHNET_EXPORT void chnet_dns_bind_iface(const char *dev);
#endif

struct CHNET_EXPORT DnsQueryResult {
private:
        size_t          capacity_;
public:
        size_t          size_;
        addr_str_t      *addresses_;
        const char      *error_;
        DnsQueryResult(void);
        ~DnsQueryResult(void);
        void Allocate(size_t target_size);
};

struct IORingSQEOP_DnsQuery {
        const char	        *hostname_;
        struct DnsQueryResult   *result_;
};

struct IORingSQEOP_DedicatedDnsQuery {
        DnsResolver             *dns_;
        const char              *hostname_;
        struct DnsQueryResult   *result_;
};

struct CHNET_EXPORT IORingSQE {
        uint8_t		op_;
        uint8_t		__pad[7];
        union {
                struct IORingSQEOP_ChnetRead	chnet_read_;
                struct IORingSQEOP_ChnetStart	chnet_start_;
                struct IORingSQEOP_DnsQuery	dns_query_;
                struct IORingSQEOP_DedicatedDnsQuery dedicated_dns_query_;
        };
        uint64_t        user_data_;

        void PrepNop(void);

        void PrepRead(CHNet* ch, uint32_t size);

        void PrepStart(CHNet* ch);

        void PrepDnsQuery(const char *hostname,
                          struct DnsQueryResult *result);

        void PrepDedicatedDnsQuery(DnsResolver *dns,
                                   const char *hostname,
                                   struct DnsQueryResult *result);

        void SetUserData(uint64_t data);

        void SetUserDataPtr(void* data);
};

inline void IORingSQE::PrepNop(void)
{
        op_ = IORING_OP_NOP;
}

inline void IORingSQE::PrepRead(CHNet *ch, uint32_t size)
{
        op_ = IORING_OP_CHNET_READ;
        chnet_read_.ch_ = ch;
        chnet_read_.size_ = size;
}

inline void IORingSQE::PrepStart(CHNet *ch)
{
        op_ = IORING_OP_CHNET_START;
        chnet_start_.ch_ = ch;
}

inline void IORingSQE::PrepDnsQuery(const char *hostname,
                                    struct DnsQueryResult *result)
{
        op_ = IORING_OP_DNS_QUERY;
        dns_query_.hostname_ = hostname;
        dns_query_.result_ = result;
}

inline void IORingSQE::PrepDedicatedDnsQuery(DnsResolver *dns,
                                             const char *hostname,
                                             struct DnsQueryResult *result)
{
        op_ = IORING_OP_DEDICATED_DNS_QUERY;
        dedicated_dns_query_.dns_ = dns;
        dedicated_dns_query_.hostname_ = hostname;
        dedicated_dns_query_.result_ = result;
}

inline void IORingSQE::SetUserData(uint64_t data)
{
        user_data_ = data;
}

inline void IORingSQE::SetUserDataPtr(void *data)
{
        uintptr_t tmp = reinterpret_cast<uintptr_t>(data);

        user_data_ = static_cast<uint64_t>(tmp);
}

struct CHNET_EXPORT IORingCQE {
        int32_t		res_;
        uint8_t		op_;
        uint8_t		__pad[3];
        uint64_t	user_data_;

        uint64_t GetUserData(void);

        void* GetUserDataPtr(void);
};

inline uint64_t IORingCQE::GetUserData(void)
{
        return user_data_;
}

inline void* IORingCQE::GetUserDataPtr(void)
{
        uintptr_t tmp = static_cast<uintptr_t>(user_data_);

        return reinterpret_cast<void *>(tmp);
}

class IORingState;

static inline uint32_t u32_diff(uint32_t a, uint32_t b)
{
        if (b > a)
                return b - a;
        else
                return a - b;
}

class CHNET_EXPORT IORing {
public:
        IORing(uint32_t entry = 512);

        ~IORing(void);

        uint32_t Submit(uint32_t to_submit = -1u);

        uint32_t CQESize(void);

        uint32_t SQESize(void);

        struct IORingCQE *GetCQEHead(void);


        bool IsCQESlotFull(void);

        bool IsSQESlotFull(void);

        IORingSQE *GetSQE(void);

        void LockCQE(void);

        void LockSQE(void);

        void UnlockCQE(void);

        void UnlockSQE(void);

        void CQAdvance(uint32_t n);

        uint32_t WaitCQE(uint32_t to_wait);

        struct IORingSQE	*sqes_;
        struct IORingCQE	*cqes_;

        std::atomic<uint32_t>	sq_tail_;
        std::atomic<uint32_t>	sq_head_;
        uint32_t		sq_mask_;

        std::atomic<uint32_t>	cq_tail_;
        std::atomic<uint32_t>	cq_head_;
        uint32_t		cq_mask_;

        std::atomic<uint32_t>	cq_max_to_wait_;

private:

#ifdef __FOR_CHROMIUM_INTERNAL
        IORingState		*state_;
#else
        void			*state_;
#endif

#ifdef __FOR_CHROMIUM_INTERNAL
public:
        bool __PostCQE(int32_t res, const struct IORingSQE *sqe);

        bool PostCQE(int32_t res, const struct IORingSQE *sqe);

        bool __PostCQEDeferred(int32_t res, const struct IORingSQE *sqe);

        bool PostCQEDeferred(int32_t res, const struct IORingSQE *sqe);

        bool PostCQEMaybeDeffered(int32_t res, const struct IORingSQE *sqe);

private:
        bool IssueSQE(const struct IORingSQE *sqe);

        bool IssueSQENop(const struct IORingSQE *sqe);

        /*
         * CHNet Start
         */
        void PrepCHNetStartChunked(net::CHNetx *chx,
                                   const struct IORingSQE *sqe);
        void PrepCHNetStartSingle(net::CHNetx *chx,
                                  const struct IORingSQE *sqe);
        bool IssueSQECHNetStart(const struct IORingSQE *sqe);

        /*
         * CHNet Read
         */
        void PrepCHNetRead(net::CHNetx *chx, const struct IORingSQE *sqe);
        bool IssueSQECHNetReadNeedStart(net::CHNetx *chx,
                                        const struct IORingSQE *sqe);
        bool __IssueSQECHNetRead(const struct IORingSQE *sqe, bool post_task);
        bool IssueSQECHNetRead(const struct IORingSQE *sqe);

        /*
         * DNS Query.
         */
        bool IssueSQEDNSQuery(const struct IORingSQE *sqe);
        bool IssueSQEDedicatedDNSQuery(const struct IORingSQE *sqe);
        void __PostCQEDNSQuery(const std::vector<net::IPEndPoint> *addr,
                               DnsQueryResult *res);

        void WaitForCQESlotAvailable(void);

        bool ShouldStop(void);

        void NotifyPostCQEWaiting(uint32_t n);

        void NotifyWaitCQE(void);

        uint32_t __WaitCQE(uint32_t to_wait, uint32_t timeout_ms,
                           std::unique_lock<std::mutex> &lock);

        void WaitStopPostCQE(void);
#endif /* #ifdef __FOR_CHROMIUM_INTERNAL */

};

inline uint32_t IORing::CQESize(void)
{
        return u32_diff(cq_head_.load(std::memory_order_acquire),
                        cq_tail_.load(std::memory_order_acquire));
}

inline uint32_t IORing::SQESize(void)
{
        return u32_diff(sq_head_.load(std::memory_order_acquire),
                        sq_tail_.load(std::memory_order_acquire));
}

inline struct IORingCQE* IORing::GetCQEHead(void)
{
        uint32_t cq_head = cq_head_.load(std::memory_order_acquire);

        return &cqes_[cq_head & cq_mask_];
}

#define ioring_for_each_cqe(ring, head, tail, cqe)			\
for (									\
        head = (ring)->cq_head_.load(std::memory_order_acquire),	\
        tail = (ring)->cq_tail_.load(std::memory_order_acquire);	\
        (cqe = (head != tail) ?						\
                &(ring)->cqes_[head & (ring)->cq_mask_] :		\
                NULL);							\
        head++								\
)

#ifdef __FOR_CHROMIUM_INTERNAL
#include <mutex>
#include <condition_variable>
#include "WorkQueue.h"

class IORingState {
public:
        IORingState(void);

        ~IORingState(void);

        std::mutex              sqe_lock_;
        std::mutex              cqe_lock_;
        std::condition_variable post_cqe_cond_;
        std::condition_variable wait_cqe_cond_;

        Wq::WorkQueue           wq_;
        std::atomic<uint32_t>   wait_cqe_to_wait_;
        std::atomic<uint32_t>   nr_post_cqe_waiting_;
        volatile bool           stop_ = false;
};
#endif /* #ifdef __FOR_CHROMIUM_INTERNAL */

#endif /* #ifndef CHNET__NET__RING_H */
