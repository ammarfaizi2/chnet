
#define __FOR_CHROMIUM_INTERNAL
#include "chnet.h"
#include "common.h"
#include "net/dns_req_ares.h"
#include "DnsResolver.h"

DnsResolver::DnsResolver(const char *server,
                         const char *bind_ip4,
                         const char *bind_iface4)
{
        ares_thread_ = create_ares_thread();
        CHECK(ares_thread_);
        if (server != nullptr) {
                ares_set_servers_csv(ares_thread_->channel, server);
        }

        if (bind_ip4 != nullptr) {
                uint32_t ip = inet_addr(bind_ip4);
                ip = ntohl(ip);
                ares_set_local_ip4(ares_thread_->channel, ip);
        }

        if (bind_iface4 != nullptr) {
                ares_set_local_dev(ares_thread_->channel, bind_iface4);
        }
}

DnsResolver::~DnsResolver()
{
        destroy_ares_thread(ares_thread_);
}
