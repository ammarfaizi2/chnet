
#ifndef CHNET_COMMON_H
#define CHNET_COMMON_H

#if defined(__clang__) || defined(__GNUC__)
#ifndef likely
#define likely(COND)		__builtin_expect(!!(COND), 1)
#endif

#ifndef unlikely
#define unlikely(COND)		__builtin_expect(!!(COND), 0)
#endif

#ifndef __hot
#define __hot			__attribute__((__hot__))
#endif

#ifndef __cold
#define __cold			__attribute__((__cold__))
#endif

#ifndef __maybe_unused
#define __maybe_unused		__attribute__((__unused__))
#endif

#ifdef __SPARSE_CHECKER__
#define __must_hold(x)		__attribute__((context(x,1,1)))
#define __acquires(x)		__attribute__((context(x,0,1)))
#define __cond_acquires(x)	__attribute__((context(x,0,-1)))
#else /* #ifdef __SPARSE_CHECKER__ */
#define __must_hold(LOCK)
#define __releases(LOCK)
#define __acquires(LOCK)
#endif /* #ifdef __SPARSE_CHECKER__ */

#ifndef __always_inline
#define __always_inline		__attribute__((__always_inline__)) inline
#endif

#ifndef __no_inline
#define __no_inline		__attribute__((__noinline__))
#endif

#else /* #if defined(__clang__) || defined(__GNUC__) */

#define __always_inline __forceinline
#define __no_inline	__declspec(noinline)
/*
 * https://learn.microsoft.com/en-us/cpp/code-quality/annotating-locking-behavior
 */
#define __acquires(LOCK)
#define __releases(LOCK)
#define __must_hold(LOCK)

#define __maybe_unused

#define __cold
#define __hot
#define likely(COND) (COND)
#define unlikely(COND) (COND)
#endif /* #if defined(__clang__) || defined(__GNUC__) */

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(X) (sizeof(X) / sizeof(X[0]))
#endif

struct func_map {
        const char	*name;
        void		*addr;
};

#define FUNC_MAP(FUNC) {#FUNC, (void *)(FUNC)}

extern "C" {

extern void chnet_fix_chromium_boringssl(void);

} /* extern "C" */

#ifdef __FOR_CHROMIUM_INTERNAL
#include "base/component_export.h"
#endif

#if defined(__clang__) || defined(__GNUC__)
#ifdef __FOR_CHROMIUM_INTERNAL
#define CHNET_EXPORT __attribute__((__visibility__(("default")))) COMPONENT_EXPORT(CHNET)
#else
#define CHNET_EXPORT __attribute__((__visibility__(("default"))))
#endif
#else /* #if defined(__clang__) || defined(__GNUC__) */
#if defined(IS_CHNET_IMPL)
#define CHNET_EXPORT __declspec(dllexport) COMPONENT_EXPORT(CHNET)
#else
#define CHNET_EXPORT __declspec(dllimport)
#endif
#endif /* #if defined(__clang__) || defined(__GNUC__) */

#include <mutex>
#include <atomic>
#include <condition_variable>

class Waiter {
public:
        inline Waiter(void):
                signaled_(false)
        {
        }

        inline void Signal(void)
        {
                lock_.lock();
                signaled_.store(true, std::memory_order_release);
                cond_.notify_one();
                lock_.unlock();
        }

        inline void Wait(void)
        {
                std::unique_lock<std::mutex> lock(lock_);

                while (!signaled_.load(std::memory_order_acquire))
                        cond_.wait(lock);
        }

private:
        std::mutex		lock_;
        std::condition_variable	cond_;
        std::atomic<bool>	signaled_;
};

#include <cstdlib>
#include <cstring>

static inline void *memdup(const void *addr, size_t len)
{
        void *ret;

        ret = malloc(len);
        if (unlikely(!ret))
                return ret;

        return memcpy(ret, addr, len);
}

static inline void free(const void *ptr)
{
        ::free(const_cast<void *>(ptr));
}

#endif /* #ifndef CHNET_COMMON_H */
