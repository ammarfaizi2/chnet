
#ifndef NET__ARCH__X86__BORINGSSL_FIXUP_H
#define NET__ARCH__X86__BORINGSSL_FIXUP_H

#include <dlfcn.h>
#include <cstring>
#include <cstdint>
#include <cstdlib>
#include <sys/mman.h>

extern "C" {

extern void EC_GROUP_set_asn1_flag(void);
extern void PKCS12_get_key_and_certs(void);
extern void X509_STORE_set_get_crl(void);
extern void GENERAL_NAMES_new(void);
extern void EVP_PKEY_get_raw_public_key(void);
extern void X509_trust_clear(void);
extern void _ZN4bssl19SSL_decline_handoffEP6ssl_st(void);
extern void OPENSSL_init_ssl(void);
extern void SSL_CTX_set_default_verify_paths(void);
extern void CBS_get_u64le(void);
extern void PEM_write_bio_RSAPublicKey(void);
extern void SSL_get_tls_channel_id(void);
extern void i2d_ASN1_SET_ANY(void);
extern void PEM_write_bio_ECPrivateKey(void);
extern void ASN1_TIME_print(void);
extern void EC_KEY_parse_private_key(void);
extern void i2d_ASN1_TIME(void);
extern void RSA_verify_pss_mgf1(void);
extern void BN_rshift1(void);
extern void GENERAL_NAME_print(void);
extern void PEM_read_SSL_SESSION(void);
extern void X509_STORE_CTX_get_error_depth(void);
extern void d2i_DSA_PUBKEY_bio(void);
extern void SSL_clear_options(void);
extern void CBS_get_u16le(void);
extern void X509_set_ex_data(void);
extern void SSL_CTX_set_verify_depth(void);
extern void ECDSA_SIG_get0(void);
extern void HRSS_marshal_public_key(void);
extern void SSL_get_signature_algorithm_name(void);
extern void CBB_init(void);
extern void EVP_PKEY_copy_parameters(void);
extern void SSL_set_enable_ech_grease(void);
extern void DSA_do_verify(void);
extern void ERR_func_error_string(void);
extern void X509_CRL_get_version(void);
extern void X509at_get_attr_count(void);
extern void X509_get_ext_d2i(void);
extern void AUTHORITY_KEYID_free(void);
extern void X509_VAL_new(void);
extern void SSL_get_current_compression(void);
extern void d2i_AutoPrivateKey(void);
extern void EVP_PKEY_paramgen(void);
extern void PKCS7_bundle_raw_certificates(void);
extern void X509V3_get_section(void);
extern void SSL_CTX_set_allow_unknown_alpn_protos(void);
extern void i2d_ASN1_IA5STRING(void);
extern void EC_POINT_add(void);
extern void DIST_POINT_NAME_it(void);
extern void TLSv1_2_server_method(void);
extern void DSA_get0_pqg(void);
extern void RSA_bits(void);
extern void X509_PUBKEY_new(void);
extern void SSL_CTX_set1_groups_list(void);
extern void ASN1_UTCTIME_free(void);
extern void ED25519_verify(void);
extern void PEM_read_DSAPrivateKey(void);
extern void X509_alias_set1(void);
extern void SHA512_256_Final(void);
extern void x509v3_looks_like_dns_name(void);
extern void NCONF_new(void);
extern void BN_mul(void);
extern void X509_REVOKED_add_ext(void);
extern void SSL_CTX_set_cipher_list(void);
extern void SSL_get_default_timeout(void);
extern void SSL_SESSION_get_ticket_lifetime_hint(void);
extern void ASN1_STRING_set_default_mask(void);
extern void BUF_memdup(void);
extern void SHA224(void);
extern void X509_REQ_new(void);
extern void DTLSv1_handle_timeout(void);
extern void SSL_CTX_up_ref(void);
extern void SSL_CTX_use_psk_identity_hint(void);
extern void BIO_meth_set_create(void);
extern void bn_is_relatively_prime(void);
extern void OBJ_nid2sn(void);
extern void CBS_strdup(void);
extern void SHA224_Update(void);
extern void X509V3_EXT_REQ_add_nconf(void);
extern void SSL_CTX_sess_connect_good(void);
extern void SSL_SESSION_set_protocol_version(void);
extern void ASN1_STRING_to_UTF8(void);
extern void X509_REVOKED_get_ext_by_critical(void);
extern void ASN1_TIME_free(void);
extern void ASN1_TIME_diff(void);
extern void CBS_get_any_ber_asn1_element(void);
extern void CBS_get_u24(void);
extern void X509_OBJECT_idx_by_subject(void);
extern void X509_CRL_verify(void);
extern void CBS_contains_zero_byte(void);
extern void X509_STORE_get0_objects(void);
extern void X509_cmp_current_time(void);
extern void d2i_RSAPublicKey_fp(void);
extern void d2i_PUBKEY_bio(void);
extern void i2d_PUBKEY_bio(void);
extern void c2i_ASN1_BIT_STRING(void);
extern void ASN1_UTCTIME_adj(void);
extern void EVP_HPKE_KDF_hkdf_md(void);
extern void X509_STORE_set_lookup_certs(void);
extern void X509_REQ_delete_attr(void);
extern void X509V3_EXT_get(void);
extern void SSL_CTX_set_alpn_protos(void);
extern void ASN1_ENUMERATED_get_int64(void);
extern void ASN1_UNIVERSALSTRING_new(void);
extern void ERR_clear_system_error(void);
extern void SSL_CTX_set_tmp_ecdh(void);
extern void RSA_get0_dmq1(void);
extern void X509_LOOKUP_free(void);
extern void X509_verify_cert(void);
extern void X509_VERIFY_PARAM_lookup(void);
extern void SSL_request_handshake_hints(void);
extern void EVP_aead_aes_128_ctr_hmac_sha256(void);
extern void EC_KEY_new_method(void);
extern void X509_EXTENSION_create_by_NID(void);
extern void SSL_clear_mode(void);
extern void SSL_get_key_block_len(void);
extern void X509_get0_signature(void);
extern void SHA512_256(void);
extern void PEM_def_callback(void);
extern void RAND_poll(void);
extern void X509_REQ_get_attr_by_NID(void);
extern void sk_value(void);
extern void EVP_PKEY_new_raw_private_key(void);
extern void EC_POINT_set_affine_coordinates(void);
extern void X509_get_default_cert_file(void);
extern void EVP_CIPHER_CTX_cipher(void);
extern void X509_digest(void);
extern void ERR_print_errors(void);
extern void DSA_marshal_private_key(void);
extern void i2d_EC_PUBKEY(void);
extern void SSL_use_RSAPrivateKey(void);
extern void BN_GENCB_call(void);
extern void SSL_SESSION_from_bytes(void);
extern void SSL_get_curve_name(void);
extern void SSL_get0_next_proto_negotiated(void);
extern void EC_GROUP_get_order(void);
extern void EVP_HPKE_KEY_copy(void);
extern void RSA_public_key_from_bytes(void);
extern void X509_set_pubkey(void);
extern void X509_PURPOSE_get0(void);
extern void SSL_CIPHER_get_value(void);
extern void SSL_CTX_clear_options(void);
extern void SSL_CTX_need_tmp_RSA(void);
extern void EVP_aes_192_ecb(void);
extern void EVP_PKEY_assign_RSA(void);
extern void ASN1_INTEGER_free(void);
extern void CBS_stow(void);
extern void DSA_parse_parameters(void);
extern void EVP_aes_256_ctr(void);
extern void SHA512_256_Update(void);
extern void EVP_aead_aes_128_ccm_matter(void);
extern void RSA_print(void);
extern void X509_REVOKED_set_revocationDate(void);
extern void X509_NAME_ENTRY_new(void);
extern void d2i_X509_REQ_bio(void);
extern void d2i_PrivateKey_bio(void);
extern void DTLS_with_buffers_method(void);
extern void SSL_set_cipher_list(void);
extern void BIO_get_init(void);
extern void CONF_parse_list(void);
extern void d2i_X509_CINF(void);
extern void SSL_get_mode(void);
extern void _ZN4bssl24SSL_CTX_set_handoff_modeEP10ssl_ctx_stb(void);
extern void SSL_set_psk_server_callback(void);
extern void CBB_discard_child(void);
extern void i2d_DSAPrivateKey_fp(void);
extern void X509_STORE_CTX_set_trust(void);
extern void X509_REQ_set_pubkey(void);
extern void ASN1_INTEGER_get(void);
extern void bn_miller_rabin_iteration(void);
extern void i2d_X509_CRL_tbs(void);
extern void ED25519_keypair_from_seed(void);
extern void EVP_PKEY_up_ref(void);
extern void CRYPTO_BUFFER_up_ref(void);
extern void CRYPTO_get_locking_callback(void);
extern void d2i_CRL_DIST_POINTS(void);
extern void SSL_CTX_load_verify_locations(void);
extern void EVP_DigestSignFinal(void);
extern void i2d_X509_REQ_INFO(void);
extern void SSL_CTX_get_tlsext_ticket_keys(void);
extern void OPENSSL_posix_to_tm(void);
extern void EVP_marshal_private_key(void);
extern void ECDH_compute_key_fips(void);
extern void X509_REQ_get0_signature(void);
extern void SSL_SESSION_to_bytes_for_ticket(void);
extern void BIO_meth_set_read(void);
extern void BN_get_rfc3526_prime_6144(void);
extern void BN_CTX_start(void);
extern void ERR_set_mark(void);
extern void SSL_set_alpn_protos(void);
extern void TLS_client_method(void);
extern void i2a_ASN1_STRING(void);
extern void EVP_DigestUpdate(void);
extern void i2d_DSA_PUBKEY_bio(void);
extern void SSL_CTX_sess_cache_full(void);
extern void ASN1_SEQUENCE_it(void);
extern void X509_STORE_CTX_get_ex_data(void);
extern void BUF_MEM_grow(void);
extern void ERR_get_error(void);
extern void ec_point_mul_scalar_public(void);
extern void X509_STORE_CTX_purpose_inherit(void);
extern void X509_get_signature_nid(void);
extern void SSL_CTX_set1_verify_cert_store(void);
extern void CBS_get_u32(void);
extern void PEM_write_DSAparams(void);
extern void PEM_write_bio_EC_PUBKEY(void);
extern void X509_get_default_cert_file_env(void);
extern void SSL_use_certificate_file(void);
extern void ASN1_BIT_STRING_num_bytes(void);
extern void OPENSSL_free(void);
extern void X509V3_NAME_from_section(void);
extern void BN_bn2cbb_padded(void);
extern void EVP_PKEY_cmp(void);
extern void RSA_generate_key_ex(void);
extern void OBJ_txt2nid(void);
extern void X509_CRL_get_REVOKED(void);
extern void X509_TRUST_cleanup(void);
extern void X509_STORE_CTX_get_ex_new_index(void);
extern void X509_STORE_CTX_set0_trusted_stack(void);
extern void X509V3_add_value_int(void);
extern void SSL_get_shared_ciphers(void);
extern void ERR_put_error(void);
extern void i2a_ASN1_OBJECT(void);
extern void ASN1_BMPSTRING_it(void);
extern void BIO_set_retry_read(void);
extern void PKCS12_free(void);
extern void X509_LOOKUP_shutdown(void);
extern void EVP_hpke_chacha20_poly1305(void);
extern void X509_gmtime_adj(void);
extern void i2d_X509_REVOKED(void);
extern void SSL_CTX_set_signing_algorithm_prefs(void);
extern void EC_POINT_is_on_curve(void);
extern void SSL_set_max_cert_list(void);
extern void SSL_CTX_sess_misses(void);
extern void BIO_set_conn_port(void);
extern void NCONF_free(void);
extern void EVP_MD_CTX_copy_ex(void);
extern void ec_hash_to_curve_p384_xmd_sha512_sswu_draft07(void);
extern void DH_set0_pqg(void);
extern void PEM_read_ECPrivateKey(void);
extern void X509_NAME_cmp(void);
extern void SSL_CTX_set_tls_channel_id_enabled(void);
extern void SSL_use_RSAPrivateKey_ASN1(void);
extern void X509_REVOKED_free(void);
extern void X25519(void);
extern void X509_STORE_CTX_set_verify_cb(void);
extern void X509_VERIFY_PARAM_get_flags(void);
extern void EC_KEY_get0_group(void);
extern void EVP_MD_nid(void);
extern void i2d_PrivateKey_fp(void);
extern void _ZN4bssl21ssl_session_serializeEPK14ssl_session_stP6cbb_st(void);
extern void EC_POINT_invert(void);
extern void d2i_PROXY_POLICY(void);
extern void BIO_write(void);
extern void RSA_set0_crt_params(void);
extern void X509_up_ref(void);
extern void SSL_CTX_set_tlsext_status_cb(void);
extern void NETSCAPE_SPKI_b64_encode(void);
extern void SSL_reset_early_data_reject(void);
extern void PEM_write(void);
extern void X509_STORE_CTX_get1_chain(void);
extern void SSL_COMP_get_name(void);
extern void SSL_set_tls_channel_id_enabled(void);
extern void OPENSSL_secure_malloc(void);
extern void RSA_verify_PKCS1_PSS_mgf1(void);
extern void sk_insert(void);
extern void X509_CRL_get0_extensions(void);
extern void X509_STORE_CTX_get_current_cert(void);
extern void _ZN4bssl20SSL_set_handoff_modeEP6ssl_stb(void);
extern void ASN1_TIME_set(void);
extern void X509_STORE_CTX_set0_param(void);
extern void RSAPublicKey_dup(void);
extern void X509_get_issuer_name(void);
extern void X509_cmp_time(void);
extern void BIO_set_nbio(void);
extern void EVP_aes_192_cbc(void);
extern void RSA_sign(void);
extern void PEM_read_PKCS7(void);
extern void d2i_X509_CRL_bio(void);
extern void ASN1_UTCTIME_print(void);
extern void BN_value_one(void);
extern void EVP_PKEY_CTX_free(void);
extern void PEM_read_PKCS8(void);
extern void X509_NAME_add_entry(void);
extern void NETSCAPE_SPKI_set_pubkey(void);
extern void SSL_SESSION_set1_id_context(void);
extern void EVP_sha512(void);
extern void EVP_MD_CTX_copy(void);
extern void HMAC_CTX_new(void);
extern void X509_STORE_CTX_get_error(void);
extern void SSL_CTX_set1_curves_list(void);
extern void SSL_CTX_sess_connect(void);
extern void EC_KEY_set_public_key_affine_coordinates(void);
extern void SSL_get0_signed_cert_timestamp_list(void);
extern void SSL_CTX_get_verify_mode(void);
extern void ERR_save_state(void);
extern void X509_VERIFY_PARAM_set1_policies(void);
extern void X509_EXTENSION_set_object(void);
extern void X509V3_EXT_add_alias(void);
extern void OPENSSL_malloc_init(void);
extern void X509_get1_email(void);
extern void SSL_CTX_set1_ech_keys(void);
extern void EVP_PKEY_CTX_set_hkdf_md(void);
extern void BIO_rw_filename(void);
extern void PKCS7_type_is_data(void);
extern void PEM_write_SSL_SESSION(void);
extern void X509_set_serialNumber(void);
extern void i2d_PrivateKey_bio(void);
extern void i2d_DHparams_bio(void);
extern void EVP_aes_256_gcm(void);
extern void BN_MONT_CTX_free(void);
extern void ERR_peek_last_error_line_data(void);
extern void ECDSA_sign(void);
extern void _ZN4bssl18SSL_apply_handbackEP6ssl_stNS_4SpanIKhEE(void);
extern void PEM_write_bio(void);
extern void X509_parse_from_buffer(void);
extern void X509_get_ext_by_critical(void);
extern void X509_STORE_CTX_zero(void);
extern void d2i_EDIPARTYNAME(void);
extern void ASN1_item_i2d(void);
extern void EVP_rc4(void);
extern void EVP_MD_type(void);
extern void AES_cfb128_encrypt(void);
extern void RSA_PSS_PARAMS_it(void);
extern void X509_STORE_get1_certs(void);
extern void X509_REQ_get1_email(void);
extern void BIO_set_shutdown(void);
extern void EVP_PKEY_get0_EC_KEY(void);
extern void SSL_is_signature_algorithm_rsa_pss(void);
extern void EVP_PKEY_CTX_set_rsa_oaep_md(void);
extern void EVP_CIPHER_CTX_reset(void);
extern void X509_STORE_set_trust(void);
extern void PROXY_CERT_INFO_EXTENSION_it(void);
extern void BIO_test_flags(void);
extern void FIPS_service_indicator_after_call(void);
extern void ASN1_FBOOLEAN_it(void);
extern void PEM_read_DSA_PUBKEY(void);
extern void SSL_set_wfd(void);
extern void SSL_get0_chain_certs(void);
extern void DES_encrypt3(void);
extern void EVP_HPKE_KEY_zero(void);
extern void PEM_write_ECPrivateKey(void);
extern void RAND_seed(void);
extern void X509_CRL_delete_ext(void);
extern void X509_REVOKED_get_ext_count(void);
extern void X509_CRL_add0_revoked(void);
extern void ASN1_STRING_new(void);
extern void EVP_PKEY_verify(void);
extern void EVP_PKEY_CTX_set_rsa_pss_keygen_saltlen(void);
extern void OBJ_ln2nid(void);
extern void X509_check_private_key(void);
extern void SSL_CTX_get_num_tickets(void);
extern void EVP_PKEY_derive_set_peer(void);
extern void i2d_NOTICEREF(void);
extern void CBS_get_asn1_uint64(void);
extern void i2d_PKCS8PrivateKey_bio(void);
extern void d2i_DIST_POINT_NAME(void);
extern void ASN1_tag2bit(void);
extern void d2i_ASN1_UNIVERSALSTRING(void);
extern void ERR_load_crypto_strings(void);
extern void ASN1_INTEGER_get_uint64(void);
extern void BN_mask_bits(void);
extern void ERR_pop_to_mark(void);
extern void X509_ATTRIBUTE_create_by_txt(void);
extern void i2d_OTHERNAME(void);
extern void PROXY_CERT_INFO_EXTENSION_free(void);
extern void BUF_MEM_new(void);
extern void BN_mul_word(void);
extern void BN_GENCB_get_arg(void);
extern void PEM_proc_type(void);
extern void X509_STORE_set_get_issuer(void);
extern void EVP_Digest(void);
extern void SSL_CTX_get_ex_new_index(void);
extern void DH_new(void);
extern void RAND_OpenSSL(void);
extern void SSL_CTX_set_tlsext_status_arg(void);
extern void SSL_SESSION_get0_peer_sha256(void);
extern void X509_STORE_CTX_get0_parent_ctx(void);
extern void BN_cmp(void);
extern void ERR_free_strings(void);
extern void v2i_GENERAL_NAME(void);
extern void SSL_set_tlsext_status_type(void);
extern void EVP_CIPHER_CTX_set_key_length(void);
extern void X509_CRL_sign(void);
extern void SSL_clear_chain_certs(void);
extern void EVP_MD_CTX_move(void);
extern void PEM_write_DSAPrivateKey(void);
extern void EVP_AEAD_CTX_zero(void);
extern void X509_NAME_hash(void);
extern void X509_NAME_ENTRY_get_object(void);
extern void NAME_CONSTRAINTS_it(void);
extern void BIO_copy_next_retry(void);
extern void DH_get0_g(void);
extern void sk_pop_free(void);
extern void X509_CRL_get0_by_serial(void);
extern void ASN1_UTF8STRING_it(void);
extern void ASN1_PRINTABLESTRING_it(void);
extern void BIO_meth_set_gets(void);
extern void EVP_PKEY_print_private(void);
extern void EVP_DecryptInit(void);
extern void i2d_GENERAL_NAMES(void);
extern void CBB_len(void);
extern void EC_KEY_new(void);
extern void EC_POINT_dup(void);
extern void SSL_ech_accepted(void);
extern void BN_mod_exp(void);
extern void i2d_X509_REQ_bio(void);
extern void SSL_total_renegotiations(void);
extern void SSL_CTX_sess_get_new_cb(void);
extern void DHparams_dup(void);
extern void CRYPTO_set_ex_data(void);
extern void X509V3_EXT_get_nid(void);
extern void X509_get1_ocsp(void);
extern void SSL_early_data_accepted(void);
extern void SHA224_Init(void);
extern void CRYPTO_secure_malloc_init(void);
extern void SSL_CIPHER_get_cipher_nid(void);
extern void SSL_CTX_set_tlsext_ticket_keys(void);
extern void RSA_check_key(void);
extern void PEM_write_bio_PUBKEY(void);
extern void X509_STORE_CTX_init(void);
extern void SSL_error_description(void);
extern void X509_STORE_set1_param(void);
extern void X509_REQ_add_extensions(void);
extern void i2d_PROXY_CERT_INFO_EXTENSION(void);
extern void SSL_CTX_new(void);
extern void SSL_get_pending_cipher(void);
extern void i2d_ASN1_PRINTABLESTRING(void);
extern void BLAKE2B256_Update(void);
extern void DH_get0_p(void);
extern void X509_verify(void);
extern void CBS_get_u32le(void);
extern void EVP_aead_aes_256_cbc_sha1_tls(void);
extern void DH_get0_q(void);
extern void RSA_blinding_on(void);
extern void SSL_alert_type_string_long(void);
extern void _ZN4bssl19SealRecordSuffixLenEPK6ssl_stm(void);
extern void BIO_free_all(void);
extern void EVP_HPKE_KEM_id(void);
extern void d2i_AUTHORITY_KEYID(void);
extern void SSL_CTX_set1_param(void);
extern void BUF_strdup(void);
extern void i2d_PKCS8PrivateKeyInfo_bio(void);
extern void d2i_X509_NAME(void);
extern void SSL_CIPHER_is_aead(void);
extern void SSL_alert_desc_string(void);
extern void EC_KEY_get_enc_flags(void);
extern void EC_KEY_up_ref(void);
extern void EVP_HPKE_KEY_init(void);
extern void X509at_add1_attr(void);
extern void EVP_DecodeBase64(void);
extern void BIO_new_connect(void);
extern void EVP_PKEY_verify_init(void);
extern void SHA256(void);
extern void X509_CRL_get_issuer(void);
extern void SSL_marshal_ech_config(void);
extern void EVP_aead_des_ede3_cbc_sha1_tls_implicit_iv(void);
extern void BN_primality_test(void);
extern void EVP_hpke_aes_128_gcm(void);
extern void X509_TRUST_set(void);
extern void X509_ALGOR_cmp(void);
extern void X509V3_string_free(void);
extern void SSL_CTX_set_verify_algorithm_prefs(void);
extern void EC_KEY_set_conv_form(void);
extern void EVP_HPKE_CTX_max_overhead(void);
extern void X509_NAME_oneline(void);
extern void ASN1_IA5STRING_free(void);
extern void EVP_MD_meth_get_flags(void);
extern void EC_GROUP_order_bits(void);
extern void BN_cmp_word(void);
extern void EVP_DigestSignInit(void);
extern void EVP_HPKE_AEAD_id(void);
extern void i2d_CRL_DIST_POINTS(void);
extern void EVP_HPKE_KEY_new(void);
extern void PEM_read_bio_DSA_PUBKEY(void);
extern void i2d_NETSCAPE_SPKI(void);
extern void SSL_SESSION_get0_id_context(void);
extern void BN_CTX_get(void);
extern void RSA_get0_pss_params(void);
extern void X509_NAME_ENTRY_create_by_txt(void);
extern void SSL_CTX_set0_client_CAs(void);
extern void SSL_alert_from_verify_result(void);
extern void OPENSSL_strhash(void);
extern void EVP_PKEY_verify_recover(void);
extern void TRUST_TOKEN_experiment_v2_pmb(void);
extern void i2d_PKCS8PrivateKeyInfo_fp(void);
extern void PEM_read_bio_RSAPublicKey(void);
extern void EDIPARTYNAME_new(void);
extern void SSL_set_verify_algorithm_prefs(void);
extern void BIO_s_socket(void);
extern void RSA_private_encrypt(void);
extern void SSL_CTX_set_select_certificate_cb(void);
extern void CONF_modules_free(void);
extern void CRYPTO_cleanup_all_ex_data(void);
extern void EVP_AEAD_CTX_init(void);
extern void ASN1_item_digest(void);
extern void d2i_X509_PUBKEY(void);
extern void SSL_set_accept_state(void);
extern void BIO_get_mem_data(void);
extern void HKDF_extract(void);
extern void CRYPTO_set_dynlock_lock_callback(void);
extern void SSL_CTX_set_tlsext_use_srtp(void);
extern void SSL_CTX_get_read_ahead(void);
extern void SSL_SESSION_get0_ticket(void);
extern void OBJ_find_sigid_by_algs(void);
extern void X509_set1_notAfter(void);
extern void X509_reject_clear(void);
extern void OTHERNAME_new(void);
extern void EVP_VerifyInit(void);
extern void CRYPTO_secure_used(void);
extern void POLICY_CONSTRAINTS_free(void);
extern void i2d_PROXY_POLICY(void);
extern void SSL_CTX_get0_certificate(void);
extern void BIO_should_read(void);
extern void PEM_write_PKCS8PrivateKey(void);
extern void X509_REQ_dup(void);
extern void ASN1_item_d2i_fp(void);
extern void OBJ_length(void);
extern void EVP_PKEY_set1_EC_KEY(void);
extern void i2d_PKCS8PrivateKey_nid_bio(void);
extern void X509_VAL_it(void);
extern void POLICY_MAPPINGS_it(void);
extern void DTLSv1_get_timeout(void);
extern void SSL_CTX_use_RSAPrivateKey(void);
extern void SSL_get_version(void);
extern void bn_rshift_secret_shift(void);
extern void MD5_Update(void);
extern void OBJ_nid2cbb(void);
extern void PKCS8_PRIV_KEY_INFO_new(void);
extern void X509_REVOKED_get0_revocationDate(void);
extern void X509_STORE_new(void);
extern void BIO_ctrl(void);
extern void EVP_PKEY_get1_DH(void);
extern void X509_VERIFY_PARAM_clear_flags(void);
extern void ASN1_ENUMERATED_set(void);
extern void ASN1_OBJECT_create(void);
extern void ASN1_STRING_set_by_NID(void);
extern void PEM_read_bio(void);
extern void X509_REQ_print(void);
extern void i2d_CERTIFICATEPOLICIES(void);
extern void SSL_get_wbio(void);
extern void SSL_want(void);
extern void EC_KEY_marshal_curve_name(void);
extern void ERR_get_error_line_data(void);
extern void DH_generate_key(void);
extern void DH_up_ref(void);
extern void FIPS_mode(void);
extern void PEM_write_bio_SSL_SESSION(void);
extern void EVP_aes_256_ecb(void);
extern void EVP_PKEY_CTX_set_ec_paramgen_curve_nid(void);
extern void CMAC_Reset(void);
extern void SSL_CTX_set_session_psk_dhe_timeout(void);
extern void BLAKE2B256_Final(void);
extern void PEM_read_X509_CRL(void);
extern void X509_ALGOR_free(void);
extern void i2d_X509_CRL_bio(void);
extern void i2d_X509_CINF(void);
extern void X509_CINF_new(void);
extern void ASN1_STRING_copy(void);
extern void BN_is_one(void);
extern void FIPS_query_algorithm_status(void);
extern void X509_EXTENSION_create_by_OBJ(void);
extern void OPENSSL_config(void);
extern void AES_set_decrypt_key(void);
extern void EC_POINT_set_compressed_coordinates_GFp(void);
extern void OBJ_cbs2nid(void);
extern void X509_OBJECT_get_type(void);
extern void BN_mod_sqr(void);
extern void X509_REQ_get_attr_by_OBJ(void);
extern void SSL_set_chain_and_key(void);
extern void X509_get_ext_by_NID(void);
extern void X509_REQ_check_private_key(void);
extern void X509_keyid_set1(void);
extern void EVP_SignUpdate(void);
extern void EVP_HPKE_KEY_private_key(void);
extern void X509_CRL_get_ext_count(void);
extern void x509v3_bytes_to_hex(void);
extern void SHA384_Final(void);
extern void SSL_num_renegotiations(void);
extern void SSL_set0_client_CAs(void);
extern void SSL_CTX_set_msg_callback(void);
extern void EVP_AEAD_CTX_tag_len(void);
extern void SSL_get_read_sequence(void);
extern void BN_dec2bn(void);
extern void i2d_DSA_SIG(void);
extern void BN_bn2le_padded(void);
extern void CRYPTO_gcm128_tag(void);
extern void BIO_set_fd(void);
extern void EVP_aead_aes_128_cbc_sha1_tls(void);
extern void SHA1_Final(void);
extern void SPAKE2_generate_msg(void);
extern void EC_POINT_get_affine_coordinates(void);
extern void X509_STORE_get_cert_crl(void);
extern void NETSCAPE_SPKI_sign(void);
extern void X509_PUBKEY_set0_param(void);
extern void EVP_PKEY_get0_DH(void);
extern void EVP_PKEY_keygen_init(void);
extern void RSA_public_key_to_bytes(void);
extern void CBS_get_u64(void);
extern void SPAKE2_process_msg(void);
extern void EVP_AEAD_max_overhead(void);
extern void BASIC_CONSTRAINTS_it(void);
extern void i2d_EDIPARTYNAME(void);
extern void i2c_ASN1_BIT_STRING(void);
extern void d2i_ASN1_SEQUENCE_ANY(void);
extern void EVP_CipherFinal_ex(void);
extern void ACCESS_DESCRIPTION_new(void);
extern void TLSv1_1_client_method(void);
extern void i2d_DISPLAYTEXT(void);
extern void PEM_write_X509_CRL(void);
extern void X509_NAME_ENTRY_dup(void);
extern void cbs_get_latin1(void);
extern void CRYPTO_STATIC_MUTEX_unlock_read(void);
extern void X509_NAME_ENTRY_get_data(void);
extern void SSL_CTX_set_trust(void);
extern void BIO_new_file(void);
extern void EVP_MD_CTX_init(void);
extern void EVP_PKEY_id(void);
extern void SSL_ECH_KEYS_marshal_retry_configs(void);
extern void EC_KEY_generate_key(void);
extern void OPENSSL_secure_clear_free(void);
extern void X509_REQ_get_attr_count(void);
extern void BN_mod_exp2_mont(void);
extern void PEM_read_EC_PUBKEY(void);
extern void X509_VERIFY_PARAM_set1_name(void);
extern void SSL_quic_max_handshake_flight_len(void);
extern void SSL_CTX_set_max_cert_list(void);
extern void SSL_CTX_sess_cb_hits(void);
extern void asn1_get_string_table_for_testing(void);
extern void DIRECTORYSTRING_new(void);
extern void HRSS_decap(void);
extern void X509at_get_attr_by_NID(void);
extern void POLICY_MAPPING_free(void);
extern void ASN1_UTF8STRING_free(void);
extern void RSA_parse_private_key(void);
extern void d2i_POLICYQUALINFO(void);
extern void BIO_set_fp(void);
extern void BN_bn2binpad(void);
extern void EVP_PKEY_get_raw_private_key(void);
extern void EVP_add_digest(void);
extern void X509V3_EXT_CRL_add_nconf(void);
extern void ISSUING_DIST_POINT_it(void);
extern void X509V3_add_value_bool_nf(void);
extern void OPENSSL_strlcat(void);
extern void RSA_parse_public_key(void);
extern void EVP_aead_aes_256_gcm(void);
extern void X509_REQ_add1_attr(void);
extern void SSL_set_mtu(void);
extern void ASN1_PRINTABLE_type(void);
extern void EVP_DecodeInit(void);
extern void EC_POINT_oct2point(void);
extern void X509_get_subject_name(void);
extern void sk_new(void);
extern void PEM_read_bio_SSL_SESSION(void);
extern void EC_POINT_mul(void);
extern void BIO_meth_set_puts(void);
extern void DSA_SIG_parse(void);
extern void BN_mod_exp_mont_word(void);
extern void DSA_free(void);
extern void DSA_SIG_set0(void);
extern void ECDSA_SIG_from_bytes(void);
extern void CMAC_Init(void);
extern void CRYPTO_STATIC_MUTEX_lock_write(void);
extern void OBJ_txt2obj(void);
extern void PEM_do_header(void);
extern void SSL_use_psk_identity_hint(void);
extern void SSL_use_PrivateKey_ASN1(void);
extern void SSL_SESSION_get_version(void);
extern void TLSv1_2_method(void);
extern void ERR_add_error_data(void);
extern void OPENSSL_cleanse(void);
extern void EC_GROUP_get_curve_GFp(void);
extern void d2i_X509_NAME_ENTRY(void);
extern void ERR_reason_error_string(void);
extern void X509_STORE_set_lookup_crls(void);
extern void X509_set1_signature_algo(void);
extern void CBS_is_valid_asn1_bitstring(void);
extern void SSL_CTX_clear_extra_chain_certs(void);
extern void ASN1_item_d2i_bio(void);
extern void CBS_get_asn1_bool(void);
extern void d2i_RSA_PUBKEY_bio(void);
extern void SSL_set_verify(void);
extern void ASN1_item_i2d_fp(void);
extern void CRYPTO_library_init(void);
extern void X509_get0_notBefore(void);
extern void SSL_state_string_long(void);
extern void EVP_aes_256_cbc(void);
extern void EVP_HPKE_CTX_cleanup(void);
extern void SSL_SESSION_is_resumable(void);
extern void SSL_set_ocsp_response(void);
extern void ASN1_INTEGER_new(void);
extern void BN_to_ASN1_INTEGER(void);
extern void BIO_wpending(void);
extern void EC_curve_nid2nist(void);
extern void HMAC_Init(void);
extern void PEM_write_bio_PKCS8_PRIV_KEY_INFO(void);
extern void EVP_HPKE_CTX_seal(void);
extern void i2d_PKCS12_bio(void);
extern void X509_VERIFY_PARAM_set_depth(void);
extern void d2i_ECPrivateKey_bio(void);
extern void SSL_add0_chain_cert(void);
extern void EVP_aead_aes_128_cbc_sha1_tls_implicit_iv(void);
extern void DH_free(void);
extern void BN_get_rfc3526_prime_8192(void);
extern void EVP_PKEY_free(void);
extern void SSL_get_current_expansion(void);
extern void SSL_set_SSL_CTX(void);
extern void DSA_get0_key(void);
extern void BN_num_bits_word(void);
extern void DH_check(void);
extern void CRYPTO_fork_detect_ignore_madv_wipeonfork_for_testing(void);
extern void SSL_is_init_finished(void);
extern void SSL_CTX_set1_chain(void);
extern void ASN1_TYPE_set1(void);
extern void BN_div_word(void);
extern void EVP_HPKE_CTX_setup_sender(void);
extern void X509_REQ_get_extensions(void);
extern void i2d_RSA_PUBKEY_fp(void);
extern void X509V3_EXT_add(void);
extern void SSL_version(void);
extern void BN_bn2mpi(void);
extern void HMAC_Final(void);
extern void BN_to_montgomery(void);
extern void CTR_DRBG_generate(void);
extern void i2d_PKCS8PrivateKey_fp(void);
extern void d2i_POLICYINFO(void);
extern void i2d_POLICYINFO(void);
extern void ASN1_GENERALSTRING_free(void);
extern void i2d_DIRECTORYSTRING(void);
extern void DSA_get0_g(void);
extern void SSL_CTX_set_tlsext_servername_callback(void);
extern void d2i_ECParameters(void);
extern void X509v3_get_ext_by_critical(void);
extern void X509_OBJECT_up_ref_count(void);
extern void SSL_get_options(void);
extern void SSL_CIPHER_get_max_version(void);
extern void EVP_DecodeFinal(void);
extern void CBB_add_u16_length_prefixed(void);
extern void CMAC_CTX_new(void);
extern void X509V3_parse_list(void);
extern void X509_PURPOSE_get0_sname(void);
extern void SSL_early_callback_ctx_extension_get(void);
extern void ASN1_BIT_STRING_set(void);
extern void ASN1_NULL_free(void);
extern void TRUST_TOKEN_CLIENT_finish_issuance(void);
extern void BIO_s_file(void);
extern void MD4(void);
extern void MD5(void);
extern void OTHERNAME_it(void);
extern void SSL_quic_read_level(void);
extern void SSL_CTX_get_options(void);
extern void EVP_PKEY_set1_DSA(void);
extern void EVP_DigestSign(void);
extern void EC_KEY_generate_key_fips(void);
extern void X509at_delete_attr(void);
extern void ISSUING_DIST_POINT_free(void);
extern void CBS_init(void);
extern void DSA_do_check_signature(void);
extern void ECDSA_SIG_get0_r(void);
extern void OPENSSL_lh_num_items(void);
extern void X509_alias_get0(void);
extern void SSL_get_rfd(void);
extern void SSL_set1_tls_channel_id(void);
extern void ASN1_INTEGER_to_BN(void);
extern void EVP_BytesToKey(void);
extern void DSA_get0_p(void);
extern void ECDSA_SIG_get0_s(void);
extern void X509_LOOKUP_file(void);
extern void DES_set_odd_parity(void);
extern void DSA_get0_q(void);
extern void EVP_PKEY_get1_RSA(void);
extern void EC_KEY_get0_public_key(void);
extern void PKCS7_get_certificates(void);
extern void X509_NAME_print_ex(void);
extern void i2d_AUTHORITY_KEYID(void);
extern void SSL_CTX_sess_set_get_cb(void);
extern void ASN1_TYPE_get(void);
extern void ASN1_UTCTIME_set(void);
extern void EVP_PKEY_new_raw_public_key(void);
extern void PEM_write_PUBKEY(void);
extern void PEM_read_X509(void);
extern void BUF_strlcpy(void);
extern void CBS_get_u24_length_prefixed(void);
extern void EVP_PKEY_CTX_set_rsa_mgf1_md(void);
extern void CBS_get_u8(void);
extern void SSL_set_quiet_shutdown(void);
extern void ASN1_OBJECT_free(void);
extern void d2i_ASN1_TYPE(void);
extern void SSL_get_peer_finished(void);
extern void CBB_add_asn1_int64(void);
extern void PKCS7_type_is_encrypted(void);
extern void sk_deep_copy(void);
extern void X509_check_email(void);
extern void CBS_get_asn1(void);
extern void CBS_get_any_asn1_element(void);
extern void MD4_Final(void);
extern void OPENSSL_clear_free(void);
extern void PEM_write_bio_PKCS8PrivateKey_nid(void);
extern void X509_sign(void);
extern void X509V3_EXT_add_list(void);
extern void RSA_get0_factors(void);
extern void SSL_CTX_set0_buffer_pool(void);
extern void BN_sub_word(void);
extern void EVP_MD_CTX_type(void);
extern void SSL_CTX_set_quiet_shutdown(void);
extern void X509v3_get_ext(void);
extern void X509V3_set_nconf(void);
extern void SSL_CTX_clear_chain_certs(void);
extern void SSL_CTX_set_private_key_method(void);
extern void TRUST_TOKEN_decode_private_metadata(void);
extern void X509_NAME_print_ex_fp(void);
extern void X509_NAME_free(void);
extern void SSL_set0_chain(void);
extern void DSA_parse_private_key(void);
extern void ec_bignum_to_scalar(void);
extern void SSL_get0_ech_name_override(void);
extern void BIO_s_mem(void);
extern void EC_KEY_derive_from_secret(void);
extern void EVP_PKEY_CTX_set_dsa_paramgen_q_bits(void);
extern void EVP_AEAD_max_tag_len(void);
extern void X509V3_section_free(void);
extern void SSL_get_cipher_by_value(void);
extern void ASN1_STRING_dup(void);
extern void d2i_RSAPublicKey_bio(void);
extern void X509V3_get_string(void);
extern void SSL_set1_sigalgs(void);
extern void SSL_SESSION_set1_id(void);
extern void cbs_get_ucs2_be(void);
extern void EC_KEY_get0_private_key(void);
extern void X509_TRUST_get_flags(void);
extern void d2i_X509_REQ_INFO(void);
extern void SSL_get_ticket_age_skew(void);
extern void sk_push(void);
extern void X509_PUBKEY_set(void);
extern void ISSUING_DIST_POINT_new(void);
extern void DTLS_client_method(void);
extern void DSA_dup_DH(void);
extern void ERR_load_RAND_strings(void);
extern void DH_compute_key_hashed(void);
extern void DH_get_rfc7919_2048(void);
extern void SHA256_Transform(void);
extern void ASN1_item_verify(void);
extern void X509_get0_authority_serial(void);
extern void HMAC_Update(void);
extern void X509_chain_up_ref(void);
extern void USERNOTICE_it(void);
extern void SSL_get_rbio(void);
extern void ASN1_GENERALIZEDTIME_set_string(void);
extern void ASN1_item_i2d_bio(void);
extern void BIO_should_io_special(void);
extern void EVP_DigestVerify(void);
extern void FIPS_service_indicator_before_call(void);
extern void X509_ATTRIBUTE_it(void);
extern void CRL_DIST_POINTS_it(void);
extern void GENERAL_NAME_set0_value(void);
extern void OpenSSL_add_all_digests(void);
extern void EVP_CipherInit(void);
extern void CRYPTO_get_lock_name(void);
extern void EVP_aead_aes_256_gcm_tls12(void);
extern void RSA_new_method(void);
extern void EVP_aead_aes_256_gcm_tls13(void);
extern void i2d_X509_NAME(void);
extern void CRYPTO_THREADID_set_pointer(void);
extern void X509_REQ_print_ex(void);
extern void i2d_RSA_PUBKEY_bio(void);
extern void DTLSv1_2_client_method(void);
extern void BIO_clear_retry_flags(void);
extern void BN_print(void);
extern void DES_decrypt3(void);
extern void DSA_up_ref(void);
extern void OTHERNAME_free(void);
extern void ASN1_T61STRING_free(void);
extern void i2d_ASN1_VISIBLESTRING(void);
extern void DIRECTORYSTRING_it(void);
extern void RAND_get_system_entropy_for_custom_prng(void);
extern void sk_delete(void);
extern void X509_STORE_up_ref(void);
extern void X509_set_version(void);
extern void SSL_CIPHER_get_protocol_id(void);
extern void i2d_ASN1_UNIVERSALSTRING(void);
extern void BN_abs_is_word(void);
extern void DH_compute_key_padded(void);
extern void X509_set_notBefore(void);
extern void X509_PURPOSE_get0_name(void);
extern void SSL_get_tlsext_status_type(void);
extern void SSL_set1_verify_cert_store(void);
extern void EVP_PKEY_CTX_set_rsa_pss_saltlen(void);
extern void PKCS7_get_raw_certificates(void);
extern void X509_REVOKED_delete_ext(void);
extern void ASN1_INTEGER_get_int64(void);
extern void BIO_meth_set_write(void);
extern void CRYPTO_MUTEX_init(void);
extern void EC_KEY_dup(void);
extern void CRYPTO_gcm128_encrypt_ctr32(void);
extern void ASN1_item_sign(void);
extern void EC_POINT_dbl(void);
extern void X509_time_adj_ex(void);
extern void X509_NAME_digest(void);
extern void SSL_set1_groups(void);
extern void SSL_get0_peer_application_settings(void);
extern void X509V3_add_value(void);
extern void CBB_cleanup(void);
extern void EVP_PKEY_CTX_get0_pkey(void);
extern void CRYPTO_set_add_lock_callback(void);
extern void X509v3_delete_ext(void);
extern void X509_VERIFY_PARAM_add1_host(void);
extern void d2i_PrivateKey_fp(void);
extern void ASN1_NULL_new(void);
extern void BIO_hexdump(void);
extern void ERR_load_ERR_strings(void);
extern void BN_mod_word(void);
extern void DH_set0_key(void);
extern void X509_VERIFY_PARAM_new(void);
extern void SSL_CTX_set1_groups(void);
extern void SSL_set_psk_client_callback(void);
extern void SSL_SESSION_get0_cipher(void);
extern void X509_LOOKUP_hash_dir(void);
extern void SSL_CTX_set_false_start_allowed_without_alpn(void);
extern void EVP_PKEY_set1_tls_encodedpoint(void);
extern void RSA_test_flags(void);
extern void X509_INFO_free(void);
extern void d2i_ASN1_UTF8STRING(void);
extern void ASN1_GENERALSTRING_it(void);
extern void BIO_set_data(void);
extern void PEM_write_bio_RSA_PUBKEY(void);
extern void X509_CRL_set_issuer_name(void);
extern void d2i_ECPrivateKey_fp(void);
extern void ASN1_INTEGER_it(void);
extern void SHA1_Init(void);
extern void CTR_DRBG_new(void);
extern void EVP_MD_size(void);
extern void X509_VERIFY_PARAM_set1_ip(void);
extern void CERTIFICATEPOLICIES_it(void);
extern void OPENSSL_lh_new(void);
extern void BN_rand_range(void);
extern void RAND_load_file(void);
extern void ASN1_STRING_print_ex(void);
extern void BIO_read(void);
extern void BN_mod_sqrt(void);
extern void SSL_process_quic_post_handshake(void);
extern void EVP_EncryptUpdate(void);
extern void EVP_PKEY_CTX_new(void);
extern void EVP_aead_aes_128_gcm_randnonce(void);
extern void EC_KEY_check_fips(void);
extern void X509_CRL_new(void);
extern void X509_NAME_ENTRY_set(void);
extern void ASN1_GENERALIZEDTIME_it(void);
extern void BIO_up_ref(void);
extern void X509_CRL_it(void);
extern void ERR_load_BIO_strings(void);
extern void BN_MONT_CTX_copy(void);
extern void SSL_has_pending(void);
extern void BN_mod_sub_quick(void);
extern void SSL_get_quiet_shutdown(void);
extern void SSL_SESSION_get_protocol_version(void);
extern void BIO_get_retry_reason(void);
extern void SSL_CIPHER_get_version(void);
extern void TLS_server_method(void);
extern void ASN1_OCTET_STRING_free(void);
extern void EVP_CIPHER_CTX_get_app_data(void);
extern void OBJ_create(void);
extern void ASN1_tag2str(void);
extern void EVP_PKEY_is_opaque(void);
extern void X509_REQ_print_fp(void);
extern void EVP_aead_aes_192_gcm(void);
extern void X509_STORE_CTX_cleanup(void);
extern void X509_get_ex_data(void);
extern void DIST_POINT_NAME_new(void);
extern void SSL_SESSION_get_time(void);
extern void d2i_ASN1_GENERALSTRING(void);
extern void d2i_ASN1_BMPSTRING(void);
extern void CONF_modules_load_file(void);
extern void BN_mod_lshift_quick(void);
extern void d2i_RSAPrivateKey_bio(void);
extern void bn_resize_words(void);
extern void AES_ecb_encrypt(void);
extern void EC_POINT_is_at_infinity(void);
extern void SSL_CTX_get_quiet_shutdown(void);
extern void SSL_get_verify_depth(void);
extern void BIO_should_write(void);
extern void X509_CRL_get0_signature(void);
extern void SSL_CTX_set_chain_and_key(void);
extern void SSL_set_ex_data(void);
extern void ASN1_GENERALIZEDTIME_check(void);
extern void OPENSSL_cleanup(void);
extern void RSA_is_opaque(void);
extern void CRYPTO_malloc(void);
extern void X509_REQ_set_version(void);
extern void EVP_SignInit(void);
extern void X509_NAME_add_entry_by_NID(void);
extern void CRYPTO_STATIC_MUTEX_lock_read(void);
extern void EVP_EncodedLength(void);
extern void EC_POINT_cmp(void);
extern void PROXY_POLICY_it(void);
extern void SSL_CIPHER_description(void);
extern void SSL_CTX_set_ex_data(void);
extern void EC_GROUP_get_asn1_flag(void);
extern void SHA512(void);
extern void X509_REVOKED_get_ext_by_NID(void);
extern void X509_TRUST_get0(void);
extern void X509_NAME_it(void);
extern void i2d_ECParameters(void);
extern void EVP_aead_aes_256_cbc_sha1_tls_implicit_iv(void);
extern void ECDSA_SIG_free(void);
extern void PEM_read_bio_PUBKEY(void);
extern void DIST_POINT_new(void);
extern void SSL_set1_sigalgs_list(void);
extern void RSA_free(void);
extern void X509_STORE_CTX_set0_crls(void);
extern void BIO_ctrl_pending(void);
extern void DSA_marshal_public_key(void);
extern void v2i_GENERAL_NAMES(void);
extern void SSL_CTX_use_certificate_file(void);
extern void SSL_dup_CA_list(void);
extern void EC_KEY_free(void);
extern void EC_KEY_set_asn1_flag(void);
extern void i2v_GENERAL_NAME(void);
extern void X509_get_extension_flags(void);
extern void _ZN4bssl22SSL_serialize_handbackEPK6ssl_stP6cbb_st(void);
extern void SSL_set_enforce_rsa_key_usage(void);
extern void CRYPTO_STATIC_MUTEX_unlock_write(void);
extern void BLAKE2B256_Init(void);
extern void CBB_did_write(void);
extern void BN_equal_consttime(void);
extern void BN_mod_sub(void);
extern void EC_GROUP_new_curve_GFp(void);
extern void X509_get_ext_by_OBJ(void);
extern void SSL_CTX_set_permute_extensions(void);
extern void BN_CTX_new(void);
extern void i2d_PKCS8_PRIV_KEY_INFO_bio(void);
extern void X509_signature_dump(void);
extern void SSL_COMP_get_compression_methods(void);
extern void SSL_get_current_cipher(void);
extern void DISPLAYTEXT_new(void);
extern void DH_parse_parameters(void);
extern void RAND_egd(void);
extern void X509_CRL_sort(void);
extern void i2d_X509_CERT_AUX(void);
extern void SSL_cutthrough_complete(void);
extern void bn_div_consttime(void);
extern void RAND_get_rand_method(void);
extern void CRYPTO_get_dynlock_destroy_callback(void);
extern void POLICYQUALINFO_free(void);
extern void EC_KEY_set_group(void);
extern void PEM_read(void);
extern void SSL_CTX_sess_get_cache_size(void);
extern void SSL_set_tlsext_host_name(void);
extern void X509_LOOKUP_init(void);
extern void DSA_verify(void);
extern void ASN1_BIT_STRING_set_bit(void);
extern void DSA_get_ex_new_index(void);
extern void EC_KEY_set_ex_data(void);
extern void SHA512_Transform(void);
extern void OBJ_find_sigid_algs(void);
extern void X509v3_get_ext_count(void);
extern void GENERAL_NAME_new(void);
extern void ASN1_put_eoc(void);
extern void X509v3_add_ext(void);
extern void SSL_CTX_set_cert_verify_callback(void);
extern void X509_VERIFY_PARAM_get0_peername(void);
extern void d2i_X509_fp(void);
extern void OPENSSL_memdup(void);
extern void ASN1_STRING_type(void);
extern void EVP_PKEY_derive(void);
extern void EC_GROUP_method_of(void);
extern void EVP_HPKE_CTX_kdf(void);
extern void PEM_X509_INFO_read(void);
extern void X509V3_EXT_print(void);
extern void SSL_CTX_set_psk_server_callback(void);
extern void POLICYINFO_it(void);
extern void POLICYQUALINFO_it(void);
extern void X509_email_free(void);
extern void SSL_CTX_set_timeout(void);
extern void i2d_DSAPrivateKey_bio(void);
extern void BIO_new_mem_buf(void);
extern void DSA_set0_pqg(void);
extern void BIO_number_written(void);
extern void RAND_status(void);
extern void X509at_get_attr_by_OBJ(void);
extern void X509_PURPOSE_get_by_sname(void);
extern void SSL_do_handshake(void);
extern void SSL_connect(void);
extern void CRYPTO_THREADID_current(void);
extern void X509_get_notAfter(void);
extern void X509_get_pathlen(void);
extern void SSL_get_session(void);
extern void SSL_get_client_random(void);
extern void X509_STORE_add_lookup(void);
extern void SSL_SESSION_to_bytes(void);
extern void SSL_COMP_free_compression_methods(void);
extern void EVP_DecodeBlock(void);
extern void HRSS_encap(void);
extern void CRYPTO_THREADID_set_numeric(void);
extern void _ZN4bssl28ssl_is_valid_ech_public_nameENS_4SpanIKhEE(void);
extern void SSL_CTX_set_ocsp_response(void);
extern void OPENSSL_malloc(void);
extern void BN_asc2bn(void);
extern void X509_CRL_print_fp(void);
extern void sk_dup(void);
extern void X509_add1_trust_object(void);
extern void SSL_set_quic_early_data_context(void);
extern void SSL_SESSION_set_ex_data(void);
extern void BN_nnmod_pow2(void);
extern void SSL_early_data_reason_string(void);
extern void EVP_DecryptInit_ex(void);
extern void X25519_keypair(void);
extern void CRYPTO_set_thread_local(void);
extern void X509_CRL_get_signature_nid(void);
extern void CBB_add_asn1_octet_string(void);
extern void EVP_CipherFinal(void);
extern void X509_CRL_free(void);
extern void X509_CRL_diff(void);
extern void FIPS_module_name(void);
extern void d2i_X509_ALGOR(void);
extern void i2d_X509_ALGOR(void);
extern void i2d_RSAPublicKey_fp(void);
extern void SSL_provide_quic_data(void);
extern void SSL_set_fd(void);
extern void SSL_CTX_get0_privatekey(void);
extern void BN_GENCB_free(void);
extern void EC_POINT_clear_free(void);
extern void d2i_PKCS8PrivateKey_bio(void);
extern void X509_CRL_get_nextUpdate(void);
extern void d2i_NETSCAPE_SPKI(void);
extern void i2d_ASN1_BMPSTRING(void);
extern void EVP_has_aes_hardware(void);
extern void X509_VERIFY_PARAM_set1(void);
extern void ASN1_STRING_set(void);
extern void PKCS12_parse(void);
extern void X509_REQ_get_signature_nid(void);
extern void CRYPTO_malloc_init(void);
extern void HMAC_CTX_copy_ex(void);
extern void PKCS7_get_CRLs(void);
extern void SSL_enable_ocsp_stapling(void);
extern void SSL_get_psk_identity_hint(void);
extern void d2i_ASN1_BOOLEAN(void);
extern void PKCS7_bundle_certificates(void);
extern void d2i_PKCS7_bio(void);
extern void pmbtoken_exp2_get_h_for_testing(void);
extern void X509_it(void);
extern void BIO_gets(void);
extern void EC_GROUP_new_by_curve_name(void);
extern void PROXY_CERT_INFO_EXTENSION_new(void);
extern void i2d_SSL_SESSION(void);
extern void ENGINE_get_RSA_method(void);
extern void BN_usub(void);
extern void TRUST_TOKEN_ISSUER_new(void);
extern void X509_VERIFY_PARAM_get_count(void);
extern void d2i_X509_REQ_fp(void);
extern void SSL_set_shed_handshake_config(void);
extern void ASN1_INTEGER_dup(void);
extern void BIO_get_retry_flags(void);
extern void BIO_set_conn_int_port(void);
extern void SSL_set_strict_cipher_list(void);
extern void BIO_pending(void);
extern void FIPS_mode_set(void);
extern void PEM_dek_info(void);
extern void X509_VERIFY_PARAM_add0_policy(void);
extern void SSL_set_retain_only_sha256_of_client_certs(void);
extern void PKCS12_PBE_add(void);
extern void DTLSv1_2_method(void);
extern void SSL_CTX_get_cert_store(void);
extern void i2d_ASN1_TYPE(void);
extern void EVP_DecodeUpdate(void);
extern void i2d_X509_SIG(void);
extern void SSL_CTX_get_keylog_callback(void);
extern void SSL_CTX_sess_hits(void);
extern void ASN1_item_unpack(void);
extern void d2i_ASN1_NULL(void);
extern void EVP_ENCODE_CTX_free(void);
extern void EVP_CIPHER_CTX_iv_length(void);
extern void EVP_marshal_digest_algorithm(void);
extern void EVP_aead_aes_128_ccm_bluetooth(void);
extern void i2d_X509(void);
extern void RAND_add(void);
extern void X509_STORE_CTX_get0_untrusted(void);
extern void X509_CERT_AUX_free(void);
extern void SSL_get0_peer_certificates(void);
extern void SSL_set_mode(void);
extern void RSA_marshal_private_key(void);
extern void PKCS8_marshal_encrypted_private_key(void);
extern void X509_ATTRIBUTE_free(void);
extern void X509_OBJECT_get0_X509(void);
extern void SSL_CIPHER_get_kx_nid(void);
extern void SSL_CTX_set_max_send_fragment(void);
extern void EVP_des_ede_cbc(void);
extern void EVP_parse_public_key(void);
extern void SSL_SESSION_set_timeout(void);
extern void DSA_generate_key(void);
extern void SSL_get_signature_algorithm_key_type(void);
extern void ASN1_STRING_set0(void);
extern void X509V3_EXT_d2i(void);
extern void SSL_CTX_set_tlsext_ticket_key_cb(void);
extern void ASN1_GENERALIZEDTIME_free(void);
extern void d2i_RSAPublicKey(void);
extern void X509_VERIFY_PARAM_set_flags(void);
extern void X509_PUBKEY_get0_public_key(void);
extern void SSL_get_info_callback(void);
extern void ASN1_T61STRING_new(void);
extern void MD4_Init(void);
extern void _ZN4bssl15SSL_SESSION_dupEP14ssl_session_sti(void);
extern void BIO_vfree(void);
extern void EVP_md5_sha1(void);
extern void X509_CRL_get_ext_d2i(void);
extern void i2d_ECPrivateKey_bio(void);
extern void DTLSv1_client_method(void);
extern void SSL_CIPHER_get_name(void);
extern void SSL_renegotiate_pending(void);
extern void i2d_ASN1_UTF8STRING(void);
extern void EC_POINT_free(void);
extern void X509_NAME_delete_entry(void);
extern void BIO_push(void);
extern void d2i_RSA_PUBKEY(void);
extern void i2d_RSA_PUBKEY(void);
extern void CTR_DRBG_free(void);
extern void _ZN4bssl19SealRecordPrefixLenEPK6ssl_stm(void);
extern void ASN1_UNIVERSALSTRING_it(void);
extern void ASN1_NULL_it(void);
extern void EVP_aes_128_ofb(void);
extern void BN_rand_range_ex(void);
extern void BN_sqrt(void);
extern void EVP_HPKE_CTX_kem(void);
extern void TRUST_TOKEN_CLIENT_add_key(void);
extern void i2d_X509_VAL(void);
extern void EVP_rc2_cbc(void);
extern void NCONF_load(void);
extern void EVP_CIPHER_block_size(void);
extern void RSA_padding_add_PKCS1_PSS_mgf1(void);
extern void X509_get0_authority_key_id(void);
extern void SSL_CTX_set_record_protocol_version(void);
extern void BN_div(void);
extern void PEM_read_bio_ECPrivateKey(void);
extern void PKCS8_PRIV_KEY_INFO_it(void);
extern void X509V3_EXT_nconf_nid(void);
extern void SSL_add_file_cert_subjects_to_stack(void);
extern void SSL_key_update(void);
extern void EVP_HPKE_KEM_enc_len(void);
extern void X509_SIG_it(void);
extern void CRYPTO_is_confidential_build(void);
extern void RSA_set_ex_data(void);
extern void X509_OBJECT_retrieve_by_subject(void);
extern void SSL_CTX_set_min_proto_version(void);
extern void SSL_get0_certificate_types(void);
extern void ASN1_STRING_TABLE_add(void);
extern void BIO_new_bio_pair(void);
extern void SSL_CTX_use_PrivateKey_file(void);
extern void SSL_CTX_set_session_cache_mode(void);
extern void CBB_flush(void);
extern void OpenSSL_version_num(void);
extern void PEM_read_bio_X509_CRL(void);
extern void PEM_read_bio_PKCS7(void);
extern void SSL_delegated_credential_used(void);
extern void PEM_read_bio_PKCS8(void);
extern void X509V3_get_d2i(void);
extern void X509_STORE_CTX_set_error(void);
extern void SSL_get_srtp_profiles(void);
extern void SSL_CTX_set_client_cert_cb(void);
extern void TLSv1_method(void);
extern void BIO_next(void);
extern void PEM_read_X509_AUX(void);
extern void CRYPTO_BUFFER_POOL_new(void);
extern void X509_check_akid(void);
extern void i2d_BASIC_CONSTRAINTS(void);
extern void DTLSv1_method(void);
extern void SSL_CTX_set_max_proto_version(void);
extern void ASN1_PRINTABLE_new(void);
extern void CRYPTO_once(void);
extern void d2i_DSAPublicKey(void);
extern void BN_mod_add(void);
extern void DH_get0_pub_key(void);
extern void EC_METHOD_get_field_type(void);
extern void BIO_meth_new(void);
extern void EVP_CIPHER_CTX_encrypting(void);
extern void PEM_read_X509_REQ(void);
extern void ASN1_mbstring_ncopy(void);
extern void OPENSSL_strnlen(void);
extern void EVP_CIPHER_CTX_free(void);
extern void TRUST_TOKEN_CLIENT_begin_issuance(void);
extern void X509_keyid_get0(void);
extern void SSL_use_certificate_ASN1(void);
extern void SSL_CTX_get_mode(void);
extern void SSL_CTX_sess_timeouts(void);
extern void EC_POINT_point2cbb(void);
extern void FIPS_read_counter(void);
extern void PKCS7_type_is_signedAndEnveloped(void);
extern void SSL_CTX_add_session(void);
extern void i2d_EC_PUBKEY_bio(void);
extern void ASN1_BIT_STRING_it(void);
extern void EVP_CIPHER_key_length(void);
extern void NCONF_get_section(void);
extern void EVP_SignInit_ex(void);
extern void X509_EXTENSIONS_it(void);
extern void SSL_get_cipher_list(void);
extern void SSL_enable_signed_cert_timestamps(void);
extern void SSL_get_psk_identity(void);
extern void BIO_printf(void);
extern void ASN1_BMPSTRING_new(void);
extern void EVP_aead_des_ede3_cbc_sha1_tls(void);
extern void AES_unwrap_key(void);
extern void X509_STORE_set_default_paths(void);
extern void SSL_use_PrivateKey(void);
extern void ASN1_TYPE_new(void);
extern void RSA_verify_raw(void);
extern void d2i_X509(void);
extern void X509at_get_attr(void);
extern void SSL_get_verify_callback(void);
extern void sk_pop_free_ex(void);
extern void X509_EXTENSION_get_data(void);
extern void d2i_CERTIFICATEPOLICIES(void);
extern void SSL_CIPHER_get_auth_nid(void);
extern void ENGINE_load_builtin_engines(void);
extern void ERR_peek_last_error_line(void);
extern void EVP_MD_CTX_new(void);
extern void EC_GROUP_dup(void);
extern void CRYPTO_tls1_prf(void);
extern void PEM_ASN1_read(void);
extern void BIO_set_ssl(void);
extern void SSL_CTX_set_default_passwd_cb(void);
extern void X509_NAME_ENTRY_it(void);
extern void SSL_certs_clear(void);
extern void SSL_set_quic_use_legacy_codepoint(void);
extern void PEM_write_X509_AUX(void);
extern void NETSCAPE_SPKI_free(void);
extern void ERR_peek_error(void);
extern void BASIC_CONSTRAINTS_free(void);
extern void PEM_write_X509_REQ(void);
extern void X509V3_EXT_cleanup(void);
extern void AES_wrap_key_padded(void);
extern void i2d_RSAPublicKey_bio(void);
extern void CRYPTO_get_fork_generation(void);
extern void AES_ofb128_encrypt(void);
extern void PKCS7_bundle_CRLs(void);
extern void X509_STORE_set_check_revocation(void);
extern void X509_STORE_get_cleanup(void);
extern void SSL_CTX_set1_sigalgs(void);
extern void PEM_write_bio_PrivateKey(void);
extern void SSL_COMP_get_id(void);
extern void SSL_CTX_add0_chain_cert(void);
extern void SSL_can_release_private_key(void);
extern void c2i_ASN1_OBJECT(void);
extern void X509_STORE_get0_param(void);
extern void SSL_CTX_set_client_CA_list(void);
extern void CBS_parse_utc_time(void);
extern void BIO_find_type(void);
extern void ED25519_keypair(void);
extern void EVP_VerifyInit_ex(void);
extern void OBJ_sn2nid(void);
extern void PEM_write_bio_DSAPrivateKey(void);
extern void X509_LOOKUP_new(void);
extern void SSL_get0_alpn_selected(void);
extern void ASN1_TIME_set_string(void);
extern void EVP_aead_chacha20_poly1305(void);
extern void i2d_PublicKey(void);
extern void SSL_set_read_ahead(void);
extern void BIO_get_new_index(void);
extern void SSL_CTX_get0_param(void);
extern void BUF_MEM_grow_clean(void);
extern void EVP_DigestFinalXOF(void);
extern void EVP_MD_CTX_md(void);
extern void PEM_write_bio_PKCS8PrivateKey(void);
extern void d2i_X509_SIG(void);
extern void X509_print_ex(void);
extern void X509_VERIFY_PARAM_set1_ip_asc(void);
extern void SSL_COMP_get0_name(void);
extern void SSL_set1_curves(void);
extern void EVP_HPKE_KEY_generate(void);
extern void DIST_POINT_it(void);
extern void DSA_SIG_get0(void);
extern void EVP_PKEY_CTX_new_id(void);
extern void EC_KEY_new_by_curve_name(void);
extern void PKCS12_create(void);
extern void SSL_set_state(void);
extern void TLSv1_1_server_method(void);
extern void ASN1_STRING_length(void);
extern void ECDSA_SIG_marshal(void);
extern void HMAC_CTX_free(void);
extern void PEM_write_bio_X509_CRL(void);
extern void i2d_RSAPrivateKey(void);
extern void RSA_generate_key_fips(void);
extern void d2i_PKCS8_PRIV_KEY_INFO_bio(void);
extern void SSL_CTX_set1_curves(void);
extern void SSL_set_tmp_dh_callback(void);
extern void EVP_MD_CTX_size(void);
extern void RSA_get0_key(void);
extern void CRYPTO_BUFFER_init_CBS(void);
extern void SSL_set_tlsext_status_ocsp_resp(void);
extern void SSLv23_client_method(void);
extern void sk_sort(void);
extern void i2d_DSA_PUBKEY_fp(void);
extern void SSL_alert_desc_string_long(void);
extern void ASN1_STRING_cmp(void);
extern void d2i_ASN1_T61STRING(void);
extern void EVP_PKEY_CTX_dup(void);
extern void PEM_read_DHparams(void);
extern void X509_REQ_get_subject_name(void);
extern void X509_CRL_dup(void);
extern void SSL_set1_host(void);
extern void X509_VERIFY_PARAM_set_time(void);
extern void ASN1_BIT_STRING_free(void);
extern void BIO_puts(void);
extern void RSA_get0_crt_params(void);
extern void CRYPTO_set_id_callback(void);
extern void X509_STORE_get_by_subject(void);
extern void BN_uadd(void);
extern void SSLv23_method(void);
extern void EVP_PKEY_paramgen_init(void);
extern void TRUST_TOKEN_ISSUER_set_srr_key(void);
extern void X509_REQ_add_extensions_nid(void);
extern void i2d_DIST_POINT_NAME(void);
extern void BIO_do_connect(void);
extern void RC4(void);
extern void ECDH_compute_key(void);
extern void ENGINE_get_ECDSA_method(void);
extern void DH_compute_key(void);
extern void PEM_write_bio_RSAPrivateKey(void);
extern void d2i_X509_VAL(void);
extern void SSL_CTX_set1_sigalgs_list(void);
extern void BIO_seek(void);
extern void X509V3_EXT_val_prn(void);
extern void SSL_max_seal_overhead(void);
extern void DSA_SIG_new(void);
extern void EVP_PKEY_derive_init(void);
extern void i2d_ASN1_BOOLEAN(void);
extern void _ZN4bssl17SSL_SESSION_parseEP6cbs_stPKNS_15SSL_X509_METHODEP21crypto_buffer_pool_st(void);
extern void ASN1_TIME_check(void);
extern void sk_new_null(void);
extern void EVP_AEAD_nonce_length(void);
extern void TRUST_TOKEN_free(void);
extern void SSL_CIPHER_get_min_version(void);
extern void OBJ_obj2nid(void);
extern void EC_POINT_set_to_infinity(void);
extern void X509_getm_notBefore(void);
extern void X509_EXTENSION_it(void);
extern void SSL_get_server_tmp_key(void);
extern void sk_set(void);
extern void EVP_aead_aes_256_gcm_randnonce(void);
extern void SSL_set1_param(void);
extern void BN_mod_exp_mont(void);
extern void EVP_DigestVerifyFinal(void);
extern void ASN1_digest(void);
extern void X509_NAME_add_entry_by_OBJ(void);
extern void X509_PUBKEY_free(void);
extern void SSL_CTX_set_num_tickets(void);
extern void i2d_ASN1_OBJECT(void);
extern void SSL_CTX_use_RSAPrivateKey_file(void);
extern void SSL_get0_ocsp_response(void);
extern void SSL_CTX_set_quic_method(void);
extern void _ZN4bssl10OpenRecordEP6ssl_stPNS_4SpanIhEEPmPhS3_(void);
extern void PEM_write_DHparams(void);
extern void BIO_method_type(void);
extern void EVP_PKEY_print_public(void);
extern void BN_is_bit_set(void);
extern void EVP_HPKE_CTX_open(void);
extern void X509_ATTRIBUTE_count(void);
extern void X509_REVOKED_get_ext_by_OBJ(void);
extern void X509_get0_extensions(void);
extern void i2c_ASN1_INTEGER(void);
extern void BIO_int_ctrl(void);
extern void EVP_PKEY_CTX_get_signature_md(void);
extern void BN_gcd(void);
extern void EC_curve_nist2nid(void);
extern void X509_CRL_add1_ext_i2d(void);
extern void X509_STORE_CTX_set_purpose(void);
extern void X509V3_set_ctx(void);
extern void d2i_ASN1_ENUMERATED(void);
extern void OPENSSL_strdup(void);
extern void EVP_CIPHER_CTX_cleanup(void);
extern void X509_issuer_name_hash(void);
extern void X509_find_by_subject(void);
extern void CBS_peek_asn1_tag(void);
extern void X509_PKEY_new(void);
extern void TRUST_TOKEN_derive_key_from_secret(void);
extern void X509_print_fp(void);
extern void DSA_marshal_parameters(void);
extern void i2d_PKCS8PrivateKey_nid_fp(void);
extern void EVP_PKEY_get1_EC_KEY(void);
extern void i2d_X509_CRL_INFO(void);
extern void d2i_GENERAL_NAME(void);
extern void sk_free(void);
extern void BIO_meth_set_destroy(void);
extern void BN_parse_asn1_unsigned(void);
extern void BN_mod_mul_montgomery(void);
extern void POLICY_CONSTRAINTS_new(void);
extern void SSL_set_private_key_method(void);
extern void cbs_get_utf8(void);
extern void ASN1_GENERALSTRING_new(void);
extern void SPAKE2_CTX_free(void);
extern void EVP_Cipher(void);
extern void HMAC_CTX_reset(void);
extern void X509_STORE_CTX_set_ex_data(void);
extern void EVP_sha384(void);
extern void EVP_PKEY_get1_tls_encodedpoint(void);
extern void RSA_PSS_PARAMS_new(void);
extern void NOTICEREF_new(void);
extern void SSL_generate_key_block(void);
extern void ASN1_INTEGER_set(void);
extern void EVP_PKEY_CTX_get_rsa_oaep_md(void);
extern void d2i_X509_EXTENSION(void);
extern void SSL_send_fatal_alert(void);
extern void SSL_cache_hit(void);
extern void ERR_print_errors_cb(void);
extern void RAND_enable_fork_unsafe_buffering(void);
extern void X509_STORE_get_verify(void);
extern void BIO_new(void);
extern void SSL_read(void);
extern void SSL_ECH_KEYS_free(void);
extern void SSL_get_client_CA_list(void);
extern void ASN1_STRING_TABLE_cleanup(void);
extern void BN_num_bits(void);
extern void CRYPTO_MUTEX_lock_write(void);
extern void HMAC(void);
extern void SSL_set_tmp_rsa_callback(void);
extern void ASN1_UTCTIME_set_string(void);
extern void OPENSSL_strndup(void);
extern void EVP_hpke_x25519_hkdf_sha256(void);
extern void SSL_set_tmp_rsa(void);
extern void BN_bn2hex(void);
extern void ERR_get_next_error_library(void);
extern void EVP_DigestFinal(void);
extern void X509_CRL_get_ext(void);
extern void SSL_state_string(void);
extern void CRYPTO_memcmp(void);
extern void PKCS7_type_is_digest(void);
extern void X509_REQ_add1_attr_by_NID(void);
extern void X509_STORE_CTX_set_default(void);
extern void X509_REQ_sign_ctx(void);
extern void X509_pubkey_digest(void);
extern void ASN1_OCTET_STRING_new(void);
extern void X509_STORE_get1_crls(void);
extern void SSL_CTX_set_default_passwd_cb_userdata(void);
extern void EVP_EncryptInit(void);
extern void CRYPTO_free(void);
extern void X509_free(void);
extern void GENERAL_NAME_dup(void);
extern void PROXY_POLICY_free(void);
extern void SSL_CTX_set_session_id_context(void);
extern void SSL_CTX_set_tmp_rsa(void);
extern void SSL_CTX_get_client_CA_list(void);
extern void BIO_indent(void);
extern void DSA_new(void);
extern void CRYPTO_set_locking_callback(void);
extern void X509_TRUST_get_trust(void);
extern void d2i_RSAPrivateKey_fp(void);
extern void SSL_get_selected_srtp_profile(void);
extern void ASN1_STRING_get_default_mask(void);
extern void EVP_MD_flags(void);
extern void CRYPTO_get_dynlock_lock_callback(void);
extern void X509_load_cert_file(void);
extern void SSL_CTX_set_early_data_enabled(void);
extern void ASN1_ANY_it(void);
extern void X509_NAME_get_index_by_NID(void);
extern void SSL_is_server(void);
extern void ASN1_INTEGER_set_uint64(void);
extern void X509_get0_serialNumber(void);
extern void i2d_X509_CRL_fp(void);
extern void SSL_set_quic_method(void);
extern void DSA_get_ex_data(void);
extern void X509_REQ_set1_signature_value(void);
extern void i2d_POLICYQUALINFO(void);
extern void SSL_CTX_set_ticket_aead_method(void);
extern void SSL_SESSION_get_id(void);
extern void SSL_SESSION_has_peer_sha256(void);
extern void i2d_DSAPublicKey(void);
extern void PEM_read_bio_X509(void);
extern void d2i_PUBKEY_fp(void);
extern void OBJ_dup(void);
extern void BN_secure_new(void);
extern void ECDSA_sign_with_nonce_and_leak_private_key_for_testing(void);
extern void X509_REVOKED_get_ext_d2i(void);
extern void d2i_X509_EXTENSIONS(void);
extern void X509V3_conf_free(void);
extern void EVP_AEAD_CTX_seal(void);
extern void RSA_PSS_PARAMS_free(void);
extern void SSLeay_version(void);
extern void BN_lshift(void);
extern void BN_exp(void);
extern void i2d_PKCS7_bio(void);
extern void PKCS8_parse_encrypted_private_key(void);
extern void X509_STORE_get_lookup_certs(void);
extern void OTHERNAME_cmp(void);
extern void X509V3_add_value_uchar(void);
extern void SSL_get_wfd(void);
extern void X509_STORE_load_locations(void);
extern void X509_time_adj(void);
extern void SSL_get_shutdown(void);
extern void cbs_get_utf32_be(void);
extern void BN_mpi2bn(void);
extern void EVP_aead_aes_128_ccm_bluetooth_8(void);
extern void OPENSSL_tm_to_posix(void);
extern void d2i_ASN1_OCTET_STRING(void);
extern void X509_NAME_ENTRY_set_object(void);
extern void MD5_Transform(void);
extern void SSL_CTX_remove_session(void);
extern void c2i_ASN1_INTEGER(void);
extern void BN_nnmod(void);
extern void EVP_PKEY2PKCS8(void);
extern void X509_check_purpose(void);
extern void i2d_ISSUING_DIST_POINT(void);
extern void EXTENDED_KEY_USAGE_new(void);
extern void CRYPTO_chacha_20(void);
extern void X509_SIG_get0(void);
extern void SSL_CTX_use_certificate_chain_file(void);
extern void OpenSSL_add_all_ciphers(void);
extern void SSL_CTX_sess_set_new_cb(void);
extern void OPENSSL_load_builtin_modules(void);
extern void TRUST_TOKEN_PRETOKEN_free(void);
extern void i2d_ASN1_NULL(void);
extern void BN_free(void);
extern void X509at_add1_attr_by_NID(void);
extern void SSL_add_application_settings(void);
extern void EVP_PKEY_CTX_get_rsa_padding(void);
extern void BN_count_low_zero_bits(void);
extern void X509_CRL_get_lastUpdate(void);
extern void X509_ATTRIBUTE_create(void);
extern void s2i_ASN1_OCTET_STRING(void);
extern void SSL_CTX_get0_chain_certs(void);
extern void BIO_ctrl_get_read_request(void);
extern void BN_generate_prime_ex(void);
extern void ERR_peek_error_line_data(void);
extern void AES_cbc_encrypt(void);
extern void BN_is_word(void);
extern void i2d_X509_CRL(void);
extern void PEM_write_bio_X509(void);
extern void X509_STORE_set_cert_crl(void);
extern void i2d_X509_tbs(void);
extern void DTLS_server_method(void);
extern void SSL_set1_delegated_credential(void);
extern void SSL_CTX_set0_verify_cert_store(void);
extern void X509_STORE_get_get_issuer(void);
extern void ASN1_item_free(void);
extern void BN_hex2bn(void);
extern void ASN1_item_pack(void);
extern void BASIC_CONSTRAINTS_new(void);
extern void SSL_accept(void);
extern void SSL_SESSION_early_data_capable(void);
extern void SSL_get0_param(void);
extern void CBS_get_until_first(void);
extern void SSL_ECH_KEYS_up_ref(void);
extern void SSL_CTX_get0_chain(void);
extern void BIO_reset(void);
extern void X509_check_ca(void);
extern void DTLSv1_2_server_method(void);
extern void SSL_ECH_KEYS_has_duplicate_config_id(void);
extern void CRYPTO_MUTEX_unlock_write(void);
extern void NETSCAPE_SPKI_it(void);
extern void SSL_get_secure_renegotiation_support(void);
extern void BN_mod_lshift1_quick(void);
extern void EDIPARTYNAME_free(void);
extern void SSL_SESSION_get_ex_new_index(void);
extern void ASN1_ENUMERATED_it(void);
extern void CBB_add_asn1(void);
extern void ECDSA_SIG_to_bytes(void);
extern void EC_GROUP_get_cofactor(void);
extern void PKCS8_decrypt(void);
extern void SSL_get_verify_result(void);
extern void SSL_get_SSL_CTX(void);
extern void sk_pop(void);
extern void d2i_ASN1_BIT_STRING(void);
extern void OPENSSL_no_config(void);
extern void DSA_check_signature(void);
extern void AES_decrypt(void);
extern void EVP_hpke_hkdf_sha256(void);
extern void X509_NAME_ENTRY_set_data(void);
extern void i2v_GENERAL_NAMES(void);
extern void SSL_add1_chain_cert(void);
extern void ASN1_ENUMERATED_set_uint64(void);
extern void RSAPrivateKey_dup(void);
extern void i2d_X509_EXTENSION(void);
extern void SSL_get_privatekey(void);
extern void CBB_add_bytes(void);
extern void EVP_MD_block_size(void);
extern void d2i_PrivateKey(void);
extern void BN_get_word(void);
extern void EVP_CIPHER_flags(void);
extern void EVP_DigestVerifyUpdate(void);
extern void i2d_PrivateKey(void);
extern void X509_STORE_get_get_crl(void);
extern void X509_CRL_sign_ctx(void);
extern void EC_KEY_check_key(void);
extern void EVP_PKEY_get0_DSA(void);
extern void POLICYINFO_free(void);
extern void SSL_SESSION_get0_signed_cert_timestamp_list(void);
extern void X509_check_host(void);
extern void TLS_with_buffers_method(void);
extern void MD5_Final(void);
extern void EVP_HPKE_CTX_free(void);
extern void CRYPTO_BUFFER_new_from_CBS(void);
extern void SSL_clear(void);
extern void SSL_set_cert_cb(void);
extern void SSL_CTX_set1_tls_channel_id(void);
extern void SSL_set_purpose(void);
extern void d2i_PublicKey(void);
extern void EVP_add_cipher_alias(void);
extern void PEM_write_EC_PUBKEY(void);
extern void SSL_library_init(void);
extern void CRYPTO_gcm128_aad(void);
extern void X509_STORE_get_verify_cb(void);
extern void i2d_EXTENDED_KEY_USAGE(void);
extern void _ZN4bssl21ssl_client_hello_initEPK6ssl_stP22ssl_early_callback_ctxNS_4SpanIKhEE(void);
extern void SSL_get_early_data_reason(void);
extern void X509_NAME_ENTRY_free(void);
extern void GENERAL_SUBTREE_it(void);
extern void SSL_CTX_sess_set_remove_cb(void);
extern void cbb_add_utf32_be(void);
extern void ASN1_GENERALIZEDTIME_new(void);
extern void EVP_sha1(void);
extern void TRUST_TOKEN_CLIENT_free(void);
extern void USERNOTICE_free(void);
extern void SSL_CTX_set_cert_cb(void);
extern void SSL_CTX_set_purpose(void);
extern void SSL_set1_chain(void);
extern void BN_sqr(void);
extern void TRUST_TOKEN_experiment_v1(void);
extern void X509_ATTRIBUTE_get0_data(void);
extern void ASN1_UTCTIME_check(void);
extern void CBS_get_u8_length_prefixed(void);
extern void CRYPTO_MUTEX_lock_read(void);
extern void PEM_read_RSA_PUBKEY(void);
extern void POLICY_MAPPING_new(void);
extern void CBS_get_u16_length_prefixed(void);
extern void EVP_aead_aes_128_gcm_siv(void);
extern void AES_encrypt(void);
extern void ECDSA_verify(void);
extern void X509_VERIFY_PARAM_add0_table(void);
extern void SSL_get0_ech_retry_configs(void);
extern void SSL_get_tls_unique(void);
extern void CMAC_Final(void);
extern void i2d_X509_NAME_ENTRY(void);
extern void SSL_get0_session_id_context(void);
extern void DIRECTORYSTRING_free(void);
extern void EVP_ENCODE_CTX_new(void);
extern void X509_get_default_cert_dir_env(void);
extern void d2i_SSL_SESSION_bio(void);
extern void d2i_ASN1_GENERALIZEDTIME(void);
extern void CBB_add_u64le(void);
extern void d2i_PKCS8PrivateKey_fp(void);
extern void RAND_file_name(void);
extern void X509_LOOKUP_ctrl(void);
extern void AUTHORITY_INFO_ACCESS_free(void);
extern void SSL_CTX_get_session_cache_mode(void);
extern void BN_is_prime_fasttest_ex(void);
extern void EVP_PKEY_assign_EC_KEY(void);
extern void ASN1_INTEGER_cmp(void);
extern void CBB_add_asn1_uint64(void);
extern void SSL_set_verify_depth(void);
extern void asn1_utctime_to_tm(void);
extern void DES_set_key_unchecked(void);
extern void HKDF_expand(void);
extern void EVP_HPKE_CTX_aead(void);
extern void X25519_public_from_private(void);
extern void X509_TRUST_get_count(void);
extern void X509_REQ_INFO_new(void);
extern void i2d_GENERAL_NAME(void);
extern void BN_set_negative(void);
extern void CBB_add_u16le(void);
extern void DSA_set0_key(void);
extern void EVP_PKEY_print_params(void);
extern void X509_REQ_digest(void);
extern void BUF_strlcat(void);
extern void SSL_CTX_cipher_in_group(void);
extern void SSL_set1_groups_list(void);
extern void CRYPTO_gcm128_decrypt(void);
extern void TRUST_TOKEN_ISSUER_redeem(void);
extern void X509_CRL_add_ext(void);
extern void BIO_callback_ctrl(void);
extern void DES_set_key(void);
extern void BN_mod_pow2(void);
extern void PEM_X509_INFO_read_bio(void);
extern void i2d_ASN1_ENUMERATED(void);
extern void EVP_des_ecb(void);
extern void X509_VERIFY_PARAM_set1_email(void);
extern void BN_mod_exp_mont_consttime(void);
extern void EVP_hpke_aes_256_gcm(void);
extern void BIO_flush(void);
extern void EC_KEY_parse_curve_name(void);
extern void MD5_Init(void);
extern void d2i_X509_CRL(void);
extern void X509_get_serialNumber(void);
extern void ACCESS_DESCRIPTION_it(void);
extern void SSL_set_hostflags(void);
extern void PEM_write_bio_DSA_PUBKEY(void);
extern void X509_CERT_AUX_new(void);
extern void X509V3_EXT_add_nconf_sk(void);
extern void SSL_get_signature_algorithm_digest(void);
extern void SSL_magic_pending_session_ptr(void);
extern void RSA_sign_pss_mgf1(void);
extern void d2i_GENERAL_NAMES(void);
extern void EVP_aes_128_ctr(void);
extern void i2s_ASN1_INTEGER(void);
extern void SSL_set_max_send_fragment(void);
extern void BIO_set_close(void);
extern void RAND_bytes(void);
extern void RSA_set0_factors(void);
extern void EVP_HPKE_CTX_setup_sender_with_seed_for_testing(void);
extern void NETSCAPE_SPKI_verify(void);
extern void a2i_IPADDRESS(void);
extern void EVP_PKEY_set1_RSA(void);
extern void BN_one(void);
extern void TRUST_TOKEN_ISSUER_issue(void);
extern void d2i_PKCS8_PRIV_KEY_INFO_fp(void);
extern void POLICYQUALINFO_new(void);
extern void d2i_PROXY_CERT_INFO_EXTENSION(void);
extern void SSL_CTX_free(void);
extern void SSL_get_servername_type(void);
extern void ASN1_GENERALIZEDTIME_adj(void);
extern void ERR_peek_error_line(void);
extern void X509_get_version(void);
extern void X509_VERIFY_PARAM_table_cleanup(void);
extern void NOTICEREF_it(void);
extern void SSL_CIPHER_get_bits(void);
extern void SHA512_Init(void);
extern void BN_clear(void);
extern void CRYPTO_BUFFER_new(void);
extern void X509_PUBKEY_it(void);
extern void EVP_PKEY_CTX_get0_rsa_oaep_label(void);
extern void X509_STORE_get_check_issued(void);
extern void EVP_get_digestbynid(void);
extern void CRYPTO_gcm128_encrypt(void);
extern void X509_SIG_getm(void);
extern void SSL_CTX_set_psk_client_callback(void);
extern void i2d_PUBKEY(void);
extern void X509_ALGOR_set0(void);
extern void X509_STORE_get_lookup_crls(void);
extern void SSL_CTX_enable_tls_channel_id(void);
extern void CRYPTO_BUFFER_data(void);
extern void TLS_method(void);
extern void EC_KEY_set_public_key(void);
extern void BIO_vsnprintf(void);
extern void EVP_SignFinal(void);
extern void CRYPTO_BUFFER_POOL_free(void);
extern void _ZN4bssl14CBBFinishArrayEP6cbb_stPNS_5ArrayIhEE(void);
extern void SSL_get_ciphers(void);
extern void i2t_ASN1_OBJECT(void);
extern void ASN1_OBJECT_it(void);
extern void PEM_read_bio_PKCS8_PRIV_KEY_INFO(void);
extern void X509_ALGOR_set_md(void);
extern void BUF_MEM_free(void);
extern void EVP_PKEY_type(void);
extern void d2i_PUBKEY(void);
extern void PEM_read_bio_RSA_PUBKEY(void);
extern void i2d_re_X509_REQ_tbs(void);
extern void X509_new(void);
extern void SSL_CTX_set_tmp_dh(void);
extern void EVP_EncryptFinal(void);
extern void EC_POINT_copy(void);
extern void DES_ncbc_encrypt(void);
extern void X509_STORE_CTX_get0_cert(void);
extern void X509_VERIFY_PARAM_get0(void);
extern void X509V3_add_standard_extensions(void);
extern void SSL_CTX_get_ciphers(void);
extern void SSL_set_min_proto_version(void);
extern void X509_REVOKED_new(void);
extern void CBS_parse_generalized_time(void);
extern void BN_is_negative(void);
extern void BN_pseudo_rand(void);
extern void PEM_write_PKCS7(void);
extern void ASN1_STRING_print(void);
extern void PEM_write_PKCS8(void);
extern void i2d_X509_EXTENSIONS(void);
extern void OBJ_get0_data(void);
extern void ASN1_item_sign_ctx(void);
extern void SSL_get_server_random(void);
extern void EVP_CIPHER_nid(void);
extern void EC_get_builtin_curves(void);
extern void PEM_write_RSA_PUBKEY(void);
extern void SIPHASH_24(void);
extern void GENERAL_NAME_set0_othername(void);
extern void SSL_set0_wbio(void);
extern void SSL_set_max_proto_version(void);
extern void BIO_free(void);
extern void ASN1_PRINTABLE_it(void);
extern void d2i_DSAPrivateKey_fp(void);
extern void d2i_ASN1_VISIBLESTRING(void);
extern void CBB_add_zeros(void);
extern void BN_CTX_end(void);
extern void ERR_remove_state(void);
extern void AES_wrap_key(void);
extern void EVP_HPKE_CTX_export(void);
extern void X509_signature_print(void);
extern void X509_get_notBefore(void);
extern void X509_STORE_CTX_get_chain(void);
extern void ASN1_IA5STRING_new(void);
extern void d2i_ASN1_UTCTIME(void);
extern void BN_mod_mul(void);
extern void d2i_DHparams(void);
extern void EC_GROUP_cmp(void);
extern void ERR_error_string(void);
extern void BN_get_u64(void);
extern void X509_LOOKUP_by_subject(void);
extern void NAME_CONSTRAINTS_new(void);
extern void X509_supported_extension(void);
extern void ASN1_STRING_print_ex_fp(void);
extern void OBJ_nid2obj(void);
extern void PEM_get_EVP_CIPHER_INFO(void);
extern void CRYPTO_BUFFER_free(void);
extern void X509V3_add_value_bool(void);
extern void SSL_CTX_get_default_passwd_cb(void);
extern void X509_CRL_INFO_new(void);
extern void SSL_SESSION_get0_ocsp_response(void);
extern void CRYPTO_refcount_dec_and_test_zero(void);
extern void EVP_CIPHER_CTX_copy(void);
extern void d2i_X509_ATTRIBUTE(void);
extern void X509_get_ex_new_index(void);
extern void X509_subject_name_hash(void);
extern void BIO_shutdown_wr(void);
extern void CMAC_Update(void);
extern void X509_VERIFY_PARAM_set_trust(void);
extern void GENERAL_NAME_get0_value(void);
extern void SSL_get_tlsext_status_ocsp_resp(void);
extern void EVP_des_ede(void);
extern void EVP_PKEY_CTX_ctrl(void);
extern void EVP_PKCS82PKEY(void);
extern void X509_set1_notBefore(void);
extern void X509_STORE_CTX_set_depth(void);
extern void SSL_CIPHER_get_rfc_name(void);
extern void SSL_get_shared_sigalgs(void);
extern void EVP_des_cbc(void);
extern void DSAparams_dup(void);
extern void EVP_PKEY_CTX_get_rsa_mgf1_md(void);
extern void X509_REQ_get_version(void);
extern void i2d_ASN1_SEQUENCE_ANY(void);
extern void BIO_set_init(void);
extern void PKCS7_sign(void);
extern void X509v3_get_ext_by_NID(void);
extern void X509_CRL_set1_nextUpdate(void);
extern void i2s_ASN1_OCTET_STRING(void);
extern void SSL_set_info_callback(void);
extern void DSA_get0_pub_key(void);
extern void PEM_ASN1_write(void);
extern void d2i_DSAparams(void);
extern void EVP_MD_CTX_destroy(void);
extern void X509_EXTENSION_get_critical(void);
extern void X509_get_pubkey(void);
extern void X509_STORE_set_check_crl(void);
extern void X509_CINF_free(void);
extern void BUF_MEM_reserve(void);
extern void RSA_private_key_to_bytes(void);
extern void SSL_set_custom_verify(void);
extern void SSL_SESSION_copy_without_early_data(void);
extern void ASN1_item_new(void);
extern void ENGINE_free(void);
extern void ERR_print_errors_fp(void);
extern void BN_mod_add_quick(void);
extern void BN_MONT_CTX_new(void);
extern void EVP_MD_CTX_cleanse(void);
extern void d2i_EC_PUBKEY_bio(void);
extern void i2d_X509_PUBKEY(void);
extern void ASN1_BIT_STRING_check(void);
extern void ECDSA_SIG_parse(void);
extern void BORINGSSL_self_test(void);
extern void RSA_get0_iqmp(void);
extern void i2d_PUBKEY_fp(void);
extern void SSL_set_quic_transport_params(void);
extern void d2i_ECPrivateKey(void);
extern void DH_set_length(void);
extern void X509_issuer_name_hash_old(void);
extern void i2d_ECPrivateKey_fp(void);
extern void EVP_CIPHER_CTX_set_app_data(void);
extern void X509_REQ_get_pubkey(void);
extern void SSL_get0_server_requested_CAs(void);
extern void SSL_CTX_enable_ocsp_stapling(void);
extern void BIO_get_fd(void);
extern void DH_check_pub_key(void);
extern void SSL_get0_peer_delegation_algorithms(void);
extern void i2d_ASN1_BIT_STRING(void);
extern void ASN1_VISIBLESTRING_it(void);
extern void SHA256_TransformBlocks(void);
extern void PEM_read_bio_X509_AUX(void);
extern void X509_CRL_cmp(void);
extern void X509_REQ_add1_attr_by_OBJ(void);
extern void BN_bn2dec(void);
extern void EVP_get_cipherbyname(void);
extern void EVP_aes_128_gcm(void);
extern void o2i_ECPublicKey(void);
extern void PEM_read_bio_X509_REQ(void);
extern void SSL_set_srtp_profiles(void);
extern void SSL_CTX_use_certificate(void);
extern void ASN1_OCTET_STRING_dup(void);
extern void CBB_data(void);
extern void EVP_DigestVerifyInit(void);
extern void PEM_ASN1_write_bio(void);
extern void ASN1_STRING_get0_data(void);
extern void BIO_set_write_buffer_size(void);
extern void TRUST_TOKEN_CLIENT_begin_redemption(void);
extern void CERTIFICATEPOLICIES_free(void);
extern void SSL_session_reused(void);
extern void BIO_set_mem_eof_return(void);
extern void BN_rshift(void);
extern void X509_NAME_get_index_by_OBJ(void);
extern void X509V3_EXT_i2d(void);
extern void DTLSv1_server_method(void);
extern void i2d_X509_bio(void);
extern void X509_REQ_it(void);
extern void DIST_POINT_NAME_free(void);
extern void X509_PURPOSE_get_trust(void);
extern void DES_ecb3_encrypt(void);
extern void FIPS_version(void);
extern void RSA_private_key_from_bytes(void);
extern void SSL_set_signing_algorithm_prefs(void);
extern void EVP_DigestFinal_ex(void);
extern void SHA256_Init(void);
extern void bn_lcm_consttime(void);
extern void HMAC_CTX_copy(void);
extern void X509_VERIFY_PARAM_set_purpose(void);
extern void CBS_asn1_oid_to_text(void);
extern void EC_GROUP_get_degree(void);
extern void TRUST_TOKEN_new(void);
extern void SSL_CIPHER_get_id(void);
extern void SSL_set_handshake_hints(void);
extern void SSL_SESSION_set_time(void);
extern void OpenSSL_version(void);
extern void EVP_PKEY_encrypt_init(void);
extern void EVP_aead_aes_128_gcm_tls12(void);
extern void d2i_DIST_POINT(void);
extern void i2d_DIST_POINT(void);
extern void SSL_set1_curves_list(void);
extern void BIO_get_fp(void);
extern void EVP_sha1_final_with_secret_suffix(void);
extern void EVP_aead_aes_128_gcm_tls13(void);
extern void X509_REQ_INFO_free(void);
extern void SSL_CIPHER_get_digest_nid(void);
extern void RSA_get_ex_new_index(void);
extern void X509_sign_ctx(void);
extern void X509_STORE_CTX_set_cert(void);
extern void d2i_EC_PUBKEY_fp(void);
extern void SSL_set_options(void);
extern void d2i_RSAPrivateKey(void);
extern void BIO_set_conn_hostname(void);
extern void EVP_enc_null(void);
extern void EVP_DigestSignUpdate(void);
extern void OPENSSL_strncasecmp(void);
extern void X509_NAME_get_entry(void);
extern void GENERAL_NAME_get0_otherName(void);
extern void SSL_quic_write_level(void);
extern void SSL_CTX_add_extra_chain_cert(void);
extern void CBS_skip(void);
extern void CRYPTO_free_ex_data(void);
extern void DSA_SIG_marshal(void);
extern void i2d_RSAPrivateKey_fp(void);
extern void d2i_DSA_PUBKEY_fp(void);
extern void SSL_CTX_set_options(void);
extern void EVP_MD_CTX_reset(void);
extern void OPENSSL_lh_delete(void);
extern void X509_load_cert_crl_file(void);
extern void X509V3_EXT_nconf(void);
extern void d2i_AUTHORITY_INFO_ACCESS(void);
extern void OPENSSL_gmtime(void);
extern void BIO_get_mem_ptr(void);
extern void i2o_ECPublicKey(void);
extern void EVP_PKEY_assign_DSA(void);
extern void bn_mod_u16_consttime(void);
extern void BN_set_bit(void);
extern void CRYPTO_gcm128_init_key(void);
extern void CMAC_CTX_free(void);
extern void CRYPTO_set_dynlock_create_callback(void);
extern void X509at_add1_attr_by_OBJ(void);
extern void NETSCAPE_SPKAC_free(void);
extern void SSL_set1_ech_config_list(void);
extern void ASN1_STRING_set_default_mask_asc(void);
extern void i2d_DSAPrivateKey(void);
extern void ECDSA_do_verify(void);
extern void BN_le2bn(void);
extern void HRSS_generate_key(void);
extern void CRYPTO_BUFFER_len(void);
extern void AUTHORITY_KEYID_new(void);
extern void BN_get_rfc3526_prime_2048(void);
extern void EVP_PKEY_CTX_set1_hkdf_key(void);
extern void X509_CRL_set_version(void);
extern void X509_CRL_set1_signature_value(void);
extern void TLSv1_client_method(void);
extern void ASN1_UTCTIME_it(void);
extern void X509_subject_name_hash_old(void);
extern void X509_REQ_set1_signature_algo(void);
extern void X509_SIG_new(void);
extern void i2d_ACCESS_DESCRIPTION(void);
extern void SSL_set_client_CA_list(void);
extern void PEM_write_bio_X509_REQ_NEW(void);
extern void PEM_read_bio_DSAparams(void);
extern void X509_REQ_sign(void);
extern void EXTENDED_KEY_USAGE_it(void);
extern void EVP_aead_aes_256_ctr_hmac_sha256(void);
extern void X509_CRL_match(void);
extern void SSL_serialize_capabilities(void);
extern void SSL_use_certificate(void);
extern void RSA_get0_d(void);
extern void X509_verify_cert_error_string(void);
extern void BIO_write_all(void);
extern void BIO_number_read(void);
extern void RSA_get0_e(void);
extern void X509_subject_name_cmp(void);
extern void X509_STORE_CTX_get0_param(void);
extern void X509_REQ_verify(void);
extern void X509_get_key_usage(void);
extern void DH_get0_priv_key(void);
extern void X509_REVOKED_add1_ext_i2d(void);
extern void X509_PURPOSE_get_id(void);
extern void SSL_CTX_clear_mode(void);
extern void SSL_process_tls13_new_session_ticket(void);
extern void BN_CTX_free(void);
extern void ENGINE_new(void);
extern void EC_POINT_get_affine_coordinates_GFp(void);
extern void MD4_Transform(void);
extern void NETSCAPE_SPKI_get_pubkey(void);
extern void OPENSSL_strcasecmp(void);
extern void PEM_write_bio_X509_AUX(void);
extern void X509_check_ip_asc(void);
extern void SSL_CTX_use_certificate_ASN1(void);
extern void ASN1_TYPE_set(void);
extern void EVP_MD_CTX_set_flags(void);
extern void PEM_read_bio_DHparams(void);
extern void X509_set_issuer_name(void);
extern void GENERAL_NAME_cmp(void);
extern void X509_REVOKED_set_serialNumber(void);
extern void BN_lshift1(void);
extern void CRYPTO_get_ex_data(void);
extern void RSA_new(void);
extern void PEM_write_bio_X509_REQ(void);
extern void sk_set_cmp_func(void);
extern void X509_STORE_set_depth(void);
extern void d2i_RSA_PUBKEY_fp(void);
extern void SSL_get_ex_new_index(void);
extern void BIO_get_data(void);
extern void EC_GROUP_free(void);
extern void EVP_PKEY_base_id(void);
extern void EVP_HPKE_CTX_setup_recipient(void);
extern void X509_EXTENSION_get_object(void);
extern void X509_set_notAfter(void);
extern void CRYPTO_new_ex_data(void);
extern void PEM_read_DSAparams(void);
extern void X509_get_X509_PUBKEY(void);
extern void i2s_ASN1_ENUMERATED(void);
extern void RSA_get0_n(void);
extern void TRUST_TOKEN_CLIENT_set_srr_key(void);
extern void X509_STORE_CTX_get0_store(void);
extern void CBB_init_fixed(void);
extern void EVP_PKEY_CTX_set_rsa_pss_keygen_mgf1_md(void);
extern void RSA_get0_p(void);
extern void OBJ_nid2ln(void);
extern void SSL_use_RSAPrivateKey_file(void);
extern void OBJ_cmp(void);
extern void ASN1_OCTET_STRING_it(void);
extern void BN_GENCB_new(void);
extern void DH_num_bits(void);
extern void EVP_MD_CTX_block_size(void);
extern void RSA_get0_q(void);
extern void X509_CRL_get0_nextUpdate(void);
extern void X509_OBJECT_retrieve_match(void);
extern void d2i_X509_CRL_INFO(void);
extern void X509V3_get_value_int(void);
extern void EVP_aes_192_ofb(void);
extern void RAND_set_rand_method(void);
extern void X509_NAME_add_entry_by_txt(void);
extern void GENERAL_NAME_free(void);
extern void EVP_CIPHER_mode(void);
extern void HMAC_CTX_cleanse(void);
extern void d2i_ASN1_INTEGER(void);
extern void OPENSSL_realloc(void);
extern void EVP_PKEY_new(void);
extern void RSA_add_pkcs1_prefix(void);
extern void SHA384_Update(void);
extern void OPENSSL_tolower(void);
extern void SSL_CTX_set_signed_cert_timestamp_list(void);
extern void PEM_read_PrivateKey(void);
extern void DTLSv1_set_initial_timeout_duration(void);
extern void SSL_SESSION_get_timeout(void);
extern void d2i_DISPLAYTEXT(void);
extern void PEM_write_PKCS8PrivateKey_nid(void);
extern void X509_getm_notAfter(void);
extern void SSL_CTX_set_next_proto_select_cb(void);
extern void CBS_get_optional_asn1_uint64(void);
extern void CBS_get_optional_asn1_bool(void);
extern void X509_set_subject_name(void);
extern void X509V3_EXT_print_fp(void);
extern void i2a_ASN1_ENUMERATED(void);
extern void SPAKE2_CTX_new(void);
extern void DH_get0_pqg(void);
extern void X509_ATTRIBUTE_new(void);
extern void X509_STORE_free(void);
extern void X509_CRL_get0_by_cert(void);
extern void SSLv23_server_method(void);
extern void ERR_clear_error(void);
extern void i2d_ASN1_UTCTIME(void);
extern void EVP_PKEY_CTX_add1_hkdf_info(void);
extern void EVP_PKEY_CTX_set_rsa_keygen_bits(void);
extern void d2i_PKCS8_bio(void);
extern void X509_REQ_INFO_it(void);
extern void X509_NAME_new(void);
extern void SSL_get_curve_id(void);
extern void DES_ede2_cbc_encrypt(void);
extern void BN_sub(void);
extern void EVP_PKEY_CTX_set_rsa_keygen_pubexp(void);
extern void X509_REQ_set_subject_name(void);
extern void OPENSSL_lh_retrieve(void);
extern void CBB_add_u16(void);
extern void DES_ecb_encrypt(void);
extern void ERR_add_error_dataf(void);
extern void a2i_IPADDRESS_NC(void);
extern void ACCESS_DESCRIPTION_free(void);
extern void SSL_set0_rbio(void);
extern void SSL_peek(void);
extern void EVP_get_cipherbynid(void);
extern void X509_CRL_get_ext_by_NID(void);
extern void X509_get0_authority_issuer(void);
extern void SSL_set_bio(void);
extern void SSL_CTX_sess_accept_good(void);
extern void DSA_get0_priv_key(void);
extern void d2i_X509_bio(void);
extern void OPENSSL_lh_insert(void);
extern void BIO_s_connect(void);
extern void HKDF(void);
extern void CTR_DRBG_init(void);
extern void ASN1_ENUMERATED_get(void);
extern void CBB_add_u32le(void);
extern void X509V3_EXT_free(void);
extern void RAND_pseudo_bytes(void);
extern void RSA_public_decrypt(void);
extern void i2d_X509_fp(void);
extern void SSL_CTX_sess_number(void);
extern void d2i_PKCS8_fp(void);
extern void X509_add1_reject_object(void);
extern void CRL_DIST_POINTS_new(void);
extern void SSL_new(void);
extern void SSL_select_next_proto(void);
extern void SHA384(void);
extern void SSL_CTX_set_keylog_callback(void);
extern void X509_ATTRIBUTE_create_by_NID(void);
extern void SSL_CTX_use_PrivateKey(void);
extern void _ZN4bssl23SSL_get_traffic_secretsEPK6ssl_stPNS_4SpanIKhEES6_(void);
extern void SSL_get_peer_quic_transport_params(void);
extern void SSL_get_finished(void);
extern void SHA256_Final(void);
extern void EVP_CIPHER_CTX_block_size(void);
extern void SSL_set_early_data_enabled(void);
extern void ASN1_IA5STRING_it(void);
extern void EVP_aes_128_ecb(void);
extern void EVP_sha512_256(void);
extern void EVP_cleanup(void);
extern void _ZN4bssl21SSL_serialize_handoffEPK6ssl_stP6cbb_stP22ssl_early_callback_ctx(void);
extern void EC_KEY_get_conv_form(void);
extern void X509_STORE_CTX_get0_current_issuer(void);
extern void SSL_load_client_CA_file(void);
extern void BN_copy(void);
extern void EC_KEY_is_opaque(void);
extern void BN_is_odd(void);
extern void PEM_ASN1_read_bio(void);
extern void X509_STORE_get_check_revocation(void);
extern void X509_PURPOSE_get_count(void);
extern void SSL_is_dtls(void);
extern void BN_num_bytes(void);
extern void BN_from_montgomery(void);
extern void i2d_RSAPublicKey(void);
extern void PEM_write_bio_DHparams(void);
extern void SSL_CTX_get_verify_depth(void);
extern void SHA256_Update(void);
extern void RSA_up_ref(void);
extern void DTLS_method(void);
extern void i2d_ECPrivateKey(void);
extern void EVP_PKEY_CTX_set_signature_md(void);
extern void EVP_AEAD_CTX_init_with_direction(void);
extern void CRYPTO_set_dynlock_destroy_callback(void);
extern void TRUST_TOKEN_ISSUER_redeem_raw(void);
extern void ASN1_UNIVERSALSTRING_free(void);
extern void EVP_CIPHER_CTX_init(void);
extern void CTR_DRBG_reseed(void);
extern void i2d_ASN1_GENERALIZEDTIME(void);
extern void ASN1_BOOLEAN_it(void);
extern void AES_ctr128_encrypt(void);
extern void X509_PURPOSE_add(void);
extern void SSL_CIPHER_is_block_cipher(void);
extern void SSL_get_peer_cert_chain(void);
extern void SSL_CTX_get_verify_callback(void);
extern void OPENSSL_hash32(void);
extern void TLSv1_2_client_method(void);
extern void bn_mod_inverse_consttime(void);
extern void RSA_public_encrypt(void);
extern void X509V3_extensions_print(void);
extern void EVP_MD_CTX_cleanup(void);
extern void OPENSSL_lh_retrieve_key(void);
extern void d2i_ASN1_OBJECT(void);
extern void X509_CERT_AUX_it(void);
extern void BIO_set_flags(void);
extern void X509_INFO_new(void);
extern void SSL_CTX_set_read_ahead(void);
extern void ERR_load_SSL_strings(void);
extern void HRSS_parse_public_key(void);
extern void SSL_check_private_key(void);
extern void EVP_CIPHER_CTX_key_length(void);
extern void HMAC_CTX_get_md(void);
extern void d2i_PKCS12_fp(void);
extern void SSL_in_early_data(void);
extern void SSL_CTX_set_compliance_policy(void);
extern void SSL_add_client_CA(void);
extern void ASN1_BIT_STRING_get_bit(void);
extern void PEM_read_bio_RSAPrivateKey(void);
extern void X509_EXTENSION_free(void);
extern void DIST_POINT_set_dpname(void);
extern void NETSCAPE_SPKI_new(void);
extern void EVP_PKEY_decrypt(void);
extern void SHA512_256_Init(void);
extern void CRYPTO_num_locks(void);
extern void TRUST_TOKEN_experiment_v2_voprf(void);
extern void SSL_set0_verify_cert_store(void);
extern void BIO_tell(void);
extern void EVP_get_digestbyobj(void);
extern void EVP_marshal_public_key(void);
extern void ECDSA_SIG_set0(void);
extern void PEM_read_bio_PrivateKey(void);
extern void PKCS7_type_is_enveloped(void);
extern void X509_ATTRIBUTE_set1_object(void);
extern void d2i_DHparams_bio(void);
extern void SSL_CTX_set_tlsext_servername_arg(void);
extern void CBS_get_optional_asn1_octet_string(void);
extern void i2d_RSAPrivateKey_bio(void);
extern void ASN1_UTCTIME_cmp_time_t(void);
extern void DISPLAYTEXT_free(void);
extern void CBB_add_u24(void);
extern void EVP_HPKE_KEY_cleanup(void);
extern void NETSCAPE_SPKAC_new(void);
extern void SSL_set_signed_cert_timestamp_list(void);
extern void i2d_PKCS8_PRIV_KEY_INFO(void);
extern void SSL_has_application_settings(void);
extern void i2d_ASN1_T61STRING(void);
extern void BIO_get_shutdown(void);
extern void CBB_add_space(void);
extern void CBS_get_last_u8(void);
extern void BN_is_prime_ex(void);
extern void X509_REQ_get_attr(void);
extern void X509_NAME_get_text_by_NID(void);
extern void i2d_EC_PUBKEY_fp(void);
extern void X509_REVOKED_it(void);
extern void SSL_CTX_add1_chain_cert(void);
extern void ASN1_T61STRING_it(void);
extern void BN_pseudo_rand_range(void);
extern void EVP_AEAD_CTX_open(void);
extern void EVP_aead_aes_128_gcm(void);
extern void SSL_set_msg_callback(void);
extern void ASN1_OCTET_STRING_set(void);
extern void BIO_ctrl_get_write_guarantee(void);
extern void EVP_CipherUpdate(void);
extern void EC_GROUP_set_generator(void);
extern void PEM_write_PrivateKey(void);
extern void X509_dup(void);
extern void X509_get_extended_key_usage(void);
extern void SSL_CTX_set_tmp_dh_callback(void);
extern void ERR_get_error_line(void);
extern void d2i_PKCS7(void);
extern void i2d_X509_AUX(void);
extern void X509_add1_ext_i2d(void);
extern void SSL_CTX_set_tmp_rsa_callback(void);
extern void SSL_get_peer_full_cert_chain(void);
extern void PEM_write_bio_PKCS7(void);
extern void X509_check_trust(void);
extern void X509_REVOKED_dup(void);
extern void X509_NAME_ENTRY_create_by_NID(void);
extern void BN_new(void);
extern void EVP_sha224(void);
extern void BN_mod_lshift1(void);
extern void i2d_X509_REQ(void);
extern void PEM_write_bio_PKCS8(void);
extern void sk_zero(void);
extern void SSL_CTX_set_mode(void);
extern void SSL_set_msg_callback_arg(void);
extern void ASN1_put_object(void);
extern void DES_ede3_cbc_encrypt(void);
extern void HMAC_CTX_init(void);
extern void EVP_PKEY_encrypt(void);
extern void d2i_PKCS8_PRIV_KEY_INFO(void);
extern void CRYPTO_BUFFER_alloc(void);
extern void X509_get_ext(void);
extern void X509_check_issued(void);
extern void SSL_CTX_use_PrivateKey_ASN1(void);
extern void EC_KEY_marshal_private_key(void);
extern void CERTIFICATEPOLICIES_new(void);
extern void SSL_CTX_set_info_callback(void);
extern void cbb_add_latin1(void);
extern void i2d_DSAparams(void);
extern void EVP_HPKE_KEY_kem(void);
extern void PKCS7_get_PEM_certificates(void);
extern void X509_STORE_CTX_get0_chain(void);
extern void d2i_ACCESS_DESCRIPTION(void);
extern void X509_get0_subject_key_id(void);
extern void cbb_add_utf8(void);
extern void BIO_s_fd(void);
extern void PEM_read_RSAPrivateKey(void);
extern void X509_delete_ext(void);
extern void d2i_X509_CRL_fp(void);
extern void ASN1_TYPE_cmp(void);
extern void OPENSSL_init_crypto(void);
extern void EVP_HPKE_CTX_new(void);
extern void s2i_ASN1_INTEGER(void);
extern void SSL_CTX_set_custom_verify(void);
extern void EVP_aes_128_cbc(void);
extern void EVP_CIPHER_CTX_new(void);
extern void X509_EXTENSION_set_data(void);
extern void SSL_renegotiate(void);
extern void SSL_CTX_get_info_callback(void);
extern void DSA_SIG_free(void);
extern void BN_ucmp(void);
extern void X509_REVOKED_get0_serialNumber(void);
extern void X509_STORE_CTX_trusted_stack(void);
extern void NOTICEREF_free(void);
extern void SSL_SESSION_up_ref(void);
extern void CBB_finish(void);
extern void BIO_set_retry_special(void);
extern void _ZN4bssl10SealRecordEP6ssl_stNS_4SpanIhEES3_S3_NS2_IKhEE(void);
extern void BN_get_rfc3526_prime_3072(void);
extern void BN_MONT_CTX_new_consttime(void);
extern void MD4_Update(void);
extern void i2d_PKCS7(void);
extern void BIO_set_mem_buf(void);
extern void CBS_get_asn1_element(void);
extern void _ZN4bssl29ssl_decode_client_hello_innerEP6ssl_stPhPNS_5ArrayIhEENS_4SpanIKhEEPK22ssl_early_callback_ctx(void);
extern void ASN1_get_object(void);
extern void X509_load_crl_file(void);
extern void SSL_SESSION_new(void);
extern void CBS_get_asn1_implicit_string(void);
extern void EC_GROUP_get_curve_name(void);
extern void sk_is_sorted(void);
extern void SSL_CTX_add_client_CA(void);
extern void DSA_size(void);
extern void EVP_VerifyFinal(void);
extern void EVP_HPKE_KEY_public_key(void);
extern void X509v3_get_ext_by_OBJ(void);
extern void X509_VERIFY_PARAM_inherit(void);
extern void i2d_AUTHORITY_INFO_ACCESS(void);
extern void CRYPTO_poly1305_update(void);
extern void CRYPTO_gcm128_finish(void);
extern void SSL_CTX_set_srtp_profiles(void);
extern void CBS_get_asn1_int64(void);
extern void CRYPTO_pre_sandbox_init(void);
extern void ERR_lib_error_string(void);
extern void CTR_DRBG_clear(void);
extern void RAND_SSLeay(void);
extern void X509_CRL_set1_lastUpdate(void);
extern void SSL_CTX_enable_signed_cert_timestamps(void);
extern void SSL_used_hello_retry_request(void);
extern void SSL_set_session(void);
extern void BN_clear_bit(void);
extern void BUF_strnlen(void);
extern void OPENSSL_gmtime_diff(void);
extern void sk_find(void);
extern void X509_ALGOR_new(void);
extern void X509_PUBKEY_get0_param(void);
extern void SHA1_Transform(void);
extern void RSA_sign_raw(void);
extern void GENERAL_SUBTREE_free(void);
extern void ASN1_item_dup(void);
extern void EVP_AEAD_key_length(void);
extern void ERR_peek_last_error(void);
extern void OPENSSL_add_all_algorithms_conf(void);
extern void AES_set_encrypt_key(void);
extern void SSL_CTX_set_msg_callback_arg(void);
extern void SSL_get_write_sequence(void);
extern void asn1_generalizedtime_to_tm(void);
extern void ASN1_TYPE_free(void);
extern void i2d_ASN1_INTEGER(void);
extern void EVP_aead_xchacha20_poly1305(void);
extern void BN_add(void);
extern void d2i_ECDSA_SIG(void);
extern void CBB_add_u32(void);
extern void ECDSA_SIG_new(void);
extern void RSA_verify(void);
extern void PEM_write_X509_REQ_NEW(void);
extern void X509_STORE_add_crl(void);
extern void BN_to_ASN1_ENUMERATED(void);
extern void EVP_CIPHER_CTX_set_flags(void);
extern void RSA_check_fips(void);
extern void PEM_write_bio_DSAparams(void);
extern void TRUST_TOKEN_ISSUER_add_key(void);
extern void SSL_set_jdk11_workaround(void);
extern void CBS_copy_bytes(void);
extern void ERR_SAVE_STATE_free(void);
extern void EC_KEY_set_enc_flags(void);
extern void d2i_X509_CERT_AUX(void);
extern void DH_generate_parameters_ex(void);
extern void EVP_PKEY_set_type(void);
extern void EVP_parse_private_key(void);
extern void X509_STORE_CTX_set_flags(void);
extern void SSL_load_error_strings(void);
extern void EVP_PKEY_size(void);
extern void bn_miller_rabin_init(void);
extern void DH_size(void);
extern void RSA_set0_key(void);
extern void X509_CRL_get_ext_by_critical(void);
extern void SSL_get_servername(void);
extern void SSL_enable_tls_channel_id(void);
extern void SSL_get_min_proto_version(void);
extern void CBS_asn1_bitstring_has_bit(void);
extern void EVP_DigestInit(void);
extern void SSL_set_permute_extensions(void);
extern void BN_zero(void);
extern void CBS_get_any_asn1(void);
extern void HMAC_CTX_cleanup(void);
extern void d2i_DSA_SIG(void);
extern void EVP_CIPHER_CTX_flags(void);
extern void CRYPTO_realloc(void);
extern void PKCS8_encrypt(void);
extern void PKCS7_type_is_signed(void);
extern void X509_TRUST_get_by_id(void);
extern void SSL_get_ex_data_X509_STORE_CTX_idx(void);
extern void SSL_export_keying_material(void);
extern void i2d_DHparams(void);
extern void EVP_DigestInit_ex(void);
extern void EVP_PKEY_CTX_hkdf_mode(void);
extern void EVP_PKEY_CTX_set1_hkdf_salt(void);
extern void SHA384_Init(void);
extern void X509_VAL_free(void);
extern void SSL_free(void);
extern void BIO_should_retry(void);
extern void SHA1_Update(void);
extern void X509V3_add1_i2d(void);
extern void SSL_CTX_set_current_time_cb(void);
extern void SSL_get_max_proto_version(void);
extern void ASN1_TIME_new(void);
extern void BIO_set_retry_reason(void);
extern void X509_NAME_print(void);
extern void X509_STORE_CTX_free(void);
extern void X509_VERIFY_PARAM_set_hostflags(void);
extern void SSL_set_renegotiate_mode(void);
extern void ASN1_mbstring_copy(void);
extern void ASN1_TIME_to_generalizedtime(void);
extern void BLAKE2B256(void);
extern void BN_enhanced_miller_rabin_primality_test(void);
extern void EVP_AEAD_CTX_seal_scatter(void);
extern void PEM_read_RSAPublicKey(void);
extern void CRYPTO_BUFFER_new_from_static_data_unsafe(void);
extern void SSL_ECH_KEYS_new(void);
extern void SSL_CIPHER_get_kx_name(void);
extern void SSL_CIPHER_get_prf_nid(void);
extern void CBB_add_u8(void);
extern void ASN1_TIME_to_posix(void);
extern void CBB_add_asn1_oid_from_text(void);
extern void BN_is_pow2(void);
extern void X509_PUBKEY_get(void);
extern void X509_EXTENSION_new(void);
extern void X509_check_ip(void);
extern void X509_PKEY_free(void);
extern void SSL_get_certificate(void);
extern void EVP_des_ede3_ecb(void);
extern void EVP_HPKE_KEY_free(void);
extern void NAME_CONSTRAINTS_check(void);
extern void AUTHORITY_INFO_ACCESS_it(void);
extern void SSL_get_max_cert_list(void);
extern void BIO_set_retry_write(void);
extern void bn_abs_sub_consttime(void);
extern void SSL_pending(void);
extern void SSL_state(void);
extern void ASN1_PRINTABLE_free(void);
extern void BIO_eof(void);
extern void BN_mod_lshift(void);
extern void EVP_AEAD_CTX_get_iv(void);
extern void AES_CMAC(void);
extern void RSA_flags(void);
extern void X509_get0_tbs_sigalg(void);
extern void SSL_in_init(void);
extern void SSL_SESSION_get0_peer(void);
extern void ASN1_GENERALIZEDTIME_set(void);
extern void EVP_blake2b256(void);
extern void X509_NAME_entry_count(void);
extern void X509_OBJECT_free_contents(void);
extern void SSL_SESSION_free(void);
extern void SSL_CTX_get_extra_chain_certs(void);
extern void BIO_ptr_ctrl(void);
extern void d2i_X509_AUX(void);
extern void X509_STORE_CTX_get0_current_crl(void);
extern void SSL_add_bio_cert_subjects_to_stack(void);
extern void SSL_get_fd(void);
extern void TLSv1_1_method(void);
extern void BIO_pop(void);
extern void RSA_decrypt(void);
extern void EC_GROUP_set_point_conversion_form(void);
extern void ec_scalar_to_bytes(void);
extern void d2i_X509_REQ(void);
extern void NETSCAPE_SPKI_b64_decode(void);
extern void POLICYINFO_new(void);
extern void SSL_get_peer_certificate(void);
extern void SSLeay(void);
extern void SSL_SESSION_has_ticket(void);
extern void CRYPTO_gcm128_setiv(void);
extern void PEM_bytes_read_bio(void);
extern void X509_ATTRIBUTE_set1_data(void);
extern void X509_get0_pubkey_bitstr(void);
extern void POLICY_CONSTRAINTS_it(void);
extern void X509_set1_signature_value(void);
extern void SSL_set_rfd(void);
extern void ASN1_item_d2i(void);
extern void ASN1_GENERALIZEDTIME_print(void);
extern void i2a_ASN1_INTEGER(void);
extern void CRYPTO_refcount_inc(void);
extern void EVP_PKEY_verify_recover_init(void);
extern void EVP_HPKE_CTX_zero(void);
extern void TRUST_TOKEN_ISSUER_free(void);
extern void v2i_GENERAL_NAME_ex(void);
extern void PROXY_POLICY_new(void);
extern void SSL_CTX_use_RSAPrivateKey_ASN1(void);
extern void EVP_AEAD_CTX_free(void);
extern void EVP_DecryptFinal(void);
extern void d2i_USERNOTICE(void);
extern void i2d_USERNOTICE(void);
extern void ASN1_ENUMERATED_to_BN(void);
extern void CBB_flush_asn1_set_of(void);
extern void ERR_remove_thread_state(void);
extern void EVP_AEAD_CTX_cleanup(void);
extern void ASN1_TIME_to_time_t(void);
extern void HMAC_size(void);
extern void SSL_get_peer_signature_algorithm(void);
extern void X509_ALGOR_get0(void);
extern void SSL_set_connect_state(void);
extern void SSL_set_tmp_ecdh(void);
extern void EVP_aes_256_ofb(void);
extern void BN_init(void);
extern void AES_unwrap_key_padded(void);
extern void GENERAL_NAMES_free(void);
extern void SSL_ECH_KEYS_add(void);
extern void BIO_read_asn1(void);
extern void CBS_is_valid_asn1_integer(void);
extern void SSL_CTX_set_dos_protection_cb(void);
extern void SSL_CTX_flush_sessions(void);
extern void ASN1_OCTET_STRING_cmp(void);
extern void RSA_marshal_public_key(void);
extern void pmbtoken_exp1_get_h_for_testing(void);
extern void SSL_set_tlsext_use_srtp(void);
extern void sk_delete_ptr(void);
extern void RSA_encrypt(void);
extern void CRYPTO_gcm128_decrypt_ctr32(void);
extern void TRUST_TOKEN_CLIENT_finish_redemption(void);
extern void SSL_use_PrivateKey_file(void);
extern void SSL_alert_type_string(void);
extern void SSL_CTX_get_min_proto_version(void);
extern void ASN1_TIME_adj(void);
extern void i2d_PKCS8_bio(void);
extern void X509_SIG_free(void);
extern void SSL_write(void);
extern void SSL_set_shutdown(void);
extern void SSL_CTX_sess_get_get_cb(void);
extern void EC_KEY_set_private_key(void);
extern void AUTHORITY_INFO_ACCESS_new(void);
extern void SSL_in_false_start(void);
extern void SSL_get1_session(void);
extern void d2i_ASN1_TIME(void);
extern void DSA_do_sign(void);
extern void EVP_PKEY_assign(void);
extern void CRYPTO_get_dynlock_create_callback(void);
extern void SSL_need_tmp_RSA(void);
extern void EVP_EncodeFinal(void);
extern void CBS_get_optional_asn1(void);
extern void PEM_write_RSAPublicKey(void);
extern void SSL_CTX_get_max_proto_version(void);
extern void BN_marshal_asn1(void);
extern void BN_get_rfc3526_prime_4096(void);
extern void EVP_PKEY_CTX_set_ec_param_enc(void);
extern void X509_CRL_INFO_free(void);
extern void CBS_asn1_ber_to_der(void);
extern void CRYPTO_get_thread_local(void);
extern void X509_CRL_get0_lastUpdate(void);
extern void X509_add_ext(void);
extern void X509_STORE_set_check_issued(void);
extern void CRL_DIST_POINTS_free(void);
extern void d2i_ISSUING_DIST_POINT(void);
extern void _ZN4bssl17SSL_apply_handoffEP6ssl_stNS_4SpanIKhEE(void);
extern void SSL_get_read_ahead(void);
extern void CRYPTO_has_asm(void);
extern void EVP_AEAD_CTX_aead(void);
extern void X509_print_ex_fp(void);
extern void i2d_X509_REQ_fp(void);
extern void EVP_EncodeInit(void);
extern void X509_REQ_extension_nid(void);
extern void ec_hash_to_scalar_p384_xmd_sha512_draft07(void);
extern void EVP_HPKE_AEAD_aead(void);
extern void sk_shift(void);
extern void X509_STORE_set_flags(void);
extern void BIO_write_filename(void);
extern void EVP_DecryptUpdate(void);
extern void EC_KEY_key2buf(void);
extern void EVP_CIPHER_CTX_set_padding(void);
extern void PKCS5_PBKDF2_HMAC(void);
extern void X509_CRL_print(void);
extern void AUTHORITY_KEYID_it(void);
extern void BUF_strndup(void);
extern void EVP_CIPHER_iv_length(void);
extern void X509_ATTRIBUTE_dup(void);
extern void SSL_get_verify_mode(void);
extern void SSL_get0_peer_verify_algorithms(void);
extern void EVP_des_ede3_cbc(void);
extern void X509_NAME_dup(void);
extern void OPENSSL_strlcpy(void);
extern void EVP_CIPHER_CTX_mode(void);
extern void EVP_PKEY_missing_parameters(void);
extern void X509_CRL_get_ext_by_OBJ(void);
extern void OPENSSL_timegm(void);
extern void EVP_md4(void);
extern void TRUST_TOKEN_ISSUER_set_metadata_key(void);
extern void EVP_md5(void);
extern void EVP_MD_CTX_free(void);
extern void X509_STORE_set_cleanup(void);
extern void d2i_NETSCAPE_SPKAC(void);
extern void OBJ_obj2txt(void);
extern void PKCS8_PRIV_KEY_INFO_free(void);
extern void X509_TRUST_get0_name(void);
extern void X509_STORE_CTX_new(void);
extern void X509_CINF_it(void);
extern void SSL_get_extms_support(void);
extern void d2i_DIRECTORYSTRING(void);
extern void OPENSSL_lh_doall_arg(void);
extern void SSL_shutdown(void);
extern void SSL_CTX_sess_connect_renegotiate(void);
extern void DSA_parse_public_key(void);
extern void SSL_get_ex_data(void);
extern void RC4_set_key(void);
extern void HMAC_Init_ex(void);
extern void ECDSA_SIG_max_len(void);
extern void EVP_PBE_scrypt(void);
extern void PKCS7_free(void);
extern void X509_ATTRIBUTE_create_by_OBJ(void);
extern void X509_REQ_add1_attr_by_txt(void);
extern void i2a_ACCESS_DESCRIPTION(void);
extern void SSL_get_ivs(void);
extern void X509_STORE_set_verify(void);
extern void DSA_bits(void);
extern void EVP_PKEY_CTX_set_rsa_pss_keygen_md(void);
extern void TRUST_TOKEN_CLIENT_new(void);
extern void SSL_set_trust(void);
extern void d2i_ASN1_PRINTABLE(void);
extern void i2d_PKCS12_fp(void);
extern void X509_get0_uids(void);
extern void SSL_CTX_check_private_key(void);
extern void SSL_CTX_get_ex_data(void);
extern void ASN1_TIME_it(void);
extern void ASN1_UTF8STRING_new(void);
extern void ASN1_PRINTABLESTRING_new(void);
extern void EVP_DecodedLength(void);
extern void CMAC_CTX_copy(void);
extern void SSL_CTX_set_verify(void);
extern void cbb_add_ucs2_be(void);
extern void SSL_CTX_add_cert_compression_alg(void);
extern void EVP_PKEY_get1_DSA(void);
extern void i2d_PKCS12(void);
extern void SSL_serialize_handshake_hints(void);
extern void CBS_is_unsigned_asn1_integer(void);
extern void NCONF_get_string(void);
extern void EVP_PKEY_CTX_set_rsa_padding(void);
extern void RSA_private_decrypt(void);
extern void CBS_len(void);
extern void d2i_EC_PUBKEY(void);
extern void d2i_RSA_PSS_PARAMS(void);
extern void BN_add_word(void);
extern void EVP_aead_null_sha1_tls(void);
extern void ERR_set_error_data(void);
extern void EVP_PKEY_decrypt_init(void);
extern void RSA_padding_add_PKCS1_OAEP_mgf1(void);
extern void X509_find_by_issuer_and_serial(void);
extern void d2i_ASN1_SET_ANY(void);
extern void EVP_aes_192_ctr(void);
extern void EVP_PKEY_get0(void);
extern void PEM_write_RSAPrivateKey(void);
extern void d2i_PKCS12(void);
extern void X509_ALGOR_it(void);
extern void i2d_PKCS8_PRIV_KEY_INFO_fp(void);
extern void d2i_SSL_SESSION(void);
extern void X509_VERIFY_PARAM_get_depth(void);
extern void sk_num(void);
extern void d2i_DSAPrivateKey(void);
extern void X509_get_default_cert_area(void);
extern void d2i_EXTENDED_KEY_USAGE(void);
extern void SSL_CTX_sess_get_remove_cb(void);
extern void BN_set_word(void);
extern void X509_STORE_add_cert(void);
extern void CBS_data(void);
extern void EVP_CipherInit_ex(void);
extern void EVP_PKEY_bits(void);
extern void EVP_PKEY_keygen(void);
extern void DH_bits(void);
extern void i2d_re_X509_CRL_tbs(void);
extern void DSA_generate_parameters_ex(void);
extern void HRSS_poly3_invert(void);
extern void PEM_read_PKCS8_PRIV_KEY_INFO(void);
extern void ASN1_ENUMERATED_new(void);
extern void BIO_meth_free(void);
extern void EVP_sha256(void);
extern void BN_MONT_CTX_new_for_modulus(void);
extern void ERR_error_string_n(void);
extern void TRUST_TOKEN_generate_key(void);
extern void X509_STORE_CTX_set_time(void);
extern void X509_CRL_digest(void);
extern void SSL_SESSION_get0_peer_certificates(void);
extern void BIO_new_socket(void);
extern void ENGINE_register_all_complete(void);
extern void EVP_PKEY_get0_RSA(void);
extern void EC_POINT_set_affine_coordinates_GFp(void);
extern void RAND_cleanup(void);
extern void SSL_CTX_set_strict_cipher_list(void);
extern void ASN1_ENUMERATED_get_uint64(void);
extern void ASN1_PRINTABLESTRING_free(void);
extern void RSA_size(void);
extern void BN_MONT_CTX_set(void);
extern void d2i_DSAPrivateKey_bio(void);
extern void X509_NAME_get0_der(void);
extern void EDIPARTYNAME_it(void);
extern void EC_KEY_get_ex_data(void);
extern void X509at_add1_attr_by_txt(void);
extern void X509_TRUST_add(void);
extern void DIST_POINT_free(void);
extern void EVP_EncodeUpdate(void);
extern void BIO_clear_flags(void);
extern void EVP_EncryptFinal_ex(void);
extern void EVP_PKEY_sign_init(void);
extern void EVP_MD_CTX_create(void);
extern void HRSS_poly3_mul(void);
extern void i2d_PKCS8_fp(void);
extern void EVP_AEAD_CTX_open_gather(void);
extern void PEM_write_DSA_PUBKEY(void);
extern void X509_PURPOSE_cleanup(void);
extern void BN_print_fp(void);
extern void CRYPTO_secure_malloc_initialized(void);
extern void SSL_CTX_sess_accept_renegotiate(void);
extern void EC_GROUP_get0_generator(void);
extern void PEM_write_X509(void);
extern void X509_NAME_get_text_by_OBJ(void);
extern void GENERAL_SUBTREE_new(void);
extern void SSL_CTX_get_timeout(void);
extern void CRYPTO_THREADID_set_callback(void);
extern void X509_issuer_name_cmp(void);
extern void BIO_meth_set_ctrl(void);
extern void CBB_add_u8_length_prefixed(void);
extern void cbb_get_utf8_len(void);
extern void CBB_add_u24_length_prefixed(void);
extern void EC_KEY_parse_parameters(void);
extern void OpenSSL_add_all_algorithms(void);
extern void X509_REVOKED_get_ext(void);
extern void a2i_GENERAL_NAME(void);
extern void PKCS7_get_PEM_CRLs(void);
extern void SSL_CTX_get_default_passwd_cb_userdata(void);
extern void BUF_MEM_append(void);
extern void ECDSA_size(void);
extern void ERR_restore_state(void);
extern void EVP_PKEY_cmp_parameters(void);
extern void X509_NAME_ENTRY_create_by_OBJ(void);
extern void i2d_SSL_SESSION_bio(void);
extern void BN_get_rfc3526_prime_1536(void);
extern void X509_CRL_set1_signature_algo(void);
extern void X509_NAME_hash_old(void);
extern void BIO_new_fd(void);
extern void BN_dup(void);
extern void X509_VERIFY_PARAM_get0_name(void);
extern void SSL_CTX_sess_set_cache_size(void);
extern void SSL_SESSION_get_ex_data(void);
extern void SSL_CTX_set0_chain(void);
extern void PEM_write_PKCS8_PRIV_KEY_INFO(void);
extern void X509_STORE_CTX_get1_issuer(void);
extern void X509_REQ_free(void);
extern void SSL_get_error(void);
extern void ED25519_sign(void);
extern void EVP_parse_digest_algorithm(void);
extern void EVP_EncryptInit_ex(void);
extern void CRYPTO_get_ex_new_index(void);
extern void EVP_CIPHER_CTX_nid(void);
extern void i2d_NETSCAPE_SPKAC(void);
extern void EVP_aead_aes_256_gcm_siv(void);
extern void i2d_X509_ATTRIBUTE(void);
extern void GENERAL_NAME_it(void);
extern void BN_bn2bin(void);
extern void CBB_add_u64(void);
extern void EC_POINT_point2oct(void);
extern void i2d_ECDSA_SIG(void);
extern void PEM_read_bio_EC_PUBKEY(void);
extern void X509_ATTRIBUTE_get0_object(void);
extern void SSL_SESSION_set_ticket(void);
extern void NCONF_load_bio(void);
extern void SHA512_Update(void);
extern void EVP_get_digestbyname(void);
extern void DH_get0_key(void);
extern void TLSv1_server_method(void);
extern void BN_bin2bn(void);
extern void EVP_PKEY_CTX_set0_rsa_oaep_label(void);
extern void EVP_HPKE_KDF_id(void);
extern void SSL_set_tmp_dh(void);
extern void SSL_CTX_sess_accept(void);
extern void BIO_new_fp(void);
extern void ASN1_STRING_type_new(void);
extern void BN_is_zero(void);
extern void DSA_set_ex_data(void);
extern void ECDSA_do_sign(void);
extern void EVP_PKEY_CTX_get_rsa_pss_saltlen(void);
extern void OBJ_cleanup(void);
extern void X509_VERIFY_PARAM_free(void);
extern void d2i_ASN1_IA5STRING(void);
extern void i2d_ASN1_PRINTABLE(void);
extern void d2i_DSA_PUBKEY(void);
extern void i2d_DSA_PUBKEY(void);
extern void BN_bn2bin_padded(void);
extern void ASN1_STRING_data(void);
extern void BIO_append_filename(void);
extern void CRYPTO_poly1305_finish(void);
extern void X509_get_ext_count(void);
extern void SSL_COMP_add_compression_method(void);
extern void ASN1_TBOOLEAN_it(void);
extern void X509_get_default_cert_dir(void);
extern void X509_STORE_set_purpose(void);
extern void i2d_re_X509_tbs(void);
extern void BIO_f_ssl(void);
extern void ASN1_VISIBLESTRING_free(void);
extern void BN_clear_free(void);
extern void GENERAL_NAMES_it(void);
extern void X509_STORE_CTX_set_chain(void);
extern void CRYPTO_MUTEX_cleanup(void);
extern void EVP_PKEY_CTX_set_dsa_paramgen_bits(void);
extern void i2d_RSA_PSS_PARAMS(void);
extern void X509_CRL_up_ref(void);
extern void X509_ALGOR_dup(void);
extern void CBB_add_asn1_bool(void);
extern void USERNOTICE_new(void);
extern void SSL_set_compliance_policy(void);
extern void SSL_SESSION_should_be_single_use(void);
extern void CRYPTO_poly1305_init(void);
extern void SHA512_Final(void);
extern void EVP_AEAD_CTX_new(void);
extern void EVP_aes_192_gcm(void);
extern void X509_print(void);
extern void NAME_CONSTRAINTS_free(void);
extern void X509V3_EXT_add_nconf(void);
extern void ASN1_BMPSTRING_free(void);
extern void DISPLAYTEXT_it(void);
extern void CBB_reserve(void);
extern void X509_cmp(void);
extern void X509_get_default_private_dir(void);
extern void X509_VERIFY_PARAM_set1_host(void);
extern void ASN1_object_size(void);
extern void OPENSSL_lh_free(void);
extern void PKCS5_PBKDF2_HMAC_SHA1(void);
extern void X509_CRL_INFO_it(void);
extern void SSL_CTX_get_max_cert_list(void);
extern void i2d_ASN1_OCTET_STRING(void);
extern void CBB_zero(void);
extern void ASN1_ENUMERATED_free(void);
extern void BN_rand(void);
extern void SSL_CTX_set_grease_enabled(void);
extern void CBS_get_bytes(void);
extern void NETSCAPE_SPKAC_it(void);
extern void SSL_CTX_set_next_protos_advertised_cb(void);
extern void DSA_sign(void);
extern void EC_GROUP_get0_order(void);
extern void ENGINE_set_RSA_method(void);
extern void X509_REVOKED_get0_extensions(void);
extern void X509_ATTRIBUTE_get0_type(void);
extern void SSL_CTX_set_reverify_on_resume(void);
extern void ASN1_STRING_free(void);
extern void X509V3_get_value_bool(void);
extern void X509_PURPOSE_get_by_id(void);
extern void d2i_ASN1_PRINTABLESTRING(void);
extern void BN_mod_inverse(void);
extern void PKCS12_verify_mac(void);
extern void BN_GENCB_set(void);
extern void EVP_CIPHER_CTX_ctrl(void);
extern void X509_get0_notAfter(void);
extern void d2i_X509_REVOKED(void);
extern void ENGINE_set_ECDSA_method(void);
extern void BN_set_u64(void);
extern void PEM_read_PUBKEY(void);
extern void d2i_BASIC_CONSTRAINTS(void);
extern void ASN1_BIT_STRING_new(void);
extern void EC_POINT_new(void);
extern void X509_STORE_set_verify_cb(void);
extern void SSL_SESSION_get_master_key(void);
extern void SSL_CTX_set_cert_store(void);
extern void i2d_ASN1_GENERALSTRING(void);
extern void CBS_get_u16(void);
extern void CRYPTO_MUTEX_unlock_read(void);
extern void RSA_get_ex_data(void);
extern void d2i_NOTICEREF(void);
extern void SSL_set_session_id_context(void);
extern void CBS_mem_equal(void);
extern void DH_marshal_parameters(void);
extern void RSA_get0_dmp1(void);
extern void X509_EXTENSION_dup(void);
extern void X509_PURPOSE_set(void);
extern void d2i_OTHERNAME(void);
extern void SSL_CTX_set_retain_only_sha256_of_client_certs(void);
extern void EVP_des_ede3(void);
extern void EVP_VerifyUpdate(void);
extern void d2i_PKCS12_bio(void);
extern void X509_STORE_get_check_crl(void);
extern void X509_EXTENSION_set_critical(void);
extern void POLICY_MAPPING_it(void);
extern void SSL_CIPHER_standard_name(void);
extern void BIO_snprintf(void);
extern void ASN1_VISIBLESTRING_new(void);
extern void BIO_mem_contents(void);
extern void SHA224_Final(void);
extern void SHA1(void);
extern void PEM_read_bio_DSAPrivateKey(void);
extern void ASN1_UTCTIME_new(void);
extern void EVP_EncodeBlock(void);
extern void EVP_PKEY_sign(void);
extern void X509_NAME_set(void);
extern void SSL_CTX_set_alpn_select_cb(void);
extern void BIO_read_filename(void);
extern void EVP_DecryptFinal_ex(void);
extern void EC_KEY_get_ex_new_index(void);
extern void EXTENDED_KEY_USAGE_free(void);

static const struct func_map boringssl_funcs[] = {
        FUNC_MAP(EC_GROUP_set_asn1_flag),
        FUNC_MAP(PKCS12_get_key_and_certs),
        FUNC_MAP(X509_STORE_set_get_crl),
        FUNC_MAP(GENERAL_NAMES_new),
        FUNC_MAP(EVP_PKEY_get_raw_public_key),
        FUNC_MAP(X509_trust_clear),
        FUNC_MAP(_ZN4bssl19SSL_decline_handoffEP6ssl_st),
        FUNC_MAP(OPENSSL_init_ssl),
        FUNC_MAP(SSL_CTX_set_default_verify_paths),
        FUNC_MAP(CBS_get_u64le),
        FUNC_MAP(PEM_write_bio_RSAPublicKey),
        FUNC_MAP(SSL_get_tls_channel_id),
        FUNC_MAP(i2d_ASN1_SET_ANY),
        FUNC_MAP(PEM_write_bio_ECPrivateKey),
        FUNC_MAP(ASN1_TIME_print),
        FUNC_MAP(EC_KEY_parse_private_key),
        FUNC_MAP(i2d_ASN1_TIME),
        FUNC_MAP(RSA_verify_pss_mgf1),
        FUNC_MAP(BN_rshift1),
        FUNC_MAP(GENERAL_NAME_print),
        FUNC_MAP(PEM_read_SSL_SESSION),
        FUNC_MAP(X509_STORE_CTX_get_error_depth),
        FUNC_MAP(d2i_DSA_PUBKEY_bio),
        FUNC_MAP(SSL_clear_options),
        FUNC_MAP(CBS_get_u16le),
        FUNC_MAP(X509_set_ex_data),
        FUNC_MAP(SSL_CTX_set_verify_depth),
        FUNC_MAP(ECDSA_SIG_get0),
        FUNC_MAP(HRSS_marshal_public_key),
        FUNC_MAP(SSL_get_signature_algorithm_name),
        FUNC_MAP(CBB_init),
        FUNC_MAP(EVP_PKEY_copy_parameters),
        FUNC_MAP(SSL_set_enable_ech_grease),
        FUNC_MAP(DSA_do_verify),
        FUNC_MAP(ERR_func_error_string),
        FUNC_MAP(X509_CRL_get_version),
        FUNC_MAP(X509at_get_attr_count),
        FUNC_MAP(X509_get_ext_d2i),
        FUNC_MAP(AUTHORITY_KEYID_free),
        FUNC_MAP(X509_VAL_new),
        FUNC_MAP(SSL_get_current_compression),
        FUNC_MAP(d2i_AutoPrivateKey),
        FUNC_MAP(EVP_PKEY_paramgen),
        FUNC_MAP(PKCS7_bundle_raw_certificates),
        FUNC_MAP(X509V3_get_section),
        FUNC_MAP(SSL_CTX_set_allow_unknown_alpn_protos),
        FUNC_MAP(i2d_ASN1_IA5STRING),
        FUNC_MAP(EC_POINT_add),
        FUNC_MAP(DIST_POINT_NAME_it),
        FUNC_MAP(TLSv1_2_server_method),
        FUNC_MAP(DSA_get0_pqg),
        FUNC_MAP(RSA_bits),
        FUNC_MAP(X509_PUBKEY_new),
        FUNC_MAP(SSL_CTX_set1_groups_list),
        FUNC_MAP(ASN1_UTCTIME_free),
        FUNC_MAP(ED25519_verify),
        FUNC_MAP(PEM_read_DSAPrivateKey),
        FUNC_MAP(X509_alias_set1),
        FUNC_MAP(SHA512_256_Final),
        FUNC_MAP(x509v3_looks_like_dns_name),
        FUNC_MAP(NCONF_new),
        FUNC_MAP(BN_mul),
        FUNC_MAP(X509_REVOKED_add_ext),
        FUNC_MAP(SSL_CTX_set_cipher_list),
        FUNC_MAP(SSL_get_default_timeout),
        FUNC_MAP(SSL_SESSION_get_ticket_lifetime_hint),
        FUNC_MAP(ASN1_STRING_set_default_mask),
        FUNC_MAP(BUF_memdup),
        FUNC_MAP(SHA224),
        FUNC_MAP(X509_REQ_new),
        FUNC_MAP(DTLSv1_handle_timeout),
        FUNC_MAP(SSL_CTX_up_ref),
        FUNC_MAP(SSL_CTX_use_psk_identity_hint),
        FUNC_MAP(BIO_meth_set_create),
        FUNC_MAP(bn_is_relatively_prime),
        FUNC_MAP(OBJ_nid2sn),
        FUNC_MAP(CBS_strdup),
        FUNC_MAP(SHA224_Update),
        FUNC_MAP(X509V3_EXT_REQ_add_nconf),
        FUNC_MAP(SSL_CTX_sess_connect_good),
        FUNC_MAP(SSL_SESSION_set_protocol_version),
        FUNC_MAP(ASN1_STRING_to_UTF8),
        FUNC_MAP(X509_REVOKED_get_ext_by_critical),
        FUNC_MAP(ASN1_TIME_free),
        FUNC_MAP(ASN1_TIME_diff),
        FUNC_MAP(CBS_get_any_ber_asn1_element),
        FUNC_MAP(CBS_get_u24),
        FUNC_MAP(X509_OBJECT_idx_by_subject),
        FUNC_MAP(X509_CRL_verify),
        FUNC_MAP(CBS_contains_zero_byte),
        FUNC_MAP(X509_STORE_get0_objects),
        FUNC_MAP(X509_cmp_current_time),
        FUNC_MAP(d2i_RSAPublicKey_fp),
        FUNC_MAP(d2i_PUBKEY_bio),
        FUNC_MAP(i2d_PUBKEY_bio),
        FUNC_MAP(c2i_ASN1_BIT_STRING),
        FUNC_MAP(ASN1_UTCTIME_adj),
        FUNC_MAP(EVP_HPKE_KDF_hkdf_md),
        FUNC_MAP(X509_STORE_set_lookup_certs),
        FUNC_MAP(X509_REQ_delete_attr),
        FUNC_MAP(X509V3_EXT_get),
        FUNC_MAP(SSL_CTX_set_alpn_protos),
        FUNC_MAP(ASN1_ENUMERATED_get_int64),
        FUNC_MAP(ASN1_UNIVERSALSTRING_new),
        FUNC_MAP(ERR_clear_system_error),
        FUNC_MAP(SSL_CTX_set_tmp_ecdh),
        FUNC_MAP(RSA_get0_dmq1),
        FUNC_MAP(X509_LOOKUP_free),
        FUNC_MAP(X509_verify_cert),
        FUNC_MAP(X509_VERIFY_PARAM_lookup),
        FUNC_MAP(SSL_request_handshake_hints),
        FUNC_MAP(EVP_aead_aes_128_ctr_hmac_sha256),
        FUNC_MAP(EC_KEY_new_method),
        FUNC_MAP(X509_EXTENSION_create_by_NID),
        FUNC_MAP(SSL_clear_mode),
        FUNC_MAP(SSL_get_key_block_len),
        FUNC_MAP(X509_get0_signature),
        FUNC_MAP(SHA512_256),
        FUNC_MAP(PEM_def_callback),
        FUNC_MAP(RAND_poll),
        FUNC_MAP(X509_REQ_get_attr_by_NID),
        FUNC_MAP(sk_value),
        FUNC_MAP(EVP_PKEY_new_raw_private_key),
        FUNC_MAP(EC_POINT_set_affine_coordinates),
        FUNC_MAP(X509_get_default_cert_file),
        FUNC_MAP(EVP_CIPHER_CTX_cipher),
        FUNC_MAP(X509_digest),
        FUNC_MAP(ERR_print_errors),
        FUNC_MAP(DSA_marshal_private_key),
        FUNC_MAP(i2d_EC_PUBKEY),
        FUNC_MAP(SSL_use_RSAPrivateKey),
        FUNC_MAP(BN_GENCB_call),
        FUNC_MAP(SSL_SESSION_from_bytes),
        FUNC_MAP(SSL_get_curve_name),
        FUNC_MAP(SSL_get0_next_proto_negotiated),
        FUNC_MAP(EC_GROUP_get_order),
        FUNC_MAP(EVP_HPKE_KEY_copy),
        FUNC_MAP(RSA_public_key_from_bytes),
        FUNC_MAP(X509_set_pubkey),
        FUNC_MAP(X509_PURPOSE_get0),
        FUNC_MAP(SSL_CIPHER_get_value),
        FUNC_MAP(SSL_CTX_clear_options),
        FUNC_MAP(SSL_CTX_need_tmp_RSA),
        FUNC_MAP(EVP_aes_192_ecb),
        FUNC_MAP(EVP_PKEY_assign_RSA),
        FUNC_MAP(ASN1_INTEGER_free),
        FUNC_MAP(CBS_stow),
        FUNC_MAP(DSA_parse_parameters),
        FUNC_MAP(EVP_aes_256_ctr),
        FUNC_MAP(SHA512_256_Update),
        FUNC_MAP(EVP_aead_aes_128_ccm_matter),
        FUNC_MAP(RSA_print),
        FUNC_MAP(X509_REVOKED_set_revocationDate),
        FUNC_MAP(X509_NAME_ENTRY_new),
        FUNC_MAP(d2i_X509_REQ_bio),
        FUNC_MAP(d2i_PrivateKey_bio),
        FUNC_MAP(DTLS_with_buffers_method),
        FUNC_MAP(SSL_set_cipher_list),
        FUNC_MAP(BIO_get_init),
        FUNC_MAP(CONF_parse_list),
        FUNC_MAP(d2i_X509_CINF),
        FUNC_MAP(SSL_get_mode),
        FUNC_MAP(_ZN4bssl24SSL_CTX_set_handoff_modeEP10ssl_ctx_stb),
        FUNC_MAP(SSL_set_psk_server_callback),
        FUNC_MAP(CBB_discard_child),
        FUNC_MAP(i2d_DSAPrivateKey_fp),
        FUNC_MAP(X509_STORE_CTX_set_trust),
        FUNC_MAP(X509_REQ_set_pubkey),
        FUNC_MAP(ASN1_INTEGER_get),
        FUNC_MAP(bn_miller_rabin_iteration),
        FUNC_MAP(i2d_X509_CRL_tbs),
        FUNC_MAP(ED25519_keypair_from_seed),
        FUNC_MAP(EVP_PKEY_up_ref),
        FUNC_MAP(CRYPTO_BUFFER_up_ref),
        FUNC_MAP(CRYPTO_get_locking_callback),
        FUNC_MAP(d2i_CRL_DIST_POINTS),
        FUNC_MAP(SSL_CTX_load_verify_locations),
        FUNC_MAP(EVP_DigestSignFinal),
        FUNC_MAP(i2d_X509_REQ_INFO),
        FUNC_MAP(SSL_CTX_get_tlsext_ticket_keys),
        FUNC_MAP(OPENSSL_posix_to_tm),
        FUNC_MAP(EVP_marshal_private_key),
        FUNC_MAP(ECDH_compute_key_fips),
        FUNC_MAP(X509_REQ_get0_signature),
        FUNC_MAP(SSL_SESSION_to_bytes_for_ticket),
        FUNC_MAP(BIO_meth_set_read),
        FUNC_MAP(BN_get_rfc3526_prime_6144),
        FUNC_MAP(BN_CTX_start),
        FUNC_MAP(ERR_set_mark),
        FUNC_MAP(SSL_set_alpn_protos),
        FUNC_MAP(TLS_client_method),
        FUNC_MAP(i2a_ASN1_STRING),
        FUNC_MAP(EVP_DigestUpdate),
        FUNC_MAP(i2d_DSA_PUBKEY_bio),
        FUNC_MAP(SSL_CTX_sess_cache_full),
        FUNC_MAP(ASN1_SEQUENCE_it),
        FUNC_MAP(X509_STORE_CTX_get_ex_data),
        FUNC_MAP(BUF_MEM_grow),
        FUNC_MAP(ERR_get_error),
        FUNC_MAP(ec_point_mul_scalar_public),
        FUNC_MAP(X509_STORE_CTX_purpose_inherit),
        FUNC_MAP(X509_get_signature_nid),
        FUNC_MAP(SSL_CTX_set1_verify_cert_store),
        FUNC_MAP(CBS_get_u32),
        FUNC_MAP(PEM_write_DSAparams),
        FUNC_MAP(PEM_write_bio_EC_PUBKEY),
        FUNC_MAP(X509_get_default_cert_file_env),
        FUNC_MAP(SSL_use_certificate_file),
        FUNC_MAP(ASN1_BIT_STRING_num_bytes),
        FUNC_MAP(OPENSSL_free),
        FUNC_MAP(X509V3_NAME_from_section),
        FUNC_MAP(BN_bn2cbb_padded),
        FUNC_MAP(EVP_PKEY_cmp),
        FUNC_MAP(RSA_generate_key_ex),
        FUNC_MAP(OBJ_txt2nid),
        FUNC_MAP(X509_CRL_get_REVOKED),
        FUNC_MAP(X509_TRUST_cleanup),
        FUNC_MAP(X509_STORE_CTX_get_ex_new_index),
        FUNC_MAP(X509_STORE_CTX_set0_trusted_stack),
        FUNC_MAP(X509V3_add_value_int),
        FUNC_MAP(SSL_get_shared_ciphers),
        FUNC_MAP(ERR_put_error),
        FUNC_MAP(i2a_ASN1_OBJECT),
        FUNC_MAP(ASN1_BMPSTRING_it),
        FUNC_MAP(BIO_set_retry_read),
        FUNC_MAP(PKCS12_free),
        FUNC_MAP(X509_LOOKUP_shutdown),
        FUNC_MAP(EVP_hpke_chacha20_poly1305),
        FUNC_MAP(X509_gmtime_adj),
        FUNC_MAP(i2d_X509_REVOKED),
        FUNC_MAP(SSL_CTX_set_signing_algorithm_prefs),
        FUNC_MAP(EC_POINT_is_on_curve),
        FUNC_MAP(SSL_set_max_cert_list),
        FUNC_MAP(SSL_CTX_sess_misses),
        FUNC_MAP(BIO_set_conn_port),
        FUNC_MAP(NCONF_free),
        FUNC_MAP(EVP_MD_CTX_copy_ex),
        FUNC_MAP(ec_hash_to_curve_p384_xmd_sha512_sswu_draft07),
        FUNC_MAP(DH_set0_pqg),
        FUNC_MAP(PEM_read_ECPrivateKey),
        FUNC_MAP(X509_NAME_cmp),
        FUNC_MAP(SSL_CTX_set_tls_channel_id_enabled),
        FUNC_MAP(SSL_use_RSAPrivateKey_ASN1),
        FUNC_MAP(X509_REVOKED_free),
        FUNC_MAP(X25519),
        FUNC_MAP(X509_STORE_CTX_set_verify_cb),
        FUNC_MAP(X509_VERIFY_PARAM_get_flags),
        FUNC_MAP(EC_KEY_get0_group),
        FUNC_MAP(EVP_MD_nid),
        FUNC_MAP(i2d_PrivateKey_fp),
        FUNC_MAP(_ZN4bssl21ssl_session_serializeEPK14ssl_session_stP6cbb_st),
        FUNC_MAP(EC_POINT_invert),
        FUNC_MAP(d2i_PROXY_POLICY),
        FUNC_MAP(BIO_write),
        FUNC_MAP(RSA_set0_crt_params),
        FUNC_MAP(X509_up_ref),
        FUNC_MAP(SSL_CTX_set_tlsext_status_cb),
        FUNC_MAP(NETSCAPE_SPKI_b64_encode),
        FUNC_MAP(SSL_reset_early_data_reject),
        FUNC_MAP(PEM_write),
        FUNC_MAP(X509_STORE_CTX_get1_chain),
        FUNC_MAP(SSL_COMP_get_name),
        FUNC_MAP(SSL_set_tls_channel_id_enabled),
        FUNC_MAP(OPENSSL_secure_malloc),
        FUNC_MAP(RSA_verify_PKCS1_PSS_mgf1),
        FUNC_MAP(sk_insert),
        FUNC_MAP(X509_CRL_get0_extensions),
        FUNC_MAP(X509_STORE_CTX_get_current_cert),
        FUNC_MAP(_ZN4bssl20SSL_set_handoff_modeEP6ssl_stb),
        FUNC_MAP(ASN1_TIME_set),
        FUNC_MAP(X509_STORE_CTX_set0_param),
        FUNC_MAP(RSAPublicKey_dup),
        FUNC_MAP(X509_get_issuer_name),
        FUNC_MAP(X509_cmp_time),
        FUNC_MAP(BIO_set_nbio),
        FUNC_MAP(EVP_aes_192_cbc),
        FUNC_MAP(RSA_sign),
        FUNC_MAP(PEM_read_PKCS7),
        FUNC_MAP(d2i_X509_CRL_bio),
        FUNC_MAP(ASN1_UTCTIME_print),
        FUNC_MAP(BN_value_one),
        FUNC_MAP(EVP_PKEY_CTX_free),
        FUNC_MAP(PEM_read_PKCS8),
        FUNC_MAP(X509_NAME_add_entry),
        FUNC_MAP(NETSCAPE_SPKI_set_pubkey),
        FUNC_MAP(SSL_SESSION_set1_id_context),
        FUNC_MAP(EVP_sha512),
        FUNC_MAP(EVP_MD_CTX_copy),
        FUNC_MAP(HMAC_CTX_new),
        FUNC_MAP(X509_STORE_CTX_get_error),
        FUNC_MAP(SSL_CTX_set1_curves_list),
        FUNC_MAP(SSL_CTX_sess_connect),
        FUNC_MAP(EC_KEY_set_public_key_affine_coordinates),
        FUNC_MAP(SSL_get0_signed_cert_timestamp_list),
        FUNC_MAP(SSL_CTX_get_verify_mode),
        FUNC_MAP(ERR_save_state),
        FUNC_MAP(X509_VERIFY_PARAM_set1_policies),
        FUNC_MAP(X509_EXTENSION_set_object),
        FUNC_MAP(X509V3_EXT_add_alias),
        FUNC_MAP(OPENSSL_malloc_init),
        FUNC_MAP(X509_get1_email),
        FUNC_MAP(SSL_CTX_set1_ech_keys),
        FUNC_MAP(EVP_PKEY_CTX_set_hkdf_md),
        FUNC_MAP(BIO_rw_filename),
        FUNC_MAP(PKCS7_type_is_data),
        FUNC_MAP(PEM_write_SSL_SESSION),
        FUNC_MAP(X509_set_serialNumber),
        FUNC_MAP(i2d_PrivateKey_bio),
        FUNC_MAP(i2d_DHparams_bio),
        FUNC_MAP(EVP_aes_256_gcm),
        FUNC_MAP(BN_MONT_CTX_free),
        FUNC_MAP(ERR_peek_last_error_line_data),
        FUNC_MAP(ECDSA_sign),
        FUNC_MAP(_ZN4bssl18SSL_apply_handbackEP6ssl_stNS_4SpanIKhEE),
        FUNC_MAP(PEM_write_bio),
        FUNC_MAP(X509_parse_from_buffer),
        FUNC_MAP(X509_get_ext_by_critical),
        FUNC_MAP(X509_STORE_CTX_zero),
        FUNC_MAP(d2i_EDIPARTYNAME),
        FUNC_MAP(ASN1_item_i2d),
        FUNC_MAP(EVP_rc4),
        FUNC_MAP(EVP_MD_type),
        FUNC_MAP(AES_cfb128_encrypt),
        FUNC_MAP(RSA_PSS_PARAMS_it),
        FUNC_MAP(X509_STORE_get1_certs),
        FUNC_MAP(X509_REQ_get1_email),
        FUNC_MAP(BIO_set_shutdown),
        FUNC_MAP(EVP_PKEY_get0_EC_KEY),
        FUNC_MAP(SSL_is_signature_algorithm_rsa_pss),
        FUNC_MAP(EVP_PKEY_CTX_set_rsa_oaep_md),
        FUNC_MAP(EVP_CIPHER_CTX_reset),
        FUNC_MAP(X509_STORE_set_trust),
        FUNC_MAP(PROXY_CERT_INFO_EXTENSION_it),
        FUNC_MAP(BIO_test_flags),
        FUNC_MAP(FIPS_service_indicator_after_call),
        FUNC_MAP(ASN1_FBOOLEAN_it),
        FUNC_MAP(PEM_read_DSA_PUBKEY),
        FUNC_MAP(SSL_set_wfd),
        FUNC_MAP(SSL_get0_chain_certs),
        FUNC_MAP(DES_encrypt3),
        FUNC_MAP(EVP_HPKE_KEY_zero),
        FUNC_MAP(PEM_write_ECPrivateKey),
        FUNC_MAP(RAND_seed),
        FUNC_MAP(X509_CRL_delete_ext),
        FUNC_MAP(X509_REVOKED_get_ext_count),
        FUNC_MAP(X509_CRL_add0_revoked),
        FUNC_MAP(ASN1_STRING_new),
        FUNC_MAP(EVP_PKEY_verify),
        FUNC_MAP(EVP_PKEY_CTX_set_rsa_pss_keygen_saltlen),
        FUNC_MAP(OBJ_ln2nid),
        FUNC_MAP(X509_check_private_key),
        FUNC_MAP(SSL_CTX_get_num_tickets),
        FUNC_MAP(EVP_PKEY_derive_set_peer),
        FUNC_MAP(i2d_NOTICEREF),
        FUNC_MAP(CBS_get_asn1_uint64),
        FUNC_MAP(i2d_PKCS8PrivateKey_bio),
        FUNC_MAP(d2i_DIST_POINT_NAME),
        FUNC_MAP(ASN1_tag2bit),
        FUNC_MAP(d2i_ASN1_UNIVERSALSTRING),
        FUNC_MAP(ERR_load_crypto_strings),
        FUNC_MAP(ASN1_INTEGER_get_uint64),
        FUNC_MAP(BN_mask_bits),
        FUNC_MAP(ERR_pop_to_mark),
        FUNC_MAP(X509_ATTRIBUTE_create_by_txt),
        FUNC_MAP(i2d_OTHERNAME),
        FUNC_MAP(PROXY_CERT_INFO_EXTENSION_free),
        FUNC_MAP(BUF_MEM_new),
        FUNC_MAP(BN_mul_word),
        FUNC_MAP(BN_GENCB_get_arg),
        FUNC_MAP(PEM_proc_type),
        FUNC_MAP(X509_STORE_set_get_issuer),
        FUNC_MAP(EVP_Digest),
        FUNC_MAP(SSL_CTX_get_ex_new_index),
        FUNC_MAP(DH_new),
        FUNC_MAP(RAND_OpenSSL),
        FUNC_MAP(SSL_CTX_set_tlsext_status_arg),
        FUNC_MAP(SSL_SESSION_get0_peer_sha256),
        FUNC_MAP(X509_STORE_CTX_get0_parent_ctx),
        FUNC_MAP(BN_cmp),
        FUNC_MAP(ERR_free_strings),
        FUNC_MAP(v2i_GENERAL_NAME),
        FUNC_MAP(SSL_set_tlsext_status_type),
        FUNC_MAP(EVP_CIPHER_CTX_set_key_length),
        FUNC_MAP(X509_CRL_sign),
        FUNC_MAP(SSL_clear_chain_certs),
        FUNC_MAP(EVP_MD_CTX_move),
        FUNC_MAP(PEM_write_DSAPrivateKey),
        FUNC_MAP(EVP_AEAD_CTX_zero),
        FUNC_MAP(X509_NAME_hash),
        FUNC_MAP(X509_NAME_ENTRY_get_object),
        FUNC_MAP(NAME_CONSTRAINTS_it),
        FUNC_MAP(BIO_copy_next_retry),
        FUNC_MAP(DH_get0_g),
        FUNC_MAP(sk_pop_free),
        FUNC_MAP(X509_CRL_get0_by_serial),
        FUNC_MAP(ASN1_UTF8STRING_it),
        FUNC_MAP(ASN1_PRINTABLESTRING_it),
        FUNC_MAP(BIO_meth_set_gets),
        FUNC_MAP(EVP_PKEY_print_private),
        FUNC_MAP(EVP_DecryptInit),
        FUNC_MAP(i2d_GENERAL_NAMES),
        FUNC_MAP(CBB_len),
        FUNC_MAP(EC_KEY_new),
        FUNC_MAP(EC_POINT_dup),
        FUNC_MAP(SSL_ech_accepted),
        FUNC_MAP(BN_mod_exp),
        FUNC_MAP(i2d_X509_REQ_bio),
        FUNC_MAP(SSL_total_renegotiations),
        FUNC_MAP(SSL_CTX_sess_get_new_cb),
        FUNC_MAP(DHparams_dup),
        FUNC_MAP(CRYPTO_set_ex_data),
        FUNC_MAP(X509V3_EXT_get_nid),
        FUNC_MAP(X509_get1_ocsp),
        FUNC_MAP(SSL_early_data_accepted),
        FUNC_MAP(SHA224_Init),
        FUNC_MAP(CRYPTO_secure_malloc_init),
        FUNC_MAP(SSL_CIPHER_get_cipher_nid),
        FUNC_MAP(SSL_CTX_set_tlsext_ticket_keys),
        FUNC_MAP(RSA_check_key),
        FUNC_MAP(PEM_write_bio_PUBKEY),
        FUNC_MAP(X509_STORE_CTX_init),
        FUNC_MAP(SSL_error_description),
        FUNC_MAP(X509_STORE_set1_param),
        FUNC_MAP(X509_REQ_add_extensions),
        FUNC_MAP(i2d_PROXY_CERT_INFO_EXTENSION),
        FUNC_MAP(SSL_CTX_new),
        FUNC_MAP(SSL_get_pending_cipher),
        FUNC_MAP(i2d_ASN1_PRINTABLESTRING),
        FUNC_MAP(BLAKE2B256_Update),
        FUNC_MAP(DH_get0_p),
        FUNC_MAP(X509_verify),
        FUNC_MAP(CBS_get_u32le),
        FUNC_MAP(EVP_aead_aes_256_cbc_sha1_tls),
        FUNC_MAP(DH_get0_q),
        FUNC_MAP(RSA_blinding_on),
        FUNC_MAP(SSL_alert_type_string_long),
        FUNC_MAP(_ZN4bssl19SealRecordSuffixLenEPK6ssl_stm),
        FUNC_MAP(BIO_free_all),
        FUNC_MAP(EVP_HPKE_KEM_id),
        FUNC_MAP(d2i_AUTHORITY_KEYID),
        FUNC_MAP(SSL_CTX_set1_param),
        FUNC_MAP(BUF_strdup),
        FUNC_MAP(i2d_PKCS8PrivateKeyInfo_bio),
        FUNC_MAP(d2i_X509_NAME),
        FUNC_MAP(SSL_CIPHER_is_aead),
        FUNC_MAP(SSL_alert_desc_string),
        FUNC_MAP(EC_KEY_get_enc_flags),
        FUNC_MAP(EC_KEY_up_ref),
        FUNC_MAP(EVP_HPKE_KEY_init),
        FUNC_MAP(X509at_add1_attr),
        FUNC_MAP(EVP_DecodeBase64),
        FUNC_MAP(BIO_new_connect),
        FUNC_MAP(EVP_PKEY_verify_init),
        FUNC_MAP(SHA256),
        FUNC_MAP(X509_CRL_get_issuer),
        FUNC_MAP(SSL_marshal_ech_config),
        FUNC_MAP(EVP_aead_des_ede3_cbc_sha1_tls_implicit_iv),
        FUNC_MAP(BN_primality_test),
        FUNC_MAP(EVP_hpke_aes_128_gcm),
        FUNC_MAP(X509_TRUST_set),
        FUNC_MAP(X509_ALGOR_cmp),
        FUNC_MAP(X509V3_string_free),
        FUNC_MAP(SSL_CTX_set_verify_algorithm_prefs),
        FUNC_MAP(EC_KEY_set_conv_form),
        FUNC_MAP(EVP_HPKE_CTX_max_overhead),
        FUNC_MAP(X509_NAME_oneline),
        FUNC_MAP(ASN1_IA5STRING_free),
        FUNC_MAP(EVP_MD_meth_get_flags),
        FUNC_MAP(EC_GROUP_order_bits),
        FUNC_MAP(BN_cmp_word),
        FUNC_MAP(EVP_DigestSignInit),
        FUNC_MAP(EVP_HPKE_AEAD_id),
        FUNC_MAP(i2d_CRL_DIST_POINTS),
        FUNC_MAP(EVP_HPKE_KEY_new),
        FUNC_MAP(PEM_read_bio_DSA_PUBKEY),
        FUNC_MAP(i2d_NETSCAPE_SPKI),
        FUNC_MAP(SSL_SESSION_get0_id_context),
        FUNC_MAP(BN_CTX_get),
        FUNC_MAP(RSA_get0_pss_params),
        FUNC_MAP(X509_NAME_ENTRY_create_by_txt),
        FUNC_MAP(SSL_CTX_set0_client_CAs),
        FUNC_MAP(SSL_alert_from_verify_result),
        FUNC_MAP(OPENSSL_strhash),
        FUNC_MAP(EVP_PKEY_verify_recover),
        FUNC_MAP(TRUST_TOKEN_experiment_v2_pmb),
        FUNC_MAP(i2d_PKCS8PrivateKeyInfo_fp),
        FUNC_MAP(PEM_read_bio_RSAPublicKey),
        FUNC_MAP(EDIPARTYNAME_new),
        FUNC_MAP(SSL_set_verify_algorithm_prefs),
        FUNC_MAP(BIO_s_socket),
        FUNC_MAP(RSA_private_encrypt),
        FUNC_MAP(SSL_CTX_set_select_certificate_cb),
        FUNC_MAP(CONF_modules_free),
        FUNC_MAP(CRYPTO_cleanup_all_ex_data),
        FUNC_MAP(EVP_AEAD_CTX_init),
        FUNC_MAP(ASN1_item_digest),
        FUNC_MAP(d2i_X509_PUBKEY),
        FUNC_MAP(SSL_set_accept_state),
        FUNC_MAP(BIO_get_mem_data),
        FUNC_MAP(HKDF_extract),
        FUNC_MAP(CRYPTO_set_dynlock_lock_callback),
        FUNC_MAP(SSL_CTX_set_tlsext_use_srtp),
        FUNC_MAP(SSL_CTX_get_read_ahead),
        FUNC_MAP(SSL_SESSION_get0_ticket),
        FUNC_MAP(OBJ_find_sigid_by_algs),
        FUNC_MAP(X509_set1_notAfter),
        FUNC_MAP(X509_reject_clear),
        FUNC_MAP(OTHERNAME_new),
        FUNC_MAP(EVP_VerifyInit),
        FUNC_MAP(CRYPTO_secure_used),
        FUNC_MAP(POLICY_CONSTRAINTS_free),
        FUNC_MAP(i2d_PROXY_POLICY),
        FUNC_MAP(SSL_CTX_get0_certificate),
        FUNC_MAP(BIO_should_read),
        FUNC_MAP(PEM_write_PKCS8PrivateKey),
        FUNC_MAP(X509_REQ_dup),
        FUNC_MAP(ASN1_item_d2i_fp),
        FUNC_MAP(OBJ_length),
        FUNC_MAP(EVP_PKEY_set1_EC_KEY),
        FUNC_MAP(i2d_PKCS8PrivateKey_nid_bio),
        FUNC_MAP(X509_VAL_it),
        FUNC_MAP(POLICY_MAPPINGS_it),
        FUNC_MAP(DTLSv1_get_timeout),
        FUNC_MAP(SSL_CTX_use_RSAPrivateKey),
        FUNC_MAP(SSL_get_version),
        FUNC_MAP(bn_rshift_secret_shift),
        FUNC_MAP(MD5_Update),
        FUNC_MAP(OBJ_nid2cbb),
        FUNC_MAP(PKCS8_PRIV_KEY_INFO_new),
        FUNC_MAP(X509_REVOKED_get0_revocationDate),
        FUNC_MAP(X509_STORE_new),
        FUNC_MAP(BIO_ctrl),
        FUNC_MAP(EVP_PKEY_get1_DH),
        FUNC_MAP(X509_VERIFY_PARAM_clear_flags),
        FUNC_MAP(ASN1_ENUMERATED_set),
        FUNC_MAP(ASN1_OBJECT_create),
        FUNC_MAP(ASN1_STRING_set_by_NID),
        FUNC_MAP(PEM_read_bio),
        FUNC_MAP(X509_REQ_print),
        FUNC_MAP(i2d_CERTIFICATEPOLICIES),
        FUNC_MAP(SSL_get_wbio),
        FUNC_MAP(SSL_want),
        FUNC_MAP(EC_KEY_marshal_curve_name),
        FUNC_MAP(ERR_get_error_line_data),
        FUNC_MAP(DH_generate_key),
        FUNC_MAP(DH_up_ref),
        FUNC_MAP(FIPS_mode),
        FUNC_MAP(PEM_write_bio_SSL_SESSION),
        FUNC_MAP(EVP_aes_256_ecb),
        FUNC_MAP(EVP_PKEY_CTX_set_ec_paramgen_curve_nid),
        FUNC_MAP(CMAC_Reset),
        FUNC_MAP(SSL_CTX_set_session_psk_dhe_timeout),
        FUNC_MAP(BLAKE2B256_Final),
        FUNC_MAP(PEM_read_X509_CRL),
        FUNC_MAP(X509_ALGOR_free),
        FUNC_MAP(i2d_X509_CRL_bio),
        FUNC_MAP(i2d_X509_CINF),
        FUNC_MAP(X509_CINF_new),
        FUNC_MAP(ASN1_STRING_copy),
        FUNC_MAP(BN_is_one),
        FUNC_MAP(FIPS_query_algorithm_status),
        FUNC_MAP(X509_EXTENSION_create_by_OBJ),
        FUNC_MAP(OPENSSL_config),
        FUNC_MAP(AES_set_decrypt_key),
        FUNC_MAP(EC_POINT_set_compressed_coordinates_GFp),
        FUNC_MAP(OBJ_cbs2nid),
        FUNC_MAP(X509_OBJECT_get_type),
        FUNC_MAP(BN_mod_sqr),
        FUNC_MAP(X509_REQ_get_attr_by_OBJ),
        FUNC_MAP(SSL_set_chain_and_key),
        FUNC_MAP(X509_get_ext_by_NID),
        FUNC_MAP(X509_REQ_check_private_key),
        FUNC_MAP(X509_keyid_set1),
        FUNC_MAP(EVP_SignUpdate),
        FUNC_MAP(EVP_HPKE_KEY_private_key),
        FUNC_MAP(X509_CRL_get_ext_count),
        FUNC_MAP(x509v3_bytes_to_hex),
        FUNC_MAP(SHA384_Final),
        FUNC_MAP(SSL_num_renegotiations),
        FUNC_MAP(SSL_set0_client_CAs),
        FUNC_MAP(SSL_CTX_set_msg_callback),
        FUNC_MAP(EVP_AEAD_CTX_tag_len),
        FUNC_MAP(SSL_get_read_sequence),
        FUNC_MAP(BN_dec2bn),
        FUNC_MAP(i2d_DSA_SIG),
        FUNC_MAP(BN_bn2le_padded),
        FUNC_MAP(CRYPTO_gcm128_tag),
        FUNC_MAP(BIO_set_fd),
        FUNC_MAP(EVP_aead_aes_128_cbc_sha1_tls),
        FUNC_MAP(SHA1_Final),
        FUNC_MAP(SPAKE2_generate_msg),
        FUNC_MAP(EC_POINT_get_affine_coordinates),
        FUNC_MAP(X509_STORE_get_cert_crl),
        FUNC_MAP(NETSCAPE_SPKI_sign),
        FUNC_MAP(X509_PUBKEY_set0_param),
        FUNC_MAP(EVP_PKEY_get0_DH),
        FUNC_MAP(EVP_PKEY_keygen_init),
        FUNC_MAP(RSA_public_key_to_bytes),
        FUNC_MAP(CBS_get_u64),
        FUNC_MAP(SPAKE2_process_msg),
        FUNC_MAP(EVP_AEAD_max_overhead),
        FUNC_MAP(BASIC_CONSTRAINTS_it),
        FUNC_MAP(i2d_EDIPARTYNAME),
        FUNC_MAP(i2c_ASN1_BIT_STRING),
        FUNC_MAP(d2i_ASN1_SEQUENCE_ANY),
        FUNC_MAP(EVP_CipherFinal_ex),
        FUNC_MAP(ACCESS_DESCRIPTION_new),
        FUNC_MAP(TLSv1_1_client_method),
        FUNC_MAP(i2d_DISPLAYTEXT),
        FUNC_MAP(PEM_write_X509_CRL),
        FUNC_MAP(X509_NAME_ENTRY_dup),
        FUNC_MAP(cbs_get_latin1),
        FUNC_MAP(CRYPTO_STATIC_MUTEX_unlock_read),
        FUNC_MAP(X509_NAME_ENTRY_get_data),
        FUNC_MAP(SSL_CTX_set_trust),
        FUNC_MAP(BIO_new_file),
        FUNC_MAP(EVP_MD_CTX_init),
        FUNC_MAP(EVP_PKEY_id),
        FUNC_MAP(SSL_ECH_KEYS_marshal_retry_configs),
        FUNC_MAP(EC_KEY_generate_key),
        FUNC_MAP(OPENSSL_secure_clear_free),
        FUNC_MAP(X509_REQ_get_attr_count),
        FUNC_MAP(BN_mod_exp2_mont),
        FUNC_MAP(PEM_read_EC_PUBKEY),
        FUNC_MAP(X509_VERIFY_PARAM_set1_name),
        FUNC_MAP(SSL_quic_max_handshake_flight_len),
        FUNC_MAP(SSL_CTX_set_max_cert_list),
        FUNC_MAP(SSL_CTX_sess_cb_hits),
        FUNC_MAP(asn1_get_string_table_for_testing),
        FUNC_MAP(DIRECTORYSTRING_new),
        FUNC_MAP(HRSS_decap),
        FUNC_MAP(X509at_get_attr_by_NID),
        FUNC_MAP(POLICY_MAPPING_free),
        FUNC_MAP(ASN1_UTF8STRING_free),
        FUNC_MAP(RSA_parse_private_key),
        FUNC_MAP(d2i_POLICYQUALINFO),
        FUNC_MAP(BIO_set_fp),
        FUNC_MAP(BN_bn2binpad),
        FUNC_MAP(EVP_PKEY_get_raw_private_key),
        FUNC_MAP(EVP_add_digest),
        FUNC_MAP(X509V3_EXT_CRL_add_nconf),
        FUNC_MAP(ISSUING_DIST_POINT_it),
        FUNC_MAP(X509V3_add_value_bool_nf),
        FUNC_MAP(OPENSSL_strlcat),
        FUNC_MAP(RSA_parse_public_key),
        FUNC_MAP(EVP_aead_aes_256_gcm),
        FUNC_MAP(X509_REQ_add1_attr),
        FUNC_MAP(SSL_set_mtu),
        FUNC_MAP(ASN1_PRINTABLE_type),
        FUNC_MAP(EVP_DecodeInit),
        FUNC_MAP(EC_POINT_oct2point),
        FUNC_MAP(X509_get_subject_name),
        FUNC_MAP(sk_new),
        FUNC_MAP(PEM_read_bio_SSL_SESSION),
        FUNC_MAP(EC_POINT_mul),
        FUNC_MAP(BIO_meth_set_puts),
        FUNC_MAP(DSA_SIG_parse),
        FUNC_MAP(BN_mod_exp_mont_word),
        FUNC_MAP(DSA_free),
        FUNC_MAP(DSA_SIG_set0),
        FUNC_MAP(ECDSA_SIG_from_bytes),
        FUNC_MAP(CMAC_Init),
        FUNC_MAP(CRYPTO_STATIC_MUTEX_lock_write),
        FUNC_MAP(OBJ_txt2obj),
        FUNC_MAP(PEM_do_header),
        FUNC_MAP(SSL_use_psk_identity_hint),
        FUNC_MAP(SSL_use_PrivateKey_ASN1),
        FUNC_MAP(SSL_SESSION_get_version),
        FUNC_MAP(TLSv1_2_method),
        FUNC_MAP(ERR_add_error_data),
        FUNC_MAP(OPENSSL_cleanse),
        FUNC_MAP(EC_GROUP_get_curve_GFp),
        FUNC_MAP(d2i_X509_NAME_ENTRY),
        FUNC_MAP(ERR_reason_error_string),
        FUNC_MAP(X509_STORE_set_lookup_crls),
        FUNC_MAP(X509_set1_signature_algo),
        FUNC_MAP(CBS_is_valid_asn1_bitstring),
        FUNC_MAP(SSL_CTX_clear_extra_chain_certs),
        FUNC_MAP(ASN1_item_d2i_bio),
        FUNC_MAP(CBS_get_asn1_bool),
        FUNC_MAP(d2i_RSA_PUBKEY_bio),
        FUNC_MAP(SSL_set_verify),
        FUNC_MAP(ASN1_item_i2d_fp),
        FUNC_MAP(CRYPTO_library_init),
        FUNC_MAP(X509_get0_notBefore),
        FUNC_MAP(SSL_state_string_long),
        FUNC_MAP(EVP_aes_256_cbc),
        FUNC_MAP(EVP_HPKE_CTX_cleanup),
        FUNC_MAP(SSL_SESSION_is_resumable),
        FUNC_MAP(SSL_set_ocsp_response),
        FUNC_MAP(ASN1_INTEGER_new),
        FUNC_MAP(BN_to_ASN1_INTEGER),
        FUNC_MAP(BIO_wpending),
        FUNC_MAP(EC_curve_nid2nist),
        FUNC_MAP(HMAC_Init),
        FUNC_MAP(PEM_write_bio_PKCS8_PRIV_KEY_INFO),
        FUNC_MAP(EVP_HPKE_CTX_seal),
        FUNC_MAP(i2d_PKCS12_bio),
        FUNC_MAP(X509_VERIFY_PARAM_set_depth),
        FUNC_MAP(d2i_ECPrivateKey_bio),
        FUNC_MAP(SSL_add0_chain_cert),
        FUNC_MAP(EVP_aead_aes_128_cbc_sha1_tls_implicit_iv),
        FUNC_MAP(DH_free),
        FUNC_MAP(BN_get_rfc3526_prime_8192),
        FUNC_MAP(EVP_PKEY_free),
        FUNC_MAP(SSL_get_current_expansion),
        FUNC_MAP(SSL_set_SSL_CTX),
        FUNC_MAP(DSA_get0_key),
        FUNC_MAP(BN_num_bits_word),
        FUNC_MAP(DH_check),
        FUNC_MAP(CRYPTO_fork_detect_ignore_madv_wipeonfork_for_testing),
        FUNC_MAP(SSL_is_init_finished),
        FUNC_MAP(SSL_CTX_set1_chain),
        FUNC_MAP(ASN1_TYPE_set1),
        FUNC_MAP(BN_div_word),
        FUNC_MAP(EVP_HPKE_CTX_setup_sender),
        FUNC_MAP(X509_REQ_get_extensions),
        FUNC_MAP(i2d_RSA_PUBKEY_fp),
        FUNC_MAP(X509V3_EXT_add),
        FUNC_MAP(SSL_version),
        FUNC_MAP(BN_bn2mpi),
        FUNC_MAP(HMAC_Final),
        FUNC_MAP(BN_to_montgomery),
        FUNC_MAP(CTR_DRBG_generate),
        FUNC_MAP(i2d_PKCS8PrivateKey_fp),
        FUNC_MAP(d2i_POLICYINFO),
        FUNC_MAP(i2d_POLICYINFO),
        FUNC_MAP(ASN1_GENERALSTRING_free),
        FUNC_MAP(i2d_DIRECTORYSTRING),
        FUNC_MAP(DSA_get0_g),
        FUNC_MAP(SSL_CTX_set_tlsext_servername_callback),
        FUNC_MAP(d2i_ECParameters),
        FUNC_MAP(X509v3_get_ext_by_critical),
        FUNC_MAP(X509_OBJECT_up_ref_count),
        FUNC_MAP(SSL_get_options),
        FUNC_MAP(SSL_CIPHER_get_max_version),
        FUNC_MAP(EVP_DecodeFinal),
        FUNC_MAP(CBB_add_u16_length_prefixed),
        FUNC_MAP(CMAC_CTX_new),
        FUNC_MAP(X509V3_parse_list),
        FUNC_MAP(X509_PURPOSE_get0_sname),
        FUNC_MAP(SSL_early_callback_ctx_extension_get),
        FUNC_MAP(ASN1_BIT_STRING_set),
        FUNC_MAP(ASN1_NULL_free),
        FUNC_MAP(TRUST_TOKEN_CLIENT_finish_issuance),
        FUNC_MAP(BIO_s_file),
        FUNC_MAP(MD4),
        FUNC_MAP(MD5),
        FUNC_MAP(OTHERNAME_it),
        FUNC_MAP(SSL_quic_read_level),
        FUNC_MAP(SSL_CTX_get_options),
        FUNC_MAP(EVP_PKEY_set1_DSA),
        FUNC_MAP(EVP_DigestSign),
        FUNC_MAP(EC_KEY_generate_key_fips),
        FUNC_MAP(X509at_delete_attr),
        FUNC_MAP(ISSUING_DIST_POINT_free),
        FUNC_MAP(CBS_init),
        FUNC_MAP(DSA_do_check_signature),
        FUNC_MAP(ECDSA_SIG_get0_r),
        FUNC_MAP(OPENSSL_lh_num_items),
        FUNC_MAP(X509_alias_get0),
        FUNC_MAP(SSL_get_rfd),
        FUNC_MAP(SSL_set1_tls_channel_id),
        FUNC_MAP(ASN1_INTEGER_to_BN),
        FUNC_MAP(EVP_BytesToKey),
        FUNC_MAP(DSA_get0_p),
        FUNC_MAP(ECDSA_SIG_get0_s),
        FUNC_MAP(X509_LOOKUP_file),
        FUNC_MAP(DES_set_odd_parity),
        FUNC_MAP(DSA_get0_q),
        FUNC_MAP(EVP_PKEY_get1_RSA),
        FUNC_MAP(EC_KEY_get0_public_key),
        FUNC_MAP(PKCS7_get_certificates),
        FUNC_MAP(X509_NAME_print_ex),
        FUNC_MAP(i2d_AUTHORITY_KEYID),
        FUNC_MAP(SSL_CTX_sess_set_get_cb),
        FUNC_MAP(ASN1_TYPE_get),
        FUNC_MAP(ASN1_UTCTIME_set),
        FUNC_MAP(EVP_PKEY_new_raw_public_key),
        FUNC_MAP(PEM_write_PUBKEY),
        FUNC_MAP(PEM_read_X509),
        FUNC_MAP(BUF_strlcpy),
        FUNC_MAP(CBS_get_u24_length_prefixed),
        FUNC_MAP(EVP_PKEY_CTX_set_rsa_mgf1_md),
        FUNC_MAP(CBS_get_u8),
        FUNC_MAP(SSL_set_quiet_shutdown),
        FUNC_MAP(ASN1_OBJECT_free),
        FUNC_MAP(d2i_ASN1_TYPE),
        FUNC_MAP(SSL_get_peer_finished),
        FUNC_MAP(CBB_add_asn1_int64),
        FUNC_MAP(PKCS7_type_is_encrypted),
        FUNC_MAP(sk_deep_copy),
        FUNC_MAP(X509_check_email),
        FUNC_MAP(CBS_get_asn1),
        FUNC_MAP(CBS_get_any_asn1_element),
        FUNC_MAP(MD4_Final),
        FUNC_MAP(OPENSSL_clear_free),
        FUNC_MAP(PEM_write_bio_PKCS8PrivateKey_nid),
        FUNC_MAP(X509_sign),
        FUNC_MAP(X509V3_EXT_add_list),
        FUNC_MAP(RSA_get0_factors),
        FUNC_MAP(SSL_CTX_set0_buffer_pool),
        FUNC_MAP(BN_sub_word),
        FUNC_MAP(EVP_MD_CTX_type),
        FUNC_MAP(SSL_CTX_set_quiet_shutdown),
        FUNC_MAP(X509v3_get_ext),
        FUNC_MAP(X509V3_set_nconf),
        FUNC_MAP(SSL_CTX_clear_chain_certs),
        FUNC_MAP(SSL_CTX_set_private_key_method),
        FUNC_MAP(TRUST_TOKEN_decode_private_metadata),
        FUNC_MAP(X509_NAME_print_ex_fp),
        FUNC_MAP(X509_NAME_free),
        FUNC_MAP(SSL_set0_chain),
        FUNC_MAP(DSA_parse_private_key),
        FUNC_MAP(ec_bignum_to_scalar),
        FUNC_MAP(SSL_get0_ech_name_override),
        FUNC_MAP(BIO_s_mem),
        FUNC_MAP(EC_KEY_derive_from_secret),
        FUNC_MAP(EVP_PKEY_CTX_set_dsa_paramgen_q_bits),
        FUNC_MAP(EVP_AEAD_max_tag_len),
        FUNC_MAP(X509V3_section_free),
        FUNC_MAP(SSL_get_cipher_by_value),
        FUNC_MAP(ASN1_STRING_dup),
        FUNC_MAP(d2i_RSAPublicKey_bio),
        FUNC_MAP(X509V3_get_string),
        FUNC_MAP(SSL_set1_sigalgs),
        FUNC_MAP(SSL_SESSION_set1_id),
        FUNC_MAP(cbs_get_ucs2_be),
        FUNC_MAP(EC_KEY_get0_private_key),
        FUNC_MAP(X509_TRUST_get_flags),
        FUNC_MAP(d2i_X509_REQ_INFO),
        FUNC_MAP(SSL_get_ticket_age_skew),
        FUNC_MAP(sk_push),
        FUNC_MAP(X509_PUBKEY_set),
        FUNC_MAP(ISSUING_DIST_POINT_new),
        FUNC_MAP(DTLS_client_method),
        FUNC_MAP(DSA_dup_DH),
        FUNC_MAP(ERR_load_RAND_strings),
        FUNC_MAP(DH_compute_key_hashed),
        FUNC_MAP(DH_get_rfc7919_2048),
        FUNC_MAP(SHA256_Transform),
        FUNC_MAP(ASN1_item_verify),
        FUNC_MAP(X509_get0_authority_serial),
        FUNC_MAP(HMAC_Update),
        FUNC_MAP(X509_chain_up_ref),
        FUNC_MAP(USERNOTICE_it),
        FUNC_MAP(SSL_get_rbio),
        FUNC_MAP(ASN1_GENERALIZEDTIME_set_string),
        FUNC_MAP(ASN1_item_i2d_bio),
        FUNC_MAP(BIO_should_io_special),
        FUNC_MAP(EVP_DigestVerify),
        FUNC_MAP(FIPS_service_indicator_before_call),
        FUNC_MAP(X509_ATTRIBUTE_it),
        FUNC_MAP(CRL_DIST_POINTS_it),
        FUNC_MAP(GENERAL_NAME_set0_value),
        FUNC_MAP(OpenSSL_add_all_digests),
        FUNC_MAP(EVP_CipherInit),
        FUNC_MAP(CRYPTO_get_lock_name),
        FUNC_MAP(EVP_aead_aes_256_gcm_tls12),
        FUNC_MAP(RSA_new_method),
        FUNC_MAP(EVP_aead_aes_256_gcm_tls13),
        FUNC_MAP(i2d_X509_NAME),
        FUNC_MAP(CRYPTO_THREADID_set_pointer),
        FUNC_MAP(X509_REQ_print_ex),
        FUNC_MAP(i2d_RSA_PUBKEY_bio),
        FUNC_MAP(DTLSv1_2_client_method),
        FUNC_MAP(BIO_clear_retry_flags),
        FUNC_MAP(BN_print),
        FUNC_MAP(DES_decrypt3),
        FUNC_MAP(DSA_up_ref),
        FUNC_MAP(OTHERNAME_free),
        FUNC_MAP(ASN1_T61STRING_free),
        FUNC_MAP(i2d_ASN1_VISIBLESTRING),
        FUNC_MAP(DIRECTORYSTRING_it),
        FUNC_MAP(RAND_get_system_entropy_for_custom_prng),
        FUNC_MAP(sk_delete),
        FUNC_MAP(X509_STORE_up_ref),
        FUNC_MAP(X509_set_version),
        FUNC_MAP(SSL_CIPHER_get_protocol_id),
        FUNC_MAP(i2d_ASN1_UNIVERSALSTRING),
        FUNC_MAP(BN_abs_is_word),
        FUNC_MAP(DH_compute_key_padded),
        FUNC_MAP(X509_set_notBefore),
        FUNC_MAP(X509_PURPOSE_get0_name),
        FUNC_MAP(SSL_get_tlsext_status_type),
        FUNC_MAP(SSL_set1_verify_cert_store),
        FUNC_MAP(EVP_PKEY_CTX_set_rsa_pss_saltlen),
        FUNC_MAP(PKCS7_get_raw_certificates),
        FUNC_MAP(X509_REVOKED_delete_ext),
        FUNC_MAP(ASN1_INTEGER_get_int64),
        FUNC_MAP(BIO_meth_set_write),
        FUNC_MAP(CRYPTO_MUTEX_init),
        FUNC_MAP(EC_KEY_dup),
        FUNC_MAP(CRYPTO_gcm128_encrypt_ctr32),
        FUNC_MAP(ASN1_item_sign),
        FUNC_MAP(EC_POINT_dbl),
        FUNC_MAP(X509_time_adj_ex),
        FUNC_MAP(X509_NAME_digest),
        FUNC_MAP(SSL_set1_groups),
        FUNC_MAP(SSL_get0_peer_application_settings),
        FUNC_MAP(X509V3_add_value),
        FUNC_MAP(CBB_cleanup),
        FUNC_MAP(EVP_PKEY_CTX_get0_pkey),
        FUNC_MAP(CRYPTO_set_add_lock_callback),
        FUNC_MAP(X509v3_delete_ext),
        FUNC_MAP(X509_VERIFY_PARAM_add1_host),
        FUNC_MAP(d2i_PrivateKey_fp),
        FUNC_MAP(ASN1_NULL_new),
        FUNC_MAP(BIO_hexdump),
        FUNC_MAP(ERR_load_ERR_strings),
        FUNC_MAP(BN_mod_word),
        FUNC_MAP(DH_set0_key),
        FUNC_MAP(X509_VERIFY_PARAM_new),
        FUNC_MAP(SSL_CTX_set1_groups),
        FUNC_MAP(SSL_set_psk_client_callback),
        FUNC_MAP(SSL_SESSION_get0_cipher),
        FUNC_MAP(X509_LOOKUP_hash_dir),
        FUNC_MAP(SSL_CTX_set_false_start_allowed_without_alpn),
        FUNC_MAP(EVP_PKEY_set1_tls_encodedpoint),
        FUNC_MAP(RSA_test_flags),
        FUNC_MAP(X509_INFO_free),
        FUNC_MAP(d2i_ASN1_UTF8STRING),
        FUNC_MAP(ASN1_GENERALSTRING_it),
        FUNC_MAP(BIO_set_data),
        FUNC_MAP(PEM_write_bio_RSA_PUBKEY),
        FUNC_MAP(X509_CRL_set_issuer_name),
        FUNC_MAP(d2i_ECPrivateKey_fp),
        FUNC_MAP(ASN1_INTEGER_it),
        FUNC_MAP(SHA1_Init),
        FUNC_MAP(CTR_DRBG_new),
        FUNC_MAP(EVP_MD_size),
        FUNC_MAP(X509_VERIFY_PARAM_set1_ip),
        FUNC_MAP(CERTIFICATEPOLICIES_it),
        FUNC_MAP(OPENSSL_lh_new),
        FUNC_MAP(BN_rand_range),
        FUNC_MAP(RAND_load_file),
        FUNC_MAP(ASN1_STRING_print_ex),
        FUNC_MAP(BIO_read),
        FUNC_MAP(BN_mod_sqrt),
        FUNC_MAP(SSL_process_quic_post_handshake),
        FUNC_MAP(EVP_EncryptUpdate),
        FUNC_MAP(EVP_PKEY_CTX_new),
        FUNC_MAP(EVP_aead_aes_128_gcm_randnonce),
        FUNC_MAP(EC_KEY_check_fips),
        FUNC_MAP(X509_CRL_new),
        FUNC_MAP(X509_NAME_ENTRY_set),
        FUNC_MAP(ASN1_GENERALIZEDTIME_it),
        FUNC_MAP(BIO_up_ref),
        FUNC_MAP(X509_CRL_it),
        FUNC_MAP(ERR_load_BIO_strings),
        FUNC_MAP(BN_MONT_CTX_copy),
        FUNC_MAP(SSL_has_pending),
        FUNC_MAP(BN_mod_sub_quick),
        FUNC_MAP(SSL_get_quiet_shutdown),
        FUNC_MAP(SSL_SESSION_get_protocol_version),
        FUNC_MAP(BIO_get_retry_reason),
        FUNC_MAP(SSL_CIPHER_get_version),
        FUNC_MAP(TLS_server_method),
        FUNC_MAP(ASN1_OCTET_STRING_free),
        FUNC_MAP(EVP_CIPHER_CTX_get_app_data),
        FUNC_MAP(OBJ_create),
        FUNC_MAP(ASN1_tag2str),
        FUNC_MAP(EVP_PKEY_is_opaque),
        FUNC_MAP(X509_REQ_print_fp),
        FUNC_MAP(EVP_aead_aes_192_gcm),
        FUNC_MAP(X509_STORE_CTX_cleanup),
        FUNC_MAP(X509_get_ex_data),
        FUNC_MAP(DIST_POINT_NAME_new),
        FUNC_MAP(SSL_SESSION_get_time),
        FUNC_MAP(d2i_ASN1_GENERALSTRING),
        FUNC_MAP(d2i_ASN1_BMPSTRING),
        FUNC_MAP(CONF_modules_load_file),
        FUNC_MAP(BN_mod_lshift_quick),
        FUNC_MAP(d2i_RSAPrivateKey_bio),
        FUNC_MAP(bn_resize_words),
        FUNC_MAP(AES_ecb_encrypt),
        FUNC_MAP(EC_POINT_is_at_infinity),
        FUNC_MAP(SSL_CTX_get_quiet_shutdown),
        FUNC_MAP(SSL_get_verify_depth),
        FUNC_MAP(BIO_should_write),
        FUNC_MAP(X509_CRL_get0_signature),
        FUNC_MAP(SSL_CTX_set_chain_and_key),
        FUNC_MAP(SSL_set_ex_data),
        FUNC_MAP(ASN1_GENERALIZEDTIME_check),
        FUNC_MAP(OPENSSL_cleanup),
        FUNC_MAP(RSA_is_opaque),
        FUNC_MAP(CRYPTO_malloc),
        FUNC_MAP(X509_REQ_set_version),
        FUNC_MAP(EVP_SignInit),
        FUNC_MAP(X509_NAME_add_entry_by_NID),
        FUNC_MAP(CRYPTO_STATIC_MUTEX_lock_read),
        FUNC_MAP(EVP_EncodedLength),
        FUNC_MAP(EC_POINT_cmp),
        FUNC_MAP(PROXY_POLICY_it),
        FUNC_MAP(SSL_CIPHER_description),
        FUNC_MAP(SSL_CTX_set_ex_data),
        FUNC_MAP(EC_GROUP_get_asn1_flag),
        FUNC_MAP(SHA512),
        FUNC_MAP(X509_REVOKED_get_ext_by_NID),
        FUNC_MAP(X509_TRUST_get0),
        FUNC_MAP(X509_NAME_it),
        FUNC_MAP(i2d_ECParameters),
        FUNC_MAP(EVP_aead_aes_256_cbc_sha1_tls_implicit_iv),
        FUNC_MAP(ECDSA_SIG_free),
        FUNC_MAP(PEM_read_bio_PUBKEY),
        FUNC_MAP(DIST_POINT_new),
        FUNC_MAP(SSL_set1_sigalgs_list),
        FUNC_MAP(RSA_free),
        FUNC_MAP(X509_STORE_CTX_set0_crls),
        FUNC_MAP(BIO_ctrl_pending),
        FUNC_MAP(DSA_marshal_public_key),
        FUNC_MAP(v2i_GENERAL_NAMES),
        FUNC_MAP(SSL_CTX_use_certificate_file),
        FUNC_MAP(SSL_dup_CA_list),
        FUNC_MAP(EC_KEY_free),
        FUNC_MAP(EC_KEY_set_asn1_flag),
        FUNC_MAP(i2v_GENERAL_NAME),
        FUNC_MAP(X509_get_extension_flags),
        FUNC_MAP(_ZN4bssl22SSL_serialize_handbackEPK6ssl_stP6cbb_st),
        FUNC_MAP(SSL_set_enforce_rsa_key_usage),
        FUNC_MAP(CRYPTO_STATIC_MUTEX_unlock_write),
        FUNC_MAP(BLAKE2B256_Init),
        FUNC_MAP(CBB_did_write),
        FUNC_MAP(BN_equal_consttime),
        FUNC_MAP(BN_mod_sub),
        FUNC_MAP(EC_GROUP_new_curve_GFp),
        FUNC_MAP(X509_get_ext_by_OBJ),
        FUNC_MAP(SSL_CTX_set_permute_extensions),
        FUNC_MAP(BN_CTX_new),
        FUNC_MAP(i2d_PKCS8_PRIV_KEY_INFO_bio),
        FUNC_MAP(X509_signature_dump),
        FUNC_MAP(SSL_COMP_get_compression_methods),
        FUNC_MAP(SSL_get_current_cipher),
        FUNC_MAP(DISPLAYTEXT_new),
        FUNC_MAP(DH_parse_parameters),
        FUNC_MAP(RAND_egd),
        FUNC_MAP(X509_CRL_sort),
        FUNC_MAP(i2d_X509_CERT_AUX),
        FUNC_MAP(SSL_cutthrough_complete),
        FUNC_MAP(bn_div_consttime),
        FUNC_MAP(RAND_get_rand_method),
        FUNC_MAP(CRYPTO_get_dynlock_destroy_callback),
        FUNC_MAP(POLICYQUALINFO_free),
        FUNC_MAP(EC_KEY_set_group),
        FUNC_MAP(PEM_read),
        FUNC_MAP(SSL_CTX_sess_get_cache_size),
        FUNC_MAP(SSL_set_tlsext_host_name),
        FUNC_MAP(X509_LOOKUP_init),
        FUNC_MAP(DSA_verify),
        FUNC_MAP(ASN1_BIT_STRING_set_bit),
        FUNC_MAP(DSA_get_ex_new_index),
        FUNC_MAP(EC_KEY_set_ex_data),
        FUNC_MAP(SHA512_Transform),
        FUNC_MAP(OBJ_find_sigid_algs),
        FUNC_MAP(X509v3_get_ext_count),
        FUNC_MAP(GENERAL_NAME_new),
        FUNC_MAP(ASN1_put_eoc),
        FUNC_MAP(X509v3_add_ext),
        FUNC_MAP(SSL_CTX_set_cert_verify_callback),
        FUNC_MAP(X509_VERIFY_PARAM_get0_peername),
        FUNC_MAP(d2i_X509_fp),
        FUNC_MAP(OPENSSL_memdup),
        FUNC_MAP(ASN1_STRING_type),
        FUNC_MAP(EVP_PKEY_derive),
        FUNC_MAP(EC_GROUP_method_of),
        FUNC_MAP(EVP_HPKE_CTX_kdf),
        FUNC_MAP(PEM_X509_INFO_read),
        FUNC_MAP(X509V3_EXT_print),
        FUNC_MAP(SSL_CTX_set_psk_server_callback),
        FUNC_MAP(POLICYINFO_it),
        FUNC_MAP(POLICYQUALINFO_it),
        FUNC_MAP(X509_email_free),
        FUNC_MAP(SSL_CTX_set_timeout),
        FUNC_MAP(i2d_DSAPrivateKey_bio),
        FUNC_MAP(BIO_new_mem_buf),
        FUNC_MAP(DSA_set0_pqg),
        FUNC_MAP(BIO_number_written),
        FUNC_MAP(RAND_status),
        FUNC_MAP(X509at_get_attr_by_OBJ),
        FUNC_MAP(X509_PURPOSE_get_by_sname),
        FUNC_MAP(SSL_do_handshake),
        FUNC_MAP(SSL_connect),
        FUNC_MAP(CRYPTO_THREADID_current),
        FUNC_MAP(X509_get_notAfter),
        FUNC_MAP(X509_get_pathlen),
        FUNC_MAP(SSL_get_session),
        FUNC_MAP(SSL_get_client_random),
        FUNC_MAP(X509_STORE_add_lookup),
        FUNC_MAP(SSL_SESSION_to_bytes),
        FUNC_MAP(SSL_COMP_free_compression_methods),
        FUNC_MAP(EVP_DecodeBlock),
        FUNC_MAP(HRSS_encap),
        FUNC_MAP(CRYPTO_THREADID_set_numeric),
        FUNC_MAP(_ZN4bssl28ssl_is_valid_ech_public_nameENS_4SpanIKhEE),
        FUNC_MAP(SSL_CTX_set_ocsp_response),
        FUNC_MAP(OPENSSL_malloc),
        FUNC_MAP(BN_asc2bn),
        FUNC_MAP(X509_CRL_print_fp),
        FUNC_MAP(sk_dup),
        FUNC_MAP(X509_add1_trust_object),
        FUNC_MAP(SSL_set_quic_early_data_context),
        FUNC_MAP(SSL_SESSION_set_ex_data),
        FUNC_MAP(BN_nnmod_pow2),
        FUNC_MAP(SSL_early_data_reason_string),
        FUNC_MAP(EVP_DecryptInit_ex),
        FUNC_MAP(X25519_keypair),
        FUNC_MAP(CRYPTO_set_thread_local),
        FUNC_MAP(X509_CRL_get_signature_nid),
        FUNC_MAP(CBB_add_asn1_octet_string),
        FUNC_MAP(EVP_CipherFinal),
        FUNC_MAP(X509_CRL_free),
        FUNC_MAP(X509_CRL_diff),
        FUNC_MAP(FIPS_module_name),
        FUNC_MAP(d2i_X509_ALGOR),
        FUNC_MAP(i2d_X509_ALGOR),
        FUNC_MAP(i2d_RSAPublicKey_fp),
        FUNC_MAP(SSL_provide_quic_data),
        FUNC_MAP(SSL_set_fd),
        FUNC_MAP(SSL_CTX_get0_privatekey),
        FUNC_MAP(BN_GENCB_free),
        FUNC_MAP(EC_POINT_clear_free),
        FUNC_MAP(d2i_PKCS8PrivateKey_bio),
        FUNC_MAP(X509_CRL_get_nextUpdate),
        FUNC_MAP(d2i_NETSCAPE_SPKI),
        FUNC_MAP(i2d_ASN1_BMPSTRING),
        FUNC_MAP(EVP_has_aes_hardware),
        FUNC_MAP(X509_VERIFY_PARAM_set1),
        FUNC_MAP(ASN1_STRING_set),
        FUNC_MAP(PKCS12_parse),
        FUNC_MAP(X509_REQ_get_signature_nid),
        FUNC_MAP(CRYPTO_malloc_init),
        FUNC_MAP(HMAC_CTX_copy_ex),
        FUNC_MAP(PKCS7_get_CRLs),
        FUNC_MAP(SSL_enable_ocsp_stapling),
        FUNC_MAP(SSL_get_psk_identity_hint),
        FUNC_MAP(d2i_ASN1_BOOLEAN),
        FUNC_MAP(PKCS7_bundle_certificates),
        FUNC_MAP(d2i_PKCS7_bio),
        FUNC_MAP(pmbtoken_exp2_get_h_for_testing),
        FUNC_MAP(X509_it),
        FUNC_MAP(BIO_gets),
        FUNC_MAP(EC_GROUP_new_by_curve_name),
        FUNC_MAP(PROXY_CERT_INFO_EXTENSION_new),
        FUNC_MAP(i2d_SSL_SESSION),
        FUNC_MAP(ENGINE_get_RSA_method),
        FUNC_MAP(BN_usub),
        FUNC_MAP(TRUST_TOKEN_ISSUER_new),
        FUNC_MAP(X509_VERIFY_PARAM_get_count),
        FUNC_MAP(d2i_X509_REQ_fp),
        FUNC_MAP(SSL_set_shed_handshake_config),
        FUNC_MAP(ASN1_INTEGER_dup),
        FUNC_MAP(BIO_get_retry_flags),
        FUNC_MAP(BIO_set_conn_int_port),
        FUNC_MAP(SSL_set_strict_cipher_list),
        FUNC_MAP(BIO_pending),
        FUNC_MAP(FIPS_mode_set),
        FUNC_MAP(PEM_dek_info),
        FUNC_MAP(X509_VERIFY_PARAM_add0_policy),
        FUNC_MAP(SSL_set_retain_only_sha256_of_client_certs),
        FUNC_MAP(PKCS12_PBE_add),
        FUNC_MAP(DTLSv1_2_method),
        FUNC_MAP(SSL_CTX_get_cert_store),
        FUNC_MAP(i2d_ASN1_TYPE),
        FUNC_MAP(EVP_DecodeUpdate),
        FUNC_MAP(i2d_X509_SIG),
        FUNC_MAP(SSL_CTX_get_keylog_callback),
        FUNC_MAP(SSL_CTX_sess_hits),
        FUNC_MAP(ASN1_item_unpack),
        FUNC_MAP(d2i_ASN1_NULL),
        FUNC_MAP(EVP_ENCODE_CTX_free),
        FUNC_MAP(EVP_CIPHER_CTX_iv_length),
        FUNC_MAP(EVP_marshal_digest_algorithm),
        FUNC_MAP(EVP_aead_aes_128_ccm_bluetooth),
        FUNC_MAP(i2d_X509),
        FUNC_MAP(RAND_add),
        FUNC_MAP(X509_STORE_CTX_get0_untrusted),
        FUNC_MAP(X509_CERT_AUX_free),
        FUNC_MAP(SSL_get0_peer_certificates),
        FUNC_MAP(SSL_set_mode),
        FUNC_MAP(RSA_marshal_private_key),
        FUNC_MAP(PKCS8_marshal_encrypted_private_key),
        FUNC_MAP(X509_ATTRIBUTE_free),
        FUNC_MAP(X509_OBJECT_get0_X509),
        FUNC_MAP(SSL_CIPHER_get_kx_nid),
        FUNC_MAP(SSL_CTX_set_max_send_fragment),
        FUNC_MAP(EVP_des_ede_cbc),
        FUNC_MAP(EVP_parse_public_key),
        FUNC_MAP(SSL_SESSION_set_timeout),
        FUNC_MAP(DSA_generate_key),
        FUNC_MAP(SSL_get_signature_algorithm_key_type),
        FUNC_MAP(ASN1_STRING_set0),
        FUNC_MAP(X509V3_EXT_d2i),
        FUNC_MAP(SSL_CTX_set_tlsext_ticket_key_cb),
        FUNC_MAP(ASN1_GENERALIZEDTIME_free),
        FUNC_MAP(d2i_RSAPublicKey),
        FUNC_MAP(X509_VERIFY_PARAM_set_flags),
        FUNC_MAP(X509_PUBKEY_get0_public_key),
        FUNC_MAP(SSL_get_info_callback),
        FUNC_MAP(ASN1_T61STRING_new),
        FUNC_MAP(MD4_Init),
        FUNC_MAP(_ZN4bssl15SSL_SESSION_dupEP14ssl_session_sti),
        FUNC_MAP(BIO_vfree),
        FUNC_MAP(EVP_md5_sha1),
        FUNC_MAP(X509_CRL_get_ext_d2i),
        FUNC_MAP(i2d_ECPrivateKey_bio),
        FUNC_MAP(DTLSv1_client_method),
        FUNC_MAP(SSL_CIPHER_get_name),
        FUNC_MAP(SSL_renegotiate_pending),
        FUNC_MAP(i2d_ASN1_UTF8STRING),
        FUNC_MAP(EC_POINT_free),
        FUNC_MAP(X509_NAME_delete_entry),
        FUNC_MAP(BIO_push),
        FUNC_MAP(d2i_RSA_PUBKEY),
        FUNC_MAP(i2d_RSA_PUBKEY),
        FUNC_MAP(CTR_DRBG_free),
        FUNC_MAP(_ZN4bssl19SealRecordPrefixLenEPK6ssl_stm),
        FUNC_MAP(ASN1_UNIVERSALSTRING_it),
        FUNC_MAP(ASN1_NULL_it),
        FUNC_MAP(EVP_aes_128_ofb),
        FUNC_MAP(BN_rand_range_ex),
        FUNC_MAP(BN_sqrt),
        FUNC_MAP(EVP_HPKE_CTX_kem),
        FUNC_MAP(TRUST_TOKEN_CLIENT_add_key),
        FUNC_MAP(i2d_X509_VAL),
        FUNC_MAP(EVP_rc2_cbc),
        FUNC_MAP(NCONF_load),
        FUNC_MAP(EVP_CIPHER_block_size),
        FUNC_MAP(RSA_padding_add_PKCS1_PSS_mgf1),
        FUNC_MAP(X509_get0_authority_key_id),
        FUNC_MAP(SSL_CTX_set_record_protocol_version),
        FUNC_MAP(BN_div),
        FUNC_MAP(PEM_read_bio_ECPrivateKey),
        FUNC_MAP(PKCS8_PRIV_KEY_INFO_it),
        FUNC_MAP(X509V3_EXT_nconf_nid),
        FUNC_MAP(SSL_add_file_cert_subjects_to_stack),
        FUNC_MAP(SSL_key_update),
        FUNC_MAP(EVP_HPKE_KEM_enc_len),
        FUNC_MAP(X509_SIG_it),
        FUNC_MAP(CRYPTO_is_confidential_build),
        FUNC_MAP(RSA_set_ex_data),
        FUNC_MAP(X509_OBJECT_retrieve_by_subject),
        FUNC_MAP(SSL_CTX_set_min_proto_version),
        FUNC_MAP(SSL_get0_certificate_types),
        FUNC_MAP(ASN1_STRING_TABLE_add),
        FUNC_MAP(BIO_new_bio_pair),
        FUNC_MAP(SSL_CTX_use_PrivateKey_file),
        FUNC_MAP(SSL_CTX_set_session_cache_mode),
        FUNC_MAP(CBB_flush),
        FUNC_MAP(OpenSSL_version_num),
        FUNC_MAP(PEM_read_bio_X509_CRL),
        FUNC_MAP(PEM_read_bio_PKCS7),
        FUNC_MAP(SSL_delegated_credential_used),
        FUNC_MAP(PEM_read_bio_PKCS8),
        FUNC_MAP(X509V3_get_d2i),
        FUNC_MAP(X509_STORE_CTX_set_error),
        FUNC_MAP(SSL_get_srtp_profiles),
        FUNC_MAP(SSL_CTX_set_client_cert_cb),
        FUNC_MAP(TLSv1_method),
        FUNC_MAP(BIO_next),
        FUNC_MAP(PEM_read_X509_AUX),
        FUNC_MAP(CRYPTO_BUFFER_POOL_new),
        FUNC_MAP(X509_check_akid),
        FUNC_MAP(i2d_BASIC_CONSTRAINTS),
        FUNC_MAP(DTLSv1_method),
        FUNC_MAP(SSL_CTX_set_max_proto_version),
        FUNC_MAP(ASN1_PRINTABLE_new),
        FUNC_MAP(CRYPTO_once),
        FUNC_MAP(d2i_DSAPublicKey),
        FUNC_MAP(BN_mod_add),
        FUNC_MAP(DH_get0_pub_key),
        FUNC_MAP(EC_METHOD_get_field_type),
        FUNC_MAP(BIO_meth_new),
        FUNC_MAP(EVP_CIPHER_CTX_encrypting),
        FUNC_MAP(PEM_read_X509_REQ),
        FUNC_MAP(ASN1_mbstring_ncopy),
        FUNC_MAP(OPENSSL_strnlen),
        FUNC_MAP(EVP_CIPHER_CTX_free),
        FUNC_MAP(TRUST_TOKEN_CLIENT_begin_issuance),
        FUNC_MAP(X509_keyid_get0),
        FUNC_MAP(SSL_use_certificate_ASN1),
        FUNC_MAP(SSL_CTX_get_mode),
        FUNC_MAP(SSL_CTX_sess_timeouts),
        FUNC_MAP(EC_POINT_point2cbb),
        FUNC_MAP(FIPS_read_counter),
        FUNC_MAP(PKCS7_type_is_signedAndEnveloped),
        FUNC_MAP(SSL_CTX_add_session),
        FUNC_MAP(i2d_EC_PUBKEY_bio),
        FUNC_MAP(ASN1_BIT_STRING_it),
        FUNC_MAP(EVP_CIPHER_key_length),
        FUNC_MAP(NCONF_get_section),
        FUNC_MAP(EVP_SignInit_ex),
        FUNC_MAP(X509_EXTENSIONS_it),
        FUNC_MAP(SSL_get_cipher_list),
        FUNC_MAP(SSL_enable_signed_cert_timestamps),
        FUNC_MAP(SSL_get_psk_identity),
        FUNC_MAP(BIO_printf),
        FUNC_MAP(ASN1_BMPSTRING_new),
        FUNC_MAP(EVP_aead_des_ede3_cbc_sha1_tls),
        FUNC_MAP(AES_unwrap_key),
        FUNC_MAP(X509_STORE_set_default_paths),
        FUNC_MAP(SSL_use_PrivateKey),
        FUNC_MAP(ASN1_TYPE_new),
        FUNC_MAP(RSA_verify_raw),
        FUNC_MAP(d2i_X509),
        FUNC_MAP(X509at_get_attr),
        FUNC_MAP(SSL_get_verify_callback),
        FUNC_MAP(sk_pop_free_ex),
        FUNC_MAP(X509_EXTENSION_get_data),
        FUNC_MAP(d2i_CERTIFICATEPOLICIES),
        FUNC_MAP(SSL_CIPHER_get_auth_nid),
        FUNC_MAP(ENGINE_load_builtin_engines),
        FUNC_MAP(ERR_peek_last_error_line),
        FUNC_MAP(EVP_MD_CTX_new),
        FUNC_MAP(EC_GROUP_dup),
        FUNC_MAP(CRYPTO_tls1_prf),
        FUNC_MAP(PEM_ASN1_read),
        FUNC_MAP(BIO_set_ssl),
        FUNC_MAP(SSL_CTX_set_default_passwd_cb),
        FUNC_MAP(X509_NAME_ENTRY_it),
        FUNC_MAP(SSL_certs_clear),
        FUNC_MAP(SSL_set_quic_use_legacy_codepoint),
        FUNC_MAP(PEM_write_X509_AUX),
        FUNC_MAP(NETSCAPE_SPKI_free),
        FUNC_MAP(ERR_peek_error),
        FUNC_MAP(BASIC_CONSTRAINTS_free),
        FUNC_MAP(PEM_write_X509_REQ),
        FUNC_MAP(X509V3_EXT_cleanup),
        FUNC_MAP(AES_wrap_key_padded),
        FUNC_MAP(i2d_RSAPublicKey_bio),
        FUNC_MAP(CRYPTO_get_fork_generation),
        FUNC_MAP(AES_ofb128_encrypt),
        FUNC_MAP(PKCS7_bundle_CRLs),
        FUNC_MAP(X509_STORE_set_check_revocation),
        FUNC_MAP(X509_STORE_get_cleanup),
        FUNC_MAP(SSL_CTX_set1_sigalgs),
        FUNC_MAP(PEM_write_bio_PrivateKey),
        FUNC_MAP(SSL_COMP_get_id),
        FUNC_MAP(SSL_CTX_add0_chain_cert),
        FUNC_MAP(SSL_can_release_private_key),
        FUNC_MAP(c2i_ASN1_OBJECT),
        FUNC_MAP(X509_STORE_get0_param),
        FUNC_MAP(SSL_CTX_set_client_CA_list),
        FUNC_MAP(CBS_parse_utc_time),
        FUNC_MAP(BIO_find_type),
        FUNC_MAP(ED25519_keypair),
        FUNC_MAP(EVP_VerifyInit_ex),
        FUNC_MAP(OBJ_sn2nid),
        FUNC_MAP(PEM_write_bio_DSAPrivateKey),
        FUNC_MAP(X509_LOOKUP_new),
        FUNC_MAP(SSL_get0_alpn_selected),
        FUNC_MAP(ASN1_TIME_set_string),
        FUNC_MAP(EVP_aead_chacha20_poly1305),
        FUNC_MAP(i2d_PublicKey),
        FUNC_MAP(SSL_set_read_ahead),
        FUNC_MAP(BIO_get_new_index),
        FUNC_MAP(SSL_CTX_get0_param),
        FUNC_MAP(BUF_MEM_grow_clean),
        FUNC_MAP(EVP_DigestFinalXOF),
        FUNC_MAP(EVP_MD_CTX_md),
        FUNC_MAP(PEM_write_bio_PKCS8PrivateKey),
        FUNC_MAP(d2i_X509_SIG),
        FUNC_MAP(X509_print_ex),
        FUNC_MAP(X509_VERIFY_PARAM_set1_ip_asc),
        FUNC_MAP(SSL_COMP_get0_name),
        FUNC_MAP(SSL_set1_curves),
        FUNC_MAP(EVP_HPKE_KEY_generate),
        FUNC_MAP(DIST_POINT_it),
        FUNC_MAP(DSA_SIG_get0),
        FUNC_MAP(EVP_PKEY_CTX_new_id),
        FUNC_MAP(EC_KEY_new_by_curve_name),
        FUNC_MAP(PKCS12_create),
        FUNC_MAP(SSL_set_state),
        FUNC_MAP(TLSv1_1_server_method),
        FUNC_MAP(ASN1_STRING_length),
        FUNC_MAP(ECDSA_SIG_marshal),
        FUNC_MAP(HMAC_CTX_free),
        FUNC_MAP(PEM_write_bio_X509_CRL),
        FUNC_MAP(i2d_RSAPrivateKey),
        FUNC_MAP(RSA_generate_key_fips),
        FUNC_MAP(d2i_PKCS8_PRIV_KEY_INFO_bio),
        FUNC_MAP(SSL_CTX_set1_curves),
        FUNC_MAP(SSL_set_tmp_dh_callback),
        FUNC_MAP(EVP_MD_CTX_size),
        FUNC_MAP(RSA_get0_key),
        FUNC_MAP(CRYPTO_BUFFER_init_CBS),
        FUNC_MAP(SSL_set_tlsext_status_ocsp_resp),
        FUNC_MAP(SSLv23_client_method),
        FUNC_MAP(sk_sort),
        FUNC_MAP(i2d_DSA_PUBKEY_fp),
        FUNC_MAP(SSL_alert_desc_string_long),
        FUNC_MAP(ASN1_STRING_cmp),
        FUNC_MAP(d2i_ASN1_T61STRING),
        FUNC_MAP(EVP_PKEY_CTX_dup),
        FUNC_MAP(PEM_read_DHparams),
        FUNC_MAP(X509_REQ_get_subject_name),
        FUNC_MAP(X509_CRL_dup),
        FUNC_MAP(SSL_set1_host),
        FUNC_MAP(X509_VERIFY_PARAM_set_time),
        FUNC_MAP(ASN1_BIT_STRING_free),
        FUNC_MAP(BIO_puts),
        FUNC_MAP(RSA_get0_crt_params),
        FUNC_MAP(CRYPTO_set_id_callback),
        FUNC_MAP(X509_STORE_get_by_subject),
        FUNC_MAP(BN_uadd),
        FUNC_MAP(SSLv23_method),
        FUNC_MAP(EVP_PKEY_paramgen_init),
        FUNC_MAP(TRUST_TOKEN_ISSUER_set_srr_key),
        FUNC_MAP(X509_REQ_add_extensions_nid),
        FUNC_MAP(i2d_DIST_POINT_NAME),
        FUNC_MAP(BIO_do_connect),
        FUNC_MAP(RC4),
        FUNC_MAP(ECDH_compute_key),
        FUNC_MAP(ENGINE_get_ECDSA_method),
        FUNC_MAP(DH_compute_key),
        FUNC_MAP(PEM_write_bio_RSAPrivateKey),
        FUNC_MAP(d2i_X509_VAL),
        FUNC_MAP(SSL_CTX_set1_sigalgs_list),
        FUNC_MAP(BIO_seek),
        FUNC_MAP(X509V3_EXT_val_prn),
        FUNC_MAP(SSL_max_seal_overhead),
        FUNC_MAP(DSA_SIG_new),
        FUNC_MAP(EVP_PKEY_derive_init),
        FUNC_MAP(i2d_ASN1_BOOLEAN),
        FUNC_MAP(_ZN4bssl17SSL_SESSION_parseEP6cbs_stPKNS_15SSL_X509_METHODEP21crypto_buffer_pool_st),
        FUNC_MAP(ASN1_TIME_check),
        FUNC_MAP(sk_new_null),
        FUNC_MAP(EVP_AEAD_nonce_length),
        FUNC_MAP(TRUST_TOKEN_free),
        FUNC_MAP(SSL_CIPHER_get_min_version),
        FUNC_MAP(OBJ_obj2nid),
        FUNC_MAP(EC_POINT_set_to_infinity),
        FUNC_MAP(X509_getm_notBefore),
        FUNC_MAP(X509_EXTENSION_it),
        FUNC_MAP(SSL_get_server_tmp_key),
        FUNC_MAP(sk_set),
        FUNC_MAP(EVP_aead_aes_256_gcm_randnonce),
        FUNC_MAP(SSL_set1_param),
        FUNC_MAP(BN_mod_exp_mont),
        FUNC_MAP(EVP_DigestVerifyFinal),
        FUNC_MAP(ASN1_digest),
        FUNC_MAP(X509_NAME_add_entry_by_OBJ),
        FUNC_MAP(X509_PUBKEY_free),
        FUNC_MAP(SSL_CTX_set_num_tickets),
        FUNC_MAP(i2d_ASN1_OBJECT),
        FUNC_MAP(SSL_CTX_use_RSAPrivateKey_file),
        FUNC_MAP(SSL_get0_ocsp_response),
        FUNC_MAP(SSL_CTX_set_quic_method),
        FUNC_MAP(_ZN4bssl10OpenRecordEP6ssl_stPNS_4SpanIhEEPmPhS3_),
        FUNC_MAP(PEM_write_DHparams),
        FUNC_MAP(BIO_method_type),
        FUNC_MAP(EVP_PKEY_print_public),
        FUNC_MAP(BN_is_bit_set),
        FUNC_MAP(EVP_HPKE_CTX_open),
        FUNC_MAP(X509_ATTRIBUTE_count),
        FUNC_MAP(X509_REVOKED_get_ext_by_OBJ),
        FUNC_MAP(X509_get0_extensions),
        FUNC_MAP(i2c_ASN1_INTEGER),
        FUNC_MAP(BIO_int_ctrl),
        FUNC_MAP(EVP_PKEY_CTX_get_signature_md),
        FUNC_MAP(BN_gcd),
        FUNC_MAP(EC_curve_nist2nid),
        FUNC_MAP(X509_CRL_add1_ext_i2d),
        FUNC_MAP(X509_STORE_CTX_set_purpose),
        FUNC_MAP(X509V3_set_ctx),
        FUNC_MAP(d2i_ASN1_ENUMERATED),
        FUNC_MAP(OPENSSL_strdup),
        FUNC_MAP(EVP_CIPHER_CTX_cleanup),
        FUNC_MAP(X509_issuer_name_hash),
        FUNC_MAP(X509_find_by_subject),
        FUNC_MAP(CBS_peek_asn1_tag),
        FUNC_MAP(X509_PKEY_new),
        FUNC_MAP(TRUST_TOKEN_derive_key_from_secret),
        FUNC_MAP(X509_print_fp),
        FUNC_MAP(DSA_marshal_parameters),
        FUNC_MAP(i2d_PKCS8PrivateKey_nid_fp),
        FUNC_MAP(EVP_PKEY_get1_EC_KEY),
        FUNC_MAP(i2d_X509_CRL_INFO),
        FUNC_MAP(d2i_GENERAL_NAME),
        FUNC_MAP(sk_free),
        FUNC_MAP(BIO_meth_set_destroy),
        FUNC_MAP(BN_parse_asn1_unsigned),
        FUNC_MAP(BN_mod_mul_montgomery),
        FUNC_MAP(POLICY_CONSTRAINTS_new),
        FUNC_MAP(SSL_set_private_key_method),
        FUNC_MAP(cbs_get_utf8),
        FUNC_MAP(ASN1_GENERALSTRING_new),
        FUNC_MAP(SPAKE2_CTX_free),
        FUNC_MAP(EVP_Cipher),
        FUNC_MAP(HMAC_CTX_reset),
        FUNC_MAP(X509_STORE_CTX_set_ex_data),
        FUNC_MAP(EVP_sha384),
        FUNC_MAP(EVP_PKEY_get1_tls_encodedpoint),
        FUNC_MAP(RSA_PSS_PARAMS_new),
        FUNC_MAP(NOTICEREF_new),
        FUNC_MAP(SSL_generate_key_block),
        FUNC_MAP(ASN1_INTEGER_set),
        FUNC_MAP(EVP_PKEY_CTX_get_rsa_oaep_md),
        FUNC_MAP(d2i_X509_EXTENSION),
        FUNC_MAP(SSL_send_fatal_alert),
        FUNC_MAP(SSL_cache_hit),
        FUNC_MAP(ERR_print_errors_cb),
        FUNC_MAP(RAND_enable_fork_unsafe_buffering),
        FUNC_MAP(X509_STORE_get_verify),
        FUNC_MAP(BIO_new),
        FUNC_MAP(SSL_read),
        FUNC_MAP(SSL_ECH_KEYS_free),
        FUNC_MAP(SSL_get_client_CA_list),
        FUNC_MAP(ASN1_STRING_TABLE_cleanup),
        FUNC_MAP(BN_num_bits),
        FUNC_MAP(CRYPTO_MUTEX_lock_write),
        FUNC_MAP(HMAC),
        FUNC_MAP(SSL_set_tmp_rsa_callback),
        FUNC_MAP(ASN1_UTCTIME_set_string),
        FUNC_MAP(OPENSSL_strndup),
        FUNC_MAP(EVP_hpke_x25519_hkdf_sha256),
        FUNC_MAP(SSL_set_tmp_rsa),
        FUNC_MAP(BN_bn2hex),
        FUNC_MAP(ERR_get_next_error_library),
        FUNC_MAP(EVP_DigestFinal),
        FUNC_MAP(X509_CRL_get_ext),
        FUNC_MAP(SSL_state_string),
        FUNC_MAP(CRYPTO_memcmp),
        FUNC_MAP(PKCS7_type_is_digest),
        FUNC_MAP(X509_REQ_add1_attr_by_NID),
        FUNC_MAP(X509_STORE_CTX_set_default),
        FUNC_MAP(X509_REQ_sign_ctx),
        FUNC_MAP(X509_pubkey_digest),
        FUNC_MAP(ASN1_OCTET_STRING_new),
        FUNC_MAP(X509_STORE_get1_crls),
        FUNC_MAP(SSL_CTX_set_default_passwd_cb_userdata),
        FUNC_MAP(EVP_EncryptInit),
        FUNC_MAP(CRYPTO_free),
        FUNC_MAP(X509_free),
        FUNC_MAP(GENERAL_NAME_dup),
        FUNC_MAP(PROXY_POLICY_free),
        FUNC_MAP(SSL_CTX_set_session_id_context),
        FUNC_MAP(SSL_CTX_set_tmp_rsa),
        FUNC_MAP(SSL_CTX_get_client_CA_list),
        FUNC_MAP(BIO_indent),
        FUNC_MAP(DSA_new),
        FUNC_MAP(CRYPTO_set_locking_callback),
        FUNC_MAP(X509_TRUST_get_trust),
        FUNC_MAP(d2i_RSAPrivateKey_fp),
        FUNC_MAP(SSL_get_selected_srtp_profile),
        FUNC_MAP(ASN1_STRING_get_default_mask),
        FUNC_MAP(EVP_MD_flags),
        FUNC_MAP(CRYPTO_get_dynlock_lock_callback),
        FUNC_MAP(X509_load_cert_file),
        FUNC_MAP(SSL_CTX_set_early_data_enabled),
        FUNC_MAP(ASN1_ANY_it),
        FUNC_MAP(X509_NAME_get_index_by_NID),
        FUNC_MAP(SSL_is_server),
        FUNC_MAP(ASN1_INTEGER_set_uint64),
        FUNC_MAP(X509_get0_serialNumber),
        FUNC_MAP(i2d_X509_CRL_fp),
        FUNC_MAP(SSL_set_quic_method),
        FUNC_MAP(DSA_get_ex_data),
        FUNC_MAP(X509_REQ_set1_signature_value),
        FUNC_MAP(i2d_POLICYQUALINFO),
        FUNC_MAP(SSL_CTX_set_ticket_aead_method),
        FUNC_MAP(SSL_SESSION_get_id),
        FUNC_MAP(SSL_SESSION_has_peer_sha256),
        FUNC_MAP(i2d_DSAPublicKey),
        FUNC_MAP(PEM_read_bio_X509),
        FUNC_MAP(d2i_PUBKEY_fp),
        FUNC_MAP(OBJ_dup),
        FUNC_MAP(BN_secure_new),
        FUNC_MAP(ECDSA_sign_with_nonce_and_leak_private_key_for_testing),
        FUNC_MAP(X509_REVOKED_get_ext_d2i),
        FUNC_MAP(d2i_X509_EXTENSIONS),
        FUNC_MAP(X509V3_conf_free),
        FUNC_MAP(EVP_AEAD_CTX_seal),
        FUNC_MAP(RSA_PSS_PARAMS_free),
        FUNC_MAP(SSLeay_version),
        FUNC_MAP(BN_lshift),
        FUNC_MAP(BN_exp),
        FUNC_MAP(i2d_PKCS7_bio),
        FUNC_MAP(PKCS8_parse_encrypted_private_key),
        FUNC_MAP(X509_STORE_get_lookup_certs),
        FUNC_MAP(OTHERNAME_cmp),
        FUNC_MAP(X509V3_add_value_uchar),
        FUNC_MAP(SSL_get_wfd),
        FUNC_MAP(X509_STORE_load_locations),
        FUNC_MAP(X509_time_adj),
        FUNC_MAP(SSL_get_shutdown),
        FUNC_MAP(cbs_get_utf32_be),
        FUNC_MAP(BN_mpi2bn),
        FUNC_MAP(EVP_aead_aes_128_ccm_bluetooth_8),
        FUNC_MAP(OPENSSL_tm_to_posix),
        FUNC_MAP(d2i_ASN1_OCTET_STRING),
        FUNC_MAP(X509_NAME_ENTRY_set_object),
        FUNC_MAP(MD5_Transform),
        FUNC_MAP(SSL_CTX_remove_session),
        FUNC_MAP(c2i_ASN1_INTEGER),
        FUNC_MAP(BN_nnmod),
        FUNC_MAP(EVP_PKEY2PKCS8),
        FUNC_MAP(X509_check_purpose),
        FUNC_MAP(i2d_ISSUING_DIST_POINT),
        FUNC_MAP(EXTENDED_KEY_USAGE_new),
        FUNC_MAP(CRYPTO_chacha_20),
        FUNC_MAP(X509_SIG_get0),
        FUNC_MAP(SSL_CTX_use_certificate_chain_file),
        FUNC_MAP(OpenSSL_add_all_ciphers),
        FUNC_MAP(SSL_CTX_sess_set_new_cb),
        FUNC_MAP(OPENSSL_load_builtin_modules),
        FUNC_MAP(TRUST_TOKEN_PRETOKEN_free),
        FUNC_MAP(i2d_ASN1_NULL),
        FUNC_MAP(BN_free),
        FUNC_MAP(X509at_add1_attr_by_NID),
        FUNC_MAP(SSL_add_application_settings),
        FUNC_MAP(EVP_PKEY_CTX_get_rsa_padding),
        FUNC_MAP(BN_count_low_zero_bits),
        FUNC_MAP(X509_CRL_get_lastUpdate),
        FUNC_MAP(X509_ATTRIBUTE_create),
        FUNC_MAP(s2i_ASN1_OCTET_STRING),
        FUNC_MAP(SSL_CTX_get0_chain_certs),
        FUNC_MAP(BIO_ctrl_get_read_request),
        FUNC_MAP(BN_generate_prime_ex),
        FUNC_MAP(ERR_peek_error_line_data),
        FUNC_MAP(AES_cbc_encrypt),
        FUNC_MAP(BN_is_word),
        FUNC_MAP(i2d_X509_CRL),
        FUNC_MAP(PEM_write_bio_X509),
        FUNC_MAP(X509_STORE_set_cert_crl),
        FUNC_MAP(i2d_X509_tbs),
        FUNC_MAP(DTLS_server_method),
        FUNC_MAP(SSL_set1_delegated_credential),
        FUNC_MAP(SSL_CTX_set0_verify_cert_store),
        FUNC_MAP(X509_STORE_get_get_issuer),
        FUNC_MAP(ASN1_item_free),
        FUNC_MAP(BN_hex2bn),
        FUNC_MAP(ASN1_item_pack),
        FUNC_MAP(BASIC_CONSTRAINTS_new),
        FUNC_MAP(SSL_accept),
        FUNC_MAP(SSL_SESSION_early_data_capable),
        FUNC_MAP(SSL_get0_param),
        FUNC_MAP(CBS_get_until_first),
        FUNC_MAP(SSL_ECH_KEYS_up_ref),
        FUNC_MAP(SSL_CTX_get0_chain),
        FUNC_MAP(BIO_reset),
        FUNC_MAP(X509_check_ca),
        FUNC_MAP(DTLSv1_2_server_method),
        FUNC_MAP(SSL_ECH_KEYS_has_duplicate_config_id),
        FUNC_MAP(CRYPTO_MUTEX_unlock_write),
        FUNC_MAP(NETSCAPE_SPKI_it),
        FUNC_MAP(SSL_get_secure_renegotiation_support),
        FUNC_MAP(BN_mod_lshift1_quick),
        FUNC_MAP(EDIPARTYNAME_free),
        FUNC_MAP(SSL_SESSION_get_ex_new_index),
        FUNC_MAP(ASN1_ENUMERATED_it),
        FUNC_MAP(CBB_add_asn1),
        FUNC_MAP(ECDSA_SIG_to_bytes),
        FUNC_MAP(EC_GROUP_get_cofactor),
        FUNC_MAP(PKCS8_decrypt),
        FUNC_MAP(SSL_get_verify_result),
        FUNC_MAP(SSL_get_SSL_CTX),
        FUNC_MAP(sk_pop),
        FUNC_MAP(d2i_ASN1_BIT_STRING),
        FUNC_MAP(OPENSSL_no_config),
        FUNC_MAP(DSA_check_signature),
        FUNC_MAP(AES_decrypt),
        FUNC_MAP(EVP_hpke_hkdf_sha256),
        FUNC_MAP(X509_NAME_ENTRY_set_data),
        FUNC_MAP(i2v_GENERAL_NAMES),
        FUNC_MAP(SSL_add1_chain_cert),
        FUNC_MAP(ASN1_ENUMERATED_set_uint64),
        FUNC_MAP(RSAPrivateKey_dup),
        FUNC_MAP(i2d_X509_EXTENSION),
        FUNC_MAP(SSL_get_privatekey),
        FUNC_MAP(CBB_add_bytes),
        FUNC_MAP(EVP_MD_block_size),
        FUNC_MAP(d2i_PrivateKey),
        FUNC_MAP(BN_get_word),
        FUNC_MAP(EVP_CIPHER_flags),
        FUNC_MAP(EVP_DigestVerifyUpdate),
        FUNC_MAP(i2d_PrivateKey),
        FUNC_MAP(X509_STORE_get_get_crl),
        FUNC_MAP(X509_CRL_sign_ctx),
        FUNC_MAP(EC_KEY_check_key),
        FUNC_MAP(EVP_PKEY_get0_DSA),
        FUNC_MAP(POLICYINFO_free),
        FUNC_MAP(SSL_SESSION_get0_signed_cert_timestamp_list),
        FUNC_MAP(X509_check_host),
        FUNC_MAP(TLS_with_buffers_method),
        FUNC_MAP(MD5_Final),
        FUNC_MAP(EVP_HPKE_CTX_free),
        FUNC_MAP(CRYPTO_BUFFER_new_from_CBS),
        FUNC_MAP(SSL_clear),
        FUNC_MAP(SSL_set_cert_cb),
        FUNC_MAP(SSL_CTX_set1_tls_channel_id),
        FUNC_MAP(SSL_set_purpose),
        FUNC_MAP(d2i_PublicKey),
        FUNC_MAP(EVP_add_cipher_alias),
        FUNC_MAP(PEM_write_EC_PUBKEY),
        FUNC_MAP(SSL_library_init),
        FUNC_MAP(CRYPTO_gcm128_aad),
        FUNC_MAP(X509_STORE_get_verify_cb),
        FUNC_MAP(i2d_EXTENDED_KEY_USAGE),
        FUNC_MAP(_ZN4bssl21ssl_client_hello_initEPK6ssl_stP22ssl_early_callback_ctxNS_4SpanIKhEE),
        FUNC_MAP(SSL_get_early_data_reason),
        FUNC_MAP(X509_NAME_ENTRY_free),
        FUNC_MAP(GENERAL_SUBTREE_it),
        FUNC_MAP(SSL_CTX_sess_set_remove_cb),
        FUNC_MAP(cbb_add_utf32_be),
        FUNC_MAP(ASN1_GENERALIZEDTIME_new),
        FUNC_MAP(EVP_sha1),
        FUNC_MAP(TRUST_TOKEN_CLIENT_free),
        FUNC_MAP(USERNOTICE_free),
        FUNC_MAP(SSL_CTX_set_cert_cb),
        FUNC_MAP(SSL_CTX_set_purpose),
        FUNC_MAP(SSL_set1_chain),
        FUNC_MAP(BN_sqr),
        FUNC_MAP(TRUST_TOKEN_experiment_v1),
        FUNC_MAP(X509_ATTRIBUTE_get0_data),
        FUNC_MAP(ASN1_UTCTIME_check),
        FUNC_MAP(CBS_get_u8_length_prefixed),
        FUNC_MAP(CRYPTO_MUTEX_lock_read),
        FUNC_MAP(PEM_read_RSA_PUBKEY),
        FUNC_MAP(POLICY_MAPPING_new),
        FUNC_MAP(CBS_get_u16_length_prefixed),
        FUNC_MAP(EVP_aead_aes_128_gcm_siv),
        FUNC_MAP(AES_encrypt),
        FUNC_MAP(ECDSA_verify),
        FUNC_MAP(X509_VERIFY_PARAM_add0_table),
        FUNC_MAP(SSL_get0_ech_retry_configs),
        FUNC_MAP(SSL_get_tls_unique),
        FUNC_MAP(CMAC_Final),
        FUNC_MAP(i2d_X509_NAME_ENTRY),
        FUNC_MAP(SSL_get0_session_id_context),
        FUNC_MAP(DIRECTORYSTRING_free),
        FUNC_MAP(EVP_ENCODE_CTX_new),
        FUNC_MAP(X509_get_default_cert_dir_env),
        FUNC_MAP(d2i_SSL_SESSION_bio),
        FUNC_MAP(d2i_ASN1_GENERALIZEDTIME),
        FUNC_MAP(CBB_add_u64le),
        FUNC_MAP(d2i_PKCS8PrivateKey_fp),
        FUNC_MAP(RAND_file_name),
        FUNC_MAP(X509_LOOKUP_ctrl),
        FUNC_MAP(AUTHORITY_INFO_ACCESS_free),
        FUNC_MAP(SSL_CTX_get_session_cache_mode),
        FUNC_MAP(BN_is_prime_fasttest_ex),
        FUNC_MAP(EVP_PKEY_assign_EC_KEY),
        FUNC_MAP(ASN1_INTEGER_cmp),
        FUNC_MAP(CBB_add_asn1_uint64),
        FUNC_MAP(SSL_set_verify_depth),
        FUNC_MAP(asn1_utctime_to_tm),
        FUNC_MAP(DES_set_key_unchecked),
        FUNC_MAP(HKDF_expand),
        FUNC_MAP(EVP_HPKE_CTX_aead),
        FUNC_MAP(X25519_public_from_private),
        FUNC_MAP(X509_TRUST_get_count),
        FUNC_MAP(X509_REQ_INFO_new),
        FUNC_MAP(i2d_GENERAL_NAME),
        FUNC_MAP(BN_set_negative),
        FUNC_MAP(CBB_add_u16le),
        FUNC_MAP(DSA_set0_key),
        FUNC_MAP(EVP_PKEY_print_params),
        FUNC_MAP(X509_REQ_digest),
        FUNC_MAP(BUF_strlcat),
        FUNC_MAP(SSL_CTX_cipher_in_group),
        FUNC_MAP(SSL_set1_groups_list),
        FUNC_MAP(CRYPTO_gcm128_decrypt),
        FUNC_MAP(TRUST_TOKEN_ISSUER_redeem),
        FUNC_MAP(X509_CRL_add_ext),
        FUNC_MAP(BIO_callback_ctrl),
        FUNC_MAP(DES_set_key),
        FUNC_MAP(BN_mod_pow2),
        FUNC_MAP(PEM_X509_INFO_read_bio),
        FUNC_MAP(i2d_ASN1_ENUMERATED),
        FUNC_MAP(EVP_des_ecb),
        FUNC_MAP(X509_VERIFY_PARAM_set1_email),
        FUNC_MAP(BN_mod_exp_mont_consttime),
        FUNC_MAP(EVP_hpke_aes_256_gcm),
        FUNC_MAP(BIO_flush),
        FUNC_MAP(EC_KEY_parse_curve_name),
        FUNC_MAP(MD5_Init),
        FUNC_MAP(d2i_X509_CRL),
        FUNC_MAP(X509_get_serialNumber),
        FUNC_MAP(ACCESS_DESCRIPTION_it),
        FUNC_MAP(SSL_set_hostflags),
        FUNC_MAP(PEM_write_bio_DSA_PUBKEY),
        FUNC_MAP(X509_CERT_AUX_new),
        FUNC_MAP(X509V3_EXT_add_nconf_sk),
        FUNC_MAP(SSL_get_signature_algorithm_digest),
        FUNC_MAP(SSL_magic_pending_session_ptr),
        FUNC_MAP(RSA_sign_pss_mgf1),
        FUNC_MAP(d2i_GENERAL_NAMES),
        FUNC_MAP(EVP_aes_128_ctr),
        FUNC_MAP(i2s_ASN1_INTEGER),
        FUNC_MAP(SSL_set_max_send_fragment),
        FUNC_MAP(BIO_set_close),
        FUNC_MAP(RAND_bytes),
        FUNC_MAP(RSA_set0_factors),
        FUNC_MAP(EVP_HPKE_CTX_setup_sender_with_seed_for_testing),
        FUNC_MAP(NETSCAPE_SPKI_verify),
        FUNC_MAP(a2i_IPADDRESS),
        FUNC_MAP(EVP_PKEY_set1_RSA),
        FUNC_MAP(BN_one),
        FUNC_MAP(TRUST_TOKEN_ISSUER_issue),
        FUNC_MAP(d2i_PKCS8_PRIV_KEY_INFO_fp),
        FUNC_MAP(POLICYQUALINFO_new),
        FUNC_MAP(d2i_PROXY_CERT_INFO_EXTENSION),
        FUNC_MAP(SSL_CTX_free),
        FUNC_MAP(SSL_get_servername_type),
        FUNC_MAP(ASN1_GENERALIZEDTIME_adj),
        FUNC_MAP(ERR_peek_error_line),
        FUNC_MAP(X509_get_version),
        FUNC_MAP(X509_VERIFY_PARAM_table_cleanup),
        FUNC_MAP(NOTICEREF_it),
        FUNC_MAP(SSL_CIPHER_get_bits),
        FUNC_MAP(SHA512_Init),
        FUNC_MAP(BN_clear),
        FUNC_MAP(CRYPTO_BUFFER_new),
        FUNC_MAP(X509_PUBKEY_it),
        FUNC_MAP(EVP_PKEY_CTX_get0_rsa_oaep_label),
        FUNC_MAP(X509_STORE_get_check_issued),
        FUNC_MAP(EVP_get_digestbynid),
        FUNC_MAP(CRYPTO_gcm128_encrypt),
        FUNC_MAP(X509_SIG_getm),
        FUNC_MAP(SSL_CTX_set_psk_client_callback),
        FUNC_MAP(i2d_PUBKEY),
        FUNC_MAP(X509_ALGOR_set0),
        FUNC_MAP(X509_STORE_get_lookup_crls),
        FUNC_MAP(SSL_CTX_enable_tls_channel_id),
        FUNC_MAP(CRYPTO_BUFFER_data),
        FUNC_MAP(TLS_method),
        FUNC_MAP(EC_KEY_set_public_key),
        FUNC_MAP(BIO_vsnprintf),
        FUNC_MAP(EVP_SignFinal),
        FUNC_MAP(CRYPTO_BUFFER_POOL_free),
        FUNC_MAP(_ZN4bssl14CBBFinishArrayEP6cbb_stPNS_5ArrayIhEE),
        FUNC_MAP(SSL_get_ciphers),
        FUNC_MAP(i2t_ASN1_OBJECT),
        FUNC_MAP(ASN1_OBJECT_it),
        FUNC_MAP(PEM_read_bio_PKCS8_PRIV_KEY_INFO),
        FUNC_MAP(X509_ALGOR_set_md),
        FUNC_MAP(BUF_MEM_free),
        FUNC_MAP(EVP_PKEY_type),
        FUNC_MAP(d2i_PUBKEY),
        FUNC_MAP(PEM_read_bio_RSA_PUBKEY),
        FUNC_MAP(i2d_re_X509_REQ_tbs),
        FUNC_MAP(X509_new),
        FUNC_MAP(SSL_CTX_set_tmp_dh),
        FUNC_MAP(EVP_EncryptFinal),
        FUNC_MAP(EC_POINT_copy),
        FUNC_MAP(DES_ncbc_encrypt),
        FUNC_MAP(X509_STORE_CTX_get0_cert),
        FUNC_MAP(X509_VERIFY_PARAM_get0),
        FUNC_MAP(X509V3_add_standard_extensions),
        FUNC_MAP(SSL_CTX_get_ciphers),
        FUNC_MAP(SSL_set_min_proto_version),
        FUNC_MAP(X509_REVOKED_new),
        FUNC_MAP(CBS_parse_generalized_time),
        FUNC_MAP(BN_is_negative),
        FUNC_MAP(BN_pseudo_rand),
        FUNC_MAP(PEM_write_PKCS7),
        FUNC_MAP(ASN1_STRING_print),
        FUNC_MAP(PEM_write_PKCS8),
        FUNC_MAP(i2d_X509_EXTENSIONS),
        FUNC_MAP(OBJ_get0_data),
        FUNC_MAP(ASN1_item_sign_ctx),
        FUNC_MAP(SSL_get_server_random),
        FUNC_MAP(EVP_CIPHER_nid),
        FUNC_MAP(EC_get_builtin_curves),
        FUNC_MAP(PEM_write_RSA_PUBKEY),
        FUNC_MAP(SIPHASH_24),
        FUNC_MAP(GENERAL_NAME_set0_othername),
        FUNC_MAP(SSL_set0_wbio),
        FUNC_MAP(SSL_set_max_proto_version),
        FUNC_MAP(BIO_free),
        FUNC_MAP(ASN1_PRINTABLE_it),
        FUNC_MAP(d2i_DSAPrivateKey_fp),
        FUNC_MAP(d2i_ASN1_VISIBLESTRING),
        FUNC_MAP(CBB_add_zeros),
        FUNC_MAP(BN_CTX_end),
        FUNC_MAP(ERR_remove_state),
        FUNC_MAP(AES_wrap_key),
        FUNC_MAP(EVP_HPKE_CTX_export),
        FUNC_MAP(X509_signature_print),
        FUNC_MAP(X509_get_notBefore),
        FUNC_MAP(X509_STORE_CTX_get_chain),
        FUNC_MAP(ASN1_IA5STRING_new),
        FUNC_MAP(d2i_ASN1_UTCTIME),
        FUNC_MAP(BN_mod_mul),
        FUNC_MAP(d2i_DHparams),
        FUNC_MAP(EC_GROUP_cmp),
        FUNC_MAP(ERR_error_string),
        FUNC_MAP(BN_get_u64),
        FUNC_MAP(X509_LOOKUP_by_subject),
        FUNC_MAP(NAME_CONSTRAINTS_new),
        FUNC_MAP(X509_supported_extension),
        FUNC_MAP(ASN1_STRING_print_ex_fp),
        FUNC_MAP(OBJ_nid2obj),
        FUNC_MAP(PEM_get_EVP_CIPHER_INFO),
        FUNC_MAP(CRYPTO_BUFFER_free),
        FUNC_MAP(X509V3_add_value_bool),
        FUNC_MAP(SSL_CTX_get_default_passwd_cb),
        FUNC_MAP(X509_CRL_INFO_new),
        FUNC_MAP(SSL_SESSION_get0_ocsp_response),
        FUNC_MAP(CRYPTO_refcount_dec_and_test_zero),
        FUNC_MAP(EVP_CIPHER_CTX_copy),
        FUNC_MAP(d2i_X509_ATTRIBUTE),
        FUNC_MAP(X509_get_ex_new_index),
        FUNC_MAP(X509_subject_name_hash),
        FUNC_MAP(BIO_shutdown_wr),
        FUNC_MAP(CMAC_Update),
        FUNC_MAP(X509_VERIFY_PARAM_set_trust),
        FUNC_MAP(GENERAL_NAME_get0_value),
        FUNC_MAP(SSL_get_tlsext_status_ocsp_resp),
        FUNC_MAP(EVP_des_ede),
        FUNC_MAP(EVP_PKEY_CTX_ctrl),
        FUNC_MAP(EVP_PKCS82PKEY),
        FUNC_MAP(X509_set1_notBefore),
        FUNC_MAP(X509_STORE_CTX_set_depth),
        FUNC_MAP(SSL_CIPHER_get_rfc_name),
        FUNC_MAP(SSL_get_shared_sigalgs),
        FUNC_MAP(EVP_des_cbc),
        FUNC_MAP(DSAparams_dup),
        FUNC_MAP(EVP_PKEY_CTX_get_rsa_mgf1_md),
        FUNC_MAP(X509_REQ_get_version),
        FUNC_MAP(i2d_ASN1_SEQUENCE_ANY),
        FUNC_MAP(BIO_set_init),
        FUNC_MAP(PKCS7_sign),
        FUNC_MAP(X509v3_get_ext_by_NID),
        FUNC_MAP(X509_CRL_set1_nextUpdate),
        FUNC_MAP(i2s_ASN1_OCTET_STRING),
        FUNC_MAP(SSL_set_info_callback),
        FUNC_MAP(DSA_get0_pub_key),
        FUNC_MAP(PEM_ASN1_write),
        FUNC_MAP(d2i_DSAparams),
        FUNC_MAP(EVP_MD_CTX_destroy),
        FUNC_MAP(X509_EXTENSION_get_critical),
        FUNC_MAP(X509_get_pubkey),
        FUNC_MAP(X509_STORE_set_check_crl),
        FUNC_MAP(X509_CINF_free),
        FUNC_MAP(BUF_MEM_reserve),
        FUNC_MAP(RSA_private_key_to_bytes),
        FUNC_MAP(SSL_set_custom_verify),
        FUNC_MAP(SSL_SESSION_copy_without_early_data),
        FUNC_MAP(ASN1_item_new),
        FUNC_MAP(ENGINE_free),
        FUNC_MAP(ERR_print_errors_fp),
        FUNC_MAP(BN_mod_add_quick),
        FUNC_MAP(BN_MONT_CTX_new),
        FUNC_MAP(EVP_MD_CTX_cleanse),
        FUNC_MAP(d2i_EC_PUBKEY_bio),
        FUNC_MAP(i2d_X509_PUBKEY),
        FUNC_MAP(ASN1_BIT_STRING_check),
        FUNC_MAP(ECDSA_SIG_parse),
        FUNC_MAP(BORINGSSL_self_test),
        FUNC_MAP(RSA_get0_iqmp),
        FUNC_MAP(i2d_PUBKEY_fp),
        FUNC_MAP(SSL_set_quic_transport_params),
        FUNC_MAP(d2i_ECPrivateKey),
        FUNC_MAP(DH_set_length),
        FUNC_MAP(X509_issuer_name_hash_old),
        FUNC_MAP(i2d_ECPrivateKey_fp),
        FUNC_MAP(EVP_CIPHER_CTX_set_app_data),
        FUNC_MAP(X509_REQ_get_pubkey),
        FUNC_MAP(SSL_get0_server_requested_CAs),
        FUNC_MAP(SSL_CTX_enable_ocsp_stapling),
        FUNC_MAP(BIO_get_fd),
        FUNC_MAP(DH_check_pub_key),
        FUNC_MAP(SSL_get0_peer_delegation_algorithms),
        FUNC_MAP(i2d_ASN1_BIT_STRING),
        FUNC_MAP(ASN1_VISIBLESTRING_it),
        FUNC_MAP(SHA256_TransformBlocks),
        FUNC_MAP(PEM_read_bio_X509_AUX),
        FUNC_MAP(X509_CRL_cmp),
        FUNC_MAP(X509_REQ_add1_attr_by_OBJ),
        FUNC_MAP(BN_bn2dec),
        FUNC_MAP(EVP_get_cipherbyname),
        FUNC_MAP(EVP_aes_128_gcm),
        FUNC_MAP(o2i_ECPublicKey),
        FUNC_MAP(PEM_read_bio_X509_REQ),
        FUNC_MAP(SSL_set_srtp_profiles),
        FUNC_MAP(SSL_CTX_use_certificate),
        FUNC_MAP(ASN1_OCTET_STRING_dup),
        FUNC_MAP(CBB_data),
        FUNC_MAP(EVP_DigestVerifyInit),
        FUNC_MAP(PEM_ASN1_write_bio),
        FUNC_MAP(ASN1_STRING_get0_data),
        FUNC_MAP(BIO_set_write_buffer_size),
        FUNC_MAP(TRUST_TOKEN_CLIENT_begin_redemption),
        FUNC_MAP(CERTIFICATEPOLICIES_free),
        FUNC_MAP(SSL_session_reused),
        FUNC_MAP(BIO_set_mem_eof_return),
        FUNC_MAP(BN_rshift),
        FUNC_MAP(X509_NAME_get_index_by_OBJ),
        FUNC_MAP(X509V3_EXT_i2d),
        FUNC_MAP(DTLSv1_server_method),
        FUNC_MAP(i2d_X509_bio),
        FUNC_MAP(X509_REQ_it),
        FUNC_MAP(DIST_POINT_NAME_free),
        FUNC_MAP(X509_PURPOSE_get_trust),
        FUNC_MAP(DES_ecb3_encrypt),
        FUNC_MAP(FIPS_version),
        FUNC_MAP(RSA_private_key_from_bytes),
        FUNC_MAP(SSL_set_signing_algorithm_prefs),
        FUNC_MAP(EVP_DigestFinal_ex),
        FUNC_MAP(SHA256_Init),
        FUNC_MAP(bn_lcm_consttime),
        FUNC_MAP(HMAC_CTX_copy),
        FUNC_MAP(X509_VERIFY_PARAM_set_purpose),
        FUNC_MAP(CBS_asn1_oid_to_text),
        FUNC_MAP(EC_GROUP_get_degree),
        FUNC_MAP(TRUST_TOKEN_new),
        FUNC_MAP(SSL_CIPHER_get_id),
        FUNC_MAP(SSL_set_handshake_hints),
        FUNC_MAP(SSL_SESSION_set_time),
        FUNC_MAP(OpenSSL_version),
        FUNC_MAP(EVP_PKEY_encrypt_init),
        FUNC_MAP(EVP_aead_aes_128_gcm_tls12),
        FUNC_MAP(d2i_DIST_POINT),
        FUNC_MAP(i2d_DIST_POINT),
        FUNC_MAP(SSL_set1_curves_list),
        FUNC_MAP(BIO_get_fp),
        FUNC_MAP(EVP_sha1_final_with_secret_suffix),
        FUNC_MAP(EVP_aead_aes_128_gcm_tls13),
        FUNC_MAP(X509_REQ_INFO_free),
        FUNC_MAP(SSL_CIPHER_get_digest_nid),
        FUNC_MAP(RSA_get_ex_new_index),
        FUNC_MAP(X509_sign_ctx),
        FUNC_MAP(X509_STORE_CTX_set_cert),
        FUNC_MAP(d2i_EC_PUBKEY_fp),
        FUNC_MAP(SSL_set_options),
        FUNC_MAP(d2i_RSAPrivateKey),
        FUNC_MAP(BIO_set_conn_hostname),
        FUNC_MAP(EVP_enc_null),
        FUNC_MAP(EVP_DigestSignUpdate),
        FUNC_MAP(OPENSSL_strncasecmp),
        FUNC_MAP(X509_NAME_get_entry),
        FUNC_MAP(GENERAL_NAME_get0_otherName),
        FUNC_MAP(SSL_quic_write_level),
        FUNC_MAP(SSL_CTX_add_extra_chain_cert),
        FUNC_MAP(CBS_skip),
        FUNC_MAP(CRYPTO_free_ex_data),
        FUNC_MAP(DSA_SIG_marshal),
        FUNC_MAP(i2d_RSAPrivateKey_fp),
        FUNC_MAP(d2i_DSA_PUBKEY_fp),
        FUNC_MAP(SSL_CTX_set_options),
        FUNC_MAP(EVP_MD_CTX_reset),
        FUNC_MAP(OPENSSL_lh_delete),
        FUNC_MAP(X509_load_cert_crl_file),
        FUNC_MAP(X509V3_EXT_nconf),
        FUNC_MAP(d2i_AUTHORITY_INFO_ACCESS),
        FUNC_MAP(OPENSSL_gmtime),
        FUNC_MAP(BIO_get_mem_ptr),
        FUNC_MAP(i2o_ECPublicKey),
        FUNC_MAP(EVP_PKEY_assign_DSA),
        FUNC_MAP(bn_mod_u16_consttime),
        FUNC_MAP(BN_set_bit),
        FUNC_MAP(CRYPTO_gcm128_init_key),
        FUNC_MAP(CMAC_CTX_free),
        FUNC_MAP(CRYPTO_set_dynlock_create_callback),
        FUNC_MAP(X509at_add1_attr_by_OBJ),
        FUNC_MAP(NETSCAPE_SPKAC_free),
        FUNC_MAP(SSL_set1_ech_config_list),
        FUNC_MAP(ASN1_STRING_set_default_mask_asc),
        FUNC_MAP(i2d_DSAPrivateKey),
        FUNC_MAP(ECDSA_do_verify),
        FUNC_MAP(BN_le2bn),
        FUNC_MAP(HRSS_generate_key),
        FUNC_MAP(CRYPTO_BUFFER_len),
        FUNC_MAP(AUTHORITY_KEYID_new),
        FUNC_MAP(BN_get_rfc3526_prime_2048),
        FUNC_MAP(EVP_PKEY_CTX_set1_hkdf_key),
        FUNC_MAP(X509_CRL_set_version),
        FUNC_MAP(X509_CRL_set1_signature_value),
        FUNC_MAP(TLSv1_client_method),
        FUNC_MAP(ASN1_UTCTIME_it),
        FUNC_MAP(X509_subject_name_hash_old),
        FUNC_MAP(X509_REQ_set1_signature_algo),
        FUNC_MAP(X509_SIG_new),
        FUNC_MAP(i2d_ACCESS_DESCRIPTION),
        FUNC_MAP(SSL_set_client_CA_list),
        FUNC_MAP(PEM_write_bio_X509_REQ_NEW),
        FUNC_MAP(PEM_read_bio_DSAparams),
        FUNC_MAP(X509_REQ_sign),
        FUNC_MAP(EXTENDED_KEY_USAGE_it),
        FUNC_MAP(EVP_aead_aes_256_ctr_hmac_sha256),
        FUNC_MAP(X509_CRL_match),
        FUNC_MAP(SSL_serialize_capabilities),
        FUNC_MAP(SSL_use_certificate),
        FUNC_MAP(RSA_get0_d),
        FUNC_MAP(X509_verify_cert_error_string),
        FUNC_MAP(BIO_write_all),
        FUNC_MAP(BIO_number_read),
        FUNC_MAP(RSA_get0_e),
        FUNC_MAP(X509_subject_name_cmp),
        FUNC_MAP(X509_STORE_CTX_get0_param),
        FUNC_MAP(X509_REQ_verify),
        FUNC_MAP(X509_get_key_usage),
        FUNC_MAP(DH_get0_priv_key),
        FUNC_MAP(X509_REVOKED_add1_ext_i2d),
        FUNC_MAP(X509_PURPOSE_get_id),
        FUNC_MAP(SSL_CTX_clear_mode),
        FUNC_MAP(SSL_process_tls13_new_session_ticket),
        FUNC_MAP(BN_CTX_free),
        FUNC_MAP(ENGINE_new),
        FUNC_MAP(EC_POINT_get_affine_coordinates_GFp),
        FUNC_MAP(MD4_Transform),
        FUNC_MAP(NETSCAPE_SPKI_get_pubkey),
        FUNC_MAP(OPENSSL_strcasecmp),
        FUNC_MAP(PEM_write_bio_X509_AUX),
        FUNC_MAP(X509_check_ip_asc),
        FUNC_MAP(SSL_CTX_use_certificate_ASN1),
        FUNC_MAP(ASN1_TYPE_set),
        FUNC_MAP(EVP_MD_CTX_set_flags),
        FUNC_MAP(PEM_read_bio_DHparams),
        FUNC_MAP(X509_set_issuer_name),
        FUNC_MAP(GENERAL_NAME_cmp),
        FUNC_MAP(X509_REVOKED_set_serialNumber),
        FUNC_MAP(BN_lshift1),
        FUNC_MAP(CRYPTO_get_ex_data),
        FUNC_MAP(RSA_new),
        FUNC_MAP(PEM_write_bio_X509_REQ),
        FUNC_MAP(sk_set_cmp_func),
        FUNC_MAP(X509_STORE_set_depth),
        FUNC_MAP(d2i_RSA_PUBKEY_fp),
        FUNC_MAP(SSL_get_ex_new_index),
        FUNC_MAP(BIO_get_data),
        FUNC_MAP(EC_GROUP_free),
        FUNC_MAP(EVP_PKEY_base_id),
        FUNC_MAP(EVP_HPKE_CTX_setup_recipient),
        FUNC_MAP(X509_EXTENSION_get_object),
        FUNC_MAP(X509_set_notAfter),
        FUNC_MAP(CRYPTO_new_ex_data),
        FUNC_MAP(PEM_read_DSAparams),
        FUNC_MAP(X509_get_X509_PUBKEY),
        FUNC_MAP(i2s_ASN1_ENUMERATED),
        FUNC_MAP(RSA_get0_n),
        FUNC_MAP(TRUST_TOKEN_CLIENT_set_srr_key),
        FUNC_MAP(X509_STORE_CTX_get0_store),
        FUNC_MAP(CBB_init_fixed),
        FUNC_MAP(EVP_PKEY_CTX_set_rsa_pss_keygen_mgf1_md),
        FUNC_MAP(RSA_get0_p),
        FUNC_MAP(OBJ_nid2ln),
        FUNC_MAP(SSL_use_RSAPrivateKey_file),
        FUNC_MAP(OBJ_cmp),
        FUNC_MAP(ASN1_OCTET_STRING_it),
        FUNC_MAP(BN_GENCB_new),
        FUNC_MAP(DH_num_bits),
        FUNC_MAP(EVP_MD_CTX_block_size),
        FUNC_MAP(RSA_get0_q),
        FUNC_MAP(X509_CRL_get0_nextUpdate),
        FUNC_MAP(X509_OBJECT_retrieve_match),
        FUNC_MAP(d2i_X509_CRL_INFO),
        FUNC_MAP(X509V3_get_value_int),
        FUNC_MAP(EVP_aes_192_ofb),
        FUNC_MAP(RAND_set_rand_method),
        FUNC_MAP(X509_NAME_add_entry_by_txt),
        FUNC_MAP(GENERAL_NAME_free),
        FUNC_MAP(EVP_CIPHER_mode),
        FUNC_MAP(HMAC_CTX_cleanse),
        FUNC_MAP(d2i_ASN1_INTEGER),
        FUNC_MAP(OPENSSL_realloc),
        FUNC_MAP(EVP_PKEY_new),
        FUNC_MAP(RSA_add_pkcs1_prefix),
        FUNC_MAP(SHA384_Update),
        FUNC_MAP(OPENSSL_tolower),
        FUNC_MAP(SSL_CTX_set_signed_cert_timestamp_list),
        FUNC_MAP(PEM_read_PrivateKey),
        FUNC_MAP(DTLSv1_set_initial_timeout_duration),
        FUNC_MAP(SSL_SESSION_get_timeout),
        FUNC_MAP(d2i_DISPLAYTEXT),
        FUNC_MAP(PEM_write_PKCS8PrivateKey_nid),
        FUNC_MAP(X509_getm_notAfter),
        FUNC_MAP(SSL_CTX_set_next_proto_select_cb),
        FUNC_MAP(CBS_get_optional_asn1_uint64),
        FUNC_MAP(CBS_get_optional_asn1_bool),
        FUNC_MAP(X509_set_subject_name),
        FUNC_MAP(X509V3_EXT_print_fp),
        FUNC_MAP(i2a_ASN1_ENUMERATED),
        FUNC_MAP(SPAKE2_CTX_new),
        FUNC_MAP(DH_get0_pqg),
        FUNC_MAP(X509_ATTRIBUTE_new),
        FUNC_MAP(X509_STORE_free),
        FUNC_MAP(X509_CRL_get0_by_cert),
        FUNC_MAP(SSLv23_server_method),
        FUNC_MAP(ERR_clear_error),
        FUNC_MAP(i2d_ASN1_UTCTIME),
        FUNC_MAP(EVP_PKEY_CTX_add1_hkdf_info),
        FUNC_MAP(EVP_PKEY_CTX_set_rsa_keygen_bits),
        FUNC_MAP(d2i_PKCS8_bio),
        FUNC_MAP(X509_REQ_INFO_it),
        FUNC_MAP(X509_NAME_new),
        FUNC_MAP(SSL_get_curve_id),
        FUNC_MAP(DES_ede2_cbc_encrypt),
        FUNC_MAP(BN_sub),
        FUNC_MAP(EVP_PKEY_CTX_set_rsa_keygen_pubexp),
        FUNC_MAP(X509_REQ_set_subject_name),
        FUNC_MAP(OPENSSL_lh_retrieve),
        FUNC_MAP(CBB_add_u16),
        FUNC_MAP(DES_ecb_encrypt),
        FUNC_MAP(ERR_add_error_dataf),
        FUNC_MAP(a2i_IPADDRESS_NC),
        FUNC_MAP(ACCESS_DESCRIPTION_free),
        FUNC_MAP(SSL_set0_rbio),
        FUNC_MAP(SSL_peek),
        FUNC_MAP(EVP_get_cipherbynid),
        FUNC_MAP(X509_CRL_get_ext_by_NID),
        FUNC_MAP(X509_get0_authority_issuer),
        FUNC_MAP(SSL_set_bio),
        FUNC_MAP(SSL_CTX_sess_accept_good),
        FUNC_MAP(DSA_get0_priv_key),
        FUNC_MAP(d2i_X509_bio),
        FUNC_MAP(OPENSSL_lh_insert),
        FUNC_MAP(BIO_s_connect),
        FUNC_MAP(HKDF),
        FUNC_MAP(CTR_DRBG_init),
        FUNC_MAP(ASN1_ENUMERATED_get),
        FUNC_MAP(CBB_add_u32le),
        FUNC_MAP(X509V3_EXT_free),
        FUNC_MAP(RAND_pseudo_bytes),
        FUNC_MAP(RSA_public_decrypt),
        FUNC_MAP(i2d_X509_fp),
        FUNC_MAP(SSL_CTX_sess_number),
        FUNC_MAP(d2i_PKCS8_fp),
        FUNC_MAP(X509_add1_reject_object),
        FUNC_MAP(CRL_DIST_POINTS_new),
        FUNC_MAP(SSL_new),
        FUNC_MAP(SSL_select_next_proto),
        FUNC_MAP(SHA384),
        FUNC_MAP(SSL_CTX_set_keylog_callback),
        FUNC_MAP(X509_ATTRIBUTE_create_by_NID),
        FUNC_MAP(SSL_CTX_use_PrivateKey),
        FUNC_MAP(_ZN4bssl23SSL_get_traffic_secretsEPK6ssl_stPNS_4SpanIKhEES6_),
        FUNC_MAP(SSL_get_peer_quic_transport_params),
        FUNC_MAP(SSL_get_finished),
        FUNC_MAP(SHA256_Final),
        FUNC_MAP(EVP_CIPHER_CTX_block_size),
        FUNC_MAP(SSL_set_early_data_enabled),
        FUNC_MAP(ASN1_IA5STRING_it),
        FUNC_MAP(EVP_aes_128_ecb),
        FUNC_MAP(EVP_sha512_256),
        FUNC_MAP(EVP_cleanup),
        FUNC_MAP(_ZN4bssl21SSL_serialize_handoffEPK6ssl_stP6cbb_stP22ssl_early_callback_ctx),
        FUNC_MAP(EC_KEY_get_conv_form),
        FUNC_MAP(X509_STORE_CTX_get0_current_issuer),
        FUNC_MAP(SSL_load_client_CA_file),
        FUNC_MAP(BN_copy),
        FUNC_MAP(EC_KEY_is_opaque),
        FUNC_MAP(BN_is_odd),
        FUNC_MAP(PEM_ASN1_read_bio),
        FUNC_MAP(X509_STORE_get_check_revocation),
        FUNC_MAP(X509_PURPOSE_get_count),
        FUNC_MAP(SSL_is_dtls),
        FUNC_MAP(BN_num_bytes),
        FUNC_MAP(BN_from_montgomery),
        FUNC_MAP(i2d_RSAPublicKey),
        FUNC_MAP(PEM_write_bio_DHparams),
        FUNC_MAP(SSL_CTX_get_verify_depth),
        FUNC_MAP(SHA256_Update),
        FUNC_MAP(RSA_up_ref),
        FUNC_MAP(DTLS_method),
        FUNC_MAP(i2d_ECPrivateKey),
        FUNC_MAP(EVP_PKEY_CTX_set_signature_md),
        FUNC_MAP(EVP_AEAD_CTX_init_with_direction),
        FUNC_MAP(CRYPTO_set_dynlock_destroy_callback),
        FUNC_MAP(TRUST_TOKEN_ISSUER_redeem_raw),
        FUNC_MAP(ASN1_UNIVERSALSTRING_free),
        FUNC_MAP(EVP_CIPHER_CTX_init),
        FUNC_MAP(CTR_DRBG_reseed),
        FUNC_MAP(i2d_ASN1_GENERALIZEDTIME),
        FUNC_MAP(ASN1_BOOLEAN_it),
        FUNC_MAP(AES_ctr128_encrypt),
        FUNC_MAP(X509_PURPOSE_add),
        FUNC_MAP(SSL_CIPHER_is_block_cipher),
        FUNC_MAP(SSL_get_peer_cert_chain),
        FUNC_MAP(SSL_CTX_get_verify_callback),
        FUNC_MAP(OPENSSL_hash32),
        FUNC_MAP(TLSv1_2_client_method),
        FUNC_MAP(bn_mod_inverse_consttime),
        FUNC_MAP(RSA_public_encrypt),
        FUNC_MAP(X509V3_extensions_print),
        FUNC_MAP(EVP_MD_CTX_cleanup),
        FUNC_MAP(OPENSSL_lh_retrieve_key),
        FUNC_MAP(d2i_ASN1_OBJECT),
        FUNC_MAP(X509_CERT_AUX_it),
        FUNC_MAP(BIO_set_flags),
        FUNC_MAP(X509_INFO_new),
        FUNC_MAP(SSL_CTX_set_read_ahead),
        FUNC_MAP(ERR_load_SSL_strings),
        FUNC_MAP(HRSS_parse_public_key),
        FUNC_MAP(SSL_check_private_key),
        FUNC_MAP(EVP_CIPHER_CTX_key_length),
        FUNC_MAP(HMAC_CTX_get_md),
        FUNC_MAP(d2i_PKCS12_fp),
        FUNC_MAP(SSL_in_early_data),
        FUNC_MAP(SSL_CTX_set_compliance_policy),
        FUNC_MAP(SSL_add_client_CA),
        FUNC_MAP(ASN1_BIT_STRING_get_bit),
        FUNC_MAP(PEM_read_bio_RSAPrivateKey),
        FUNC_MAP(X509_EXTENSION_free),
        FUNC_MAP(DIST_POINT_set_dpname),
        FUNC_MAP(NETSCAPE_SPKI_new),
        FUNC_MAP(EVP_PKEY_decrypt),
        FUNC_MAP(SHA512_256_Init),
        FUNC_MAP(CRYPTO_num_locks),
        FUNC_MAP(TRUST_TOKEN_experiment_v2_voprf),
        FUNC_MAP(SSL_set0_verify_cert_store),
        FUNC_MAP(BIO_tell),
        FUNC_MAP(EVP_get_digestbyobj),
        FUNC_MAP(EVP_marshal_public_key),
        FUNC_MAP(ECDSA_SIG_set0),
        FUNC_MAP(PEM_read_bio_PrivateKey),
        FUNC_MAP(PKCS7_type_is_enveloped),
        FUNC_MAP(X509_ATTRIBUTE_set1_object),
        FUNC_MAP(d2i_DHparams_bio),
        FUNC_MAP(SSL_CTX_set_tlsext_servername_arg),
        FUNC_MAP(CBS_get_optional_asn1_octet_string),
        FUNC_MAP(i2d_RSAPrivateKey_bio),
        FUNC_MAP(ASN1_UTCTIME_cmp_time_t),
        FUNC_MAP(DISPLAYTEXT_free),
        FUNC_MAP(CBB_add_u24),
        FUNC_MAP(EVP_HPKE_KEY_cleanup),
        FUNC_MAP(NETSCAPE_SPKAC_new),
        FUNC_MAP(SSL_set_signed_cert_timestamp_list),
        FUNC_MAP(i2d_PKCS8_PRIV_KEY_INFO),
        FUNC_MAP(SSL_has_application_settings),
        FUNC_MAP(i2d_ASN1_T61STRING),
        FUNC_MAP(BIO_get_shutdown),
        FUNC_MAP(CBB_add_space),
        FUNC_MAP(CBS_get_last_u8),
        FUNC_MAP(BN_is_prime_ex),
        FUNC_MAP(X509_REQ_get_attr),
        FUNC_MAP(X509_NAME_get_text_by_NID),
        FUNC_MAP(i2d_EC_PUBKEY_fp),
        FUNC_MAP(X509_REVOKED_it),
        FUNC_MAP(SSL_CTX_add1_chain_cert),
        FUNC_MAP(ASN1_T61STRING_it),
        FUNC_MAP(BN_pseudo_rand_range),
        FUNC_MAP(EVP_AEAD_CTX_open),
        FUNC_MAP(EVP_aead_aes_128_gcm),
        FUNC_MAP(SSL_set_msg_callback),
        FUNC_MAP(ASN1_OCTET_STRING_set),
        FUNC_MAP(BIO_ctrl_get_write_guarantee),
        FUNC_MAP(EVP_CipherUpdate),
        FUNC_MAP(EC_GROUP_set_generator),
        FUNC_MAP(PEM_write_PrivateKey),
        FUNC_MAP(X509_dup),
        FUNC_MAP(X509_get_extended_key_usage),
        FUNC_MAP(SSL_CTX_set_tmp_dh_callback),
        FUNC_MAP(ERR_get_error_line),
        FUNC_MAP(d2i_PKCS7),
        FUNC_MAP(i2d_X509_AUX),
        FUNC_MAP(X509_add1_ext_i2d),
        FUNC_MAP(SSL_CTX_set_tmp_rsa_callback),
        FUNC_MAP(SSL_get_peer_full_cert_chain),
        FUNC_MAP(PEM_write_bio_PKCS7),
        FUNC_MAP(X509_check_trust),
        FUNC_MAP(X509_REVOKED_dup),
        FUNC_MAP(X509_NAME_ENTRY_create_by_NID),
        FUNC_MAP(BN_new),
        FUNC_MAP(EVP_sha224),
        FUNC_MAP(BN_mod_lshift1),
        FUNC_MAP(i2d_X509_REQ),
        FUNC_MAP(PEM_write_bio_PKCS8),
        FUNC_MAP(sk_zero),
        FUNC_MAP(SSL_CTX_set_mode),
        FUNC_MAP(SSL_set_msg_callback_arg),
        FUNC_MAP(ASN1_put_object),
        FUNC_MAP(DES_ede3_cbc_encrypt),
        FUNC_MAP(HMAC_CTX_init),
        FUNC_MAP(EVP_PKEY_encrypt),
        FUNC_MAP(d2i_PKCS8_PRIV_KEY_INFO),
        FUNC_MAP(CRYPTO_BUFFER_alloc),
        FUNC_MAP(X509_get_ext),
        FUNC_MAP(X509_check_issued),
        FUNC_MAP(SSL_CTX_use_PrivateKey_ASN1),
        FUNC_MAP(EC_KEY_marshal_private_key),
        FUNC_MAP(CERTIFICATEPOLICIES_new),
        FUNC_MAP(SSL_CTX_set_info_callback),
        FUNC_MAP(cbb_add_latin1),
        FUNC_MAP(i2d_DSAparams),
        FUNC_MAP(EVP_HPKE_KEY_kem),
        FUNC_MAP(PKCS7_get_PEM_certificates),
        FUNC_MAP(X509_STORE_CTX_get0_chain),
        FUNC_MAP(d2i_ACCESS_DESCRIPTION),
        FUNC_MAP(X509_get0_subject_key_id),
        FUNC_MAP(cbb_add_utf8),
        FUNC_MAP(BIO_s_fd),
        FUNC_MAP(PEM_read_RSAPrivateKey),
        FUNC_MAP(X509_delete_ext),
        FUNC_MAP(d2i_X509_CRL_fp),
        FUNC_MAP(ASN1_TYPE_cmp),
        FUNC_MAP(OPENSSL_init_crypto),
        FUNC_MAP(EVP_HPKE_CTX_new),
        FUNC_MAP(s2i_ASN1_INTEGER),
        FUNC_MAP(SSL_CTX_set_custom_verify),
        FUNC_MAP(EVP_aes_128_cbc),
        FUNC_MAP(EVP_CIPHER_CTX_new),
        FUNC_MAP(X509_EXTENSION_set_data),
        FUNC_MAP(SSL_renegotiate),
        FUNC_MAP(SSL_CTX_get_info_callback),
        FUNC_MAP(DSA_SIG_free),
        FUNC_MAP(BN_ucmp),
        FUNC_MAP(X509_REVOKED_get0_serialNumber),
        FUNC_MAP(X509_STORE_CTX_trusted_stack),
        FUNC_MAP(NOTICEREF_free),
        FUNC_MAP(SSL_SESSION_up_ref),
        FUNC_MAP(CBB_finish),
        FUNC_MAP(BIO_set_retry_special),
        FUNC_MAP(_ZN4bssl10SealRecordEP6ssl_stNS_4SpanIhEES3_S3_NS2_IKhEE),
        FUNC_MAP(BN_get_rfc3526_prime_3072),
        FUNC_MAP(BN_MONT_CTX_new_consttime),
        FUNC_MAP(MD4_Update),
        FUNC_MAP(i2d_PKCS7),
        FUNC_MAP(BIO_set_mem_buf),
        FUNC_MAP(CBS_get_asn1_element),
        FUNC_MAP(_ZN4bssl29ssl_decode_client_hello_innerEP6ssl_stPhPNS_5ArrayIhEENS_4SpanIKhEEPK22ssl_early_callback_ctx),
        FUNC_MAP(ASN1_get_object),
        FUNC_MAP(X509_load_crl_file),
        FUNC_MAP(SSL_SESSION_new),
        FUNC_MAP(CBS_get_asn1_implicit_string),
        FUNC_MAP(EC_GROUP_get_curve_name),
        FUNC_MAP(sk_is_sorted),
        FUNC_MAP(SSL_CTX_add_client_CA),
        FUNC_MAP(DSA_size),
        FUNC_MAP(EVP_VerifyFinal),
        FUNC_MAP(EVP_HPKE_KEY_public_key),
        FUNC_MAP(X509v3_get_ext_by_OBJ),
        FUNC_MAP(X509_VERIFY_PARAM_inherit),
        FUNC_MAP(i2d_AUTHORITY_INFO_ACCESS),
        FUNC_MAP(CRYPTO_poly1305_update),
        FUNC_MAP(CRYPTO_gcm128_finish),
        FUNC_MAP(SSL_CTX_set_srtp_profiles),
        FUNC_MAP(CBS_get_asn1_int64),
        FUNC_MAP(CRYPTO_pre_sandbox_init),
        FUNC_MAP(ERR_lib_error_string),
        FUNC_MAP(CTR_DRBG_clear),
        FUNC_MAP(RAND_SSLeay),
        FUNC_MAP(X509_CRL_set1_lastUpdate),
        FUNC_MAP(SSL_CTX_enable_signed_cert_timestamps),
        FUNC_MAP(SSL_used_hello_retry_request),
        FUNC_MAP(SSL_set_session),
        FUNC_MAP(BN_clear_bit),
        FUNC_MAP(BUF_strnlen),
        FUNC_MAP(OPENSSL_gmtime_diff),
        FUNC_MAP(sk_find),
        FUNC_MAP(X509_ALGOR_new),
        FUNC_MAP(X509_PUBKEY_get0_param),
        FUNC_MAP(SHA1_Transform),
        FUNC_MAP(RSA_sign_raw),
        FUNC_MAP(GENERAL_SUBTREE_free),
        FUNC_MAP(ASN1_item_dup),
        FUNC_MAP(EVP_AEAD_key_length),
        FUNC_MAP(ERR_peek_last_error),
        FUNC_MAP(OPENSSL_add_all_algorithms_conf),
        FUNC_MAP(AES_set_encrypt_key),
        FUNC_MAP(SSL_CTX_set_msg_callback_arg),
        FUNC_MAP(SSL_get_write_sequence),
        FUNC_MAP(asn1_generalizedtime_to_tm),
        FUNC_MAP(ASN1_TYPE_free),
        FUNC_MAP(i2d_ASN1_INTEGER),
        FUNC_MAP(EVP_aead_xchacha20_poly1305),
        FUNC_MAP(BN_add),
        FUNC_MAP(d2i_ECDSA_SIG),
        FUNC_MAP(CBB_add_u32),
        FUNC_MAP(ECDSA_SIG_new),
        FUNC_MAP(RSA_verify),
        FUNC_MAP(PEM_write_X509_REQ_NEW),
        FUNC_MAP(X509_STORE_add_crl),
        FUNC_MAP(BN_to_ASN1_ENUMERATED),
        FUNC_MAP(EVP_CIPHER_CTX_set_flags),
        FUNC_MAP(RSA_check_fips),
        FUNC_MAP(PEM_write_bio_DSAparams),
        FUNC_MAP(TRUST_TOKEN_ISSUER_add_key),
        FUNC_MAP(SSL_set_jdk11_workaround),
        FUNC_MAP(CBS_copy_bytes),
        FUNC_MAP(ERR_SAVE_STATE_free),
        FUNC_MAP(EC_KEY_set_enc_flags),
        FUNC_MAP(d2i_X509_CERT_AUX),
        FUNC_MAP(DH_generate_parameters_ex),
        FUNC_MAP(EVP_PKEY_set_type),
        FUNC_MAP(EVP_parse_private_key),
        FUNC_MAP(X509_STORE_CTX_set_flags),
        FUNC_MAP(SSL_load_error_strings),
        FUNC_MAP(EVP_PKEY_size),
        FUNC_MAP(bn_miller_rabin_init),
        FUNC_MAP(DH_size),
        FUNC_MAP(RSA_set0_key),
        FUNC_MAP(X509_CRL_get_ext_by_critical),
        FUNC_MAP(SSL_get_servername),
        FUNC_MAP(SSL_enable_tls_channel_id),
        FUNC_MAP(SSL_get_min_proto_version),
        FUNC_MAP(CBS_asn1_bitstring_has_bit),
        FUNC_MAP(EVP_DigestInit),
        FUNC_MAP(SSL_set_permute_extensions),
        FUNC_MAP(BN_zero),
        FUNC_MAP(CBS_get_any_asn1),
        FUNC_MAP(HMAC_CTX_cleanup),
        FUNC_MAP(d2i_DSA_SIG),
        FUNC_MAP(EVP_CIPHER_CTX_flags),
        FUNC_MAP(CRYPTO_realloc),
        FUNC_MAP(PKCS8_encrypt),
        FUNC_MAP(PKCS7_type_is_signed),
        FUNC_MAP(X509_TRUST_get_by_id),
        FUNC_MAP(SSL_get_ex_data_X509_STORE_CTX_idx),
        FUNC_MAP(SSL_export_keying_material),
        FUNC_MAP(i2d_DHparams),
        FUNC_MAP(EVP_DigestInit_ex),
        FUNC_MAP(EVP_PKEY_CTX_hkdf_mode),
        FUNC_MAP(EVP_PKEY_CTX_set1_hkdf_salt),
        FUNC_MAP(SHA384_Init),
        FUNC_MAP(X509_VAL_free),
        FUNC_MAP(SSL_free),
        FUNC_MAP(BIO_should_retry),
        FUNC_MAP(SHA1_Update),
        FUNC_MAP(X509V3_add1_i2d),
        FUNC_MAP(SSL_CTX_set_current_time_cb),
        FUNC_MAP(SSL_get_max_proto_version),
        FUNC_MAP(ASN1_TIME_new),
        FUNC_MAP(BIO_set_retry_reason),
        FUNC_MAP(X509_NAME_print),
        FUNC_MAP(X509_STORE_CTX_free),
        FUNC_MAP(X509_VERIFY_PARAM_set_hostflags),
        FUNC_MAP(SSL_set_renegotiate_mode),
        FUNC_MAP(ASN1_mbstring_copy),
        FUNC_MAP(ASN1_TIME_to_generalizedtime),
        FUNC_MAP(BLAKE2B256),
        FUNC_MAP(BN_enhanced_miller_rabin_primality_test),
        FUNC_MAP(EVP_AEAD_CTX_seal_scatter),
        FUNC_MAP(PEM_read_RSAPublicKey),
        FUNC_MAP(CRYPTO_BUFFER_new_from_static_data_unsafe),
        FUNC_MAP(SSL_ECH_KEYS_new),
        FUNC_MAP(SSL_CIPHER_get_kx_name),
        FUNC_MAP(SSL_CIPHER_get_prf_nid),
        FUNC_MAP(CBB_add_u8),
        FUNC_MAP(ASN1_TIME_to_posix),
        FUNC_MAP(CBB_add_asn1_oid_from_text),
        FUNC_MAP(BN_is_pow2),
        FUNC_MAP(X509_PUBKEY_get),
        FUNC_MAP(X509_EXTENSION_new),
        FUNC_MAP(X509_check_ip),
        FUNC_MAP(X509_PKEY_free),
        FUNC_MAP(SSL_get_certificate),
        FUNC_MAP(EVP_des_ede3_ecb),
        FUNC_MAP(EVP_HPKE_KEY_free),
        FUNC_MAP(NAME_CONSTRAINTS_check),
        FUNC_MAP(AUTHORITY_INFO_ACCESS_it),
        FUNC_MAP(SSL_get_max_cert_list),
        FUNC_MAP(BIO_set_retry_write),
        FUNC_MAP(bn_abs_sub_consttime),
        FUNC_MAP(SSL_pending),
        FUNC_MAP(SSL_state),
        FUNC_MAP(ASN1_PRINTABLE_free),
        FUNC_MAP(BIO_eof),
        FUNC_MAP(BN_mod_lshift),
        FUNC_MAP(EVP_AEAD_CTX_get_iv),
        FUNC_MAP(AES_CMAC),
        FUNC_MAP(RSA_flags),
        FUNC_MAP(X509_get0_tbs_sigalg),
        FUNC_MAP(SSL_in_init),
        FUNC_MAP(SSL_SESSION_get0_peer),
        FUNC_MAP(ASN1_GENERALIZEDTIME_set),
        FUNC_MAP(EVP_blake2b256),
        FUNC_MAP(X509_NAME_entry_count),
        FUNC_MAP(X509_OBJECT_free_contents),
        FUNC_MAP(SSL_SESSION_free),
        FUNC_MAP(SSL_CTX_get_extra_chain_certs),
        FUNC_MAP(BIO_ptr_ctrl),
        FUNC_MAP(d2i_X509_AUX),
        FUNC_MAP(X509_STORE_CTX_get0_current_crl),
        FUNC_MAP(SSL_add_bio_cert_subjects_to_stack),
        FUNC_MAP(SSL_get_fd),
        FUNC_MAP(TLSv1_1_method),
        FUNC_MAP(BIO_pop),
        FUNC_MAP(RSA_decrypt),
        FUNC_MAP(EC_GROUP_set_point_conversion_form),
        FUNC_MAP(ec_scalar_to_bytes),
        FUNC_MAP(d2i_X509_REQ),
        FUNC_MAP(NETSCAPE_SPKI_b64_decode),
        FUNC_MAP(POLICYINFO_new),
        FUNC_MAP(SSL_get_peer_certificate),
        FUNC_MAP(SSLeay),
        FUNC_MAP(SSL_SESSION_has_ticket),
        FUNC_MAP(CRYPTO_gcm128_setiv),
        FUNC_MAP(PEM_bytes_read_bio),
        FUNC_MAP(X509_ATTRIBUTE_set1_data),
        FUNC_MAP(X509_get0_pubkey_bitstr),
        FUNC_MAP(POLICY_CONSTRAINTS_it),
        FUNC_MAP(X509_set1_signature_value),
        FUNC_MAP(SSL_set_rfd),
        FUNC_MAP(ASN1_item_d2i),
        FUNC_MAP(ASN1_GENERALIZEDTIME_print),
        FUNC_MAP(i2a_ASN1_INTEGER),
        FUNC_MAP(CRYPTO_refcount_inc),
        FUNC_MAP(EVP_PKEY_verify_recover_init),
        FUNC_MAP(EVP_HPKE_CTX_zero),
        FUNC_MAP(TRUST_TOKEN_ISSUER_free),
        FUNC_MAP(v2i_GENERAL_NAME_ex),
        FUNC_MAP(PROXY_POLICY_new),
        FUNC_MAP(SSL_CTX_use_RSAPrivateKey_ASN1),
        FUNC_MAP(EVP_AEAD_CTX_free),
        FUNC_MAP(EVP_DecryptFinal),
        FUNC_MAP(d2i_USERNOTICE),
        FUNC_MAP(i2d_USERNOTICE),
        FUNC_MAP(ASN1_ENUMERATED_to_BN),
        FUNC_MAP(CBB_flush_asn1_set_of),
        FUNC_MAP(ERR_remove_thread_state),
        FUNC_MAP(EVP_AEAD_CTX_cleanup),
        FUNC_MAP(ASN1_TIME_to_time_t),
        FUNC_MAP(HMAC_size),
        FUNC_MAP(SSL_get_peer_signature_algorithm),
        FUNC_MAP(X509_ALGOR_get0),
        FUNC_MAP(SSL_set_connect_state),
        FUNC_MAP(SSL_set_tmp_ecdh),
        FUNC_MAP(EVP_aes_256_ofb),
        FUNC_MAP(BN_init),
        FUNC_MAP(AES_unwrap_key_padded),
        FUNC_MAP(GENERAL_NAMES_free),
        FUNC_MAP(SSL_ECH_KEYS_add),
        FUNC_MAP(BIO_read_asn1),
        FUNC_MAP(CBS_is_valid_asn1_integer),
        FUNC_MAP(SSL_CTX_set_dos_protection_cb),
        FUNC_MAP(SSL_CTX_flush_sessions),
        FUNC_MAP(ASN1_OCTET_STRING_cmp),
        FUNC_MAP(RSA_marshal_public_key),
        FUNC_MAP(pmbtoken_exp1_get_h_for_testing),
        FUNC_MAP(SSL_set_tlsext_use_srtp),
        FUNC_MAP(sk_delete_ptr),
        FUNC_MAP(RSA_encrypt),
        FUNC_MAP(CRYPTO_gcm128_decrypt_ctr32),
        FUNC_MAP(TRUST_TOKEN_CLIENT_finish_redemption),
        FUNC_MAP(SSL_use_PrivateKey_file),
        FUNC_MAP(SSL_alert_type_string),
        FUNC_MAP(SSL_CTX_get_min_proto_version),
        FUNC_MAP(ASN1_TIME_adj),
        FUNC_MAP(i2d_PKCS8_bio),
        FUNC_MAP(X509_SIG_free),
        FUNC_MAP(SSL_write),
        FUNC_MAP(SSL_set_shutdown),
        FUNC_MAP(SSL_CTX_sess_get_get_cb),
        FUNC_MAP(EC_KEY_set_private_key),
        FUNC_MAP(AUTHORITY_INFO_ACCESS_new),
        FUNC_MAP(SSL_in_false_start),
        FUNC_MAP(SSL_get1_session),
        FUNC_MAP(d2i_ASN1_TIME),
        FUNC_MAP(DSA_do_sign),
        FUNC_MAP(EVP_PKEY_assign),
        FUNC_MAP(CRYPTO_get_dynlock_create_callback),
        FUNC_MAP(SSL_need_tmp_RSA),
        FUNC_MAP(EVP_EncodeFinal),
        FUNC_MAP(CBS_get_optional_asn1),
        FUNC_MAP(PEM_write_RSAPublicKey),
        FUNC_MAP(SSL_CTX_get_max_proto_version),
        FUNC_MAP(BN_marshal_asn1),
        FUNC_MAP(BN_get_rfc3526_prime_4096),
        FUNC_MAP(EVP_PKEY_CTX_set_ec_param_enc),
        FUNC_MAP(X509_CRL_INFO_free),
        FUNC_MAP(CBS_asn1_ber_to_der),
        FUNC_MAP(CRYPTO_get_thread_local),
        FUNC_MAP(X509_CRL_get0_lastUpdate),
        FUNC_MAP(X509_add_ext),
        FUNC_MAP(X509_STORE_set_check_issued),
        FUNC_MAP(CRL_DIST_POINTS_free),
        FUNC_MAP(d2i_ISSUING_DIST_POINT),
        FUNC_MAP(_ZN4bssl17SSL_apply_handoffEP6ssl_stNS_4SpanIKhEE),
        FUNC_MAP(SSL_get_read_ahead),
        FUNC_MAP(CRYPTO_has_asm),
        FUNC_MAP(EVP_AEAD_CTX_aead),
        FUNC_MAP(X509_print_ex_fp),
        FUNC_MAP(i2d_X509_REQ_fp),
        FUNC_MAP(EVP_EncodeInit),
        FUNC_MAP(X509_REQ_extension_nid),
        FUNC_MAP(ec_hash_to_scalar_p384_xmd_sha512_draft07),
        FUNC_MAP(EVP_HPKE_AEAD_aead),
        FUNC_MAP(sk_shift),
        FUNC_MAP(X509_STORE_set_flags),
        FUNC_MAP(BIO_write_filename),
        FUNC_MAP(EVP_DecryptUpdate),
        FUNC_MAP(EC_KEY_key2buf),
        FUNC_MAP(EVP_CIPHER_CTX_set_padding),
        FUNC_MAP(PKCS5_PBKDF2_HMAC),
        FUNC_MAP(X509_CRL_print),
        FUNC_MAP(AUTHORITY_KEYID_it),
        FUNC_MAP(BUF_strndup),
        FUNC_MAP(EVP_CIPHER_iv_length),
        FUNC_MAP(X509_ATTRIBUTE_dup),
        FUNC_MAP(SSL_get_verify_mode),
        FUNC_MAP(SSL_get0_peer_verify_algorithms),
        FUNC_MAP(EVP_des_ede3_cbc),
        FUNC_MAP(X509_NAME_dup),
        FUNC_MAP(OPENSSL_strlcpy),
        FUNC_MAP(EVP_CIPHER_CTX_mode),
        FUNC_MAP(EVP_PKEY_missing_parameters),
        FUNC_MAP(X509_CRL_get_ext_by_OBJ),
        FUNC_MAP(OPENSSL_timegm),
        FUNC_MAP(EVP_md4),
        FUNC_MAP(TRUST_TOKEN_ISSUER_set_metadata_key),
        FUNC_MAP(EVP_md5),
        FUNC_MAP(EVP_MD_CTX_free),
        FUNC_MAP(X509_STORE_set_cleanup),
        FUNC_MAP(d2i_NETSCAPE_SPKAC),
        FUNC_MAP(OBJ_obj2txt),
        FUNC_MAP(PKCS8_PRIV_KEY_INFO_free),
        FUNC_MAP(X509_TRUST_get0_name),
        FUNC_MAP(X509_STORE_CTX_new),
        FUNC_MAP(X509_CINF_it),
        FUNC_MAP(SSL_get_extms_support),
        FUNC_MAP(d2i_DIRECTORYSTRING),
        FUNC_MAP(OPENSSL_lh_doall_arg),
        FUNC_MAP(SSL_shutdown),
        FUNC_MAP(SSL_CTX_sess_connect_renegotiate),
        FUNC_MAP(DSA_parse_public_key),
        FUNC_MAP(SSL_get_ex_data),
        FUNC_MAP(RC4_set_key),
        FUNC_MAP(HMAC_Init_ex),
        FUNC_MAP(ECDSA_SIG_max_len),
        FUNC_MAP(EVP_PBE_scrypt),
        FUNC_MAP(PKCS7_free),
        FUNC_MAP(X509_ATTRIBUTE_create_by_OBJ),
        FUNC_MAP(X509_REQ_add1_attr_by_txt),
        FUNC_MAP(i2a_ACCESS_DESCRIPTION),
        FUNC_MAP(SSL_get_ivs),
        FUNC_MAP(X509_STORE_set_verify),
        FUNC_MAP(DSA_bits),
        FUNC_MAP(EVP_PKEY_CTX_set_rsa_pss_keygen_md),
        FUNC_MAP(TRUST_TOKEN_CLIENT_new),
        FUNC_MAP(SSL_set_trust),
        FUNC_MAP(d2i_ASN1_PRINTABLE),
        FUNC_MAP(i2d_PKCS12_fp),
        FUNC_MAP(X509_get0_uids),
        FUNC_MAP(SSL_CTX_check_private_key),
        FUNC_MAP(SSL_CTX_get_ex_data),
        FUNC_MAP(ASN1_TIME_it),
        FUNC_MAP(ASN1_UTF8STRING_new),
        FUNC_MAP(ASN1_PRINTABLESTRING_new),
        FUNC_MAP(EVP_DecodedLength),
        FUNC_MAP(CMAC_CTX_copy),
        FUNC_MAP(SSL_CTX_set_verify),
        FUNC_MAP(cbb_add_ucs2_be),
        FUNC_MAP(SSL_CTX_add_cert_compression_alg),
        FUNC_MAP(EVP_PKEY_get1_DSA),
        FUNC_MAP(i2d_PKCS12),
        FUNC_MAP(SSL_serialize_handshake_hints),
        FUNC_MAP(CBS_is_unsigned_asn1_integer),
        FUNC_MAP(NCONF_get_string),
        FUNC_MAP(EVP_PKEY_CTX_set_rsa_padding),
        FUNC_MAP(RSA_private_decrypt),
        FUNC_MAP(CBS_len),
        FUNC_MAP(d2i_EC_PUBKEY),
        FUNC_MAP(d2i_RSA_PSS_PARAMS),
        FUNC_MAP(BN_add_word),
        FUNC_MAP(EVP_aead_null_sha1_tls),
        FUNC_MAP(ERR_set_error_data),
        FUNC_MAP(EVP_PKEY_decrypt_init),
        FUNC_MAP(RSA_padding_add_PKCS1_OAEP_mgf1),
        FUNC_MAP(X509_find_by_issuer_and_serial),
        FUNC_MAP(d2i_ASN1_SET_ANY),
        FUNC_MAP(EVP_aes_192_ctr),
        FUNC_MAP(EVP_PKEY_get0),
        FUNC_MAP(PEM_write_RSAPrivateKey),
        FUNC_MAP(d2i_PKCS12),
        FUNC_MAP(X509_ALGOR_it),
        FUNC_MAP(i2d_PKCS8_PRIV_KEY_INFO_fp),
        FUNC_MAP(d2i_SSL_SESSION),
        FUNC_MAP(X509_VERIFY_PARAM_get_depth),
        FUNC_MAP(sk_num),
        FUNC_MAP(d2i_DSAPrivateKey),
        FUNC_MAP(X509_get_default_cert_area),
        FUNC_MAP(d2i_EXTENDED_KEY_USAGE),
        FUNC_MAP(SSL_CTX_sess_get_remove_cb),
        FUNC_MAP(BN_set_word),
        FUNC_MAP(X509_STORE_add_cert),
        FUNC_MAP(CBS_data),
        FUNC_MAP(EVP_CipherInit_ex),
        FUNC_MAP(EVP_PKEY_bits),
        FUNC_MAP(EVP_PKEY_keygen),
        FUNC_MAP(DH_bits),
        FUNC_MAP(i2d_re_X509_CRL_tbs),
        FUNC_MAP(DSA_generate_parameters_ex),
        FUNC_MAP(HRSS_poly3_invert),
        FUNC_MAP(PEM_read_PKCS8_PRIV_KEY_INFO),
        FUNC_MAP(ASN1_ENUMERATED_new),
        FUNC_MAP(BIO_meth_free),
        FUNC_MAP(EVP_sha256),
        FUNC_MAP(BN_MONT_CTX_new_for_modulus),
        FUNC_MAP(ERR_error_string_n),
        FUNC_MAP(TRUST_TOKEN_generate_key),
        FUNC_MAP(X509_STORE_CTX_set_time),
        FUNC_MAP(X509_CRL_digest),
        FUNC_MAP(SSL_SESSION_get0_peer_certificates),
        FUNC_MAP(BIO_new_socket),
        FUNC_MAP(ENGINE_register_all_complete),
        FUNC_MAP(EVP_PKEY_get0_RSA),
        FUNC_MAP(EC_POINT_set_affine_coordinates_GFp),
        FUNC_MAP(RAND_cleanup),
        FUNC_MAP(SSL_CTX_set_strict_cipher_list),
        FUNC_MAP(ASN1_ENUMERATED_get_uint64),
        FUNC_MAP(ASN1_PRINTABLESTRING_free),
        FUNC_MAP(RSA_size),
        FUNC_MAP(BN_MONT_CTX_set),
        FUNC_MAP(d2i_DSAPrivateKey_bio),
        FUNC_MAP(X509_NAME_get0_der),
        FUNC_MAP(EDIPARTYNAME_it),
        FUNC_MAP(EC_KEY_get_ex_data),
        FUNC_MAP(X509at_add1_attr_by_txt),
        FUNC_MAP(X509_TRUST_add),
        FUNC_MAP(DIST_POINT_free),
        FUNC_MAP(EVP_EncodeUpdate),
        FUNC_MAP(BIO_clear_flags),
        FUNC_MAP(EVP_EncryptFinal_ex),
        FUNC_MAP(EVP_PKEY_sign_init),
        FUNC_MAP(EVP_MD_CTX_create),
        FUNC_MAP(HRSS_poly3_mul),
        FUNC_MAP(i2d_PKCS8_fp),
        FUNC_MAP(EVP_AEAD_CTX_open_gather),
        FUNC_MAP(PEM_write_DSA_PUBKEY),
        FUNC_MAP(X509_PURPOSE_cleanup),
        FUNC_MAP(BN_print_fp),
        FUNC_MAP(CRYPTO_secure_malloc_initialized),
        FUNC_MAP(SSL_CTX_sess_accept_renegotiate),
        FUNC_MAP(EC_GROUP_get0_generator),
        FUNC_MAP(PEM_write_X509),
        FUNC_MAP(X509_NAME_get_text_by_OBJ),
        FUNC_MAP(GENERAL_SUBTREE_new),
        FUNC_MAP(SSL_CTX_get_timeout),
        FUNC_MAP(CRYPTO_THREADID_set_callback),
        FUNC_MAP(X509_issuer_name_cmp),
        FUNC_MAP(BIO_meth_set_ctrl),
        FUNC_MAP(CBB_add_u8_length_prefixed),
        FUNC_MAP(cbb_get_utf8_len),
        FUNC_MAP(CBB_add_u24_length_prefixed),
        FUNC_MAP(EC_KEY_parse_parameters),
        FUNC_MAP(OpenSSL_add_all_algorithms),
        FUNC_MAP(X509_REVOKED_get_ext),
        FUNC_MAP(a2i_GENERAL_NAME),
        FUNC_MAP(PKCS7_get_PEM_CRLs),
        FUNC_MAP(SSL_CTX_get_default_passwd_cb_userdata),
        FUNC_MAP(BUF_MEM_append),
        FUNC_MAP(ECDSA_size),
        FUNC_MAP(ERR_restore_state),
        FUNC_MAP(EVP_PKEY_cmp_parameters),
        FUNC_MAP(X509_NAME_ENTRY_create_by_OBJ),
        FUNC_MAP(i2d_SSL_SESSION_bio),
        FUNC_MAP(BN_get_rfc3526_prime_1536),
        FUNC_MAP(X509_CRL_set1_signature_algo),
        FUNC_MAP(X509_NAME_hash_old),
        FUNC_MAP(BIO_new_fd),
        FUNC_MAP(BN_dup),
        FUNC_MAP(X509_VERIFY_PARAM_get0_name),
        FUNC_MAP(SSL_CTX_sess_set_cache_size),
        FUNC_MAP(SSL_SESSION_get_ex_data),
        FUNC_MAP(SSL_CTX_set0_chain),
        FUNC_MAP(PEM_write_PKCS8_PRIV_KEY_INFO),
        FUNC_MAP(X509_STORE_CTX_get1_issuer),
        FUNC_MAP(X509_REQ_free),
        FUNC_MAP(SSL_get_error),
        FUNC_MAP(ED25519_sign),
        FUNC_MAP(EVP_parse_digest_algorithm),
        FUNC_MAP(EVP_EncryptInit_ex),
        FUNC_MAP(CRYPTO_get_ex_new_index),
        FUNC_MAP(EVP_CIPHER_CTX_nid),
        FUNC_MAP(i2d_NETSCAPE_SPKAC),
        FUNC_MAP(EVP_aead_aes_256_gcm_siv),
        FUNC_MAP(i2d_X509_ATTRIBUTE),
        FUNC_MAP(GENERAL_NAME_it),
        FUNC_MAP(BN_bn2bin),
        FUNC_MAP(CBB_add_u64),
        FUNC_MAP(EC_POINT_point2oct),
        FUNC_MAP(i2d_ECDSA_SIG),
        FUNC_MAP(PEM_read_bio_EC_PUBKEY),
        FUNC_MAP(X509_ATTRIBUTE_get0_object),
        FUNC_MAP(SSL_SESSION_set_ticket),
        FUNC_MAP(NCONF_load_bio),
        FUNC_MAP(SHA512_Update),
        FUNC_MAP(EVP_get_digestbyname),
        FUNC_MAP(DH_get0_key),
        FUNC_MAP(TLSv1_server_method),
        FUNC_MAP(BN_bin2bn),
        FUNC_MAP(EVP_PKEY_CTX_set0_rsa_oaep_label),
        FUNC_MAP(EVP_HPKE_KDF_id),
        FUNC_MAP(SSL_set_tmp_dh),
        FUNC_MAP(SSL_CTX_sess_accept),
        FUNC_MAP(BIO_new_fp),
        FUNC_MAP(ASN1_STRING_type_new),
        FUNC_MAP(BN_is_zero),
        FUNC_MAP(DSA_set_ex_data),
        FUNC_MAP(ECDSA_do_sign),
        FUNC_MAP(EVP_PKEY_CTX_get_rsa_pss_saltlen),
        FUNC_MAP(OBJ_cleanup),
        FUNC_MAP(X509_VERIFY_PARAM_free),
        FUNC_MAP(d2i_ASN1_IA5STRING),
        FUNC_MAP(i2d_ASN1_PRINTABLE),
        FUNC_MAP(d2i_DSA_PUBKEY),
        FUNC_MAP(i2d_DSA_PUBKEY),
        FUNC_MAP(BN_bn2bin_padded),
        FUNC_MAP(ASN1_STRING_data),
        FUNC_MAP(BIO_append_filename),
        FUNC_MAP(CRYPTO_poly1305_finish),
        FUNC_MAP(X509_get_ext_count),
        FUNC_MAP(SSL_COMP_add_compression_method),
        FUNC_MAP(ASN1_TBOOLEAN_it),
        FUNC_MAP(X509_get_default_cert_dir),
        FUNC_MAP(X509_STORE_set_purpose),
        FUNC_MAP(i2d_re_X509_tbs),
        FUNC_MAP(BIO_f_ssl),
        FUNC_MAP(ASN1_VISIBLESTRING_free),
        FUNC_MAP(BN_clear_free),
        FUNC_MAP(GENERAL_NAMES_it),
        FUNC_MAP(X509_STORE_CTX_set_chain),
        FUNC_MAP(CRYPTO_MUTEX_cleanup),
        FUNC_MAP(EVP_PKEY_CTX_set_dsa_paramgen_bits),
        FUNC_MAP(i2d_RSA_PSS_PARAMS),
        FUNC_MAP(X509_CRL_up_ref),
        FUNC_MAP(X509_ALGOR_dup),
        FUNC_MAP(CBB_add_asn1_bool),
        FUNC_MAP(USERNOTICE_new),
        FUNC_MAP(SSL_set_compliance_policy),
        FUNC_MAP(SSL_SESSION_should_be_single_use),
        FUNC_MAP(CRYPTO_poly1305_init),
        FUNC_MAP(SHA512_Final),
        FUNC_MAP(EVP_AEAD_CTX_new),
        FUNC_MAP(EVP_aes_192_gcm),
        FUNC_MAP(X509_print),
        FUNC_MAP(NAME_CONSTRAINTS_free),
        FUNC_MAP(X509V3_EXT_add_nconf),
        FUNC_MAP(ASN1_BMPSTRING_free),
        FUNC_MAP(DISPLAYTEXT_it),
        FUNC_MAP(CBB_reserve),
        FUNC_MAP(X509_cmp),
        FUNC_MAP(X509_get_default_private_dir),
        FUNC_MAP(X509_VERIFY_PARAM_set1_host),
        FUNC_MAP(ASN1_object_size),
        FUNC_MAP(OPENSSL_lh_free),
        FUNC_MAP(PKCS5_PBKDF2_HMAC_SHA1),
        FUNC_MAP(X509_CRL_INFO_it),
        FUNC_MAP(SSL_CTX_get_max_cert_list),
        FUNC_MAP(i2d_ASN1_OCTET_STRING),
        FUNC_MAP(CBB_zero),
        FUNC_MAP(ASN1_ENUMERATED_free),
        FUNC_MAP(BN_rand),
        FUNC_MAP(SSL_CTX_set_grease_enabled),
        FUNC_MAP(CBS_get_bytes),
        FUNC_MAP(NETSCAPE_SPKAC_it),
        FUNC_MAP(SSL_CTX_set_next_protos_advertised_cb),
        FUNC_MAP(DSA_sign),
        FUNC_MAP(EC_GROUP_get0_order),
        FUNC_MAP(ENGINE_set_RSA_method),
        FUNC_MAP(X509_REVOKED_get0_extensions),
        FUNC_MAP(X509_ATTRIBUTE_get0_type),
        FUNC_MAP(SSL_CTX_set_reverify_on_resume),
        FUNC_MAP(ASN1_STRING_free),
        FUNC_MAP(X509V3_get_value_bool),
        FUNC_MAP(X509_PURPOSE_get_by_id),
        FUNC_MAP(d2i_ASN1_PRINTABLESTRING),
        FUNC_MAP(BN_mod_inverse),
        FUNC_MAP(PKCS12_verify_mac),
        FUNC_MAP(BN_GENCB_set),
        FUNC_MAP(EVP_CIPHER_CTX_ctrl),
        FUNC_MAP(X509_get0_notAfter),
        FUNC_MAP(d2i_X509_REVOKED),
        FUNC_MAP(ENGINE_set_ECDSA_method),
        FUNC_MAP(BN_set_u64),
        FUNC_MAP(PEM_read_PUBKEY),
        FUNC_MAP(d2i_BASIC_CONSTRAINTS),
        FUNC_MAP(ASN1_BIT_STRING_new),
        FUNC_MAP(EC_POINT_new),
        FUNC_MAP(X509_STORE_set_verify_cb),
        FUNC_MAP(SSL_SESSION_get_master_key),
        FUNC_MAP(SSL_CTX_set_cert_store),
        FUNC_MAP(i2d_ASN1_GENERALSTRING),
        FUNC_MAP(CBS_get_u16),
        FUNC_MAP(CRYPTO_MUTEX_unlock_read),
        FUNC_MAP(RSA_get_ex_data),
        FUNC_MAP(d2i_NOTICEREF),
        FUNC_MAP(SSL_set_session_id_context),
        FUNC_MAP(CBS_mem_equal),
        FUNC_MAP(DH_marshal_parameters),
        FUNC_MAP(RSA_get0_dmp1),
        FUNC_MAP(X509_EXTENSION_dup),
        FUNC_MAP(X509_PURPOSE_set),
        FUNC_MAP(d2i_OTHERNAME),
        FUNC_MAP(SSL_CTX_set_retain_only_sha256_of_client_certs),
        FUNC_MAP(EVP_des_ede3),
        FUNC_MAP(EVP_VerifyUpdate),
        FUNC_MAP(d2i_PKCS12_bio),
        FUNC_MAP(X509_STORE_get_check_crl),
        FUNC_MAP(X509_EXTENSION_set_critical),
        FUNC_MAP(POLICY_MAPPING_it),
        FUNC_MAP(SSL_CIPHER_standard_name),
        FUNC_MAP(BIO_snprintf),
        FUNC_MAP(ASN1_VISIBLESTRING_new),
        FUNC_MAP(BIO_mem_contents),
        FUNC_MAP(SHA224_Final),
        FUNC_MAP(SHA1),
        FUNC_MAP(PEM_read_bio_DSAPrivateKey),
        FUNC_MAP(ASN1_UTCTIME_new),
        FUNC_MAP(EVP_EncodeBlock),
        FUNC_MAP(EVP_PKEY_sign),
        FUNC_MAP(X509_NAME_set),
        FUNC_MAP(SSL_CTX_set_alpn_select_cb),
        FUNC_MAP(BIO_read_filename),
        FUNC_MAP(EVP_DecryptFinal_ex),
        FUNC_MAP(EC_KEY_get_ex_new_index),
        FUNC_MAP(EXTENDED_KEY_USAGE_free),
};

__cold static void __fix_chromium_boring_ssl(void *lib,
                                             const struct func_map *f)
{
        const char *name = f->name;
        void *orig_func = f->addr;
        void *target;
        void *page;
        int ret;
        char *p;

        target = dlsym(lib, name);
        if (unlikely(!target))
                return;

        if (unlikely(target == orig_func))
                return;

        p = (char *)orig_func;
        page = (void *)((uintptr_t)p & (uintptr_t)-4096UL);
        ret = mprotect(page, 4096 * 2, PROT_READ|PROT_WRITE|PROT_EXEC);
        if (unlikely(ret))
                return;

        /*
         * movq	$func, %rax
         */
        memcpy(p, "\x48\xb8", 2);
        memcpy(p + 2, &target, sizeof(target));

        /*
         * jmp	%rax
         */
        memcpy(p + 2 + sizeof(target), "\xff\xe0", 2);
        mprotect(page, 4096 * 2, PROT_READ|PROT_EXEC);
}

__cold static void fix_chromium_boring_ssl(void)
{
        static const char *BORING_SSL_PATH;
        static void *lib;
        size_t i;

        BORING_SSL_PATH = getenv("BORING_SSL_PATH");
        if (!BORING_SSL_PATH)
                return;

        if (!lib) {
                lib = dlopen(BORING_SSL_PATH, RTLD_GLOBAL|RTLD_NOW);
                if (unlikely(!lib))
                        return;
        }

        for (i = 0; i < ARRAY_SIZE(boringssl_funcs); i++)
                __fix_chromium_boring_ssl(lib, &boringssl_funcs[i]);
}

} /* extern "C" */

#endif /* #ifndef NET__ARCH__X86__BORINGSSL_FIXUP_H */
