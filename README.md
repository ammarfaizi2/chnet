# Chnet

Chromium networking stack as a NodeJS addon. This addon currently only
works on Linux.

The Windows support is still in a WIP state.


# Directory Info
- `chnet` contains CHNet library sources.

- `chromium` contains chromium specific files.

- `Documentation` contains the chnet library documentation.

- `tests` contains unit tests (both JS and C++).

- `node-addon-api` is a submodule of the latest NodeJS addon API. This needs to
  be updated regularly.


# Installation Steps

## 1. Install Basic Tools
```
apt update -y;
apt install build-essential gcc g++ make curl wget nload net-tools \
   iputils-ping strace htop sudo clang git lsb-core autoconf automake \
   autotools-dev libtool ninja-build libc-ares-dev -y;
```

## 2. Install Node JS
```
curl -fsSL https://deb.nodesource.com/setup_18.x | sudo -E bash - && \
  sudo apt-get install -y nodejs
```

## 3. Download and build chnet
```
git clone https://github.com/Hoody-Network/chnet;
cd chnet;
make -j$(nproc) init;
make -j$(nproc);
make -j$(nproc) pack;
```

The `make pack` command will generate a tarball file `chnet-0.0.0.tgz`. Copy it
to the directory of the project that wants to use the chnet. Then run
`npm install chnet-0.0.0.tgz`.


# Maintainers

  - Ammar Faizi &lt;ammarfaizi2@gnuweeb.org&gt;
